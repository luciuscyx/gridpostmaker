//
//  guideHelpView.h
//  MyWardrobe
//
//  Created by macbookair on 1/8/2019.
//  Copyright © 2019 macbookair. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol  guideHelpViewDelegate<NSObject>

- (void)finishGuideHeip;

@end
@interface guideHelpView : UIView
@property (nonatomic,weak)id <guideHelpViewDelegate> delegate;
-(void)addImageViewImage:(UIImage *)helpImage helpImageRect:(CGRect)imageRect buttonText:(NSString *)buttonTtxt buttonRect:(CGRect)buttonRect maskViewRect:(CGRect)maskViewRect isFinish:(BOOL)isfinish;
/**
 在指定view上显示蒙版（过渡动画） 不调用用此方法可使用 addSubview:自己添加展示
 */
- (void)showMaskViewInView:(UIView *)view;
/**
 *  销毁蒙版view(默认点击空白区自动销毁)
 */
- (void)dismissMaskView;
@end

NS_ASSUME_NONNULL_END
