//
//  guideHelpView.m
//  MyWardrobe
//
//  Created by macbookair on 1/8/2019.
//  Copyright © 2019 macbookair. All rights reserved.
//

#import "guideHelpView.h"


@interface guideHelpView ()
/// 图层
@property (nonatomic, weak)   CAShapeLayer   *fillLayer;
/// 路径
@property (nonatomic, strong) UIBezierPath   *overlayPath;
@property (nonatomic,assign)CGRect maskRect;
@end

@implementation guideHelpView

#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame: [UIScreen mainScreen].bounds];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp {
    self.backgroundColor = [UIColor clearColor];
    self.userInteractionEnabled = YES;
    UIColor *maskColor = [UIColor colorWithWhite:0 alpha:0.7];
    self.fillLayer.path      = self.overlayPath.CGPath;
    self.fillLayer.fillRule  = kCAFillRuleEvenOdd;
    self.fillLayer.fillColor = maskColor.CGColor;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
}
- (void)dismissMaskView{
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
- (void)refreshMask {
    UIBezierPath *overlayPath = [self generateOverlayPath];
    self.overlayPath = overlayPath;
}
- (UIBezierPath *)generateOverlayPath {
    UIBezierPath *overlayPath = [UIBezierPath bezierPathWithRect:self.bounds];
    [overlayPath setUsesEvenOddFillRule:YES];
    return overlayPath;
}

-(void)addImageViewImage:(UIImage *)helpImage helpImageRect:(CGRect)imageRect buttonText:(NSString *)buttonTtxt buttonRect:(CGRect)buttonRect maskViewRect:(CGRect)maskViewRect isFinish:(BOOL)isfinish{
     self.maskRect =  maskViewRect;
     [self addImage:helpImage withFrame:imageRect];
     [self addTapButton:buttonTtxt withFrame:buttonRect];
     UIBezierPath *transparentPath = [UIBezierPath bezierPathWithRoundedRect:maskViewRect cornerRadius:5];
     [self addTransparentPath:transparentPath];
}

- (void)addMaskTapView{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClickedMaskView)];
    [self addGestureRecognizer:tapGesture];
}
- (void)addTransparentPath:(UIBezierPath *)transparentPath {
    
    [self.overlayPath appendPath:transparentPath];
    self.fillLayer.path = self.overlayPath.CGPath;
}
- (void)addImage:(UIImage*)image withFrame:(CGRect)frame{
    UIImageView * imageView   = [[UIImageView alloc]initWithFrame:frame];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.image           = image;
    [self addSubview:imageView];
}
- (void)addTapButton:(NSString *)text withFrame:(CGRect)frame{
    UIButton *tapButton = [[UIButton alloc]initWithFrame:frame];
    tapButton.backgroundColor = [UIColor clearColor];
    [tapButton setTitle:text forState:UIControlStateNormal];
    [tapButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    tapButton.titleLabel.font = [UIFont systemFontOfSize:15];
    tapButton.layer.masksToBounds = YES;
    tapButton.layer.cornerRadius = frame.size.height/2;
    tapButton.layer.borderColor =[UIColor whiteColor].CGColor;
    tapButton.layer.borderWidth = 1.5;
    [tapButton addTarget:self action:@selector(tapAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:tapButton];
}
-(void)tapAction{
    [self.delegate finishGuideHeip];
}
#pragma mark - 显示/隐藏
- (void)showMaskViewInView:(UIView *)view{
    
    self.alpha = 0;
    if (view != nil) {
        [view addSubview:self];
    }else{
        [[UIApplication sharedApplication].keyWindow addSubview:self];
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1;
    }];
}
#pragma mark - 懒加载Getter Methods

- (UIBezierPath *)overlayPath {
    if (!_overlayPath) {
        _overlayPath = [self generateOverlayPath];
    }
    
    return _overlayPath;
}

- (CAShapeLayer *)fillLayer {
    if (!_fillLayer) {
        CAShapeLayer *fillLayer = [CAShapeLayer layer];
        fillLayer.frame = self.bounds;
        [self.layer addSublayer:fillLayer];
        
        _fillLayer = fillLayer;
    }
    
    return _fillLayer;
}

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *hitView = [super hitTest:point withEvent:event];
    if(CGRectContainsPoint(self.maskRect, point)){
        return nil;
    }
    return hitView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
