//
//  PushOtherInfoView.m
//  magiceyecoloreffect
//
//  Created by macbookair on 5/6/19.
//

#import "PushOtherInfoView.h"
#import "TWSReleaseNotesDownloadOperation.h"

@interface PushOtherInfoView()
@property (nonatomic,strong)UIView *mainView;
@property (nonatomic,strong)UIImageView *iconImageView;
@property (nonatomic,strong)UILabel *otherNameLabel;
@property (nonatomic,strong)UILabel *otherinfoLabel;
@property (nonatomic,strong)UIButton *otherAppButton;
@property (nonatomic,strong)UIView *iconView;
@property (nonatomic,strong)UIButton *cancelButton;
@end
@implementation PushOtherInfoView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    [self loadUI];
    return self;
}
- (void)loadUI{
    self.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.8];
    self.mainView = [[UIView alloc]initWithFrame:CGRectZero];
    self.mainView.backgroundColor = [UIColor blackColor];
    self.mainView.layer.masksToBounds = YES;
    self.mainView.layer.cornerRadius = 8;
    [self addSubview:self.mainView];
    

    self.iconView = [[UIView alloc]initWithFrame:CGRectZero];
    self.iconView.backgroundColor = [UIColor whiteColor];
    [self.mainView addSubview:self.iconView];
    
    self.cancelButton = [[UIButton alloc]initWithFrame:CGRectZero];
    [self.cancelButton setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [self.cancelButton addTarget:self action:@selector(CancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self.iconView addSubview:self.cancelButton];
    
    self.iconImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
    self.iconImageView.layer.masksToBounds = YES;
    self.iconImageView.layer.cornerRadius = 10;
    [self.iconView addSubview:self.iconImageView];
    
    self.otherNameLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    self.otherNameLabel.textAlignment = NSTextAlignmentCenter;
    self.otherNameLabel.font = [UIFont boldSystemFontOfSize:18];
    [self.mainView addSubview: self.otherNameLabel];
    
    self.otherinfoLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    self.otherinfoLabel.numberOfLines = 0;
    self.otherinfoLabel.textAlignment = NSTextAlignmentCenter;
    //self.otherinfoLabel.adjustsFontSizeToFitWidth = YES;
    self.otherinfoLabel.font = [UIFont systemFontOfSize:15];
    [self.mainView addSubview: self.otherinfoLabel];
    
    
    
    self.otherAppButton = [[UIButton alloc]initWithFrame:CGRectZero];
    self.otherAppButton.backgroundColor = [UIColor whiteColor];
    self.otherAppButton.layer.masksToBounds = YES;
    self.otherAppButton.layer.cornerRadius = 8;
    self.otherAppButton.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [self.otherAppButton setTitle:NSLocalizedString(@"Free download",nil) forState:UIControlStateNormal];
    [self.otherAppButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.otherAppButton addTarget:self action:@selector(otherAppAction) forControlEvents:UIControlEventTouchUpInside];
    [self.mainView addSubview:self.otherAppButton];
}
- (void)setupViewWithAppIdentifier:(NSString *)appIdentifier  completionBlock:(void (^)(NSError *))completionBlock
{
    // Setup operation queue
    NSOperationQueue *operationQueue = [[NSOperationQueue alloc] init];
    operationQueue.maxConcurrentOperationCount = 1;
    TWSReleaseNotesDownloadOperation *operation = [[TWSReleaseNotesDownloadOperation alloc] initWithAppIdentifier:appIdentifier];
    
    __weak TWSReleaseNotesDownloadOperation *weakOperation = operation;
    [operation setCompletionBlock:^{
        TWSReleaseNotesDownloadOperation *strongOperation = weakOperation;
        if (completionBlock)
        {
            if (strongOperation.error)
            {
                NSError *error = strongOperation.error;
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    // Perform completion block with error
                    completionBlock(error);
                }];
            }
            else
            {
                //NSString *releaseNotesText = strongOperation.releaseNotesText;
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    NSString *path = strongOperation.appIconUrlPath;
                    NSData *data = [NSData dataWithContentsOfURL:[NSURL  URLWithString:path]];
                    UIImage *image = [UIImage imageWithData:data]; // 取得图片
                    self.iconImageView.image = image;
                    self.otherNameLabel.text = strongOperation.appFullName ;
                    self.otherinfoLabel.text = strongOperation.appDescribe ;
                   // self.otherActionLabel.text = strongOperation.appSimpleDescribe;
                    // Create and show release notes view
                    //                    TWSReleaseNotesView *releaseNotesView = [TWSReleaseNotesView viewWithReleaseNotesTitle:releaseNotesTitle text:releaseNotesText closeButtonTitle:closeButtonTitle];
                    //
                    //                    // Perform completion block
                    completionBlock(nil);
                }];
            }
        }
    }];
    
    // Add operation
    [operationQueue addOperation:operation];
}
-(void)layoutSubviews{
    self.mainView.frame = CGRectMake(20,0,kDeviceWidth - 40, kDeviceWidth - 40);
    self.mainView.center = CGPointMake(kDeviceWidth/2,ScreenHeight/2);
    self.iconView.frame = CGRectMake(0, 0,VIEW_W(self.mainView), VIEW_W(self.mainView)/5*2);
    self.cancelButton.frame = CGRectMake(VIEW_W(self.mainView)-50,10,40, 40);
    self.iconImageView.frame = CGRectMake(VIEW_W(self.iconView)/2-(VIEW_H(self.iconView)-40)/2, 20,VIEW_H(self.iconView)-40,VIEW_H(self.iconView)-40);
    self.otherNameLabel.frame = CGRectMake(0,VIEW_YH(self.iconView)+20,VIEW_W(self.mainView), 40);
    self.otherNameLabel.frame = CGRectMake(0,VIEW_YH(self.iconView)+20,VIEW_W(self.mainView), 40);
    self.otherinfoLabel.frame = CGRectMake(0, VIEW_YH(self.otherNameLabel),VIEW_W(self.mainView),80);
    self.otherAppButton.frame = CGRectMake(VIEW_W(self.mainView)/2-60,VIEW_H(self.mainView)-60, 120, 40);
    
//    self.otherinfoLabel.text = self.appName;
    self.otherinfoLabel.textColor = [UIColor whiteColor];
//    self.otherActionLabel.text = self.appDescribe;
    self.otherNameLabel.textColor = [UIColor whiteColor];
//    self.otherAppButton.backgroundColor = self.textColor;
//    [self.otherAppButton setTitleColor:self.backgroundColor forState:UIControlStateNormal];
}
- (void)otherAppAction{
    //跳到对应的app
    [self.deletage PushAppStorePage];
    self.hidden = YES;
}
- (void)CancelAction{
    self.hidden = YES;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
