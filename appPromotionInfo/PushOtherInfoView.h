//
//  PushOtherInfoView.h
//  magiceyecoloreffect
//
//  Created by macbookair on 5/6/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol  PushOtherInfoViewDeletage<NSObject>

- (void)backClear;

- (void)PushAppStorePage;

@end
@interface PushOtherInfoView : UIView
@property (nonatomic,weak)id <PushOtherInfoViewDeletage>deletage;

- (void)setupViewWithAppIdentifier:(NSString *)appIdentifier  completionBlock:(void (^)(NSError *))completionBlock;
@end

NS_ASSUME_NONNULL_END
