//
//  TWSReleaseNotesDownloadOperation.h
//  TWSReleaseNotesViewSample
//
//  Created by Matteo Lallone on 03/08/13.
//  Copyright (c) 2013 Tapwings. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 Use the `TWSReleaseNotesDownloadOperation` class to create an operation with the purpose of downloading the release notes text for a specified app, using the iTunes Search API.
 
 The result of the operation is accessible in the `completionBlock`, using the <releaseNotesText> and the <error> properties.
 
 */
@interface TWSReleaseNotesDownloadOperation : NSOperation


/** @name Getting main properties */

/// The downloaded release notes text.
@property (readonly, copy, nonatomic) NSString *releaseNotesText; //更新信息
@property (readonly, copy, nonatomic) NSString *appName; //app名称
@property (readonly, copy, nonatomic) NSString *appSimpleDescribe; //app简介
@property (readonly, copy, nonatomic) NSString *appDescribe; //app描述
@property (readonly, copy, nonatomic) NSString *appIconUrlPath; //app图标地址
@property (readonly, copy, nonatomic) NSString *appFullName; //appe全称

/// An error object associated to the failed operation.
@property (readonly, strong, nonatomic) NSError *error;


/** @name Creating the operation */

/**
 Creates and operation with custom parameters.
 @param appIdentifier The App Store app identifier for remote release notes retrieval.
 @return The initialized operation.
 */
- (id)initWithAppIdentifier:(NSString *)appIdentifier;

@end
