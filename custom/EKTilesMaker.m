//
//  EKTileMaker.m
//  EKTilesMakerDemo
//
//  Created by Evgeniy Kirpichenko on 2/3/14.
//  Copyright (c) 2014 Evgeniy Kirpichenko. All rights reserved.
//

#import "EKTilesMaker.h"

#import "UIImage+EKTilesMaker.h"

@interface EKTilesMaker ()
@property (nonatomic) dispatch_queue_t queue;
@property (nonatomic) dispatch_group_t group;
@end

@implementation EKTilesMaker
@synthesize arraySourceTiles;
#pragma mark - life cycle

- (id)init
{
    if (self = [super init]) {
        [self setOutputFileType:OutputFileTypeJPG];
        [self setZoomLevels:@[@1]];
        imageCount = 0;
        self.arraySourceTiles = [[NSMutableArray alloc] init];
        [self setQueue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)];
        [self setGroup:dispatch_group_create()];
    }
    return self;
}

#pragma mark - public

- (void)createTiles
{
    [self createTilesForZoomLevel:1.0];
    if (self.completionBlock) {
         self.completionBlock(self.arraySourceTiles);
    }
}

#pragma mark - private

- (void)createTilesForZoomLevel:(CGFloat)zoom
{
    UIImage *sourceImage = [UIImage imageWithScale:zoom atPath:self.sourceImage];
    
    
    NSUInteger row = 0;
    NSUInteger offsetY = 0;
    while (offsetY < sourceImage.size.height) {
        NSUInteger column = 0;
        CGFloat offsetX = 0;
        
        while (offsetX < sourceImage.size.width) {
            CGRect tileFrame = CGRectMake(offsetX, offsetY, self.tileSize.width, self.tileSize.height);
            
            UIImage *tileImage = [sourceImage imageInRect:tileFrame];
            //NSString *tileName = [self tileNameWithZoom:zoom row:row column:column];
            
            NSData *imageData =  UIImageJPEGRepresentation(tileImage, 1.0);
            
            
//               NSString *documentDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
//               // *.igo is exclusive to instagram
//               NSString *saveImagePath = [documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Image%i.igo",imageCount]];
//            NSLog(@"saveImagePath %@",saveImagePath);
//               imageCount++;
//            [imageData writeToFile:saveImagePath atomically:YES];
            
            
            [self.arraySourceTiles addObject:imageData];
            column += 1;
            offsetX += self.tileSize.width;
        }
        row += 1;
        offsetY += self.tileSize.height;
    }
    
}

- (void)prepareOutputFolder
{

    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:self.outputFolderPath]) {
        [fileManager createDirectoryAtPath:self.outputFolderPath
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:nil];
    }
}

- (NSString *)tileNameWithZoom:(CGFloat)scale row:(NSUInteger)row column:(NSUInteger)column
{
    NSString *fileExtension = (self.outputFileType == OutputFileTypePNG) ? @"png" : @"jpg";
    NSString *fileName = [NSString stringWithFormat:@"%@_%d_%lu_%lu.%@",
                          self.outputFileName,
                          (int)(scale * 1000),
                          (unsigned long)row,
                          (unsigned long)column,
                          fileExtension];
    return fileName;
}

- (void)saveImage:(UIImage *)image withName:(NSString *)fileName
{
//    NSString *documentDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
//    // *.igo is exclusive to instagram
//    NSString *saveImagePath = [documentDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"Image%i.igo",imageCount]];
//    imageCount++;
    NSData *imageData =  UIImageJPEGRepresentation(image, 1.0);
   // [imageData writeToFile:saveImagePath atomically:YES];
    
    [self.arraySourceTiles addObject:imageData];
}

@end
