//
//  HandlerVideo.h
//

#import <UIKit/UIKit.h>

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

typedef void(^SplitCompleteBlock)(BOOL success, NSMutableArray *splitimgs);
typedef void(^CompCompletedBlock)(BOOL success);
typedef void(^CompFinalCompletedBlock)(BOOL success, NSString *errorMsg);
typedef void(^CompProgressBlcok)(CGFloat progress);

@interface HandlerVideo : NSObject
+ (instancetype)sharedInstance;

/**
  *  图片合成视频
  *@param videoFullPath 合成路径
  *@param frameImgs 图片数组
  *@param fps 帧率
  *@param progressImageBlock 进度回调
  *@param completedBlock 完成回调
 */
- (void)composesVideoFullPath:(NSString *)videoFullPath
                    frameImgs:(NSArray<UIImage *> *)frameImgs
                          fps:(int32_t)fps
           progressImageBlock:(CompProgressBlcok)progressImageBlock
               completedBlock:(CompCompletedBlock)completedBlock;

/**
 *  多个小视频合成大视频
 *@param subsectionPaths 视频地址数组
 *@param videoFullPath 合成视频路径
 *@param completedBlock 完成回调
 */
- (void)combinationVideosWithVideoPath:(NSArray<NSString *> *)subsectionPaths videoFullPath:(NSString *)videoFullPath completedBlock:(CompFinalCompletedBlock)completedBlock;

/**
 * 将视频分解成图片
 *@param fileUrl 视频路径
 *@param fps 帧率
 *@param splitCompleteBlock 分解完成回调
 */
- (void)splitVideo:(NSURL *)fileUrl fps:(float)fps splitCompleteBlock:(SplitCompleteBlock) splitCompleteBlock;
/**
 * 单张图片合成视频
 *@param index 标记
 *@param image 图片
 *@param videoSize 视频的大小
 *@param completedBlock 完成回调
 */
-(void)testCompressionSession:(int)index image:(UIImage*)image videoSize:(CGSize)videoSize composesBlock:(CompFinalCompletedBlock)composesBlock;
/**
 *  两视频合成视频
 *@param videoFullPath 合成路径
 *@param audioUrl  视频1
 *@param videoUrl  视频2
 *@param progressImageBlock 进度回调
 */
-(void)compositeVideosAndAudios:(NSURL*)videoUrl audioUrl:(NSURL*)audioUrl videoFullPath:(NSString *)videoFullPath completedBlock:(CompFinalCompletedBlock)completedBlock;

- (void)videosWithVideoPath:(NSArray<NSURL *> *)subsectionPaths videoFullPath:(NSString *)videoFullPath completedBlock:(CompFinalCompletedBlock)completedBlock;

//- (void)videosOneWithVideoPath:(NSURL *)subsectionPaths lastVideoPath:(NSURL *)lastPath videoFullPath:(NSString *)videoFullPath videoScale:(BOOL)videoScale VideocuPoint:(CGPoint )videocuPoint VideoW :(CGFloat)videoW VideoAngle:(CGFloat)videoAngle VideoColor:(UIColor *)videocolor completedBlock:(CompFinalCompletedBlock)completedBlock ;
- (void)videosOneWithVideoPath:(NSURL *)subsectionPaths lastVideoPath:(NSURL *)lastPath videoFullPath:(NSString *)videoFullPath videoScale:(BOOL)videoScale VideocuPoint:(CGPoint )videocuPoint VideoW :(CGFloat)videoW VideoAngle:(CGFloat)videoAngle VideoColor:(UIColor *)videocolor VideoImage:(UIImage *)videoimage completedBlock:(CompFinalCompletedBlock)completedBlock;
@end
