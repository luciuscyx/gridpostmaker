//
//  NSString+UrlEncode.m
//  Backcam
//
//  Created by mangal on 09/04/15.
//  Copyright (c) 2015 RJ Labs. All rights reserved.
//

#import "NSString+UrlEncode.h"

@implementation NSString (UrlEncode)

- (NSString*)urlencodedString
{
    return [self stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
}

@end
