//
//  HandlerVideo.m
//

#import "HandlerVideo.h"
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ImageHelper.h"
static HandlerVideo *instance = nil;

@interface HandlerVideo () {
    int32_t _fps;
    BOOL isprocessing;
    BOOL isCompositing;
}
@end

@implementation HandlerVideo

+ (instancetype)sharedInstance {
    if (!instance) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            instance = [HandlerVideo new];
        });
    }
    return instance;
}

- (instancetype)copyWithZone:(struct _NSZone *)zone {
    return [HandlerVideo sharedInstance];
}

#pragma mark - Method
- (void)composesVideoFullPath:(NSString *)videoFullPath
                    frameImgs:(NSArray<UIImage *> *)frameImgs
                          fps:(int32_t)fps
           progressImageBlock:(CompProgressBlcok)progressImageBlock
               completedBlock:(CompCompletedBlock)completedBlock {
    if ([[NSFileManager defaultManager] fileExistsAtPath:videoFullPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:videoFullPath error:nil];
    }
    
    NSError *error = nil;
    AVAssetWriter *videoWriter = [[AVAssetWriter alloc] initWithURL:[NSURL fileURLWithPath:videoFullPath]
                                                           fileType:AVFileTypeQuickTimeMovie
                                                              error:&error];
    NSParameterAssert(videoWriter);
    if(error)
        NSLog(@"error = %@", [error localizedDescription]);
    
    //获取原视频尺寸
    UIImage *img = frameImgs.firstObject;
    CGSize size = CGSizeMake(CGImageGetWidth(img.CGImage), CGImageGetHeight(img.CGImage));
    //    NSLog(@"Size: %@", NSStringFromCGSize(size));
    
    NSDictionary *videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:AVVideoCodecH264, AVVideoCodecKey,
                                   [NSNumber numberWithInt:size.width], AVVideoWidthKey,
                                   [NSNumber numberWithInt:size.height], AVVideoHeightKey, nil];
    AVAssetWriterInput *writerInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:videoSettings];
    
    NSDictionary *sourcePixelBufferAttributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:kCVPixelFormatType_32ARGB], kCVPixelBufferPixelFormatTypeKey, nil];
    
    AVAssetWriterInputPixelBufferAdaptor *adaptor = [AVAssetWriterInputPixelBufferAdaptor
                                                     assetWriterInputPixelBufferAdaptorWithAssetWriterInput:writerInput sourcePixelBufferAttributes:sourcePixelBufferAttributesDictionary];
    NSParameterAssert(writerInput);
    NSParameterAssert([videoWriter canAddInput:writerInput]);
    
    if ([videoWriter canAddInput:writerInput]) {
        //        printf("can add\n");
    } else {
        //        printf("can't add\n");
    }
    
    [videoWriter addInput:writerInput];
    
    [videoWriter startWriting];
    [videoWriter startSessionAtSourceTime:kCMTimeZero];
    
    //合成多张图片为一个视频文件
    dispatch_queue_t dispatchQueue = dispatch_queue_create("mediaInputQueue", DISPATCH_QUEUE_SERIAL);
    __block int frame = -1;
    NSInteger count = frameImgs.count;
    [writerInput requestMediaDataWhenReadyOnQueue:dispatchQueue usingBlock:^{
        while ([writerInput isReadyForMoreMediaData]) {
            if(++frame >= count) {
                [writerInput markAsFinished];
                [videoWriter finishWriting];
                printf("comp completed\n");
                if (completedBlock) {
                    completedBlock(YES);
                }
                break;
            }
            
            CVPixelBufferRef buffer = NULL;
            UIImage *currentFrameImg = frameImgs[frame];
            buffer = (CVPixelBufferRef)[self pixelBufferFromCGImage:[currentFrameImg CGImage] size:size];
            if (progressImageBlock) {
                CGFloat progress = frame * 1.0 / count;
                progressImageBlock(progress);
            }
            if (buffer) {
                if(![adaptor appendPixelBuffer:buffer withPresentationTime:CMTimeMake(frame, fps)]) {
                    NSLog(@"FAIL");
                    if (completedBlock) {
                        completedBlock(NO);
                    }
                } else {
                    CFRelease(buffer);
                }
            }
        }
    }];
}

- (void)composesVideoFullPath:(NSString *)videoFullPath
               frameImgPathes:(NSArray<UIImage *> *)frameImgPathes
                          fps:(int32_t)fps
           progressImageBlock:(CompProgressBlcok)progressImageBlock
               completedBlock:(CompCompletedBlock)completedBlock {
    if ([[NSFileManager defaultManager] fileExistsAtPath:videoFullPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:videoFullPath error:nil];
    }
    NSError *error = nil;
    AVAssetWriter *videoWriter = [[AVAssetWriter alloc] initWithURL:[NSURL fileURLWithPath:videoFullPath]
                                                           fileType:AVFileTypeQuickTimeMovie
                                                              error:&error];
    NSParameterAssert(videoWriter);
    if(error)
        NSLog(@"error = %@", [error localizedDescription]);
    
    //获取原视频尺寸
    UIImage *img = frameImgPathes.firstObject;
    CGSize size = CGSizeMake(CGImageGetWidth(img.CGImage), CGImageGetHeight(img.CGImage));
    //    NSLog(@"Size: %@", NSStringFromCGSize(size));
    
    NSDictionary *videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:AVVideoCodecH264, AVVideoCodecKey,
                                   [NSNumber numberWithInt:size.width], AVVideoWidthKey,
                                   [NSNumber numberWithInt:size.height], AVVideoHeightKey, nil];
    AVAssetWriterInput *writerInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:videoSettings];
    
    NSDictionary *sourcePixelBufferAttributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:kCVPixelFormatType_32ARGB], kCVPixelBufferPixelFormatTypeKey, nil];
    
    AVAssetWriterInputPixelBufferAdaptor *adaptor = [AVAssetWriterInputPixelBufferAdaptor
                                                     assetWriterInputPixelBufferAdaptorWithAssetWriterInput:writerInput sourcePixelBufferAttributes:sourcePixelBufferAttributesDictionary];
    NSParameterAssert(writerInput);
    NSParameterAssert([videoWriter canAddInput:writerInput]);
    
    if ([videoWriter canAddInput:writerInput]) {
        //        printf("can add\n");
    } else {
        //        printf("can't add\n");
    }
    
    [videoWriter addInput:writerInput];
    
    [videoWriter startWriting];
    [videoWriter startSessionAtSourceTime:kCMTimeZero];
    
    //合成多张图片为一个视频文件
    dispatch_queue_t dispatchQueue = dispatch_queue_create("mediaInputQueue", DISPATCH_QUEUE_SERIAL);
    __block int frame = -1;
    NSInteger count = frameImgPathes.count;
    [writerInput requestMediaDataWhenReadyOnQueue:dispatchQueue usingBlock:^{
        while ([writerInput isReadyForMoreMediaData]) {
            if(++frame >= count) {
                [writerInput markAsFinished];
                [videoWriter finishWriting];
                printf("comp completed\n");
                if (completedBlock) {
                    completedBlock(YES);
                }
                break;
            }
            
            CVPixelBufferRef buffer = NULL;
            UIImage *currentFrameImg = frameImgPathes[frame];
            buffer = (CVPixelBufferRef)[self pixelBufferFromCGImage:[currentFrameImg CGImage] size:size];
            currentFrameImg = nil;
            if (progressImageBlock) {
                CGFloat progress = frame * 1.0 / count;
                progressImageBlock(progress);
            }
            if (buffer) {
                if(![adaptor appendPixelBuffer:buffer withPresentationTime:CMTimeMake(frame, fps)]) {
                    NSLog(@"FAIL");
                    if (completedBlock) {
                        completedBlock(NO);
                    }
                } else {
                    CFRelease(buffer);
                    buffer = NULL;
                }
            }
        }
    }];
}
- (CVPixelBufferRef )pixelBufferFromCGImage:(CGImageRef)image size:(CGSize)size {
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                             [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                             nil];
    CVPixelBufferRef pxbuffer = NULL;
    
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, size.width,
                                          size.height, kCVPixelFormatType_32ARGB, (__bridge CFDictionaryRef) options,
                                          &pxbuffer);
    NSParameterAssert(status == kCVReturnSuccess && pxbuffer != NULL);
    
    CVPixelBufferLockBaseAddress(pxbuffer, 0);
    void *pxdata = CVPixelBufferGetBaseAddress(pxbuffer);
    NSParameterAssert(pxdata != NULL);
    
    CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pxdata, size.width,
                                                 size.height, 8, 4*size.width, rgbColorSpace,
                                                 kCGImageAlphaNoneSkipFirst);
    NSParameterAssert(context);
    CGContextConcatCTM(context, CGAffineTransformMakeRotation(0));
    CGContextDrawImage(context, CGRectMake(0,0, CGImageGetWidth(image),
                                           CGImageGetHeight(image)), image);
    CGColorSpaceRelease(rgbColorSpace);
    CGContextRelease(context);
    
    CVPixelBufferUnlockBaseAddress(pxbuffer, 0);
    return pxbuffer;
}
//多视频的拼接
- (void)combinationVideosWithVideoPath:(NSArray<NSURL *> *)subsectionPaths videoFullPath:(NSString *)videoFullPath completedBlock:(CompFinalCompletedBlock)completedBlock {
    if (!subsectionPaths || subsectionPaths.count == 0) {
        NSLog(@"No such SubsectionNames");
        completedBlock(NO, @"合并失败");
        return;
    }
    subsectionPaths = [[subsectionPaths reverseObjectEnumerator] allObjects];
    NSDictionary *optDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
    NSURL *firstPath = subsectionPaths.firstObject;
    AVAsset *firstVideo = [[AVURLAsset alloc] initWithURL:firstPath options:optDict];
    
    AVMutableComposition *videoComposition = [AVMutableComposition composition];
    //添加
    //  AVMutableVideoComposition *videoCompositions = [AVMutableVideoComposition videoComposition];
    
    __block AVMutableCompositionTrack *track = [videoComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    NSArray *firstVideoTracks = [firstVideo tracksWithMediaType:AVMediaTypeVideo];
    if (firstVideoTracks.count <= 0) {
        completedBlock(NO, @"合成失败");
        return;
    }
    
    [track insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstVideo.duration) ofTrack:firstVideoTracks.firstObject atTime:kCMTimeZero error:nil];
    /**
     PS: 如果视频有声音,就需要将注释掉的打开
     */
    __block AVMutableCompositionTrack *audioTrack = [videoComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    NSArray *firstVideoAudioTracks = [firstVideo tracksWithMediaType:AVMediaTypeAudio];
    if (firstVideoAudioTracks.count > 0) {
        [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstVideo.duration) ofTrack:firstVideoAudioTracks.firstObject  atTime:kCMTimeZero error:nil];
    }
    [subsectionPaths enumerateObjectsUsingBlock:^(NSURL *videoPath, NSUInteger idx, BOOL * _Nonnull stop) {
        if (idx == 0) {
            return;
        }
        AVURLAsset *currentVideo = [[AVURLAsset alloc] initWithURL:videoPath options:optDict];
        NSArray *tracks = [currentVideo tracksWithMediaType:AVMediaTypeVideo];
        if (tracks <= 0) {
            *stop = YES;
            completedBlock(NO, @"合成失败");
            return;
        }
        [track insertTimeRange:CMTimeRangeMake(kCMTimeZero, currentVideo.duration) ofTrack:tracks.firstObject atTime:kCMTimeZero error:nil];
        
    }];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:videoFullPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:videoFullPath error:nil];
    }
    
    /**
     我们可以通过设置AVCaptureSession的一些属性来改变捕捉画面的质量
     但是要注意:size相关的属性的时候需要首先进行测试设备是否支持
     判断方法是  canSetSessionPreset
     
     AVAssetExportPresetLowQuality       低质量 可以通过移动网络分享
     AVAssetExportPresetMediumQuality    中等质量 可以通过WIFI网络分享
     AVAssetExportPresetHighestQuality   高等质量 (默认低质量)
     AVAssetExportPreset640x480
     AVAssetExportPreset960x540
     AVAssetExportPreset1280x720    720pHD
     AVAssetExportPreset1920x1080   1080pHD
     AVAssetExportPreset3840x2160
     */
    AVAssetExportSession *exportor = [[AVAssetExportSession alloc] initWithAsset:videoComposition presetName:AVAssetExportPresetHighestQuality];
    exportor.outputFileType = AVFileTypeMPEG4;
    //exportor.videoComposition = videoCompositions;
    //[exportor setOutputFileType:AVFileTypeQuickTimeMovie];
    exportor.outputURL = [NSURL fileURLWithPath:videoFullPath];
    exportor.shouldOptimizeForNetworkUse = YES;
    [exportor exportAsynchronouslyWithCompletionHandler:^{
        BOOL isSuccess = NO;
        NSString *msg = @"合并完成";
        switch (exportor.status) {
            case AVAssetExportSessionStatusFailed:
                NSLog(@"HandlerVideo -> combinationVidesError: %@", exportor.error.localizedDescription);
                msg = @"合并失败";
                break;
            case AVAssetExportSessionStatusUnknown:
            case AVAssetExportSessionStatusCancelled:
                break;
            case AVAssetExportSessionStatusWaiting:
                break;
            case AVAssetExportSessionStatusExporting:
                break;
            case AVAssetExportSessionStatusCompleted:
                isSuccess = YES;
                break;
        }
        if (completedBlock) {
            completedBlock(isSuccess, msg);
        }
    }];
}
//两视频的拼接
-(void)compositeVideosAndAudios:(NSURL*)videoUrl audioUrl:(NSURL*)audioUrl videoFullPath:(NSString *)videoFullPath completedBlock:(CompFinalCompletedBlock)completedBlock
{
    NSDictionary *optDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
    AVAsset* firstAsset = [[AVURLAsset alloc] initWithURL:audioUrl options:optDict];
    AVAsset* secondAsset = [[AVURLAsset alloc] initWithURL:videoUrl options:optDict];
    CGFloat outputwidth = 720;
    if (firstAsset!=nil&&secondAsset!=nil) {
        
        //Create AVMutableComposition Object.This object will hold our multiple AVMutableCompositionTrack.
        AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
        
        //VIDEO TRACK
        AVMutableCompositionTrack *firstTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        [firstTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstAsset.duration) ofTrack:[[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];
        
        AVMutableCompositionTrack *secondTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        [secondTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, secondAsset.duration) ofTrack:[[secondAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:firstAsset.duration error:nil];
        //获取AVAsset中的音频
        AVAssetTrack *assetAudioTrack = [[secondAsset tracksWithMediaType:AVMediaTypeAudio] firstObject];
        if(assetAudioTrack){
            //向通道内加入音频
            [[mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid] insertTimeRange:CMTimeRangeMake(kCMTimeZero, secondAsset.duration)
                                                                                                                                  ofTrack:assetAudioTrack
                                                                                                                                   atTime:kCMTimeZero
                                                                                                                                    error:nil];
        }
        //        __block AVMutableCompositionTrack *audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        //        NSArray *firstVideoAudioTracks = [secondAsset tracksWithMediaType:AVMediaTypeAudio];
        //        if (firstVideoAudioTracks.count > 0) {
        //            [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, secondAsset.duration) ofTrack:firstVideoAudioTracks.firstObject  atTime:kCMTimeZero error:nil];
        //        }
        //方向
        AVMutableVideoCompositionInstruction * MainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
        MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeAdd(firstAsset.duration, secondAsset.duration));
        
        //FIXING ORIENTATION//
        AVMutableVideoCompositionLayerInstruction *FirstlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:firstTrack];
        AVAssetTrack *FirstAssetTrack = [[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        UIImageOrientation FirstAssetOrientation_  = UIImageOrientationUp;
        BOOL  isFirstAssetPortrait_  = NO;
        CGAffineTransform firstTransform = FirstAssetTrack.preferredTransform;
        if(firstTransform.a == 0 && firstTransform.b == 1.0 && firstTransform.c == -1.0 && firstTransform.d == 0)  {FirstAssetOrientation_= UIImageOrientationRight; isFirstAssetPortrait_ = YES;}
        if(firstTransform.a == 0 && firstTransform.b == -1.0 && firstTransform.c == 1.0 && firstTransform.d == 0)  {FirstAssetOrientation_ =  UIImageOrientationLeft; isFirstAssetPortrait_ = YES;}
        if(firstTransform.a == 1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == 1.0)   {FirstAssetOrientation_ =  UIImageOrientationUp;}
        if(firstTransform.a == -1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == -1.0) {FirstAssetOrientation_ = UIImageOrientationDown;}
        CGFloat FirstAssetScaleToFitRatio ;//= ScreenWidth/FirstAssetTrack.naturalSize.width;
        if (FirstAssetTrack.naturalSize.width <= FirstAssetTrack.naturalSize.height) {
            FirstAssetScaleToFitRatio = outputwidth/FirstAssetTrack.naturalSize.width;
            
        }else
        {
            FirstAssetScaleToFitRatio = outputwidth/FirstAssetTrack.naturalSize.height;
        }
        if(isFirstAssetPortrait_){
            // FirstAssetScaleToFitRatio = ScreenWidth/FirstAssetTrack.naturalSize.height;
            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
            [FirstlayerInstruction setTransform:CGAffineTransformConcat(FirstAssetTrack.preferredTransform, FirstAssetScaleFactor) atTime:kCMTimeZero];
        }else{
            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
            [FirstlayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(FirstAssetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(0, 0)) atTime:kCMTimeZero];
        }
        [FirstlayerInstruction setOpacity:0.0 atTime:firstAsset.duration];
        
        AVMutableVideoCompositionLayerInstruction *SecondlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:secondTrack];
        AVAssetTrack *SecondAssetTrack = [[secondAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        UIImageOrientation SecondAssetOrientation_  = UIImageOrientationUp;
        BOOL  isSecondAssetPortrait_  = NO;
        CGAffineTransform secondTransform = SecondAssetTrack.preferredTransform;
        if(secondTransform.a == 0 && secondTransform.b == 1.0 && secondTransform.c == -1.0 && secondTransform.d == 0)  {SecondAssetOrientation_= UIImageOrientationRight; isSecondAssetPortrait_ = YES;}
        if(secondTransform.a == 0 && secondTransform.b == -1.0 && secondTransform.c == 1.0 && secondTransform.d == 0)  {SecondAssetOrientation_ =  UIImageOrientationLeft; isSecondAssetPortrait_ = YES;}
        if(secondTransform.a == 1.0 && secondTransform.b == 0 && secondTransform.c == 0 && secondTransform.d == 1.0)   {SecondAssetOrientation_ =  UIImageOrientationUp;}
        if(secondTransform.a == -1.0 && secondTransform.b == 0 && secondTransform.c == 0 && secondTransform.d == -1.0) {SecondAssetOrientation_ = UIImageOrientationDown;}
        CGFloat SecondAssetScaleToFitRatio;
        if (SecondAssetTrack.naturalSize.width <= SecondAssetTrack.naturalSize.height) {
            SecondAssetScaleToFitRatio = outputwidth/SecondAssetTrack.naturalSize.width;
        }else
        {
            SecondAssetScaleToFitRatio = outputwidth/SecondAssetTrack.naturalSize.height;
        }
        // CGFloat SecondAssetScaleToFitRatio = ScreenWidth/SecondAssetTrack.naturalSize.width;
        if(isSecondAssetPortrait_){
            // SecondAssetScaleToFitRatio = ScreenWidth/SecondAssetTrack.naturalSize.height;
            CGAffineTransform SecondAssetScaleFactor = CGAffineTransformMakeScale(SecondAssetScaleToFitRatio,SecondAssetScaleToFitRatio);
            [SecondlayerInstruction setTransform:CGAffineTransformConcat(SecondAssetTrack.preferredTransform, SecondAssetScaleFactor) atTime:firstAsset.duration];
        }else{
            ;
            CGAffineTransform SecondAssetScaleFactor = CGAffineTransformMakeScale(SecondAssetScaleToFitRatio,SecondAssetScaleToFitRatio);
            [SecondlayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(SecondAssetTrack.preferredTransform, SecondAssetScaleFactor),CGAffineTransformMakeTranslation(0,0)) atTime:firstAsset.duration];
        }
        
        MainInstruction.layerInstructions = [NSArray arrayWithObjects:FirstlayerInstruction,SecondlayerInstruction,nil];;
        
        AVMutableVideoComposition *MainCompositionInst = [AVMutableVideoComposition videoComposition];
        MainCompositionInst.instructions = [NSArray arrayWithObject:MainInstruction];
        //  设置每秒帧频数
        //  设置视频的大小
        float minframerate = FirstAssetTrack.nominalFrameRate;
        float secframerate = SecondAssetTrack.nominalFrameRate;
        if (minframerate > secframerate) {
            MainCompositionInst.frameDuration = CMTimeMake(1, secframerate);
        }else
        {
            MainCompositionInst.frameDuration = CMTimeMake(1, minframerate);
        }
        
        
        MainCompositionInst.renderSize = CGSizeMake(720,720 );//CGSizeMake(320,480);
        
        //        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //        NSString *documentsDirectory = [paths objectAtIndex:0];
        //        //  设置视频的大小
        //        NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"mergeVideo-%d.mov",arc4random() % 1000]];
        
        NSURL *url = [NSURL fileURLWithPath:videoFullPath];
        
        AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetMediumQuality];
        exporter.outputURL=url;
        exporter.outputFileType = AVFileTypeQuickTimeMovie;
        exporter.videoComposition = MainCompositionInst;
        // exporter.shouldOptimizeForNetworkUse = YES;
        [exporter exportAsynchronouslyWithCompletionHandler:^
         {
             BOOL isSuccess = NO;
             NSString *msg = @"加载成功";
             switch (exporter.status) {
                 case AVAssetExportSessionStatusFailed:
                     NSLog(@"HandlerVideo -> combinationVidesError: %@", exporter.error.localizedDescription);
                     msg = @"加载失败";
                     break;
                 case AVAssetExportSessionStatusUnknown:
                 case AVAssetExportSessionStatusCancelled:
                     break;
                 case AVAssetExportSessionStatusWaiting:
                     break;
                 case AVAssetExportSessionStatusExporting:
                     break;
                 case AVAssetExportSessionStatusCompleted:
                     isSuccess = YES;
                     break;
             }
             if (completedBlock) {
                 completedBlock(isSuccess, msg);
             }
         }];
    }
}
//音频和视频的结合
-(void)compositeVideosAndAudioss:(NSURL*)videoUrl audioUrl:(NSURL*)audioUrl videoFullPath:(NSString *)videoFullPath completedBlock:(CompFinalCompletedBlock)completedBlock
{
    NSDictionary *optDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
    AVURLAsset* audioAsset = [[AVURLAsset alloc] initWithURL:audioUrl options:optDict];
    AVURLAsset* videoAsset = [[AVURLAsset alloc] initWithURL:videoUrl options:optDict];
    NSArray * audios = [audioAsset tracksWithMediaType:AVMediaTypeAudio];
    NSArray * videos = [videoAsset tracksWithMediaType:AVMediaTypeVideo];
    if (videos == nil || videos.count == 0 || audios == nil || audios.count == 0) {
        return;
    }
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    
    AVMutableCompositionTrack *compositionCommentaryTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio
                                                                                        preferredTrackID:kCMPersistentTrackID_Invalid];
    [compositionCommentaryTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, audioAsset.duration)
                                        ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
                                         atTime:kCMTimeZero error:nil];
    
    
    AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                                   preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.frameDuration = CMTimeMake(1,30);
    
    videoComposition.renderScale = 1.0;
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    
    AVMutableVideoCompositionLayerInstruction *layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:compositionVideoTrack];
    AVAssetTrack *sourceVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize temp = CGSizeApplyAffineTransform(sourceVideoTrack.naturalSize, sourceVideoTrack.preferredTransform);
    CGSize size = CGSizeMake(fabsf(temp.width), fabsf(temp.height));
    CGAffineTransform transform = sourceVideoTrack.preferredTransform;
    videoComposition.renderSize = sourceVideoTrack.naturalSize;
    [layerInstruction setTransform:transform atTime:kCMTimeZero];
    compositionVideoTrack.preferredTransform = transform;
    /* if (size.width > size.height) {
     
     [layerInstruction setTransform:transform atTime:kCMTimeZero];
     } else {
     
     
     float s = size.width/size.height;
     
     
     CGAffineTransform new = CGAffineTransformConcat(transform, CGAffineTransformMakeScale(s,s));
     
     float x = (size.height - size.width*s)/2;
     
     CGAffineTransform newer = CGAffineTransformConcat(new, CGAffineTransformMakeTranslation(x, 0));
     
     [layerInstruction setTransform:newer atTime:kCMTimeZero];
     }*/
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)
                                   ofTrack:sourceVideoTrack
                                    atTime:kCMTimeZero error:nil];
    
    instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
    instruction.timeRange = compositionVideoTrack.timeRange;
    
    
    videoComposition.instructions = [NSArray arrayWithObject:instruction];
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    NSString * presetname = AVAssetExportPresetHighestQuality;
    if (version >= 6.0)
    {
        presetname = AVAssetExportPresetPassthrough;
    }
    AVAssetExportSession* _assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition
                                                                          presetName:presetname];
    _assetExport.videoComposition = videoComposition;
    
    NSURL *exportUrl = [NSURL fileURLWithPath:videoFullPath];
    
    _assetExport.outputFileType =AVFileTypeMPEG4;// @"com.apple.quicktime-movie";
    NSLog(@"file type %@",_assetExport.outputFileType);
    _assetExport.outputURL = exportUrl;
    _assetExport.shouldOptimizeForNetworkUse = YES;
    isCompositing = YES;
    [_assetExport exportAsynchronouslyWithCompletionHandler:
     ^{
         BOOL isSuccess = NO;
         NSString *msg = @"合并完成";
         switch (_assetExport.status) {
             case AVAssetExportSessionStatusFailed:
                 NSLog(@"HandlerVideo -> combinationVidesError: %@", _assetExport.error.localizedDescription);
                 msg = @"合并失败";
                 break;
             case AVAssetExportSessionStatusUnknown:
             case AVAssetExportSessionStatusCancelled:
                 break;
             case AVAssetExportSessionStatusWaiting:
                 break;
             case AVAssetExportSessionStatusExporting:
                 break;
             case AVAssetExportSessionStatusCompleted:
                 isSuccess = YES;
                 break;
         }
         if (completedBlock) {
             completedBlock(isSuccess, msg);
         }
     }];
}
- (void)videosWithVideoPath:(NSArray<NSURL *> *)subsectionPaths videoFullPath:(NSString *)videoFullPath completedBlock:(CompFinalCompletedBlock)completedBlock {
    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
    
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    
    AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                        preferredTrackID:kCMPersistentTrackID_Invalid];
    //    AVMutableCompositionTrack *audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    CMTime totalDuration = kCMTimeZero;
    //取出最后一个(以最后一个为标准)
    AVURLAsset *lastAsset = [AVURLAsset assetWithURL:subsectionPaths.lastObject];
    AVAssetTrack *LastAssetVideoTrack = [[lastAsset tracksWithMediaType:AVMediaTypeVideo]firstObject];
    CGFloat rendeW  = LastAssetVideoTrack.naturalSize.width;
    CGFloat rendeH  = LastAssetVideoTrack.naturalSize.height;
    AVMutableVideoCompositionLayerInstruction * avMutableVideoCompositionLayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    for (int i = 0; i < subsectionPaths.count; i++) {
        AVURLAsset *asset = [AVURLAsset assetWithURL:subsectionPaths[i]];
        Float64 seconds = CMTimeGetSeconds(asset.duration);
        NSLog(@"视频的时间：----%f",seconds);
        NSError *erroraudio = nil;
        //获取AVAsset中的音频
        AVAssetTrack *assetAudioTrack = [[asset tracksWithMediaType:AVMediaTypeAudio] firstObject];
        if(assetAudioTrack){
            //向通道内加入音频
            [[mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid] insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration)
                                                                                                                                  ofTrack:assetAudioTrack
                                                                                                                                   atTime:totalDuration
                                                                                                                                    error:&erroraudio];
        }
        NSError *errorVideo = nil;
        AVAssetTrack *assetVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo]firstObject];
        [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration)
                            ofTrack:assetVideoTrack
                             atTime:totalDuration
                              error:&errorVideo];
        
        videoComposition.frameDuration = CMTimeMake(1, 30);
        //视频输出尺寸
        //if(assetVideoTrack.naturalSize.height >= )
        //CGFloat newHeight = assetVideoTrack.naturalSize.height/3*4;
        
        //videoComposition.renderSize = CGSizeMake(640,640);
        // videoComposition.renderSize = CGSizeMake(assetVideoTrack.naturalSize.height,assetVideoTrack.naturalSize.height*(ScreenHeight/(ScreenHeight-140)));
        //缩放比例
        //数据类型转换
        //CGFloat floatW = rendeW/assetVideoTrack.naturalSize.width;
        //NSNumber *objNo = [NSNumber numberWithFloat:floatW];
        //CGFloat rateW = [objNo floatValue];
        
        //AVAssetTrack *FirstAssetTrack = [[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        UIImageOrientation FirstAssetOrientation_  = UIImageOrientationUp;
        BOOL  isFirstAssetPortrait_  = NO;
        CGAffineTransform firstTransform = assetVideoTrack.preferredTransform;
        if(firstTransform.a == 0 && firstTransform.b == 1.0 && firstTransform.c == -1.0 && firstTransform.d == 0)
        {
            FirstAssetOrientation_= UIImageOrientationRight;
            isFirstAssetPortrait_ = YES;
        }
        if(firstTransform.a == 0 && firstTransform.b == -1.0 && firstTransform.c == 1.0 && firstTransform.d == 0)  {
            FirstAssetOrientation_ =  UIImageOrientationLeft;
            isFirstAssetPortrait_ = YES;
        }
        if(firstTransform.a == 1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == 1.0)   {
            FirstAssetOrientation_ =  UIImageOrientationUp;
            
        }
        if(firstTransform.a == -1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == -1.0) {
            FirstAssetOrientation_ = UIImageOrientationDown;
        }
        CGFloat FirstAssetScaleToFitRatio ;//= ScreenWidth/FirstAssetTrack.naturalSize.width;
        if (assetVideoTrack.naturalSize.width <= assetVideoTrack.naturalSize.height) {
            FirstAssetScaleToFitRatio = rendeW/assetVideoTrack.naturalSize.width;
        }else
        {
            FirstAssetScaleToFitRatio = rendeH/assetVideoTrack.naturalSize.height;
        }
        if(isFirstAssetPortrait_){
            // FirstAssetScaleToFitRatio = ScreenWidth/FirstAssetTrack.naturalSize.height;
            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
            [avMutableVideoCompositionLayerInstruction setTransform:CGAffineTransformConcat(assetVideoTrack.preferredTransform, FirstAssetScaleFactor) atTime:totalDuration];
        }else{
            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
            [avMutableVideoCompositionLayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(assetVideoTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(0, 0)) atTime:totalDuration];
        }
        totalDuration = CMTimeAdd(totalDuration, asset.duration);
        
        /*  CGSize lastSize;
         lastSize = CGSizeMake(assetVideoTrack.naturalSize.width,assetVideoTrack.naturalSize.height);
         CGFloat rateW = rendeSize.width/lastSize.width;
         CGFloat rateH = rendeSize.width/lastSize.width;
         CGAffineTransform layerTransform = CGAffineTransformMake(assetVideoTrack.preferredTransform.a, assetVideoTrack.preferredTransform.b, assetVideoTrack.preferredTransform.c, assetVideoTrack.preferredTransform.d, assetVideoTrack.preferredTransform.tx * rateW, assetVideoTrack.preferredTransform.ty * rateH);
         layerTransform = CGAffineTransformConcat(layerTransform, CGAffineTransformMake(1, 0, 0, 1, 0, 0));
         layerTransform = CGAffineTransformScale(layerTransform, rateW, rateH);//放缩，解决前后摄像结果大小不对称
         */
        //CGFloat rateH = rendeH/assetVideoTrack.naturalSize.height;
        // CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(rateW,rateH);
        //CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(1.18,1.48);
        NSLog(@"视频的大小 %d----%d",(int)assetVideoTrack.naturalSize.width,(int)assetVideoTrack.naturalSize.height);
        //        if(isFirstAssetPortrait_){
        //            //videoComposition.renderSize = CGSizeMake(assetVideoTrack.naturalSize.height,assetVideoTrack.naturalSize.width);
        //            videoComposition.renderSize = CGSizeMake(assetVideoTrack.naturalSize.height,assetVideoTrack.naturalSize.height);
        //        }else{
        //            videoComposition.renderSize = CGSizeMake(assetVideoTrack.naturalSize.width,assetVideoTrack.naturalSize.height);
        //        }
        videoComposition.renderSize =CGSizeMake(720,720);
        AVMutableVideoCompositionInstruction * avMutableVideoCompositionInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
        [avMutableVideoCompositionInstruction setTimeRange:CMTimeRangeMake(kCMTimeZero, totalDuration)];
        // [avMutableVideoCompositionLayerInstruction setTransform:layerTransform atTime:kCMTimeZero];
        //[avMutableVideoCompositionLayerInstruction setTransform:CGAffineTransformConcat(assetVideoTrack.preferredTransform, FirstAssetScaleFactor) atTime:kCMTimeZero];
        
        //  [avMutableVideoCompositionLayerInstruction setTransform:assetVideoTrack.preferredTransform atTime:kCMTimeZero];
        
        avMutableVideoCompositionInstruction.layerInstructions = [NSArray arrayWithObject:avMutableVideoCompositionLayerInstruction];
        videoComposition.instructions = [NSArray arrayWithObject:avMutableVideoCompositionInstruction];
    }
    
    NSFileManager* fileManager=[NSFileManager defaultManager];
    BOOL blHave=[[NSFileManager defaultManager] fileExistsAtPath:videoFullPath];
    if (blHave) {
        [fileManager removeItemAtPath:videoFullPath error:nil];
    }
    NSURL *mergeFileURL = [NSURL fileURLWithPath:videoFullPath];
    //        NSLog(@"starvideorecordVC: 345 outpath = %@",outpath);
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetMediumQuality];
    exporter.outputURL = mergeFileURL;
    exporter.videoComposition = videoComposition;
    exporter.outputFileType = AVFileTypeMPEG4;
    exporter.shouldOptimizeForNetworkUse = YES;
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        NSLog(@" exporter%@",exporter.error);
        if (exporter.status == AVAssetExportSessionStatusCompleted) {
            
            BOOL isSuccess = NO;
            NSString *msg = @"合并完成";
            switch (exporter.status) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"HandlerVideo -> combinationVidesError: %@", exporter.error.localizedDescription);
                    msg = @"合并失败";
                    break;
                case AVAssetExportSessionStatusUnknown:
                case AVAssetExportSessionStatusCancelled:
                    break;
                case AVAssetExportSessionStatusWaiting:
                    break;
                case AVAssetExportSessionStatusExporting:
                    break;
                case AVAssetExportSessionStatusCompleted:
                    isSuccess = YES;
                    break;
            }
            if (completedBlock) {
                completedBlock(isSuccess, msg);
            }
        }
    }];
}
- (void)splitVideo:(NSURL *)fileUrl fps:(float)fps splitCompleteBlock:(SplitCompleteBlock)splitCompleteBlock {
    if (!fileUrl) {
        return;
    }
    NSMutableArray *splitImages = [NSMutableArray array];
    NSDictionary *optDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
    AVURLAsset *avasset = [[AVURLAsset alloc] initWithURL:fileUrl options:optDict];
    
    CMTime cmtime = avasset.duration; //视频时间信息结构体
    Float64 durationSeconds = CMTimeGetSeconds(cmtime); //视频总秒数
    
    NSMutableArray *times = [NSMutableArray array];
    Float64 totalFrames = durationSeconds * fps; //获得视频总帧数
    CMTime timeFrame;
    for (int i = 1; i <= totalFrames; i++) {
        timeFrame = CMTimeMake(i, fps); //第i帧  帧率
        NSValue *timeValue = [NSValue valueWithCMTime:timeFrame];
        [times addObject:timeValue];
    }
    
    AVAssetImageGenerator *imgGenerator = [[AVAssetImageGenerator alloc] initWithAsset:avasset];
    //防止时间出现偏差
    imgGenerator.requestedTimeToleranceBefore = kCMTimeZero;
    imgGenerator.requestedTimeToleranceAfter = kCMTimeZero;
    
    NSInteger timesCount = [times count];
    [imgGenerator generateCGImagesAsynchronouslyForTimes:times completionHandler:^(CMTime requestedTime, CGImageRef  _Nullable image, CMTime actualTime, AVAssetImageGeneratorResult result, NSError * _Nullable error) {
        printf("current-----: %lld\n", requestedTime.value);
        printf("timeScale----: %d\n",requestedTime.timescale);
        BOOL isSuccess = NO;
        switch (result) {
            case AVAssetImageGeneratorCancelled:
                NSLog(@"Cancelled");
                break;
            case AVAssetImageGeneratorFailed:
                NSLog(@"Failed");
                break;
            case AVAssetImageGeneratorSucceeded: {
                UIImage *frameImg = [UIImage imageWithCGImage:image];
                [splitImages addObject:frameImg];
                
                if (requestedTime.value == timesCount) {
                    isSuccess = YES;
                    NSLog(@"completed");
                }
            }
                break;
        }
        if (splitCompleteBlock) {
            splitCompleteBlock(isSuccess,splitImages);
        }
    }];
}
-(void)testCompressionSession:(int)index image:(UIImage*)image videoSize:(CGSize)videoSize composesBlock:(CompFinalCompletedBlock)composesBlock{
    //设置mov存储路径
    NSArray *paths =NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSDate *date = [NSDate date];
    //获取当前时间
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
    NSString *curretDateAndTime = [dateFormat stringFromDate:date];
    NSString *moviePath =[[paths objectAtIndex:0]stringByAppendingPathComponent:[NSString stringWithFormat:@"%d%@.mov",index,curretDateAndTime]];
    //判断路径是否存在
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    success = [fileManager fileExistsAtPath:moviePath];
    if (success) {
        [fileManager removeItemAtPath:moviePath error:nil];
    }
    NSLog(@"地址0：  %@",moviePath);
    //self.theVideoPath=moviePath;
    
    //设置image的尺寸
    CGRect rect = CGRectMake(0, 0,image.size.width, image.size.height);
    if (rect.size.width < rect.size.height)
    {
        rect.origin.y = (rect.size.height - rect.size.width)/2;
        rect.size.height = rect.size.width;
    }else
    {
        rect.origin.x = (rect.size.width - rect.size.height)/2;
        rect.size.width = rect.size.height;
    }
    //裁剪
    UIImage *newImage = [self  croppedImage:image bounds:rect];
    /**
     *  缩放
     */
    image = [self clipImage:newImage ScaleWithsize:CGSizeMake(640,640)];//640 ,640
    //视频的大小
    videoSize = CGSizeMake(640, 640);
    CGSize size = videoSize;
    
    NSError *error =nil;
    //    转成UTF-8编码
    unlink([moviePath UTF8String]);
    NSLog(@"path->%@",moviePath);
    //     iphone提供了AVFoundation库来方便的操作多媒体设备，AVAssetWriter这个类可以方便的将图像和音频写成一个完整的视频文件
    AVAssetWriter *videoWriter = [[AVAssetWriter alloc] initWithURL:[NSURL fileURLWithPath:moviePath] fileType:AVFileTypeQuickTimeMovie error:&error];
    
    NSParameterAssert(videoWriter);
    if(error)
        NSLog(@"error =%@", [error localizedDescription]);
    //mov的格式设置 编码格式 宽度 高度
    NSDictionary *videoSettings =[NSDictionary dictionaryWithObjectsAndKeys:AVVideoCodecH264,AVVideoCodecKey,
                                  [NSNumber numberWithInt:size.width],AVVideoWidthKey,
                                  [NSNumber numberWithInt:size.height],AVVideoHeightKey,nil];
    
    AVAssetWriterInput *writerInput =[AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:videoSettings];
    
    NSDictionary*sourcePixelBufferAttributesDictionary =[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:kCVPixelFormatType_32ARGB],kCVPixelBufferPixelFormatTypeKey,nil];
    //    AVAssetWriterInputPixelBufferAdaptor提供CVPixelBufferPool实例,
    //    可以使用分配像素缓冲区写入输出文件。使用提供的像素为缓冲池分配通常
    //    是更有效的比添加像素缓冲区分配使用一个单独的池
    AVAssetWriterInputPixelBufferAdaptor *adaptor =[AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:writerInput sourcePixelBufferAttributes:sourcePixelBufferAttributesDictionary];
    
    NSParameterAssert(writerInput);
    NSParameterAssert([videoWriter canAddInput:writerInput]);
    
    if ([videoWriter canAddInput:writerInput])
    {
        NSLog(@"11111");
    }
    else
    {
        NSLog(@"22222");
    }
    
    [videoWriter addInput:writerInput];
    
    [videoWriter startWriting];
    [videoWriter startSessionAtSourceTime:kCMTimeZero];
    
    //合成多张图片为一个视频文件
    dispatch_queue_t dispatchQueue =dispatch_queue_create("mediaInputQueue",NULL);
    // int __block frame =0;
    [writerInput requestMediaDataWhenReadyOnQueue:dispatchQueue usingBlock:^{
        while([writerInput isReadyForMoreMediaData])
        {
            CVPixelBufferRef buffer = (CVPixelBufferRef)[self pixelBufferFromCGImage:[image CGImage] size:size];
            if (buffer)
            {
                //                RECORD_VIDEO_FPS
                if(![adaptor appendPixelBuffer:buffer withPresentationTime:kCMTimeZero]) {
                    
                    CFRelease(buffer);
                    // 出错的情况吓会执行这些。
                    // 此处应该恢复刚进来的状况
                    NSLog(@"视频录制出错了");
                }else
                    CFRelease(buffer);
                [writerInput markAsFinished];
                [videoWriter finishWriting];
                if(composesBlock){
                    composesBlock(YES,moviePath);
                }
                break;
            }
        }
        isprocessing = NO;
    }];
    while (isprocessing) {
        sleep(0.1);
    }
    
}
//对图片尺寸进行压缩--
-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    //    新创建的位图上下文 newSize为其大小
    UIGraphicsBeginImageContext(newSize);
    //    对图片进行尺寸的改变
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    //    从当前上下文中获取一个UIImage对象  即获取新的图片对象
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    // Return the new image.
    return newImage;
}

//图片剪切
- (UIImage *)croppedImage:(UIImage *)image bounds:(CGRect)bounds
{
    //                                                        UIImage *resizeImage;
    //                                                        CGFloat iw = image.size.width;
    //                                                        CGFloat ih = image.size.height;
    //                                                        if (iw > ih) {//长大于宽
    //                                                            CGImageRef cgimage =CGImageCreateWithImageInRect([image CGImage], CGRectMake((iw-ih)/2,0, ih, ih));
    //                                                            resizeImage = [[UIImage alloc] initWithCGImage:cgimage scale:2 orientation:image.imageOrientation];
    //                                                        } else {
    //                                                            CGImageRef cgimage =CGImageCreateWithImageInRect([image CGImage], CGRectMake(0,(ih-iw)/2, iw, iw));
    //                                                            resizeImage = [[UIImage alloc] initWithCGImage:cgimage scale:2 orientation:image.imageOrientation];
    //                                                        }
    //                                                        image = resizeImage;
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], bounds);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return croppedImage;
}
//图片缩放
- (UIImage *)clipImage:(UIImage *)image ScaleWithsize:(CGSize)asize
{
    UIImage *newimage;
    if (nil == image) {
        newimage = nil;
    }
    else{
        CGSize oldsize = image.size;
        CGRect rect;
        if (asize.width/asize.height > oldsize.width/oldsize.height) {
            rect.size.width = asize.width;
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            rect.origin.x = 0;
            rect.origin.y = (asize.height - rect.size.height)/2;
        }
        else{
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            rect.size.height = asize.height;
            rect.origin.x = (asize.width - rect.size.width)/2;
            rect.origin.y = 0;
        }
        UIGraphicsBeginImageContext(asize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextClipToRect(context, CGRectMake(0, 0, asize.width, asize.height));
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        [image drawInRect:rect];
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newimage;
}
////单一视频的修改（该视频的大小）
//- (void)videosOneWithVideoPath:(NSURL *)subsectionPaths lastVideoPath:(NSURL *)lastPath videoFullPath:(NSString *)videoFullPath videoScale:(BOOL)videoScale VideocuPoint:(CGPoint )videocuPoint VideoW :(CGFloat)videoW VideoAngle:(CGFloat)videoAngle completedBlock:(CompFinalCompletedBlock)completedBlock {
//    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
//
//    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
//
//    AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
//                                                                        preferredTrackID:kCMPersistentTrackID_Invalid];
//    // CMTime totalDuration = kCMTimeZero;
//    //  取出最后一个(以最后一个为标准)
//    AVURLAsset *lastAsset = [AVURLAsset assetWithURL:subsectionPaths];
//    AVAssetTrack *LastAssetVideoTrack = [[lastAsset tracksWithMediaType:AVMediaTypeVideo]firstObject];
//    //无用，大小固定（1080，1080）
//    CGFloat rendeW  = 1080;//LastAssetVideoTrack.naturalSize.width;
//    CGFloat rendeH  = 1080;//LastAssetVideoTrack.naturalSize.height;
//    AVMutableVideoCompositionLayerInstruction * avMutableVideoCompositionLayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
//    AVURLAsset *asset = [AVURLAsset assetWithURL:subsectionPaths];
//
//    //    CMTime duration =kCMTimeZero;
//    //    duration=CMTimeAdd(duration, asset.duration);
//    //    //视频的频率
//    //    CGFloat timeScale = asset.duration.timescale;
//    //    //视频的长度：second   要定义的长度：videoDuration
//    //    CGFloat second = asset.duration.value/timeScale;
//    //       // Float64 seconds = CMTimeGetSeconds(asset.duration);
//    //     NSLog(@"视频的时间：----%f",timeScale);
//
//    videoComposition.frameDuration = CMTimeMake(1, 30);
//
//    NSError *erroraudio = nil;
//    //获取AVAsset中的音频
//    AVAssetTrack *assetAudioTrack = [[asset tracksWithMediaType:AVMediaTypeAudio] firstObject];
//    if(assetAudioTrack){
//        //向通道内加入音频
//        [[mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid] insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration)
//                                                                                                                              ofTrack:assetAudioTrack
//                                                                                                                               atTime:kCMTimeZero
//                                                                                                                                error:&erroraudio];
//    }
//    NSError *errorVideo = nil;
//    AVAssetTrack *assetVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo]firstObject];
//    [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration)
//                        ofTrack:assetVideoTrack
//                         atTime:kCMTimeZero
//                          error:&errorVideo];
//
//    UIImageOrientation FirstAssetOrientation_  = UIImageOrientationUp;
//    BOOL  isFirstAssetPortrait_  = NO;
//    CGAffineTransform firstTransform = assetVideoTrack.preferredTransform;
//    if(firstTransform.a == 0 && firstTransform.b == 1.0 && firstTransform.c == -1.0 && firstTransform.d == 0)
//    {
//        FirstAssetOrientation_= UIImageOrientationRight;
//        isFirstAssetPortrait_ = YES;
//    }
//    if(firstTransform.a == 0 && firstTransform.b == -1.0 && firstTransform.c == 1.0 && firstTransform.d == 0)  {
//        FirstAssetOrientation_ =  UIImageOrientationLeft;
//        isFirstAssetPortrait_ = YES;
//    }
//    if(firstTransform.a == 1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == 1.0)   {
//        FirstAssetOrientation_ =  UIImageOrientationUp;
//
//    }
//    if(firstTransform.a == -1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == -1.0) {
//        FirstAssetOrientation_ = UIImageOrientationDown;
//    }
//    CGFloat FirstAssetScaleToFitRatio ;//= ScreenWidth/FirstAssetTrack.naturalSize.width;
//    if (assetVideoTrack.naturalSize.width <= assetVideoTrack.naturalSize.height) {
//        FirstAssetScaleToFitRatio = rendeW/assetVideoTrack.naturalSize.width;
//    }else
//    {
//        FirstAssetScaleToFitRatio = rendeH/assetVideoTrack.naturalSize.height;
//    }
//    if(videoScale){ //这里改变大小videoW
//        float    scaleX = videoW/assetVideoTrack.naturalSize.width;
//        float    scaleY = videoW/assetVideoTrack.naturalSize.height;
//        float    scale  = MIN(scaleX, scaleY);
//        if(isFirstAssetPortrait_){
//
//            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(scale,scale);
//            CGAffineTransform FirstAssetRotaFactor =  CGAffineTransformMakeRotation(videoAngle);
//            CGAffineTransform assetScaleFactor;
//            if(assetVideoTrack.naturalSize.width <=
//               assetVideoTrack.naturalSize.height){
//                assetScaleFactor = CGAffineTransformMakeTranslation(videocuPoint.y,videocuPoint.x);
//            }else{
//                assetScaleFactor = CGAffineTransformMakeTranslation(videocuPoint.x,videocuPoint.y);
//            }
//            [avMutableVideoCompositionLayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(CGAffineTransformConcat(assetVideoTrack.preferredTransform, FirstAssetScaleFactor),assetScaleFactor),FirstAssetRotaFactor) atTime:kCMTimeZero];
//           // [avMutableVideoCompositionLayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(assetVideoTrack.preferredTransform, FirstAssetScaleFactor),assetScaleFactor) atTime:kCMTimeZero];
//           // [avMutableVideoCompositionLayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(CGAffineTransformConcat(assetVideoTrack.preferredTransform, FirstAssetScaleFactor),FirstAssetRotaFactor),assetScaleFactor) atTime:kCMTimeZero];
//            //videoComposition.renderSize = CGSizeMake(scale*assetVideoTrack.naturalSize.height,1080);
//        }else{
//            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(scale,scale);
//            CGAffineTransform FirstAssetRotaFactor =  CGAffineTransformMakeRotation(videoAngle);
//            CGAffineTransform assetScaleFactor;
////            if(assetVideoTrack.naturalSize.width <=
////               assetVideoTrack.naturalSize.height){
////                assetScaleFactor = CGAffineTransformMakeTranslation((1080-scale*assetVideoTrack.naturalSize.width)/2,0);
////
////
////            }else{
//                assetScaleFactor = CGAffineTransformMakeTranslation(videocuPoint.x,videocuPoint.y);
////            }
//            [avMutableVideoCompositionLayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(CGAffineTransformConcat(assetVideoTrack.preferredTransform, FirstAssetScaleFactor),assetScaleFactor),FirstAssetRotaFactor) atTime:kCMTimeZero];
//           // [avMutableVideoCompositionLayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(CGAffineTransformConcat(assetVideoTrack.preferredTransform, FirstAssetScaleFactor),FirstAssetRotaFactor),assetScaleFactor) atTime:kCMTimeZero];
//            //videoComposition.renderSize = CGSizeMake(1080,scale*assetVideoTrack.naturalSize.height);
//        }
//    }else{
//        if(isFirstAssetPortrait_){
//            // FirstAssetScaleToFitRatio = ScreenWidth/FirstAssetTrack.naturalSize.height;
//            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
//            //CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(1,1);
//            [avMutableVideoCompositionLayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(assetVideoTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(0,(1080-FirstAssetScaleToFitRatio*assetVideoTrack.naturalSize.width)/2)) atTime:kCMTimeZero];
//        }else{
//            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
//            //CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(1,1);
//            [avMutableVideoCompositionLayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(assetVideoTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation((1080-FirstAssetScaleToFitRatio*assetVideoTrack.naturalSize.width)/2, 0)) atTime:kCMTimeZero];
//        }
//    }
//    videoComposition.renderSize = CGSizeMake(1080,1080);
//    // totalDuration = CMTimeAdd(totalDuration, asset.duration);
//    // [avMutableVideoCompositionLayerInstruction setCropRectangle:CGRectMake(0, 0, 1000,1080) atTime:kCMTimeZero];
//
//    AVMutableVideoCompositionInstruction * avMutableVideoCompositionInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
//    [avMutableVideoCompositionInstruction setTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration)];
//    avMutableVideoCompositionInstruction.layerInstructions = [NSArray arrayWithObject:avMutableVideoCompositionLayerInstruction];
//
//    videoComposition.instructions = [NSArray arrayWithObject:avMutableVideoCompositionInstruction];
//
//
//    NSLog(@"视频的大小 %d----%d",(int)assetVideoTrack.naturalSize.width,(int)assetVideoTrack.naturalSize.height);
//
//    NSFileManager* fileManager=[NSFileManager defaultManager];
//    BOOL blHave=[[NSFileManager defaultManager] fileExistsAtPath:videoFullPath];
//    if (blHave) {
//        [fileManager removeItemAtPath:videoFullPath error:nil];
//    }
//    NSURL *mergeFileURL = [NSURL fileURLWithPath:videoFullPath];
//    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
//    exporter.outputURL = mergeFileURL;
//    exporter.videoComposition = videoComposition;
//    exporter.outputFileType = AVFileTypeMPEG4;
//    exporter.shouldOptimizeForNetworkUse = YES;
//    [exporter exportAsynchronouslyWithCompletionHandler:^{
//        NSLog(@" exporter%@",exporter.error);
//        if (exporter.status == AVAssetExportSessionStatusCompleted) {
//
//            BOOL isSuccess = NO;
//            NSString *msg = @"合并完成";
//            switch (exporter.status) {
//                case AVAssetExportSessionStatusFailed:
//                    NSLog(@"HandlerVideo -> combinationVidesError: %@", exporter.error.localizedDescription);
//                    msg = @"合并失败";
//                    break;
//                case AVAssetExportSessionStatusUnknown:
//                case AVAssetExportSessionStatusCancelled:
//                    break;
//                case AVAssetExportSessionStatusWaiting:
//                    break;
//                case AVAssetExportSessionStatusExporting:
//                    break;
//                case AVAssetExportSessionStatusCompleted:
//                    isSuccess = YES;
//                    break;
//            }
//            if (completedBlock) {
//                completedBlock(isSuccess, msg);
//            }
//        }
//    }];
//}


//单一视频的修改（该视频的大小）
- (void)videosOneWithVideoPath:(NSURL *)subsectionPaths lastVideoPath:(NSURL *)lastPath videoFullPath:(NSString *)videoFullPath videoScale:(BOOL)videoScale VideocuPoint:(CGPoint )videocuPoint VideoW :(CGFloat)videoW VideoAngle:(CGFloat)videoAngle VideoColor:(UIColor *)videocolor VideoImage:(UIImage *)videoimage completedBlock:(CompFinalCompletedBlock)completedBlock {
    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
    
    AVMutableVideoComposition *videoComposition = [AVMutableVideoComposition videoComposition];
    
    AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                        preferredTrackID:kCMPersistentTrackID_Invalid];
    // CMTime totalDuration = kCMTimeZero;
    //  取出最后一个(以最后一个为标准)
    AVURLAsset *lastAsset = [AVURLAsset assetWithURL:subsectionPaths];
    AVAssetTrack *LastAssetVideoTrack = [[lastAsset tracksWithMediaType:AVMediaTypeVideo]firstObject];
    //无用，大小固定（1080，1080）
    CGFloat rendeW  = 1080;//LastAssetVideoTrack.naturalSize.width;
    CGFloat rendeH  = 1080;//LastAssetVideoTrack.naturalSize.height;
    AVMutableVideoCompositionLayerInstruction * avMutableVideoCompositionLayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    AVURLAsset *asset = [AVURLAsset assetWithURL:subsectionPaths];
    
    videoComposition.frameDuration = CMTimeMake(1, 30);
    
    NSError *erroraudio = nil;
    //获取AVAsset中的音频
    AVAssetTrack *assetAudioTrack = [[asset tracksWithMediaType:AVMediaTypeAudio] firstObject];
    if(assetAudioTrack){
        //向通道内加入音频
        [[mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid] insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration)
                                                                                                                              ofTrack:assetAudioTrack
                                                                                                                               atTime:kCMTimeZero
                                                                                                                                error:&erroraudio];
    }
    NSError *errorVideo = nil;
    AVAssetTrack *assetVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo]firstObject];
    [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration)
                        ofTrack:assetVideoTrack
                         atTime:kCMTimeZero
                          error:&errorVideo];
    
    UIImageOrientation FirstAssetOrientation_  = UIImageOrientationUp;
    BOOL  isFirstAssetPortrait_  = NO;
    CGAffineTransform firstTransform = assetVideoTrack.preferredTransform;
    if(firstTransform.a == 0 && firstTransform.b == 1.0 && firstTransform.c == -1.0 && firstTransform.d == 0)
    {
        FirstAssetOrientation_= UIImageOrientationRight;
        isFirstAssetPortrait_ = YES;
    }
    if(firstTransform.a == 0 && firstTransform.b == -1.0 && firstTransform.c == 1.0 && firstTransform.d == 0)  {
        FirstAssetOrientation_ =  UIImageOrientationLeft;
        isFirstAssetPortrait_ = YES;
    }
    if(firstTransform.a == 1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == 1.0)   {
        FirstAssetOrientation_ =  UIImageOrientationUp;
        
    }
    if(firstTransform.a == -1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == -1.0) {
        FirstAssetOrientation_ = UIImageOrientationDown;
    }
    
    float    scaleX = videoW/assetVideoTrack.naturalSize.width;
    float    scaleY = videoW/assetVideoTrack.naturalSize.height;
    float    scale  = MIN(scaleX, scaleY);
    NSLog(@"大小 ---- %@------%f",NSStringFromCGSize(assetVideoTrack.naturalSize),videoW);
    CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(scale,scale);
    CGAffineTransform FirstAssetRotaFactor =  CGAffineTransformMakeRotation(videoAngle);
    CGAffineTransform assetScaleFactor;
    if(isFirstAssetPortrait_){
        if(assetVideoTrack.naturalSize.width <=
           assetVideoTrack.naturalSize.height){
            assetScaleFactor = CGAffineTransformMakeTranslation(videocuPoint.y,videocuPoint.x);
        }else{
            if(firstTransform.tx == 0){
                assetScaleFactor = CGAffineTransformMakeTranslation(scale*assetVideoTrack.naturalSize.height+videocuPoint.x,videocuPoint.y);
            }else{
                assetScaleFactor = CGAffineTransformMakeTranslation(videocuPoint.x,videocuPoint.y);
            }
            //            assetScaleFactor = CGAffineTransformMakeTranslation(scale*assetVideoTrack.naturalSize.height+videocuPoint.x,videocuPoint.y);
        }
        [avMutableVideoCompositionLayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(CGAffineTransformConcat(assetVideoTrack.preferredTransform, FirstAssetScaleFactor),assetScaleFactor),FirstAssetRotaFactor) atTime:kCMTimeZero];
    }
    else{
        if(assetVideoTrack.naturalSize.width <=
           assetVideoTrack.naturalSize.height){
            assetScaleFactor = CGAffineTransformMakeTranslation(videocuPoint.x,videocuPoint.y);
        }else{
            assetScaleFactor = CGAffineTransformMakeTranslation(videocuPoint.x,videocuPoint.y);
        }
        [avMutableVideoCompositionLayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(CGAffineTransformConcat(assetVideoTrack.preferredTransform, FirstAssetScaleFactor),assetScaleFactor),FirstAssetRotaFactor) atTime:kCMTimeZero];
    }
    
    
    videoComposition.renderSize = CGSizeMake(1080,1080);
    AVMutableVideoCompositionInstruction * avMutableVideoCompositionInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    [avMutableVideoCompositionInstruction setTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration)];
    avMutableVideoCompositionInstruction.layerInstructions = [NSArray arrayWithObject:avMutableVideoCompositionLayerInstruction];
    if(videocolor){
        avMutableVideoCompositionInstruction.backgroundColor = videocolor.CGColor;
    }else{
        CALayer *parentLayer = [CALayer layer];
        parentLayer.frame = CGRectMake(0, 0,1080,1080);
        UIImage *viewImage = [ImageHelper image:videoimage fillSize:CGSizeMake(1080,1080)];
        CALayer *backgroundLayer = [CALayer layer];
        [backgroundLayer setContents:(id)[viewImage CGImage]];
        backgroundLayer.frame = CGRectMake(0, 0,1080,1080);
        [backgroundLayer setMasksToBounds:YES];
        
        CALayer *videoLayer = [CALayer layer];
        videoLayer.frame = CGRectMake(videocuPoint.x,videocuPoint.y ,
                                      1080 ,720*scale);
        [parentLayer addSublayer:backgroundLayer];
        [parentLayer addSublayer:videoLayer];
        videoComposition.animationTool = [AVVideoCompositionCoreAnimationTool
                                     videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
       // avMutableVideoCompositionInstruction.backgroundColor = [UIColor colorWithPatternImage:viewImage].CGColor;
    }
    
    videoComposition.instructions = [NSArray arrayWithObject:avMutableVideoCompositionInstruction];
    
    NSLog(@"视频的大小 %d----%d",(int)assetVideoTrack.naturalSize.width,(int)assetVideoTrack.naturalSize.height);
    
    NSFileManager* fileManager=[NSFileManager defaultManager];
    BOOL blHave=[[NSFileManager defaultManager] fileExistsAtPath:videoFullPath];
    if (blHave) {
        [fileManager removeItemAtPath:videoFullPath error:nil];
    }
    NSURL *mergeFileURL = [NSURL fileURLWithPath:videoFullPath];
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    exporter.outputURL = mergeFileURL;
    exporter.videoComposition = videoComposition;
    exporter.outputFileType = AVFileTypeMPEG4;
    exporter.shouldOptimizeForNetworkUse = YES;
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        NSLog(@" exporter%@",exporter.error);
        if (exporter.status == AVAssetExportSessionStatusCompleted) {
            
            BOOL isSuccess = NO;
            NSString *msg = @"合并完成";
            switch (exporter.status) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"HandlerVideo -> combinationVidesError: %@", exporter.error.localizedDescription);
                    msg = @"合并失败";
                    break;
                case AVAssetExportSessionStatusUnknown:
                case AVAssetExportSessionStatusCancelled:
                    break;
                case AVAssetExportSessionStatusWaiting:
                    break;
                case AVAssetExportSessionStatusExporting:
                    break;
                case AVAssetExportSessionStatusCompleted:
                    isSuccess = YES;
                    break;
            }
            if (completedBlock) {
                completedBlock(isSuccess, msg);
            }
        }
    }];
}
@end
