//
//  NSString+UrlEncode.h
//  Backcam
//
//  Created by mangal on 09/04/15.
//  Copyright (c) 2015 RJ Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (UrlEncode)
{
    
}

- (NSString*)urlencodedString;

@end
