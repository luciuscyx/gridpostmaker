//
//  IAPManager.m
//  Classes
//
//  Created by Heracles on 08.02.14


#import "Imageprocess.h"
#import "UIView+Frame.h"
@interface Imageprocess ()
@end

@implementation Imageprocess

+(UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
    
    CGImageRef maskRef = maskImage.CGImage;
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
    CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
    UIImage * retImage = [UIImage imageWithCGImage:masked];
    CGImageRelease(masked);
    CGImageRelease(mask);
    return  retImage;
    
}
+ (UIImage *) imageWithBits: (unsigned char *) bits withSize: (CGSize) size
{
    // Create a color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
        free(bits);
        return nil;
    }
    
    CGContextRef context = CGBitmapContextCreate (bits, size.width, size.height, 8, size.width * 4, colorSpace, kCGImageAlphaPremultipliedFirst);
    if (context == NULL)
    {
        fprintf (stderr, "Error: Context not created!");
        free (bits);
        CGColorSpaceRelease(colorSpace );
        return nil;
    }
    
    CGColorSpaceRelease(colorSpace );
    CGImageRef ref = CGBitmapContextCreateImage(context);
    free(CGBitmapContextGetData(context));
    CGContextRelease(context);
    
    UIImage *img = [UIImage imageWithCGImage:ref];
    CFRelease(ref);
    return img;
}

- (UIImage *) doUnrotateImage: (UIImage *) image
{
    CGSize size = image.size;
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGAffineTransform transform = CGAffineTransformMakeScale(-1.0f, 1.0f);
    size = CGSizeMake(size.height, size.width);
    CGContextConcatCTM(context, transform);
    
    
    CGRect rect=CGRectMake(0.0f, 0.0f,image.size.width, image.size.height);
    UIGraphicsBeginImageContext(rect.size);//根据size大小创建一个基于位图的图形上下文
    CGContextRef currentContext = UIGraphicsGetCurrentContext();//获取当前quartz 2d绘图环境
    CGContextClipToRect(currentContext, rect);//设置当前绘图环境到矩形框
    
    
    CGContextRotateCTM(currentContext, M_PI);
    CGContextTranslateCTM(currentContext, -rect.size.width, -rect.size.height);
    CGContextDrawImage(currentContext, rect, image.CGImage);//绘图
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();//获得图片
    UIGraphicsEndImageContext();//从当前堆栈中删除quartz 2d绘图环境
    //释放
    
    return newimg;
}

//剪切图片image1中间一部分大小判断用size Cut in the middle part of the picture.

- (UIImage *) image: (UIImage *) image1 CutPartOfSize: (CGSize ) size
{
    
    CGSize newsize=image1.size;
    
    if((image1.size.width>size.width)&& (image1.size.height< size.height))
    {
        newsize=CGSizeMake(image1.size.height*size.width/size.height, image1.size.height);
    }
    if((image1.size.width<size.width)&& (image1.size.height>size.height))
    {
        newsize=CGSizeMake(image1.size.width, image1.size.width*size.height/size.width);
    }

    UIGraphicsBeginImageContext(newsize);
    if((image1.size.width>size.width)&& (image1.size.height< size.height))
    {
        [image1 drawAtPoint:CGPointMake(-(image1.size.width-image1.size.height*size.width/size.height)/2.0f, 0.0f)];

    }
    else if((image1.size.width<size.width)&& (image1.size.height>size.height))
    {
        [image1 drawAtPoint:CGPointMake(0.0f, -(image1.size.height-image1.size.width*size.height/size.width)/2.0f)];
    }
    else
    {
        [image1 drawInRect:CGRectMake(0, 0, newsize.width, newsize.height)];
    }
    
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;

    
    
    
    
    
    
    
//    CGSize newsize=image1.size;
//    
//    if((image1.size.width>size.width)&& (image1.size.height< size.height))
//    {
//        newsize=CGSizeMake(size.width, image1.size.height);
//    }
//    
//    UIGraphicsBeginImageContext(newsize);
//    
//    if((image1.size.width>size.width)&& (image1.size.height<size.height))
//    {
//        [image1 drawAtPoint:CGPointMake(-(image1.size.width-size.width)/2.0f, 0.0f)];
//    }
//    else
//    {
//        [image1 drawInRect:CGRectMake(0, 0, newsize.width, newsize.height)];
//    }
//    
//    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return newimg;
}
//把图片放大，图片大小放大，内容daxiao不变
- (UIImage *) image: (UIImage *) image1 AddscaleToSize: (CGSize) Addsize
{
    CGSize newsize=CGSizeMake(Addsize.width, Addsize.height);
    UIGraphicsBeginImageContext(newsize);
    [image1 drawAtPoint:CGPointMake(1*(Addsize.width-image1.size.width)/2, 1*(Addsize.height-image1.size.height)/2)];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
}


//遍历图片像素，更改图片颜色
void ProviderReleaseData (void *info, const void *data, size_t size)
{
    free((void*)data);
}

- (UIImage*) imageBlackToTransparent:(UIImage*) image
{
    // 分配内存
    const int imageWidth = image.size.width;
    const int imageHeight = image.size.height;
    size_t      bytesPerRow = imageWidth * 4;
    uint32_t* rgbImageBuf = (uint32_t*)malloc(bytesPerRow * imageHeight);
    
    // 创建context
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbImageBuf, imageWidth, imageHeight, 8, bytesPerRow, colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGContextDrawImage(context, CGRectMake(0, 0, imageWidth, imageHeight), image.CGImage);
    
    // 遍历像素
    int pixelNum = imageWidth * imageHeight;
    uint32_t* pCurPtr = rgbImageBuf;
    for (int i = 0; i < pixelNum; i++, pCurPtr++)
    {
        if ((*pCurPtr & 0xFFFFFF00) == 0xffffff00)    // 将白色变成透明
        {
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[0] = 0;
        }
        else
        {
            // 改成下面的代码，会将图片转成想要的颜色
            //  uint8_t* ptr = (uint8_t*)pCurPtr;
            // ptr[3] = 0; //0~255
            //  ptr[2] = 0;
            //  ptr[1] = 0;
            
        }
        
    }
    
    // 将内存转成image
    CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, rgbImageBuf, bytesPerRow * imageHeight, ProviderReleaseData);
    CGImageRef imageRef = CGImageCreate(imageWidth, imageHeight, 8, 32, bytesPerRow, colorSpace,
                                        kCGImageAlphaLast | kCGBitmapByteOrder32Little, dataProvider,
                                        NULL, true, kCGRenderingIntentDefault);
    CGDataProviderRelease(dataProvider);
    
    UIImage* resultUIImage = [UIImage imageWithCGImage:imageRef];
    
    // 释放
    CGImageRelease(imageRef);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    // free(rgbImageBuf) 创建dataProvider时已提供释放函数，这里不用free
    
    return resultUIImage;
}

//设置图片透明度
- (UIImage *)imageByApplyingAlpha:(CGFloat)alpha  image:(UIImage*)image
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, 0.0f);
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGRect area = CGRectMake(0, 0, image.size.width, image.size.height);
    
    CGContextScaleCTM(ctx, 1, -1);
    CGContextTranslateCTM(ctx, 0, -area.size.height);
    
    CGContextSetBlendMode(ctx, kCGBlendModeMultiply);
    
    CGContextSetAlpha(ctx, alpha);
    
    CGContextDrawImage(ctx, area, image.CGImage);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2 ToSize:(CGSize )size AddToX:(float )X AddToY:(float )Y  AddToOffsetX:(float )OffsetX AddToOffsetY:(float )OffsetY{
    
    UIGraphicsBeginImageContext(size);
    
    // Draw image1
    [image2 drawInRect:CGRectMake(0, 0, image2.size.width, image2.size.height)];
    
    // Draw image2
    [image1 drawInRect:CGRectMake(X-OffsetX, Y-OffsetY, image1.size.width, image1.size.height)];
    
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return resultingImage;
}

- (UIImage *)scaleToSize:(UIImage *)image size:(CGSize)viewsize{
    //    // 创建一个bitmap的context
    //    // 并把它设置成为当前正在使用的context
    //    UIGraphicsBeginImageContext(size);
    //    // 绘制改变大小的图片
    //    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    //    // 从当前context中创建一个改变大小后的图片
    //    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    //    // 使当前的context出堆栈
    //    UIGraphicsEndImageContext();
    //    // 返回新的改变大小后的图片
    //    return scaledImage;
    
    CGSize size = image.size;
    
    CGFloat scalex = viewsize.width / size.width;
    CGFloat scaley = viewsize.height / size.height;
    CGFloat scale = MAX(scalex, scaley);
    
    UIGraphicsBeginImageContext(viewsize);
    
    CGFloat width = size.width * scale;
    CGFloat height = size.height * scale;
    
    float dwidth = ((viewsize.width - width) / 2.0f);
    float dheight = ((viewsize.height - height) / 2.0f);
    
    CGRect rect = CGRectMake(dwidth, dheight, size.width * scale, size.height * scale);
    [image drawInRect:rect];
    
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newimg;
}
//截图view转成image
-(UIImage *)getImageFromView:(UIView *)orgView{
    // CGSize size = CGSizeMake(100, 100);
    UIGraphicsBeginImageContext(orgView.bounds.size);
    [orgView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}
-(UIImage *)getImageFromImageView:(UIImageView *)orgView{
    // CGSize size = CGSizeMake(100, 100);
    UIGraphicsBeginImageContext(orgView.bounds.size);
    [orgView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


- (UIImage*) imageAlphaToWhite:(UIImage*) image
{
    // 分配内存
    const int imageWidth = image.size.width;
    const int imageHeight = image.size.height;
    size_t      bytesPerRow = imageWidth * 4;
    uint32_t* rgbImageBuf = (uint32_t*)malloc(bytesPerRow * imageHeight);
    
    // 创建context
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbImageBuf, imageWidth, imageHeight, 8, bytesPerRow, colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGContextDrawImage(context, CGRectMake(0, 0, imageWidth, imageHeight), image.CGImage);
    
    // 遍历像素
    int pixelNum = imageWidth * imageHeight;
    uint32_t* pCurPtr = rgbImageBuf;
    for (int i = 0; i < pixelNum; i++, pCurPtr++)
    {
        if ((*pCurPtr & 0xFFFFFF00) == 0x00000000) //not red to white
        {
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[3] = 255; //   white
            ptr[2] = 255;
            ptr[1] = 255;
        }
        else
        {
            // 改成下面的代码，会将图片转成想要的颜色
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[3] = 255; //   white
            ptr[2] = 255;
            ptr[1] = 255;
            
        }
        
    }
    
    // 将内存转成image
    CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, rgbImageBuf, bytesPerRow * imageHeight, ProviderReleaseData);
    CGImageRef imageRef = CGImageCreate(imageWidth, imageHeight, 8, 32, bytesPerRow, colorSpace,
                                        kCGImageAlphaLast | kCGBitmapByteOrder32Little, dataProvider,
                                        NULL, true, kCGRenderingIntentDefault);
    CGDataProviderRelease(dataProvider);
    
    UIImage* resultUIImage = [UIImage imageWithCGImage:imageRef];
    
    // 释放
    CGImageRelease(imageRef);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    // free(rgbImageBuf) 创建dataProvider时已提供释放函数，这里不用free
    
    return resultUIImage;
}

- (UIImage *)CutPartOfImage:(UIImage *)image  mainimage:(UIImage*)mimage offX:(float)offx offY:(float)offy Size: (CGSize ) size
{
    
    CGSize newsize=size;
    
    UIGraphicsBeginImageContext(newsize);
    
    // [image1 drawAtPoint:CGPointMake(-(image1.size.width-image1.size.height*size.width/size.height)/2.0f, 0.0f)];
    [image drawInRect:CGRectMake(0, 0, newsize.width, newsize.height)];
    [mimage drawInRect:CGRectMake(-offx, -offy, mimage.size.width, mimage.size.height)];
    
    //[image drawInRect:CGRectMake(offx, offy, newsize.width, newsize.height)];
    
    
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
}

-(UIImage *)MakeWatemarkImage:(UIImage *)image HaveWatemark:(BOOL)wbool
{
    UIImage *newimage = [[UIImage alloc]init];
    newimage = image;
    if (wbool) {
        UIImage *watemarkImage = [UIImage imageNamed:@"watermark"];
        if (watemarkImage.size.width > image.size.width) {
            watemarkImage = [self scaleToSize:watemarkImage size:CGSizeMake(image.size.width, watemarkImage.size.height * image.size.width / watemarkImage.size.width)];
        }else{
            watemarkImage = [self scaleToSize:watemarkImage size:CGSizeMake(image.size.width/2.0, watemarkImage.size.height * (image.size.width/2.0) / watemarkImage.size.width)];
             newimage = [self addImage:watemarkImage toImage:image ToSize:image.size AddToX: (image.size.width - watemarkImage.size.width)/1.0 AddToY:image.size.height - watemarkImage.size.height AddToOffsetX:0.0 AddToOffsetY:0.0];
        }
    }
    return newimage;
}

- (UIImage*) imageAlpha:(UIImage*) image
{
    // 分配内存
    const int imageWidth = image.size.width;
    const int imageHeight = image.size.height;
    size_t      bytesPerRow = imageWidth * 4;
    uint32_t* rgbImageBuf = (uint32_t*)malloc(bytesPerRow * imageHeight);
    
    // 创建context
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbImageBuf, imageWidth, imageHeight, 8, bytesPerRow, colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGContextDrawImage(context, CGRectMake(0, 0, imageWidth, imageHeight), image.CGImage);
    
    // 遍历像素
    int pixelNum = imageWidth * imageHeight;
    uint32_t* pCurPtr = rgbImageBuf;
    for (int i = 0; i < pixelNum; i++, pCurPtr++)
    {
        
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[3] = 0; //   white
            ptr[2] = 0;
            ptr[1] = 0;
            ptr[0] = 0;
        
        
        
    }
    
    // 将内存转成image
    CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, rgbImageBuf, bytesPerRow * imageHeight, ProviderReleaseData);
    CGImageRef imageRef = CGImageCreate(imageWidth, imageHeight, 8, 32, bytesPerRow, colorSpace,
                                        kCGImageAlphaLast | kCGBitmapByteOrder32Little, dataProvider,
                                        NULL, true, kCGRenderingIntentDefault);
    CGDataProviderRelease(dataProvider);
    
    UIImage* resultUIImage = [UIImage imageWithCGImage:imageRef];
    
    // 释放
    CGImageRelease(imageRef);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    // free(rgbImageBuf) 创建dataProvider时已提供释放函数，这里不用free
    
    return resultUIImage;
}

//截屏（3D图）
- (UIImage *) imageFromViewDeep: (UIView *) theView
{
    UIGraphicsBeginImageContext(theView.frame.size);
    //CGContextRef context = UIGraphicsGetCurrentContext();
    //[theView.layer renderInContext:context];
    [theView drawViewHierarchyInRect:theView.bounds afterScreenUpdates:YES];
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}


+(UIImage *)fixrotation:(UIImage *)image
{
    
    if (image.imageOrientation == UIImageOrientationUp)
    {
        return image;
    }
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}


+(UIImage*) imageSwapFace:(UIImage*) image FaceImage:(UIImage *)otherImage
{
    // 分配内存
    const int imageWidth = image.size.width;
    const int imageHeight = image.size.height;
    size_t      bytesPerRow = imageWidth * 4;
    uint32_t* rgbImageBuf = (uint32_t*)malloc(bytesPerRow * imageHeight);
    
    // 创建context
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbImageBuf, imageWidth, imageHeight, 8, bytesPerRow, colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGContextDrawImage(context, CGRectMake(0, 0, imageWidth, imageHeight), image.CGImage);
    
    // 遍历像素
    int pixelNum = imageWidth * imageHeight;
    uint32_t* pCurPtr = rgbImageBuf;
    
    size_t      bytesPerRow2 = imageWidth * 4;
    uint32_t* rgbImageBuf2 = (uint32_t*)malloc(bytesPerRow2 * imageHeight);
    
    // 创建context
    CGColorSpaceRef colorSpace2 = CGColorSpaceCreateDeviceRGB();
    CGContextRef context2 = CGBitmapContextCreate(rgbImageBuf2, imageWidth, imageHeight, 8, bytesPerRow2, colorSpace2,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGContextDrawImage(context2, CGRectMake(0, 0, imageWidth, imageHeight), otherImage.CGImage);
    
    // 遍历像素
    uint32_t* pCurPtr2 = rgbImageBuf2;
    
    
    for (int i = 0; i < pixelNum; i++, pCurPtr++, pCurPtr2++)
    {
        if ((*pCurPtr2 & 0xFFFFFF00) == 0x00000000) //not red to white
        {
            uint8_t* ptr = (uint8_t*)pCurPtr;
            ptr[3] = 0; //   white
            ptr[2] = 0;
            ptr[1] = 0;
            ptr[0] = 0;
        }
        else if ((*pCurPtr & 0xFFFFFF00) == 0x00000000) //not red to white
        {
            uint8_t* ptr = (uint8_t*)pCurPtr;
                        ptr[3] = 0; //   white
                        ptr[2] = 0;
                        ptr[1] = 0;
            ptr[0] = 0;
        }else
        {
            uint8_t* ptr = (uint8_t*)pCurPtr;
            uint8_t* ptr2 = (uint8_t*)pCurPtr2;
          //  float Y= 0.299*ptr2[3]+ 0.587*ptr2[2]+ 0.114*ptr2[1];
          //  float Cb = -0.148*ptr2[3] - 0.291*ptr2[2] + 0.439*ptr2[1]+ 128;
          //  float Cr = 0.439*ptr2[3] - 0.368*ptr2[2] - 0.071*ptr2[1] + 128;
            
            ptr[1]  =0.299*ptr[3]+ 0.587*ptr[2]+ 0.114*ptr[1];// (ptr[3] + ptr2[3])/2;
            ptr[2]  = -0.148*ptr[3] - 0.291*ptr[2] + 0.439*ptr[1]+ 128;//(ptr[2] + ptr2[2])/2;
            ptr[3]  = 0.439*ptr[3] - 0.368*ptr[2] - 0.071*ptr[1] + 128;//(ptr[1] + ptr2[1])/2;
           // ptr[0]  = (ptr[0] + ptr2[0])/2;
            
        }
        
    }
    
    // 将内存转成image
    CGDataProviderRef dataProvider = CGDataProviderCreateWithData(NULL, rgbImageBuf, bytesPerRow * imageHeight, ProviderReleaseData);
    CGImageRef imageRef = CGImageCreate(imageWidth, imageHeight, 8, 32, bytesPerRow, colorSpace,
                                        kCGImageAlphaLast | kCGBitmapByteOrder32Little, dataProvider,
                                        NULL, true, kCGRenderingIntentDefault);
    CGDataProviderRelease(dataProvider);
    
    UIImage* resultUIImage = [UIImage imageWithCGImage:imageRef];
    
    // 释放
    CGImageRelease(imageRef);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    CGContextRelease(context2);
    CGColorSpaceRelease(colorSpace2);
    // free(rgbImageBuf) 创建dataProvider时已提供释放函数，这里不用free
    
    return resultUIImage;
}


-(UIImage *)getNewImageWithImage:(UIImage *)image otherImage:(UIImage *)otherImage
{
    UIImage *image1 = image;
    UIImage *image2 = otherImage;
    Imageprocess *imageproc = [[Imageprocess alloc]init];
    if (image1.size.height > image2.size.height) {
        image2 = [imageproc scaleToSize:image2 size:CGSizeMake(image2.size.width * image1.size.height/image2.size.height, image1.size.height)];
    }else{
        image1 = [imageproc scaleToSize:image1 size:CGSizeMake(image1.size.width * image2.size.height/image1.size.height, image2.size.height)];
    }
    
    UIGraphicsBeginImageContext(CGSizeMake(image1.size.width + image2.size.width, image1.size.height));
    [image1 drawInRect:CGRectMake(0, 0, image1.size.width, image1.size.height)];
    [image2 drawInRect:CGRectMake(image1.size.width, 0, image2.size.width, image2.size.height)];
    
    UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    image1 = nil;
    image2 = nil;
    return newImage;
}

+(UIImage *)getImageFromView:(UIView *)orgView withScale:(float )scale{
    UIView * outView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, orgView.width*scale, orgView.height*scale)];
    outView.autoresizesSubviews = YES;
    orgView.transform = CGAffineTransformMakeScale(scale, scale);
    orgView.center = outView.center;
    [outView addSubview:orgView];
    // CGSize size = CGSizeMake(100, 100);
    UIGraphicsBeginImageContext(outView.frame.size);
    [outView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    
}


@end
