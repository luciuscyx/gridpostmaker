//
//  RJAnimation.m
//  Instagrid Post
//
//  Created by Mangal on 01/07/15.
//  Copyright (c) 2015 RJLabs. All rights reserved.
//

#import "RJAnimation.h"

UIView *childViewAnimate;

@implementation RJAnimation

+(void)bounceAddSubViewToParentView:(UIView *)objParentView ChildView:(UIView *)objChildView;
{
    childViewAnimate = objChildView;
    [objParentView setAutoresizesSubviews:YES];
    objChildView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3/1.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped)];
     [objParentView addSubview:objChildView];
    objChildView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    [UIView commitAnimations];
}

- (void)bounce1AnimationStopped
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3/2];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped)];
    childViewAnimate.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
    [UIView commitAnimations];
}

- (void)bounce2AnimationStopped
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3/2];
    childViewAnimate.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
}

@end
