//
//  RJAnimation.h
//  Instagrid Post
//
//  Created by Mangal on 01/07/15.
//  Copyright (c) 2015 RJLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RJAnimation : NSObject
{
    
}

+(void)bounceAddSubViewToParentView:(UIView *)objParentView ChildView:(UIView *)objChildView;

@end
