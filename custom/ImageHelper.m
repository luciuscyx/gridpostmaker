

#import "ImageHelper.h"
#import <QuartzCore/QuartzCore.h>


#pragma mark Bitmap Offsets
// ARGB Offset Helpers
NSUInteger alphaOffset(NSUInteger x, NSUInteger y, NSUInteger w){return y * w * 4 + x * 4 + 0;}
NSUInteger redOffset(NSUInteger x, NSUInteger y, NSUInteger w){return y * w * 4 + x * 4 + 1;}
NSUInteger greenOffset(NSUInteger x, NSUInteger y, NSUInteger w){return y * w * 4 + x * 4 + 2;}
NSUInteger blueOffset(NSUInteger x, NSUInteger y, NSUInteger w){return y * w * 4 + x * 4 + 3;}


#pragma mark Functions that build contexts

// The data must be freed from this and the bitmap released
// Use free(CGBitmapContextGetData(context)); and CGContextRelease(context);
CGContextRef CreateARGBBitmapContext (CGSize size)
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
        return NULL;
    }
	
    void *bitmapData = malloc(size.width * size.height * 4);
    if (bitmapData == NULL)
    {
        fprintf (stderr, "Error: Memory not allocated!");
        CGColorSpaceRelease(colorSpace);
        return NULL;
    }
	
    CGContextRef context = CGBitmapContextCreate (bitmapData, size.width, size.height, 8, size.width * 4, colorSpace, kCGImageAlphaPremultipliedFirst);
    CGColorSpaceRelease(colorSpace );
    if (context == NULL)
    {
        fprintf (stderr, "Error: Context not created!");
        free (bitmapData);
		return NULL;
    }
	
    return context;
}
CGContextRef CreateRGBABitmapContext (CGSize size)
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
        return NULL;
    }
	
    void *bitmapData = malloc(size.width * size.height * 4);
    if (bitmapData == NULL)
    {
        fprintf (stderr, "Error: Memory not allocated!");
        CGColorSpaceRelease(colorSpace);
        return NULL;
    }
	
    CGContextRef context = CGBitmapContextCreate (bitmapData, size.width, size.height, 8, size.width * 4, colorSpace, kCGImageAlphaPremultipliedLast);
    CGColorSpaceRelease(colorSpace );
    if (context == NULL)
    {
        fprintf (stderr, "Error: Context not created!");
        free (bitmapData);
		return NULL;
    }
	
    return context;
}
#pragma mark Context Utilities

// Fix the context when using image contexts because there are times you must live in quartzland
void FlipContextVertically(CGContextRef context, CGSize size)
{
	CGAffineTransform transform = CGAffineTransformIdentity;
	transform = CGAffineTransformScale(transform, 1.0f, -1.0f);
	transform = CGAffineTransformTranslate(transform, 0.0f, -size.height);
	CGContextConcatCTM(context, transform);
}

// Add rounded rectangle to context
void addRoundedRectToContext(CGContextRef context, CGRect rect, CGSize ovalSize)
{
	if (ovalSize.width == 0.0f || ovalSize.height == 0.0f) 
	{
		CGContextSaveGState(context);
		CGContextTranslateCTM(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
		CGContextAddRect(context, rect);
		CGContextClosePath(context);
		CGContextRestoreGState(context);
		return;
	}
	
	CGContextSaveGState(context);
	CGContextTranslateCTM(context, CGRectGetMinX(rect), CGRectGetMinY(rect));
	CGContextScaleCTM(context, ovalSize.width, ovalSize.height);
	float fw = CGRectGetWidth(rect) / ovalSize.width;
	float fh = CGRectGetHeight(rect) / ovalSize.height;
	
	CGContextMoveToPoint(context, fw, fh/2); 
	CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
	CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
	CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1); 
	CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
	
	CGContextClosePath(context);
	CGContextRestoreGState(context);
}

@implementation ImageHelper

#pragma mark Create Image

// Screen shot the view
+ (UIImage *) imageFromView: (UIView *) theView
{
	UIGraphicsBeginImageContext(theView.frame.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	[theView.layer renderInContext:context];
	UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return theImage;
}

+ (UIImage *) imageFromViewDeep: (UIView *) theView
{
    UIGraphicsBeginImageContext(theView.frame.size);
    //CGContextRef context = UIGraphicsGetCurrentContext();
    //[theView.layer renderInContext:context];
    [theView drawViewHierarchyInRect:theView.bounds afterScreenUpdates:YES];
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

+ (unsigned char *) bitmapFromImageForGL: (UIImage *) image
{
	CGContextRef context = CreateRGBABitmapContext(image.size);
    if (context == NULL) return NULL;
	
    CGRect rect = CGRectMake(0.0f, 0.0f, image.size.width, image.size.height);
    CGContextDrawImage(context, rect, image.CGImage);
	unsigned char *data = CGBitmapContextGetData (context);
	CGContextRelease(context);
	return data;
}
// Fill image with bits
+ (UIImage *) imageWithBitsForGL: (unsigned char *) bits withSize: (CGSize) size
{
	// Create a color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
		free(bits);
        return nil;
    }
	
    CGContextRef context = CGBitmapContextCreate (bits, size.width, size.height, 8, size.width * 4, colorSpace, kCGImageAlphaPremultipliedLast);
    if (context == NULL)
    {
        fprintf (stderr, "Error: Context not created!");
        free (bits);
		CGColorSpaceRelease(colorSpace );
		return nil;
    }
	
    CGColorSpaceRelease(colorSpace );
	CGImageRef ref = CGBitmapContextCreateImage(context);
	free(CGBitmapContextGetData(context));
	CGContextRelease(context);
	
	UIImage *img = [UIImage imageWithCGImage:ref scale:1 orientation:UIImageOrientationDownMirrored];//[UIImage imageWithCGImage:ref];
	CFRelease(ref);
	return img;
}
// Fill image with bits
+ (UIImage *) imageWithBits: (unsigned char *) bits withSize: (CGSize) size
{
	// Create a color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
		free(bits);
        return nil;
    }
	
    CGContextRef context = CGBitmapContextCreate (bits, size.width, size.height, 8, size.width * 4, colorSpace, kCGImageAlphaPremultipliedFirst);
    if (context == NULL)
    {
        fprintf (stderr, "Error: Context not created!");
        free (bits);
		CGColorSpaceRelease(colorSpace );
		return nil;
    }
	
    CGColorSpaceRelease(colorSpace );
	CGImageRef ref = CGBitmapContextCreateImage(context);
	free(CGBitmapContextGetData(context));
	CGContextRelease(context);
	
	UIImage *img = [UIImage imageWithCGImage:ref];
	CFRelease(ref);
	return img;
}



+ (UIImage *) imageWithBits: (unsigned char *) bits withSize: (CGSize) size isMirror:(BOOL)isMirror
{
	// Create a color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
		free(bits);
        return nil;
    }
	
    CGContextRef context = CGBitmapContextCreate (bits, size.width, size.height, 8, size.width * 4, colorSpace, kCGImageAlphaPremultipliedFirst);
    if (context == NULL)
    {
        fprintf (stderr, "Error: Context not created!");
        free (bits);
		CGColorSpaceRelease(colorSpace );
		return nil;
    }
	
    CGColorSpaceRelease(colorSpace );
	CGImageRef ref = CGBitmapContextCreateImage(context);
	free(CGBitmapContextGetData(context));
	CGContextRelease(context);
	
	UIImage *img = isMirror ? [UIImage imageWithCGImage:ref scale:1 orientation:UIImageOrientationLeftMirrored]:[UIImage imageWithCGImage:ref];
	CFRelease(ref);
	return img;
}
+ (UIImage *) image: (UIImage *) image scaleToFitInSize: (CGSize) viewsize
{
	UIGraphicsBeginImageContext(viewsize);
	[image drawInRect:CGRectMake(0, 0, viewsize.width, viewsize.height)];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
}
#pragma mark Contexts and Bitmaps
+ (unsigned char *) bitmapFromImage: (UIImage *) image
{
	CGContextRef context = CreateARGBBitmapContext(image.size);
    if (context == NULL) return NULL;
	
    CGRect rect = CGRectMake(0.0f, 0.0f, image.size.width, image.size.height);
    CGContextDrawImage(context, rect, image.CGImage);
	unsigned char *data = CGBitmapContextGetData (context);
	CGContextRelease(context);
	return data;
}
+ (unsigned char *) bitmapFromImage: (UIImage *) image size:(CGSize)size
{
	CGContextRef context = CreateARGBBitmapContext(size);
    if (context == NULL) return NULL;
	
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    CGContextDrawImage(context, rect, image.CGImage);
	unsigned char *data = CGBitmapContextGetData (context);
	CGContextRelease(context);
	return data;
}

#pragma mark Base Image Utility
+ (CGSize) fitSize: (CGSize)thisSize inSize: (CGSize) aSize
{
	CGFloat scale;
	CGSize newsize = thisSize;
	
	if (newsize.height && (newsize.height > aSize.height))
	{
		scale = aSize.height / newsize.height;
		newsize.width *= scale;
		newsize.height *= scale;
	}
	
	if (newsize.width && (newsize.width >= aSize.width))
	{
		scale = aSize.width / newsize.width;
		newsize.width *= scale;
		newsize.height *= scale;
	}
	
	return newsize;
}

+ (CGRect) frameSize: (CGSize)thisSize inSize: (CGSize) aSize
{
	CGSize size = [self fitSize:thisSize inSize: aSize];
	float dWidth = aSize.width - size.width;
	float dHeight = aSize.height - size.height;
	
	return CGRectMake(dWidth / 2.0f, dHeight / 2.0f, size.width, size.height);
}

+(UIImage*)getSplitImageFromMask:(UIImage*)maskImage rowCount:(int)rowCount indexx:(int)indexx indexy:(int)indexy
{
    CGSize size = maskImage.size;
    CGSize subSize = CGSizeMake(size.width/3, size.height/rowCount);
    CGPoint startPoint = CGPointMake(-subSize.width*indexx, -subSize.height*indexy);
    UIGraphicsBeginImageContext(subSize);
    [maskImage drawAtPoint:startPoint];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
}
//横向固定，纵向变化
+(UIImage*)gerGridHomeImage:(int**)features rowCount:(int)rowCount splitLen:(CGFloat)splitLen gridLen:(CGFloat)gridLen heartImg:(UIImage*)heartImg
{
    CGSize imgSize = CGSizeMake(gridLen*rowCount+splitLen*(rowCount+1), gridLen+splitLen*2);
    CGFloat addx = 0;
    CGFloat addy = 0;
    if (imgSize.width > imgSize.height) {
        addy = (imgSize.width-imgSize.height)/2;
        imgSize.height = imgSize.width;
    }else
    {
        addx = (imgSize.height-imgSize.width)/2;
        imgSize.width = imgSize.height;
    }
    UIGraphicsBeginImageContext(imgSize);
    CGContextRef context = UIGraphicsGetCurrentContext();
    for (int i = 0; i < rowCount; i++) {
        for (int j = 0; j < 1; j++) {
            CGContextAddRect(context, CGRectMake(addx+splitLen*(i+1)+gridLen*i, addy+splitLen*(j+1)+gridLen*j, gridLen, gridLen));
            
            if (features[j][i] == 0) {
                [mainColor set];
            }else
            {
                [RGB(207,210,213) set];
            }
            CGContextFillPath(context);
        }
    }
    if (heartImg) {
        [heartImg drawInRect:CGRectMake(0, 0, imgSize.width, imgSize.height)];
    }
    
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
}
//纵向固定，横向变化
+(UIImage*)gerGridRSKImage:(int**)features rowCount:(int)rowCount splitLen:(CGFloat)splitLen gridLen:(CGFloat)gridLen heartImg:(UIImage*)heartImg
{
    CGSize imgSize = CGSizeMake(gridLen*3+splitLen*4, gridLen*rowCount+splitLen*(rowCount+1));
    CGFloat addx = 0;
    CGFloat addy = 0;
    if (imgSize.width > imgSize.height) {
        addy = (imgSize.width-imgSize.height)/2;
        imgSize.height = imgSize.width;
    }else
    {
        addx = (imgSize.height-imgSize.width)/2;
        imgSize.width = imgSize.height;
    }
    UIGraphicsBeginImageContext(imgSize);
    CGContextRef context = UIGraphicsGetCurrentContext();
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < rowCount; j++) {
            CGContextAddRect(context, CGRectMake(addx+splitLen*(i+1)+gridLen*i, addy+splitLen*(j+1)+gridLen*j, gridLen, gridLen));
            
            if (features[j][i] == 0) {
                [mainColor set];
            }else
            {
                [RGB(207,210,213) set];
            }
            CGContextFillPath(context);
        }
    }
    if (heartImg) {
        [heartImg drawInRect:CGRectMake(0, 0, imgSize.width, imgSize.height)];
    }
    
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
}
#define MIRRORED ((image.imageOrientation == UIImageOrientationUpMirrored) || (image.imageOrientation == UIImageOrientationLeftMirrored) || (image.imageOrientation == UIImageOrientationRightMirrored) || (image.imageOrientation == UIImageOrientationDownMirrored))	
#define ROTATED90	((image.imageOrientation == UIImageOrientationLeft) || (image.imageOrientation == UIImageOrientationLeftMirrored) || (image.imageOrientation == UIImageOrientationRight) || (image.imageOrientation == UIImageOrientationRightMirrored))

+ (UIImage *) doUnrotateImage: (UIImage *) image
{
	CGSize size = image.size;
	if (ROTATED90) size = CGSizeMake(image.size.height, image.size.width);
	
	UIGraphicsBeginImageContext(size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGAffineTransform transform = CGAffineTransformIdentity;

	// Rotate as needed
	switch(image.imageOrientation)
	{  
        case UIImageOrientationLeft:
		case UIImageOrientationRightMirrored:
			transform = CGAffineTransformRotate(transform, M_PI / 2.0f);
			transform = CGAffineTransformTranslate(transform, 0.0f, -size.width);
			size = CGSizeMake(size.height, size.width);
			CGContextConcatCTM(context, transform);
            break;
        case UIImageOrientationRight: 
		case UIImageOrientationLeftMirrored:
			transform = CGAffineTransformRotate(transform, -M_PI / 2.0f);
			transform = CGAffineTransformTranslate(transform, -size.height, 0.0f);
			size = CGSizeMake(size.height, size.width);
			CGContextConcatCTM(context, transform);
            break;
		case UIImageOrientationDown:
		case UIImageOrientationDownMirrored:
			transform = CGAffineTransformRotate(transform, M_PI);
			transform = CGAffineTransformTranslate(transform, -size.width, -size.height);
			CGContextConcatCTM(context, transform);
			break;
        default:  
			break;
    }
	

	if (MIRRORED)
	{
		// de-mirror
		transform = CGAffineTransformMakeTranslation(size.width, 0.0f);
		transform = CGAffineTransformScale(transform, -1.0f, 1.0f);
		CGContextConcatCTM(context, transform);
	}
	    
	// Draw the image into the transformed context and return the image
	[image drawAtPoint:CGPointMake(0.0f, 0.0f)];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();  
    return newimg;  
}	

+ (UIImage *) unrotateImage: (UIImage *) image
{
	if (image.imageOrientation == UIImageOrientationUp) return image;
	return [ImageHelper doUnrotateImage:image];
}



// Proportionately resize, completely fit in view, no cropping
+ (UIImage *) image: (UIImage *) image fitInSize: (CGSize) viewsize
{
	UIGraphicsBeginImageContext(viewsize);
	[image drawInRect:[ImageHelper frameSize:image.size inSize:viewsize]];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();  
    return newimg;  
}

// Proportionately resize, completely fit in view, no cropping
+ (UIImage *) image: (UIImage *) image fitInAllSize: (CGSize) viewsize
{
	UIGraphicsBeginImageContext(viewsize);
	[image drawInRect:CGRectMake(0, 0, viewsize.width, viewsize.height)];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
}

+ (UIImage *) image: (UIImage *) image fitInView: (UIView *) view
{
	return [self image:image fitInSize:view.frame.size];
}

// No resize, may crop
+ (UIImage *) image: (UIImage *) image centerInSize: (CGSize) viewsize
{
	CGSize size = image.size;
	
	UIGraphicsBeginImageContext(viewsize);
	float dwidth = (viewsize.width - size.width) / 2.0f;
	float dheight = (viewsize.height - size.height) / 2.0f;
	
	CGRect rect = CGRectMake(dwidth, dheight, size.width, size.height);
	[image drawInRect:rect];
	
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();  
	
    return newimg;  
}
+ (CGRect)fillSize:(CGSize)viewsize imageSize:(CGSize)imageSize
{
	CGSize size = imageSize;
	
	CGFloat scalex = viewsize.width / size.width;
	CGFloat scaley = viewsize.height / size.height;
	CGFloat scale = MAX(scalex, scaley);
	
	CGFloat width = size.width * scale;
	CGFloat height = size.height * scale;
	
	float dwidth = ((viewsize.width - width) / 2.0f);
	float dheight = ((viewsize.height - height) / 2.0f);
	
    return  CGRectMake(dwidth, dheight, size.width * scale, size.height * scale);
}

+ (CGRect)fillAllSize:(CGSize)viewsize imageSize:(CGSize)imageSize
{
	CGSize size = imageSize;
	
	CGFloat scalex = viewsize.width / size.width;
	CGFloat scaley = viewsize.height / size.height;
	CGFloat scale = MIN(scalex, scaley);
    
	CGFloat width = size.width * scale;
	CGFloat height = size.height * scale;
	
	float dwidth = ((viewsize.width - width) / 2.0f);
	float dheight = ((viewsize.height - height) / 2.0f);
    
    return CGRectMake(dwidth, dheight, size.width * scale, size.height * scale);
}
+ (UIImage *) image: (UIImage *) image drawInRect: (CGRect) r
{
    CGSize size = r.size;
	//uint64_t start = mach_absolute_time();
	UIGraphicsBeginImageContext(size);

	
	//[image drawInRect:CGRectMake(-r.origin.x/2, -r.origin.y/2, size.width, size.height)];
    [image drawAtPoint:CGPointMake(-r.origin.x, -r.origin.y)];
	
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return newimg;
}
+ (UIImage *) image: (UIImage *) image drawInRect: (CGRect) r drawSize:(CGSize)drawSize
{	
	UIGraphicsBeginImageContext(drawSize);

	//[image drawInRect:CGRectMake(-r.origin.x/2, -r.origin.y/2, size.width, size.height)];
    //[image drawAtPoint:CGPointMake(-r.origin.x, -r.origin.y)];
	[image drawInRect:r];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    return newimg;
}
+ (UIImage *)imageDrawInRect: (UIImage *) image
{
    CGPoint drawPoint;
    CGSize imgSize;
    if (image.size.width > image.size.height) {
        imgSize = CGSizeMake(image.size.width, image.size.width);
        drawPoint = CGPointMake(0, (image.size.width - image.size.height)/2);
    }else
    {
        imgSize = CGSizeMake(image.size.height, image.size.height);
        drawPoint = CGPointMake((image.size.height - image.size.width)/2,0);
    }
	UIGraphicsBeginImageContext(imgSize);
    
	//[image drawInRect:CGRectMake(-r.origin.x/2, -r.origin.y/2, size.width, size.height)];
    [image drawAtPoint:drawPoint];

    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    return newimg;
}
+ (UIImage *) image: (UIImage *) image centerInView: (UIView *) view
{
	return [self image:image centerInSize:view.frame.size];
}
+ (UIImage *) combineImage: (UIImage *) image1 image2:(UIImage *) image2 fillSize: (CGSize) viewsize
{
    CGSize size1 = CGSizeMake(viewsize.width, viewsize.width*image1.size.height/image1.size.width);
    CGSize size2 = CGSizeMake(viewsize.width, viewsize.width*image2.size.height/image2.size.width);
    CGSize realSize = CGSizeMake(viewsize.width, size1.height+size2.height);
    CGRect rect1 ;
    CGRect rect2 ;
    if (realSize.width/realSize.height > viewsize.width/viewsize.height) {
        rect1 = CGRectMake(0, (viewsize.height-realSize.height)/2, viewsize.width, size1.height);
        rect2 = CGRectMake(0, size1.height+rect1.origin.y, viewsize.width, size2.height);
    }else
    {
        CGFloat w = viewsize.height*realSize.width/realSize.height;
        rect1 = CGRectMake((viewsize.width-w)/2,0, w, size1.height*w/size1.width);
        rect2 = CGRectMake(rect1.origin.x, rect1.size.height, w, size2.height*w/size2.width);
    }
    UIGraphicsBeginImageContext(viewsize);
    [image1 drawInRect:rect1];
    [image2 drawInRect:rect2];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newimg;
}
+ (UIImage *) sampleCombineImage: (UIImage *) image1 image2:(UIImage *) image2 fillSize: (CGSize) viewsize
{
    CGSize size1 = CGSizeMake(viewsize.width, viewsize.height / 2 );
    CGSize size2 = CGSizeMake(viewsize.width, viewsize.height / 2);
    CGSize realSize = CGSizeMake(viewsize.width, size1.height+size2.height);
    CGRect rect1 ;
    CGRect rect2 ;
    if (realSize.width/realSize.height > viewsize.width/viewsize.height) {
        rect1 = CGRectMake(0, (viewsize.height-realSize.height)/2, viewsize.width, size1.height);
        rect2 = CGRectMake(0, size1.height+rect1.origin.y, viewsize.width, size2.height);
    }else
    {
        CGFloat w = viewsize.height*realSize.width/realSize.height;
        rect1 = CGRectMake((viewsize.width-w)/2,0, w, size1.height*w/size1.width);
        rect2 = CGRectMake(rect1.origin.x, rect1.size.height, w, size2.height*w/size2.width);
    }
    UIGraphicsBeginImageContext(viewsize);
    [image1 drawInRect:rect1];
    [image2 drawInRect:rect2];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newimg;
}

// Fill every view pixel with no black borders, resize and crop if needed
+ (UIImage *) image: (UIImage *) image fillSize: (CGSize) viewsize

{
	CGSize size = image.size;
	
	CGFloat scalex = viewsize.width / size.width;
	CGFloat scaley = viewsize.height / size.height;
	CGFloat scale = MAX(scalex, scaley);
	
	UIGraphicsBeginImageContext(viewsize);
	
	CGFloat width = size.width * scale;
	CGFloat height = size.height * scale;
	
	float dwidth = ((viewsize.width - width) / 2.0f);
	float dheight = ((viewsize.height - height) / 2.0f);
    
	CGRect rect = CGRectMake(dwidth, dheight, size.width * scale, size.height * scale);
	[image drawInRect:rect];
	
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    return newimg;
}
// Fill every view pixel with no black borders, resize and crop if needed
+ (UIImage *) image: (UIImage *) image fillAllSize: (CGSize) viewsize

{
	CGSize size = image.size;
	
	CGFloat scalex = viewsize.width / size.width;
	CGFloat scaley = viewsize.height / size.height;
	CGFloat scale = MIN(scalex, scaley);
	
	UIGraphicsBeginImageContext(viewsize);
    CGContextRef context = UIGraphicsGetCurrentContext();
//	CGContextSetRGBFillColor(context, 1, 1, 1, 1.0);
//    
//	CGContextAddRect(context,CGRectMake(0, 0, viewsize.width, viewsize.height));
//	CGContextDrawPath(context, kCGPathFill);

	CGFloat width = size.width * scale;
	CGFloat height = size.height * scale;
	
	float dwidth = ((viewsize.width - width) / 2.0f);
	float dheight = ((viewsize.height - height) / 2.0f);
    float addw = 0;
    float addh = 0;
    if (dwidth < 3) {
        addw = dwidth*2;
        dwidth = 0;
    }
    if (dheight < 3) {
        addh = dheight*2;
        dheight = 0;
    }
	CGRect rect = CGRectMake(dwidth, dheight, size.width * scale+addw, size.height * scale+addh);
	[image drawInRect:rect];
	
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    return newimg;
}
// Fill every view pixel with no black borders, resize and crop if needed
+ (UIImage *) image: (UIImage *) image fillAllSizeNoBG: (CGSize) viewsize

{
    CGSize size = image.size;
    
    CGFloat scalex = viewsize.width / size.width;
    CGFloat scaley = viewsize.height / size.height;
    CGFloat scale = MIN(scalex, scaley);
    
    UIGraphicsBeginImageContext(viewsize);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat width = size.width * scale;
    CGFloat height = size.height * scale;
    
    float dwidth = ((viewsize.width - width) / 2.0f);
    float dheight = ((viewsize.height - height) / 2.0f);
    float addw = 0;
    float addh = 0;
    if (dwidth < 3) {
        addw = dwidth*2;
        dwidth = 0;
    }
    if (dheight < 3) {
        addh = dheight*2;
        dheight = 0;
    }
    CGRect rect = CGRectMake(dwidth, dheight, size.width * scale+addw, size.height * scale+addh);
    [image drawInRect:rect];
    
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newimg;
}

+ (UIImage *) image: (UIImage *) image fillInSize: (CGSize) viewsize

{
	CGSize size = image.size;
	
	CGFloat scalex = viewsize.width / size.width;
	CGFloat scaley = viewsize.height / size.height; 
	CGFloat scale = MAX(scalex, scaley);
	
	UIGraphicsBeginImageContext(viewsize);
	
	CGFloat width = size.width * scale;
	CGFloat height = size.height * scale;
	
	float dwidth = ((viewsize.width - width) / 2.0f);
	float dheight = ((viewsize.height - height) / 2.0f);
		
	CGRect rect = CGRectMake(dwidth, dheight, viewsize.width, viewsize.height);
	[image drawInRect:rect];
	
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();  
	
    return newimg;  
}

+ (UIImage *) image: (UIImage *) image fillView: (UIView *) view
{
	return [self image:image fillSize:view.frame.size];
}

#pragma mark Paths

// Convenience function for rounded rect corners that hides built-in function
+ (void) addRoundedRect:(CGRect) rect toContext:(CGContextRef) context withOvalSize:(CGSize) ovalSize
{
	addRoundedRectToContext(context, rect, ovalSize);
}

+ (UIImage *) roundedImage: (UIImage *) image withOvalSize: (CGSize) ovalSize withInset: (CGFloat) inset
{
	
	UIGraphicsBeginImageContext(image.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGRect rect = CGRectMake(inset, inset, image.size.width - inset * 2.0f, image.size.height - inset * 2.0f);
	addRoundedRectToContext(context, rect, ovalSize);
	CGContextClip(context);

	[image drawInRect:rect];
	
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();  
    return newimg;  
}
+ (UIImage *) flipImageHorizon:(UIImage*)image
{
    UIImageView * imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    imageview.image = image;
    imageview.backgroundColor = [UIColor clearColor];
    
    UIView * backview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    [backview addSubview:imageview];
    imageview.transform = CGAffineTransformMakeScale(-1, 1);
    return [ImageHelper imageFromView:backview];
}
+ (UIImage *) flipImageVertical:(UIImage*)image
{
    UIImageView * imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    imageview.image = image;
    imageview.backgroundColor = [UIColor clearColor];
    
    UIView * backview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    [backview addSubview:imageview];
    imageview.transform = CGAffineTransformMakeScale(1, -1);
    return [ImageHelper imageFromView:backview];
}
+ (UIImage *) roundedImage: (UIImage *) image withLock:(BOOL)withLock
{
    CGFloat slen = image.size.width > image.size.height ? image.size.height : image.size.width;
    UIGraphicsBeginImageContext(CGSizeMake(slen, slen));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect rect = CGRectMake(0,0, slen, slen);
//    addRoundedRectToContext(context, rect, CGSizeMake(slen/2, slen/2));
//    CGContextClip(context);
    
    [image drawAtPoint:CGPointMake(-(image.size.width-slen)/2, -(image.size.height-slen)/2)];
    if (withLock) {
        [[UIImage imageNamed:@"lock.png"] drawInRect:rect];
    }
    
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;  
}

+ (UIImage *) squareImage: (UIImage *) image
{
    CGFloat slen = image.size.width > image.size.height ? image.size.width : image.size.height;
    UIGraphicsBeginImageContext(CGSizeMake(slen, slen));
    [image drawAtPoint:CGPointMake(-(image.size.width-slen)/2, -(image.size.height-slen)/2)];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
}

+ (UIImage *) roundedImage: (UIImage *) image withOvalSize: (CGSize) ovalSize
{
	return [ImageHelper roundedImage:image withOvalSize:ovalSize withInset: 0.0f];
}

+ (UIImage *) roundedBacksplashOfSize: (CGSize)size andColor:(UIColor *) color withRounding: (CGFloat) rounding andInset: (CGFloat) inset
{

	UIGraphicsBeginImageContext(size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGRect rect = CGRectMake(inset, inset, size.width - 2.0f * inset, size.height - 2.0f * inset);
	addRoundedRectToContext(context, rect, CGSizeMake(rounding, rounding));
	CGContextSetFillColorWithColor(context, [color CGColor]);
	CGContextFillPath(context);
	CGContextClip(context);	
	UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newimg;
}

+ (UIImage *) ellipseImage: (UIImage *) image withInset: (CGFloat) inset
{
	UIGraphicsBeginImageContext(image.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGRect rect = CGRectMake(inset, inset, image.size.width - inset * 2.0f, image.size.height - inset * 2.0f);
	CGContextAddEllipseInRect(context, rect);
	CGContextClip(context);
	
	[image drawInRect:rect];
	
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();  
    return newimg;  
}
//+(UIImage*)createNewHairColor:(UIColor*)color rect:(CGRect)rect alpha:(float)alpha  bgImg:(UIImage*)bgImg bgSize:(CGSize)bgSize showSize:(CGSize)showSize totalV:(int)totalV angle:(float)angle roundsize:(CGSize)roundsize
//{
//
//    int theheight = (int) bgImg.size.height;
//    int thewidth = (int) bgImg.size.width;
//
//    // Get input and output bits
//    unsigned char *inbits = [self bitmapFromImage:bgImg];
//    unsigned char *outbits = (unsigned char *)malloc((thewidth) * (theheight) * 4);
//
//
//    int R,G,B;
//
//    CGColorRef colorr = [color CGColor];
//    int numComponents = CGColorGetNumberOfComponents(colorr);
//    R = 0;
//    G = 0;
//    B = 0;
//    if (numComponents == 4)
//    {
//        const CGFloat *components = CGColorGetComponents(colorr);
//        R = components[0]*255;
//        G = components[1]*255;
//        B = components[2]*255;
//    }
//
//    float rate = 1;
//    int tR,tG,tB,R1,G1,B1;
//    for (int y = 0; y < (theheight); y++)
//        for (int x = 0; x < (thewidth); x++)
//        {
//            if (inbits[alphaOffset(x, y, thewidth)] > 0)
//            {
//
//                tR = inbits[redOffset(x, y, thewidth)];
//                tG = inbits[greenOffset(x, y, thewidth)];
//                tB = inbits[blueOffset(x, y, thewidth)];
//
//                R1 = (R+(tR+tG+tB-totalV)/3)*0.5+0.5*tR;
//                G1 = (G+(tR+tG+tB-totalV)/3)*0.5+0.5*tG;
//                B1 = (B+(tR+tG+tB-totalV)/3)*0.5+0.5*tB;
//
//                G1= G1>255 ? 255 : G1;
//                B1= B1>255 ? 255 : B1;
//                R1= R1>255 ? 255 : R1;
//                R1 = R1>0 ? R1 : 0;
//                G1 = G1>0 ? G1 : 0;
//                B1 = B1>0 ? B1 : 0;
//
//
//
//                rate =inbits[alphaOffset(x, y, thewidth)]/255.0;
//
//                outbits[redOffset(x, y, thewidth)] = R1*rate;//*alrate*alpha+(1-alrate*alpha)*tR;
//                outbits[greenOffset(x, y, thewidth)] = G1*rate;//*alrate*alpha+(1-alrate*alpha)*tG;
//                outbits[blueOffset(x, y, thewidth)] = B1*rate;//*alrate*alpha+(1-alrate*alpha)*tB;
//                outbits[alphaOffset(x, y, thewidth)] = 255*rate;// inbits[alphaOffset(x, y, thewidth)];//(unsigned char) 0xff*(alpha>1 ? 1 :alpha);
//            }else {
//                outbits[redOffset(x, y, thewidth)] = 0;
//                outbits[greenOffset(x, y, thewidth)] = 0;
//                outbits[blueOffset(x, y, thewidth)] = 0 ;
//                outbits[alphaOffset(x, y, thewidth)] =  0;
//                // outbits[alphaOffset(x, y, thewidth)] =  0;;
//            }
//
//        }
//
//    free(inbits);
//    return [ImageHelper imageWithBits:outbits withSize:CGSizeMake(thewidth, theheight)];
//}
+(UIImage*)createNewHairColor:(UIColor*)color rect:(CGRect)rect alpha:(float)alpha  bgImg:(UIImage*)bgImg bgSize:(CGSize)bgSize showSize:(CGSize)showSize totalV:(int)totalV angle:(float)angle roundsize:(CGSize)roundsize
{
    
    int theheight = (int) bgImg.size.height;
    int thewidth = (int) bgImg.size.width;
    
    // Get input and output bits
    unsigned char *inbits = [self bitmapFromImage:bgImg];
    unsigned char *outbits = (unsigned char *)malloc((thewidth) * (theheight) * 4);
    
    
    int R,G,B;
    
    CGColorRef colorr = [color CGColor];
    int numComponents = CGColorGetNumberOfComponents(colorr);
    R = 0;
    G = 0;
    B = 0;
    if (numComponents == 4)
    {
        const CGFloat *components = CGColorGetComponents(colorr);
        R = components[0]*255;
        G = components[1]*255;
        B = components[2]*255;
    }
    
    float rate = 1;
    int tR,tG,tB,R1,G1,B1;
    for (int y = 0; y < (theheight); y++)
        for (int x = 0; x < (thewidth); x++)
        {
            if (inbits[alphaOffset(x, y, thewidth)] > 0)
            {
                
                //                tR = inbits[redOffset(x, y, thewidth)];
                //                tG = inbits[greenOffset(x, y, thewidth)];
                //                tB = inbits[blueOffset(x, y, thewidth)];
                //
                //                R1 = (R+(tR+tG+tB-totalV)/3)*0.5+0.5*tR;
                //                G1 = (G+(tR+tG+tB-totalV)/3)*0.5+0.5*tG;
                //                B1 = (B+(tR+tG+tB-totalV)/3)*0.5+0.5*tB;
                //
                //                G1= G1>255 ? 255 : G1;
                //                B1= B1>255 ? 255 : B1;
                //                R1= R1>255 ? 255 : R1;
                //                R1 = R1>0 ? R1 : 0;
                //                G1 = G1>0 ? G1 : 0;
                //                B1 = B1>0 ? B1 : 0;
                
                
                
                rate =inbits[alphaOffset(x, y, thewidth)]/255.0;
                
                outbits[redOffset(x, y, thewidth)] = R*rate;//*alrate*alpha+(1-alrate*alpha)*tR;
                outbits[greenOffset(x, y, thewidth)] = G*rate;//*alrate*alpha+(1-alrate*alpha)*tG;
                outbits[blueOffset(x, y, thewidth)] = B*rate;//*alrate*alpha+(1-alrate*alpha)*tB;
                outbits[alphaOffset(x, y, thewidth)] = 255*rate;// inbits[alphaOffset(x, y, thewidth)];//(unsigned char) 0xff*(alpha>1 ? 1 :alpha);
            }else {
                outbits[redOffset(x, y, thewidth)] = 0;
                outbits[greenOffset(x, y, thewidth)] = 0;
                outbits[blueOffset(x, y, thewidth)] = 0 ;
                outbits[alphaOffset(x, y, thewidth)] =  0;
                // outbits[alphaOffset(x, y, thewidth)] =  0;;
            }
            
        }
    
    free(inbits);
    return [ImageHelper imageWithBits:outbits withSize:CGSizeMake(thewidth, theheight)];
}
+(UIImage*)createHairRainBowColor:(UIImage*)cimg rect:(CGRect)rect  scaleX:(float)scalex scaleY:(float)scaley  bgImg:(UIImage*)bgImg totalV:(int)totalV
{
    
    
    int thewidth = bgImg.size.width;
    int theheight = (int) bgImg.size.height;
    unsigned char *inbits = (unsigned char *)[ImageHelper bitmapFromImage:cimg];
    unsigned char *inbitss = [self bitmapFromImage:[self drawimageatpt:bgImg rect:rect]];
    unsigned char *outbits = (unsigned char *)malloc(rect.size.width * rect.size.height * 4);
    
    
    int * rc = malloc(3*sizeof(int));
    float rate = 1;
    int tR,tG,tB,R1,G1,B1;
    for (int y = 0; y < (theheight); y++)
        for (int x = 0; x < (thewidth); x++)
        {
            if (inbitss[alphaOffset(x, y, thewidth)] > 0)
            {
                
                tR = inbitss[redOffset(x, y, thewidth)];
                tG = inbitss[greenOffset(x, y, thewidth)];
                tB = inbitss[blueOffset(x, y, thewidth)];
                
                
                rc[0]=inbits[redOffset((int)(x/scalex), (int)(y/scaley), (int) cimg.size.width)];
                rc[1]=inbits[greenOffset((int)(x/scalex), (int)(y/scaley),  (int) cimg.size.width)];
                rc[2]=inbits[blueOffset((int)(x/scalex), (int)(y/scaley),  (int) cimg.size.width)];
                
                
                R1 = (rc[0]+(tR+tG+tB-totalV)/3);
                G1 = (rc[1]+(tR+tG+tB-totalV)/3);
                B1 = (rc[2]+(tR+tG+tB-totalV)/3);
                
                G1= G1>255 ? 255 : G1;
                B1= B1>255 ? 255 : B1;
                R1= R1>255 ? 255 : R1;
                R1 = R1>0 ? R1 : 0;
                G1 = G1>0 ? G1 : 0;
                B1 = B1>0 ? B1 : 0;
                
                
                
                rate =inbitss[alphaOffset(x, y, thewidth)]/255.0;
                
                outbits[redOffset(x, y, thewidth)] = R1*rate;//*alrate*alpha+(1-alrate*alpha)*tR;
                outbits[greenOffset(x, y, thewidth)] = G1*rate;//*alrate*alpha+(1-alrate*alpha)*tG;
                outbits[blueOffset(x, y, thewidth)] = B1*rate;//*alrate*alpha+(1-alrate*alpha)*tB;
                outbits[alphaOffset(x, y, thewidth)] = 255*rate;// inbits[alphaOffset(x, y, thewidth)];//(unsigned char) 0xff*(alpha>1 ? 1 :alpha);
            }else {
                outbits[redOffset(x, y, thewidth)] = 0;
                outbits[greenOffset(x, y, thewidth)] = 0;
                outbits[blueOffset(x, y, thewidth)] = 0 ;
                outbits[alphaOffset(x, y, thewidth)] =  0;
                // outbits[alphaOffset(x, y, thewidth)] =  0;;
            }
            
        }
    free(rc);
    free(inbits);
    free(inbitss);
    return [ImageHelper imageWithBits:outbits withSize:rect.size];
}
+ (CGRect) getImageArea:(UIImage *)image
{
    int theheight = (int) image.size.height;
    int thewidth = (int) image.size.width;
    
    // Get input and output bits
    unsigned char *inbits = (unsigned char *)[ImageHelper bitmapFromImage:image];
    
    int maxx,maxy,minx,miny;
    maxx = 0;
    maxy = 0;
    minx = 999999999;
    miny = 999999999;
    for (int y = 0; y < (theheight); y++)
        for (int x = 0; x < (thewidth); x++)
        {
            if (inbits[alphaOffset(x, y, thewidth)] > 10) {
                if (x > maxx) {
                    maxx = x;
                }
                if (y > maxy) {
                    maxy = y;
                }
                if (x <= minx) {
                    minx = x;
                }
                if (y <= miny) {
                    miny = y;
                }
            }
        }
    
    free(inbits); // Release the original bitmap
    return CGRectMake(minx, miny, maxx-minx, maxy-miny);
}
+(UIImage*)drawimageatpt:(UIImage*)bgImg rect:(CGRect)rect
{
    UIGraphicsBeginImageContext(rect.size);
    
    [bgImg drawAtPoint:CGPointMake(-rect.origin.x, -rect.origin.y)];
    
    UIImage * ret = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return ret;
}
/*
@param image     原始的图片
@param logoImage 要添加的logo
@return 返回一张新的图片
*/
+(UIImage *)addImageLogo:(UIImage *)image image:(UIImage *)logoImage rect:(CGRect)rectReal
{
    CGFloat w = image.size.width;
    CGFloat h = image.size.height;
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0, 0, w, h)];
    [logoImage drawInRect:rectReal];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
}
@end


