//
//  InAppSharedClass.h
//
//
//  Created by Ajay on 10/09/14.
//  Copyright (c) 2014 Dotsquares. All rights reserved.
//

@protocol inAppDelegate <NSObject>
- (void)productPurchaseSuccess:(NSString *)bundleId strKey:(NSString *)strKey;
- (void)productPurchaseFailure:(NSString *)bundleId strKey:(NSString *)strKey;
@end

#import <Foundation/Foundation.h>
#import "IAPShare.h"
@interface InAppSharedClass : NSObject<UIActionSheetDelegate>
{

}
@property (nonatomic,retain)id <inAppDelegate> delegate;
@property (nonatomic,retain) NSString *strBundleId;
@property (nonatomic,retain) NSString *strProductKey;
+ (InAppSharedClass *)sharedInstance;
-(void)loadProduct:(UIViewController *)controller withDelegate:(id)InAppDelegate bundleId:(NSString *)bundleId strKey:(NSString *)strKey ;
@end
