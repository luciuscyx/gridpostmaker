//
//  InAppSharedClass.m
//  Emergency
//
//  Created by Ajay on 10/09/14.
//  Copyright (c) 2014 Dotsquares. All rights reserved.
//

#import "InAppSharedClass.h"
#import "SVProgressHUD.h"
@implementation InAppSharedClass
@synthesize delegate;
@synthesize strBundleId;
@synthesize strProductKey;

+ (InAppSharedClass *)sharedInstance
{
	static dispatch_once_t predicate = 0;
	static InAppSharedClass *object = nil; // Object
	dispatch_once(&predicate, ^{ object = [self new]; });
	return object;
}

-(void)loadProduct:(UIViewController *)controller withDelegate:(id)InAppDelegate bundleId:(NSString *)bundleId strKey:(NSString *)strKey
{    
    self.delegate=InAppDelegate;
    self.strBundleId=bundleId;
    self.strProductKey=strKey;

    UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Buy",@"Restore", nil];
    [actionSheet showInView:controller.view];
    
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex==actionSheet.cancelButtonIndex) {
        return;
    }
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    if (buttonIndex==0) {
        [self byProduct];
    }
    else if (buttonIndex==1){
        [self restoreProduct];
    }
}
- (void)byProduct{
   
    NSLog(@"bundle id %@",self.strBundleId);
    
    if(![IAPShare sharedHelper].iap) {
        NSSet* dataSet = [[NSSet alloc] initWithObjects:self.strBundleId, nil];
        
        [IAPShare sharedHelper].iap = [[IAPHelper alloc] initWithProductIdentifiers:dataSet];
    }else{
        NSSet* dataSet = [[NSSet alloc] initWithObjects:self.strBundleId, nil];
        [IAPShare sharedHelper].iap.productIdentifiers=dataSet;
    }
    [IAPShare sharedHelper].iap.production = YES;
    
    //clear product
    [[IAPShare sharedHelper].iap clearSavedPurchasedProducts];
    
    [[IAPShare sharedHelper].iap requestProductsWithCompletion:^(SKProductsRequest* request,SKProductsResponse* response)
     {
         
         NSLog(@"self.strBundleId %@ self.strProductKey%@",self.strBundleId,self.strProductKey);
        
         if (![response isKindOfClass:[SKProductsResponse class]]) {
             CIError(@"Error to load product");
             return;
         }
         
         if(response > 0 ) {
             SKProduct* product =[[IAPShare sharedHelper].iap.products objectAtIndex:0];
             
             [[IAPShare sharedHelper].iap buyProduct:product
                                        onCompletion:^(SKPaymentTransaction* trans){
                                            
                                            if(trans.error)
                                            {
                                                if (delegate) {
                                                    [delegate productPurchaseFailure:self.strBundleId strKey:self.strProductKey];
                                                }
                                                [SVProgressHUD dismiss];
                                                NSLog(@"Fail %@",[trans.error localizedDescription]);
                                            }
                                            else if(trans.transactionState == SKPaymentTransactionStatePurchased) {
                                                NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
                                                NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
                                                [[IAPShare sharedHelper].iap checkReceipt:receipt AndSharedSecret:@"d7572fd9cdfb40ce8df7821505715f24" onCompletion:^(NSString *response, NSError *error) {
                                                    
                                                    //Convert JSON String to NSDictionary
                                                    NSDictionary* rec = [IAPShare toJSON:response];
                                                    
                                                    if([rec[@"status"] integerValue]==0)
                                                    {
                                                        NSString *productIdentifier = trans.payment.productIdentifier;
                                                        [[IAPShare sharedHelper].iap provideContent:productIdentifier];
                                                        NSLog(@"SUCCESS %@",response);
                                                        NSLog(@"Pruchases %@",[IAPShare sharedHelper].iap.purchasedProducts);
                                                        if (delegate) {
                                                            [delegate productPurchaseSuccess:self.strBundleId strKey:self.strProductKey];
                                                        }
                                                    }
                                                    else {
                                                        
                                                        if (delegate) {
                                                            [delegate productPurchaseFailure:self.strBundleId strKey:self.strProductKey];
                                                        }
                                                        
                                                        NSLog(@"Fail");
                                                    }
                                                }];
                                            }
                                            else if(trans.transactionState == SKPaymentTransactionStateFailed) {
                                                NSLog(@"Fail");
                                                if (delegate) {
                                                    [delegate productPurchaseFailure:self.strBundleId strKey:self.strProductKey];
                                                }
                                                

                                            }
                                        }];//end of buy product
         }
     }];
}
-(void)restoreProduct
{
    /*
    [[IAPShare sharedHelper].iap restoreProductsWithCompletion:^(SKPaymentQueue *payment, NSError *error) {
        NSLog(@"Error %@",error);

        BOOL isRestoreIdenfitierGet=NO;
        
        for (SKPaymentTransaction *transaction in payment.transactions)
        {
            NSString *purchased = transaction.payment.productIdentifier;
            if([purchased isEqualToString:self.strBundleId])
            {
                //enable the prodcut here
                NSLog(@"Success");
                if (delegate) {
                    [delegate productPurchaseSuccess:self.strBundleId strKey:self.strProductKey];
                }
                isRestoreIdenfitierGet=YES;
            }
        }
        
        if (!isRestoreIdenfitierGet) {
            NSLog(@"Identifier Does Not get");
            if (delegate) {
                [delegate productPurchaseFailure:self.strBundleId strKey:self.strProductKey];
            }
        }
    }];
     */
    [self byProduct];
}
@end
