

#import <Foundation/Foundation.h>

#define CGAutorelease(x) (__typeof(x))[NSMakeCollectable(x) autorelease]
#define mixlengthrate 0.1
// ARGB Offset Helpers
NSUInteger alphaOffset(NSUInteger x, NSUInteger y, NSUInteger w);
NSUInteger redOffset(NSUInteger x, NSUInteger y, NSUInteger w);
NSUInteger greenOffset(NSUInteger x, NSUInteger y, NSUInteger w);
NSUInteger blueOffset(NSUInteger x, NSUInteger y, NSUInteger w);

// This version mallocs an actual bitmap, so make sure you understand the memory issues:
// Use free(CGBitmapContextGetData(context)); and CGContextRelease(context);
CGContextRef CreateARGBBitmapContext (CGSize size);
CGContextRef CreateRGBABitmapContext (CGSize size);

@interface ImageHelper : NSObject 
// Create image
+ (UIImage *) imageFromView: (UIView *) theView;
+ (UIImage *) image: (UIImage *) image drawInRect: (CGRect) r;
// Bits
+ (UIImage *) imageWithBits: (unsigned char *) bits withSize: (CGSize) size;
+ (UIImage *) imageWithBits: (unsigned char *) bits withSize: (CGSize) size isMirror:(BOOL)isMirror;
+ (unsigned char *) bitmapFromImage: (UIImage *) image;
+ (unsigned char *) bitmapFromImage: (UIImage *) image size:(CGSize)size;

// Base Image Fitting
+ (CGSize) fitSize: (CGSize)thisSize inSize: (CGSize) aSize;
+ (CGRect) frameSize: (CGSize)thisSize inSize: (CGSize) aSize;

+ (UIImage *) unrotateImage: (UIImage *) image;

+ (UIImage *) image: (UIImage *) image fitInSize: (CGSize) size; // retain proportions, fit in size
+ (UIImage *) image: (UIImage *) image fitInView: (UIView *) view; 

+ (UIImage *) image: (UIImage *) image centerInSize: (CGSize) size; // center, no resize
+ (UIImage *) image: (UIImage *) image centerInView: (UIView *) view; 

+ (UIImage *) image: (UIImage *) image fillSize: (CGSize) size; // fill all pixels
+ (UIImage *) image: (UIImage *) image fillView: (UIView *) view; 
+ (UIImage *) image: (UIImage *) image fillInSize: (CGSize) viewsize;
// Paths
+ (void) addRoundedRect:(CGRect) rect toContext:(CGContextRef) context withOvalSize:(CGSize) ovalSize;
+ (UIImage *) roundedImage: (UIImage *) image withOvalSize: (CGSize) ovalSize withInset: (CGFloat) inset;
+ (UIImage *) roundedImage: (UIImage *) img withOvalSize: (CGSize) ovalSize;
+ (UIImage *) roundedImage: (UIImage *) image withLock:(BOOL)withLock;
+ (UIImage *) roundedBacksplashOfSize: (CGSize)size andColor:(UIColor *) color withRounding: (CGFloat) rounding andInset: (CGFloat) inset;
+ (UIImage *) ellipseImage: (UIImage *) image withInset: (CGFloat) inset;
+ (UIImage *) imageWithBitsForGL: (unsigned char *) bits withSize: (CGSize) size;
+ (unsigned char *) bitmapFromImageForGL: (UIImage *) image;
+ (UIImage *) image: (UIImage *) image fillAllSize: (CGSize) viewsize;
+ (UIImage *) image: (UIImage *) image scaleToFitInSize: (CGSize) viewsize;
+ (UIImage *)imageDrawInRect: (UIImage *) image;
+ (CGRect)fillSize:(CGSize)viewsize imageSize:(CGSize)imageSize;
+ (CGRect)fillAllSize:(CGSize)viewsize imageSize:(CGSize)imageSize;
+ (UIImage *) image: (UIImage *) image fitInAllSize: (CGSize) viewsize;
+ (UIImage *) combineImage: (UIImage *) image1 image2:(UIImage *) image2 fillSize: (CGSize) viewsize;
+ (UIImage *) sampleCombineImage: (UIImage *) image1 image2:(UIImage *) image2 fillSize: (CGSize) viewsize;
+ (UIImage *) imageFromViewDeep: (UIView *) theView;
+ (UIImage *) squareImage: (UIImage *) image;
+ (UIImage *) flipImageHorizon:(UIImage*)image;
+ (UIImage *) flipImageVertical:(UIImage*)image;
+ (UIImage *) image: (UIImage *) image fillAllSizeNoBG: (CGSize) viewsize;
//+(UIImage*)gerGridImage:(int**)features rowCount:(int)rowCount splitLen:(CGFloat)splitLen gridLen:(CGFloat)gridLen heartImg:(UIImage*)heartImg;
+(UIImage*)getSplitImageFromMask:(UIImage*)maskImage rowCount:(int)rowCount indexx:(int)indexx indexy:(int)indexy;
+(UIImage*)createNewHairColor:(UIColor*)color rect:(CGRect)rect alpha:(float)alpha  bgImg:(UIImage*)bgImg bgSize:(CGSize)bgSize showSize:(CGSize)showSize totalV:(int)totalV angle:(float)angle roundsize:(CGSize)roundsize;
+(UIImage*)createHairRainBowColor:(UIImage*)cimg rect:(CGRect)rect  scaleX:(float)scalex scaleY:(float)scaley  bgImg:(UIImage*)bgImg totalV:(int)totalV;
+ (CGRect) getImageArea:(UIImage *)image;
//横向固定，纵向变化
+(UIImage*)gerGridHomeImage:(int**)features rowCount:(int)rowCount splitLen:(CGFloat)splitLen gridLen:(CGFloat)gridLen heartImg:(UIImage*)heartImg;
//纵向固定，横向变化
+(UIImage*)gerGridRSKImage:(int**)features rowCount:(int)rowCount splitLen:(CGFloat)splitLen gridLen:(CGFloat)gridLen heartImg:(UIImage*)heartImg;

+(UIImage *)addImageLogo:(UIImage *)image image:(UIImage *)logoImage rect:(CGRect)rectReal;
@end

