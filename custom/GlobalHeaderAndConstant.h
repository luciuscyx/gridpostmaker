//
//  GlobalHeaderAndConstant.h
//  Instagrid Post
//
//  Created by Mangal on 29/06/15.
//  Copyright (c) 2015 RJLabs. All rights reserved.
//

#ifndef Instagrid_Post_GlobalHeaderAndConstant_h
#define Instagrid_Post_GlobalHeaderAndConstant_h


#endif

#import "RJAnimation.h"
#import "CIAlert.h"
#import <StoreKit/StoreKit.h>
#import "InAppSharedClass.h"
#import "iRate.h"
#import "SVProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import "UIImage+Resize.h"
#import "RSKImageCropViewController.h"
#import "EKTilesMaker.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "NSString+UrlEncode.h"

//SET RGB COLOR MACRO
#define RGB(R,G,B) [UIColor colorWithRed:R/255.0f green:G/255.0f blue:B/255.0f alpha:1.0f]

//CHECK iOS VERSION MACRO
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)

#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

//SET CUSTOM FONT MACRO
#define CUSTOM_FONT_REGULAR(s) [UIFont fontWithName:@"Helvetica-Condensed-Black-Se" size:s]
#define CUSTOM_FONT_BOLD(s) [UIFont fontWithName:@"Helvetica-Condensed-Light-Li" size:s]
#define CUSTOM_FONT_BOLD_ITALIC(s) [UIFont fontWithName:@"Helvetica-Condensed-Light-Light" size:s]
#define CUSTOM_FONT_ITALIC(s) [UIFont fontWithName:@"Helvetica-Condensed-Thin" size:s]


#define SET_USER_DEFAULTS(key,object)   [[NSUserDefaults standardUserDefaults] setObject:object forKey:key]
#define GET_USER_DEFAULTS(key)  [[NSUserDefaults standardUserDefaults] objectForKey:key]

#define SHARED_IMAGE_COUNT @"shareimagecountirate"

//IN-APP UPGRADE KEY
#define UPGRADE_PRO @"com.instagridpost.upgradetopro"
#define UPGRADE_PRO_KEY @"Upgradetopro"

#define RATE_APP @"rateApp"
//IN-APP REMOVE ADS KEY
#define REMOVE_ADS  @"com.instagridpost.removeads"
#define REMOVE_ADS_KEY  @"removeadsKey"

//CHECK FOR DEVICE PORTRAIT
#define IS_PORTRAIT_VIDEO @"isPortrait"

//APPLE_ID
#define APPLE_ID @"1014917264"
#define RATEDNOW @"InstagridRated"
#define RATEDFAILED @"InstagridRatedFailed"
//APPDELEGATE MACRO
#define APPDELEGATE                 (AppDelegate *)[[UIApplication sharedApplication] delegate]

#define XPLODE_APPHANDLE @"SnapUploader"
#define XPLODE_APPSECRET @"133745f3602e1f83b11df06171cba221"
#define XPLODE_CACHEPT   @"igp_point"
//WINDOW MACRO
#define WINDOW                      [[[UIApplication sharedApplication] delegate] window]

// CHECK FOR DEVICE TYPE MACRO
#define ScreenWidth                         [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight                        [[UIScreen mainScreen] bounds].size.height
//视图的宽,高
#define VIEW_W(view)    (view.frame.size.width)
#define VIEW_H(view)    (view.frame.size.height)
//相对于某视图的X,Y
#define VIEW_XW(view)   (view.frame.origin.x + view.frame.size.width)
#define VIEW_YH(view)   (view.frame.origin.y + view.frame.size.height)
#define WeakSelf __weak typeof(self) weakSelf = self

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0f)
#define IS_IPHONE_6Plus (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0f)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0f)

#define IS_PAD (UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad)
#define IPHONEX_SERIES (ScreenHeight == 812 || ScreenHeight == 896)
#define KDNavH (IPHONEX_SERIES?20:0) //额外的高度
#define mainColor RGB(220,0,255)
