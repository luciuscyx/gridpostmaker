//
//  IAPManager.h
//  Classes
//
//  Created by Heracles on 08.02.14

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface Imageprocess : NSObject


+(UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage;
+(UIImage *) imageWithBits: (unsigned char *) bits withSize: (CGSize) size;

- (UIImage*) imageBlackToTransparent:(UIImage*) image;
- (UIImage *)imageByApplyingAlpha:(CGFloat)alpha  image:(UIImage*)image;
- (UIImage *)scaleToSize:(UIImage *)image size:(CGSize)viewsize;
- (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2 ToSize:(CGSize )size AddToX:(float )X AddToY:(float )Y   AddToOffsetX:(float )OffsetX AddToOffsetY:(float )OffsetY;
- (UIImage *) doUnrotateImage: (UIImage *) image;
- (UIImage *) image: (UIImage *) image1 AddscaleToSize: (CGSize) Addsize;
- (UIImage *) image: (UIImage *) image1 CutPartOfSize: (CGSize ) size;
- (UIImage *) getImageFromView:(UIView *)orgView;
-(UIImage *)getImageFromImageView:(UIImageView *)orgView;
- (UIImage*) imageAlphaToWhite:(UIImage*) image;
- (UIImage *)CutPartOfImage:(UIImage *)image  mainimage:(UIImage*)mimage offX:(float)offx offY:(float)offy Size: (CGSize ) size;
-(UIImage *)MakeWatemarkImage:(UIImage *)image HaveWatemark:(BOOL)wbool;
- (UIImage*) imageAlpha:(UIImage*) image;
+(UIImage *)fixrotation:(UIImage *)image;
+(UIImage*) imageSwapFace:(UIImage*) image FaceImage:(UIImage *)otherImage;
+(UIImage *)getImageFromView:(UIView *)orgView withScale:(float )scale;
@end
