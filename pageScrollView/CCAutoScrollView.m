//
//  CCAutoScrollView.m
//  B2C
//
//  Created by chichi on 16/1/28.
//  Copyright © 2016年 胜意科技. All rights reserved.
//

#import "CCAutoScrollView.h"

@interface CCAutoScrollView() <UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
//@property (nonatomic, strong) NSMutableArray <NSString*> *imagesArray;
@property (nonatomic, strong) NSMutableArray <NSString*> *URLArray;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic,copy)NSString *placeholder;//默认图

@end

@implementation CCAutoScrollView

- (instancetype)initWithFrame:(CGRect)frame WithImagesArray:(NSArray<NSString*>*)images timerInterval:(CGFloat)timerInterval {
    if (self = [super initWithFrame:frame]) {
        _timerInterval = timerInterval;
        _imagesArray = [[NSMutableArray alloc] initWithArray:images];
        [self setScrollViews:frame];
        [self setPageControl];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame WithURLArray:(NSArray<NSString*>*)images timerInterval:(CGFloat)timerInterval withPlaceholder:(NSString*)placeholder{
    self = [super initWithFrame:frame];
    if (self) {
        _timerInterval = timerInterval;
        _placeholder = placeholder;
        _URLArray = [[NSMutableArray alloc]initWithArray:images];
        if (_URLArray.count >= 10) {
            [_URLArray removeAllObjects];
            for (int i = 0 ; i<=10; i++) {
                [_URLArray addObject:images[i]];
            }
        }
        [self setScrollViews:frame];
        [self setPageControl];
    }
    return self;
}

//设置滚动视图
- (void)setScrollViews:(CGRect)frame {
    //定时轮播
    _timer = [NSTimer scheduledTimerWithTimeInterval:_timerInterval target:self selector:@selector(autoPlay) userInfo:nil repeats:YES];
    
    //滑动视图
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, VIEW_W(self), VIEW_H(self))];
    self.scrollView.delegate = self;//设置代理
    self.scrollView.pagingEnabled = YES;//打开整屏滑动
    self.scrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:self.scrollView];

    //图片
    if (self.imagesArray.count > 0) {
        if (self.imagesArray.count > 1) {
            //将数组最后一项加到index为0的位置
            [self.imagesArray insertObject:_imagesArray.lastObject atIndex:0];
        }
        //循环创建图片视图
        for (int i = 0; i < self.imagesArray.count; i ++) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:(CGRectMake(frame.size.width * i, 0, frame.size.width, frame.size.height))];
            imageView.tag = 100+i;
            imageView.image = [UIImage imageNamed:_imagesArray[i]];
            [self.scrollView addSubview:imageView];
            
            
            //就不算滑动视图的内容大小
            self.scrollView.contentSize = CGSizeMake(frame.size.width * self.imagesArray.count, frame.size.height);//设置内容大小

        }
    }else{
        if (self.URLArray.count >1) {
            [self.URLArray insertObject:_URLArray.lastObject atIndex:0];
        }
        
        for (int i = 0; i < self.URLArray.count; i ++) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:(CGRectMake(frame.size.width * i, 0, frame.size.width, frame.size.height))];
           // [imageView sd_setImageWithURL:[NSURL URLWithString:_URLArray[i]] placeholderImage:[UIImage imageNamed:_placeholder]];
            [self.scrollView addSubview:imageView];
            

            self.scrollView.contentSize = CGSizeMake(frame.size.width * self.URLArray.count, frame.size.height);//设置内容大小
        }
    }
    if (self.URLArray.count>1 ) {
        self.scrollView.contentOffset = CGPointMake(frame.size.width, 0);//设置内容偏移量
    }
}
- (void)setRefreshImageArray:(NSArray *)igArray{
    self.imagesArray = [[NSMutableArray alloc]initWithArray:igArray];
    if (self.imagesArray.count > 1) {
        //将数组最后一项加到index为0的位置
        [self.imagesArray insertObject:_imagesArray.lastObject atIndex:0];
    }
    for(int i = 0;i<self.imagesArray.count;i++){
        UIImageView *igView = [self viewWithTag:100+i];
        igView.image = [UIImage imageNamed:self.imagesArray[i]];
    }
    //self.scrollView.contentOffset = CGPointMake(VIEW_W(self), 0);
    //_pageControl.currentPage = 0;
}
//设置pageControl
- (void)setPageControl {
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0,VIEW_H(self)-110,VIEW_W(self),30)];
    if (_imagesArray.count>1) {
        self.pageControl.numberOfPages = self.imagesArray.count - 1;
    }else{
        self.pageControl.numberOfPages = self.URLArray.count -1;

    }
    
    if (_pageControl.numberOfPages == 0) {
        _pageControl.numberOfPages = 1;
    }
    self.pageControl.currentPage = 0;
    self.pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    self.pageControl.currentPageIndicatorTintColor = mainColor;//[UIColor whiteColor];
    _pageControl.backgroundColor = [UIColor clearColor];//RGB(0, 0, 0);
    [self.pageControl addTarget:self action:@selector(valueChanged:) forControlEvents:(UIControlEventValueChanged)];
    _pageControl.alpha = 0.6;
    [self addSubview:self.pageControl];
}

#pragma mark -滑动视图的代理

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat x = scrollView.contentOffset.x;
    if (x > (self.imagesArray.count - 1) * VIEW_W(self)) {//最后一张图 再次滑动 重置 把偏移量置为（0，0）
        [_scrollView setContentOffset:(CGPointMake(0, 0)) animated:NO];//重置scrollView偏移量
    }
    if (x < 0) {//当为第一张图时 再滑动把偏移量置为最后一张图
        [_scrollView setContentOffset:(CGPointMake((self.imagesArray.count - 1) *VIEW_W(self), 0)) animated:NO];
    }
}
// 滚动动画结束的时候更改pageControl
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    CGFloat x = scrollView.contentOffset.x;//获取当前的偏移量
    NSInteger page = x / VIEW_W(self) - 1;//对应的pageControl值
    //如果page小于0 说明是衔接图 pageControl下标为最后
    if (page < 0) {
        _pageControl.currentPage = self.imagesArray.count - 1;
    }else {
        _pageControl.currentPage = page;
    }
}
//滚动视图后更改对应pageControl 的点
//滚动视图代理方法 拖拽，将要停止，已经停止，方法是针对用户操作的拖拽动作的，如果只是更改了偏移量，并不会执行
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat x = scrollView.contentOffset.x;//获取当前的偏移量
    NSInteger page = x / VIEW_W(self) - 1;//对应的pageControl值
    //如果page小于0 ，说明是衔接图，pageControl下标为最后
    if (page < 0) {
        _pageControl.currentPage = self.imagesArray.count - 1;
    }else {
        _pageControl.currentPage = page;
    }
}
- (void)valueChanged:(UIPageControl *)pageControl {
    NSInteger page = pageControl.currentPage;
    CGFloat x = (page + 1) * VIEW_W(self);
    [self.scrollView setContentOffset:(CGPointMake(x, 0)) animated:YES];
}
#pragma mark --定时器自动轮播方法----
- (void)autoPlay {
   
    if (_isStopScroll == NO) {
        if (_imagesArray.count >1 || _URLArray.count >1) {
            //更改scrollView   countOffSet
            //如果偏移量>=contentSize.width 要先把偏移量置为（0， 0） 再加上一个width
            CGFloat x = _scrollView.contentOffset.x;//获取偏移量
            if (x + VIEW_W(self) >= _scrollView.contentSize.width) {//判断增加后的偏移量是否超过范围
                [_scrollView setContentOffset:(CGPointMake(0, 0)) animated:NO];//先把偏移量置为（0，0）
                [_scrollView setContentOffset:(CGPointMake(VIEW_W(self), 0)) animated:YES];//再偏移width宽度
            }else {
                [_scrollView setContentOffset:(CGPointMake(x + VIEW_W(self), 0)) animated:YES];
            }
        }
    }
    else{
    
        
    
    }
}

@end
