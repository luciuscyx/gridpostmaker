//
//  CCAutoScrollView.h
//  B2C
//
//  Created by chichi on 16/1/28.
//  Copyright © 2016年 胜意科技. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCAutoScrollView : UIView

@property (nonatomic)CGFloat timerInterval;//间隔时间
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSMutableArray <NSString*> *imagesArray;

@property (nonatomic)BOOL isStopScroll;//是否停止自动滑动，当设为YES的时候，不再有自动滑动的效果。
- (instancetype)initWithFrame:(CGRect)frame WithImagesArray:(NSArray<NSString*>*)images timerInterval:(CGFloat)timerInterval;//本地地址

/**
 *  从网络上下载的图
 *  参数: images        图片的网络地址数组
 *  参数: timerInterval 间隔
 *  参数: placeholder   默认图
 */
- (instancetype)initWithFrame:(CGRect)frame WithURLArray:(NSArray<NSString*>*)images timerInterval:(CGFloat)timerInterval withPlaceholder:(NSString*)placeholder;

- (void)setRefreshImageArray:(NSArray *)igArray;
@end
