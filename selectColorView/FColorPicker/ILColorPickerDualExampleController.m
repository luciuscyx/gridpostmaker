//
//  ILColorPickerExampleViewController.m
//  ILColorPickerExample
//
//  Created by Jon Gilkison on 9/1/11.
//  Copyright 2011 Interfacelab LLC. All rights reserved.
//
#define SCRWIDTH [UIScreen mainScreen].bounds.size.width
#define SCRHEIGHT [UIScreen mainScreen].bounds.size.height
#define VIEW_XW(view)   (view.frame.origin.x + view.frame.size.width)
#define VIEW_YH(view)   (view.frame.origin.y + view.frame.size.height)

#import "HZSigmentView.h"
#import "ILColorPickerDualExampleController.h"
#import "ImageHelper.h"
//#import "CIFaceFeature.h"
//#import "ColorModel.h"
#define HairHight 2268.0                   //头发图片高
#define HairWidth 1701.0                   //头发图片宽
#define eyeProportion_Y  853.0             //眼睛中心在头发图片Y
#define eyeProportion_X  837.0             //眼睛中心在头发图片X
#define eyeDistance     255.0              //两眼睛中心在头发图片距离
#define RainBowCount    9
#define kDeviceWidth [UIScreen mainScreen].bounds.size.width
#define kDeviceHeight [UIScreen mainScreen].bounds.size.height 

@interface ILColorPickerDualExampleController()<HZSigmentViewDelegate>{
    UIColor *layerColor;
    UIButton * singleButton;
    UIButton * gardienButton;
}
@property (nonatomic,strong) HZSigmentView * sigment;
@property (nonatomic,strong)  UIColor  *c;
@property (nonatomic,strong)  UIColor  *cs;
@property (nonatomic,assign) NSInteger delSelect;
@property (nonatomic,strong)UIImageView *colorChip;
@property (nonatomic,strong)UIImageView *colorChip1;
@property (nonatomic,assign)CGFloat viewHeight;
@end
@implementation ILColorPickerDualExampleController

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadDate];
    [self loadUI];
}

//加载视图
- (void)loadUI{
    //屏幕比
    self.viewHeight = (SCRHEIGHT-55-20-20-10-10-30-60)/3;
    //带有发型的试图
    CGSize imageSize = self.initSize;
    self.colorChip = [[UIImageView alloc]initWithFrame:CGRectZero];
    self.colorChip.frame = CGRectMake(SCRWIDTH-imageSize.width/imageSize.height*self.viewHeight-20,55,imageSize.width/imageSize.height*self.viewHeight,self.viewHeight-10);
    //纯视图
    self.colorChip1 = [[UIImageView alloc]initWithFrame:CGRectZero];
    self.colorChip1.frame = CGRectMake(0,0,imageSize.width/imageSize.height*self.viewHeight,self.viewHeight-10);
    //按图片比例来展示图片（imageW,imageH）
    //self.colorChip.image = [self imageCompressForSize:[UIImage imageNamed:@"image"] targetSize:CGSizeMake(100,100)];
    //self.colorChip.image = self.toopImage;
    if (!singleButton) {
        singleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [singleButton setTitle:NSLocalizedString(@"Single Color",nil) forState:UIControlStateNormal];
        [singleButton addTarget:self action:@selector(singleColor:) forControlEvents:UIControlEventTouchUpInside];
        singleButton.layer.cornerRadius = 5;
        singleButton.layer.borderWidth = 1;
        singleButton.layer.borderColor = [UIColor whiteColor].CGColor;
        [singleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [singleButton setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
    }
    
    singleButton.frame = CGRectMake(SCRWIDTH/2+20, 65, SCRWIDTH/2-50, self.viewHeight/3);
    [self.view addSubview:singleButton];
    
    if (!gardienButton) {
        gardienButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [gardienButton setTitle:NSLocalizedString(@"Gradient Color",nil) forState:UIControlStateNormal];
        [gardienButton addTarget:self action:@selector(gradientColor:) forControlEvents:UIControlEventTouchUpInside];
        gardienButton.layer.cornerRadius = 5;
        gardienButton.layer.borderWidth = 1;
        gardienButton.layer.borderColor = [UIColor whiteColor].CGColor;
        [gardienButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [gardienButton setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        gardienButton.selected = YES;
    }
    
    gardienButton.frame = CGRectMake(SCRWIDTH/2+20, 85+self.viewHeight/3, SCRWIDTH/2-50, self.viewHeight/3);
    [self.view addSubview:gardienButton];
//    //判断
    //self.colorChip.image = self.materialImages;
    UIImageView * containView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 55, SCRWIDTH/2-20, self.viewHeight-10)];
    if (containView.frame.size.width/containView.frame.size.height < self.colorChip1.frame.size.width/self.colorChip1.frame.size.height) {
        CGFloat height = containView.frame.size.width*self.colorChip1.frame.size.height/self.colorChip1.frame.size.width;
        self.colorChip1.frame = CGRectMake(0, (containView.frame.size.height-height)/2, containView.frame.size.width, height);
    }else
    {
        CGFloat width = containView.frame.size.height*self.colorChip1.frame.size.width/self.colorChip1.frame.size.height;
        self.colorChip1.frame = CGRectMake((containView.frame.size.width-width)/2,0,width,containView.frame.size.height);
    }
    [self.view addSubview:containView];
    containView.clipsToBounds = YES;
    [containView addSubview:self.colorChip1];
    //[self.view addSubview:self.colorChip];
    [self StartPhoto];
    colorPicker.frame = CGRectMake(0,VIEW_YH(self.colorChip),SCRWIDTH,self.viewHeight+10);
    huePicker.frame = CGRectMake(10,VIEW_YH(colorPicker)+5,SCRWIDTH-20,20);
    //[self.view addSubview:huePicker1];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10,VIEW_YH(huePicker),SCRWIDTH-20,30)];
    label.text = NSLocalizedString(@"Secondary color",nil);
    label.font = [UIFont systemFontOfSize:15];
    label.textColor = [UIColor whiteColor];
    [self.view addSubview:label];
    colorPicker1 = [[ILSaturationBrightnessPickerView alloc]initWithFrame:CGRectMake(13,VIEW_YH(label)+5,SCRWIDTH-26,self.viewHeight-10)];
    [self.view addSubview:colorPicker1];
    huePicker1 = [[ILHuePickerView alloc]initWithFrame:CGRectMake(10,VIEW_YH(colorPicker1)+5,SCRWIDTH-20,20)];
    [self.view addSubview:huePicker1];
    huePicker1.delegate = colorPicker1;
    self.c=[UIColor colorWithRed:(arc4random()%100)/100.0f
                               green:(arc4random()%100)/100.0f
                                blue:(arc4random()%100)/100.0f
                               alpha:1.0];
    self.colorChip1.backgroundColor=self.c;
    colorPicker.color=self.c;
    huePicker.color=self.c;
    huePicker1.color = self.c;
    self.cs = [UIColor grayColor];
    self.delSelect = 1;
    [self createNat];
     __weak ILColorPickerDualExampleController *safe = self;
    colorPicker1.colorBlock = ^(UIColor *colors) {
        safe.cs = colors;
        //safe.colorChip.image = [UIImage imageNamed:@"image"];
        if(safe.delSelect == 5){
            [safe createRadialstartColor:safe.c.CGColor endColor:safe.cs.CGColor];
          //  [safe changeHarImage:[safe getImageFromView:safe.colorChip1]];
        }else if(safe.delSelect == 6){
            [safe createRadialstartColor:safe.cs.CGColor endColor:safe.c.CGColor];
         //   [safe changeHarImage:[safe getImageFromView:safe.colorChip1]];
        }
        else{
            //[safe.colorChip.layer addSublayer:[safe Myindex:safe.delSelect Mycolor:safe.c Mycolors:safe.cs]];
            for (CALayer * subLayer in safe.colorChip1.layer.sublayers) {
                if ([subLayer isKindOfClass:[CAGradientLayer class]]) {
                    [subLayer removeFromSuperlayer];
                    break;
                }
            }
            [safe.colorChip1.layer addSublayer:[safe Myindex:safe.delSelect Mycolor:safe.c Mycolors:safe.cs MyView:safe.colorChip1]];
          //  [safe changeHarImage:[safe getImageFromView:safe.colorChip1]];
        }
    };
    if(self.isVideo){
        singleButton.selected = YES;
        [self singleColor:nil];
        gardienButton.hidden = YES;
    }
    [self navigat];
}
-(BOOL)prefersStatusBarHidden { return YES; }
-(IBAction)singleColor:(id)sender
{
    singleButton.selected = YES;
    gardienButton.selected = NO;
    colorPicker1.hidden = YES;
    huePicker1.hidden = YES;
    self.sigment.hidden = YES;
    self.colorChip1.image = nil;
    self.colorChip1.backgroundColor = self.c;
    for (CALayer * subLayer in self.colorChip1.layer.sublayers) {
        if ([subLayer isKindOfClass:[CAGradientLayer class]]) {
            [subLayer removeFromSuperlayer];
            break;
        }
    }
}

-(IBAction)gradientColor:(id)sender
{
    singleButton.selected = NO;
    gardienButton.selected = YES;
    colorPicker1.hidden = NO;
    huePicker1.hidden = NO;
    self.sigment.hidden = NO;
}

//按比例缩小图片
- (UIImage*)resizeImage:(UIImage*)image withWidth:(CGFloat)width withHeight:(CGFloat)height
{
    CGSize newSize = CGSizeMake(width, height);
    CGFloat widthRatio = newSize.width/image.size.width;
    CGFloat heightRatio = newSize.height/image.size.height;
    
    if(widthRatio > heightRatio)
    {
        newSize=CGSizeMake(image.size.width*heightRatio,image.size.height*heightRatio);
    }
    else
    {
        newSize=CGSizeMake(image.size.width*widthRatio,image.size.height*widthRatio);
    }
    
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
#pragma mark 人脸识别
-(void)StartPhoto
{

}

-(void)restoreHairPos
{

}

-(void)focusOntheHair:(CGRect)rect
{
}

//截图view转成image
-(UIImage *)getImageFromView:(UIView *)View{
    //self.harimageView.layer.borderColor = [UIColor clearColor].CGColor;
    UIGraphicsBeginImageContext(View.frame.size);
    [View.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //self.harimageView.layer.borderColor = [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:0.85].CGColor;
    return image;
}

//加载数据
- (void)loadDate{
    //self.imageArray = [[NSMutableArray alloc]init];
}
//左右按钮
- (void)navigat{
    UIButton *cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(15,5,50,50)];
    cancelButton.titleLabel.font = [UIFont systemFontOfSize: 16];
    cancelButton.layer.borderColor=[UIColor whiteColor].CGColor;
    [cancelButton setImage:[UIImage imageNamed:@"fcancel"] forState:UIControlStateNormal];
    //设置矩形四个圆角半径
    //边框宽度
    cancelButton.layer.cornerRadius = 20;
    [cancelButton.layer setBorderWidth:0];
    //[cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    //cancelButton.backgroundColor = [UIColor blackColor];
    [self.view addSubview:cancelButton];
    [cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *certainButton = [[UIButton alloc]initWithFrame:CGRectMake(SCRWIDTH-65,5,50,50)];
    [certainButton setImage:[UIImage imageNamed:@"fcertain"] forState:UIControlStateNormal];
    certainButton.layer.cornerRadius = 20;
    //[certainButton setTitle:@"确定" forState:UIControlStateNormal];
    certainButton.titleLabel.font = [UIFont systemFontOfSize: 16];
    //边框宽度
    [certainButton.layer setBorderWidth:0];
    certainButton.layer.borderColor=[UIColor whiteColor].CGColor;
    certainButton.backgroundColor = [UIColor blackColor];
    [self.view addSubview:certainButton];
    [certainButton addTarget:self action:@selector(certainAction:) forControlEvents:UIControlEventTouchUpInside];
}
//确定的点击事件
-(void)certainAction:(UIButton *)sender{
    if (colorPicker1.hidden) {
        [self.delegate setSelectedColor:self.colorChip1.backgroundColor];
    }else
    {
        [self.delegate enterStartcolor:[self getImageFromView:self.colorChip1]];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

//取消的点击事件
-(void)cancelAction:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
//渐变的方向选择
- (void)createNat{
    self.sigment = [[HZSigmentView alloc]initWithOrgin:CGPointMake(0,VIEW_YH(huePicker1)+5) andHeight:50];
    self.sigment.delegate = self;
    self.sigment.titleArry = [NSArray arrayWithObjects:[UIImage imageNamed:@"layer1"], [UIImage imageNamed:@"layer2"],[UIImage imageNamed:@"layer3"],[UIImage imageNamed:@"layer4"],[UIImage imageNamed:@"layer5"],[UIImage imageNamed:@"layer6"],[UIImage imageNamed:@"layer7"],[UIImage imageNamed:@"layer8"],[UIImage imageNamed:@"layer9"], nil];
    self.sigment.titleLineColor = [UIColor grayColor];
    [self.view addSubview:self.sigment];
   }
-(void)segment:(HZSigmentView *)sengment didSelectColumnIndex:(NSInteger)index {
    self.delSelect = index;
    self.colorChip.backgroundColor = [self.c colorWithAlphaComponent:(10.0-index)/10.0];
    self.colorChip1.backgroundColor = [self.c colorWithAlphaComponent:(10.0-index)/10.0];
    for (CALayer * subLayer in self.colorChip1.layer.sublayers) {
        if ([subLayer isKindOfClass:[CAGradientLayer class]]) {
            [subLayer removeFromSuperlayer];
            break;
        }
    }
    if(index == 5){
    [self createRadialstartColor:self.c.CGColor endColor:self.cs.CGColor];
    //[self changeHarImage:[self getImageFromView:self.colorChip1]];
    }else if(index == 6){
    [self createRadialstartColor:self.cs.CGColor endColor:self.c.CGColor];
   // [self changeHarImage:[self getImageFromView:self.colorChip1]];
    }
    else{
        self.colorChip1.image = nil;
    //[self.colorChip.layer addSublayer:[self Myindex:index Mycolor:self.c Mycolors:self.cs]];
        
        [self.colorChip1.layer addSublayer:[self Myindex:index Mycolor:self.c Mycolors:self.cs MyView:self.colorChip1]];
        //[self changeHarImage:[self getImageFromView:self.colorChip1]];
        //self.colorChip1.image = [self getImageFromView:self.colorChip1];
    }
}

//渐变的方向
- (CAGradientLayer*)Myindex:(NSInteger)index Mycolor:(UIColor*)mycolor Mycolors:(UIColor*)mycolors MyView:(UIImageView*)myview{

    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = myview.bounds;
    if(index<=4){
    gradient.colors = [NSArray arrayWithObjects:
                       (id)mycolor.CGColor,
                       (id)mycolors.CGColor, nil];
    if(index ==1){
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(0, 1);
    }
    if(index ==2){
        gradient.startPoint = CGPointMake(0, 1);
        gradient.endPoint = CGPointMake(0, 0);
    }
    if(index ==3){
        gradient.startPoint = CGPointMake(0,0);
        gradient.endPoint = CGPointMake(1, 0);
    }
    if(index ==4){
        gradient.startPoint = CGPointMake(1, 0);
        gradient.endPoint = CGPointMake(0, 0);
     }
    }
    if(index ==7){
        gradient.colors = [NSArray arrayWithObjects:
                           (id)mycolors.CGColor,
                           (id)mycolor.CGColor,
                           (id)mycolors.CGColor, nil];

        gradient.startPoint = CGPointMake(0, 0);
        gradient.endPoint = CGPointMake(1, 0);
        gradient.locations = @[@0.2,@0.5, @0.8];
    }
    if(index == 8){
        gradient.colors = [NSArray arrayWithObjects:
                           (id)mycolor.CGColor,
                           (id)mycolors.CGColor,
                           (id)mycolor.CGColor, nil];

        gradient.startPoint = CGPointMake(0, 0);
        gradient.endPoint = CGPointMake(1, 0);
        gradient.locations = @[@0.2,@0.5, @0.8];
    }
    if(index == 9){
        gradient.colors = [NSArray arrayWithObjects:
                           (id)mycolor.CGColor,
                           (id)mycolors.CGColor,
                           (id)mycolor.CGColor, nil];
        gradient.startPoint = CGPointMake(0, 0);
        gradient.endPoint = CGPointMake(0, 1);
        gradient.locations = @[@0.2,@0.5, @0.8];
    }
    //gradient.locations = @[@0.5, @1.0];
    gradient.type = kCAGradientLayerAxial;
    return gradient;
}

//按比例缩放,size 是你要把图显示到 多大区域 CGSizeMake(300, 140)
-(UIImage *) imageCompressForSize:(UIImage *)sourceImage targetSize:(CGSize)size{
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = size.width;
    CGFloat targetHeight = size.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    if(CGSizeEqualToSize(imageSize, size) == NO){
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if(widthFactor > heightFactor){
            scaleFactor = widthFactor;
        }
        else{
            scaleFactor = heightFactor;
        }
        scaledWidth = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        if(widthFactor > heightFactor){
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if(widthFactor < heightFactor){
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    UIGraphicsBeginImageContext(size);
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil){
        NSLog(@"scale image fail");
    }
    UIGraphicsEndImageContext();
    return newImage;
}
#pragma mark - ILSaturationBrightnessPickerDelegate implementation

-(void)colorPicked:(UIColor *)newColor forPicker:(ILSaturationBrightnessPickerView *)picker
{
   // colorChip.backgroundColor=newColor;
    self.c = newColor;
    if (colorPicker1.hidden) {
        self.colorChip1.backgroundColor = newColor;
    }else
    {
        if(self.delSelect == 5){
            [self createRadialstartColor:self.c.CGColor endColor:self.cs.CGColor];
            //  [self changeHarImage:[self getImageFromView:self.colorChip1]];
        }else if(self.delSelect == 6){
            [self createRadialstartColor:self.cs.CGColor endColor:self.c.CGColor];
            // [self changeHarImage:[self getImageFromView:self.colorChip1]];
        }
        else{
            //[self.colorChip.layer addSublayer:[self Myindex:self.delSelect Mycolor:self.c Mycolors:self.cs]];
            for (CALayer * subLayer in self.colorChip1.layer.sublayers) {
                if ([subLayer isKindOfClass:[CAGradientLayer class]]) {
                    [subLayer removeFromSuperlayer];
                    break;
                }
            }
            [self.colorChip1.layer addSublayer:[self Myindex:self.delSelect Mycolor:self.c Mycolors:self.cs MyView:self.colorChip1]];
            // [self changeHarImage:[self getImageFromView:self.colorChip1]];
        }
    }
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//圆心渐变视图
- (void)createRadialstartColor:(CGColorRef)startColor
          endColor:(CGColorRef)endColor  {
    //创建CGContextRef
    UIGraphicsBeginImageContext(self.colorChip1.bounds.size);
    CGContextRef gc = UIGraphicsGetCurrentContext();
    
    //创建CGMutablePathRef
    CGMutablePathRef path = CGPathCreateMutable();
    
    //绘制Path
    CGRect rect = CGRectMake(0,0,self.colorChip1.frame.size.width,self.colorChip1.frame.size.height);
    CGPathMoveToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMaxY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetWidth(rect), CGRectGetMaxY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetWidth(rect), CGRectGetMinY(rect));
    CGPathCloseSubpath(path);
    
    //绘制渐变
    [self drawRadialGradient:gc path:path startColor:startColor endColor:endColor];
    
    //注意释放CGMutablePathRef
    CGPathRelease(path);
    
    //从Context中获取图像，并显示在界面上
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
   // UIImageView *imgView = [[UIImageView alloc] initWithImage:img];
    self.colorChip1.backgroundColor = [UIColor clearColor];
    self.colorChip1.image = img;
   // [self.colorChip1 addSubview:imgView];
}
//圆心渐变
- (void)drawRadialGradient:(CGContextRef)context
                      path:(CGPathRef)path
                startColor:(CGColorRef)startColor
                  endColor:(CGColorRef)endColor
{
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.2,1};
    
    NSArray *colors = @[(__bridge id) startColor, (__bridge id) endColor];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);
    
    
    CGRect pathRect = CGPathGetBoundingBox(path);
    CGPoint center = CGPointMake(CGRectGetMidX(pathRect), CGRectGetMidY(pathRect));
    CGFloat radius = MAX(pathRect.size.width / 2.0, pathRect.size.height / 2.0) * sqrt(2);
    
    CGContextSaveGState(context);
    CGContextAddPath(context, path);
    CGContextEOClip(context);
    
    CGContextDrawRadialGradient(context, gradient, center, 0, center, radius, 0);
    
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}
//-(void)drawRect:(CGRect)rect{
//    CGGradientRef gradientRef;
//    CGColorSpaceRef colorSpaceRef;
//    CGFloat locs[2] = {0,1};
//    CGFloat colors[8] = { 1.0, 0, 0, 1.0, // 前4个为起始颜色的rgba
//        1, 1, 1, 1.0 }; // 后4个为结束颜色的rgba
//    colorSpaceRef = CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB);
//    gradientRef = CGGradientCreateWithColorComponents (colorSpaceRef, colors,
//                                                       locs, 2);
//    CGContextRef contextRef = UIGraphicsGetCurrentContext();
//    CGContextDrawRadialGradient(contextRef, gradientRef, CGPointMake(rect.origin.x+rect.size.width/2, rect.origin.y+rect.size.height/2), rect.size.width/2, CGPointMake(rect.origin.x+rect.size.width/2, rect.origin.y+rect.size.height/2-100), 0, 0);
//}

@end
