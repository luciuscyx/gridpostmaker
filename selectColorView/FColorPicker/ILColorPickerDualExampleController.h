//
//  ILColorPickerExampleViewController.h
//  ILColorPickerExample
//
//  Created by Jon Gilkison on 9/1/11.
//  Copyright 2011 Interfacelab LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILSaturationBrightnessPickerView.h"
//#import "ILSSaturationBrightnessPickerView.h"
#import "ILHuePickerView.h"
//#import "ColorModel.h"
@protocol ILColorPickerDualExampleControllerDelegate;
@interface ILColorPickerDualExampleController : UIViewController<ILSaturationBrightnessPickerViewDelegate>
{
//    IBOutlet UIView *colorChip;
    IBOutlet ILSaturationBrightnessPickerView *colorPicker;
    IBOutlet ILHuePickerView *huePicker;
    ILSaturationBrightnessPickerView *colorPicker1;
    ILHuePickerView *huePicker1;
}
//@property (nonatomic, copy)void (^certainColorBlock)(ColorModel *model);
//@property (nonatomic,copy)NSMutableArray *imageArray;
@property (nonatomic,strong)UIImage *Ilimage;
@property (nonatomic,assign)NSInteger imageW;
@property (nonatomic,assign)NSInteger imageH;
@property (nonatomic,assign)CGSize initSize;
@property (nonatomic,assign)BOOL isVideo;  // NO :图片   Yes:视频
//@property (nonatomic,strong)UIImage *toopImage;
@property (nonatomic, strong) id <ILColorPickerDualExampleControllerDelegate> delegate;
@end
@protocol ILColorPickerDualExampleControllerDelegate
- (void)enterStartcolor:(UIImage *)hairImage;
- (void)setSelectedColor:(UIColor *)color;
//- (void)colorSelectControllerDidFinished:(UIColor *)color;
//-(void)applythecoloronhair:(UIColor*)color;
//-(void)addonecolor:(UIColor*)color;
//-(void)dropdownlist;
//- (void)changeLightButtonClick:(id)sender;
//-(void)moveBoxColoronhair:(UIColor *)color;
@end
