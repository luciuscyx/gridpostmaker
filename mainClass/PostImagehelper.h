//
//  PostImagehelper.h
//  Instagrid Post
//
//  Created by macbookair on 1/8/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface PostImagehelper : NSObject
/**
 *  剪切图片为正方形
 *
 *  @param image   原始图片比如size大小为(400x200)pixels
 *  @param newSize 正方形的size比如400pixels
 *
 *  @return 返回正方形图片(400x400)pixels
 */
+ (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize ;

//图片缩放
+ (UIImage *)clipImage:(UIImage *)image ScaleWithsize:(CGSize)asize;

// 获取视频第一帧
+(UIImage*) getVideoPreViewImage:(NSURL *)path;

//图片适应大小，太大剪切中间，太小（空白填充）
+ (UIImage *) image: (UIImage *) image fitInSize: (CGSize) viewsize;

//拼接图片
+ (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2  AddToX:(float )X AddToY:(float )Y ;

//图片剪切适中展示（图片全充）
+ (UIImage *) image: (UIImage *) image centerInSize: (CGSize) size;

//画方格线 3*n
+(UIImage*)getImageByGridCount:(int)count frame:(CGRect)frame;
//分割全景图片  1*n
+(UIImage*)getHomeImageByGridCount:(int)count frame:(CGRect)frame;

//添加水印
+(UIImage*)createWithWaterMarkonImage:(UIImage*)image;

+ (UIImage *) imageFromView: (UIView *) theView;

+(UIImage *)addImageLogo:(UIImage *)image image:(UIImage *)logoImage rect:(CGRect)rectReal;
@end

NS_ASSUME_NONNULL_END
