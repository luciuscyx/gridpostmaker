//
//  VideoFilterBaseFilter.h
//  Eraser
//
//  Created by chenteng on 2018/2/27.
//  Copyright © 2018年 BiggerLensStore. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GPUImage.h"

@interface ImagePostBaseFilter : NSObject
+ (instancetype)sharedFilter;

- (UIImage*)filteredImage:(UIImage*)image withFilterName:(NSString*)filterName withFilterVersion2:(NSInteger)index;
- (UIImage*)applyEffect1:(UIImage*)image;
- (UIImage*)applyEffect2:(UIImage*)image sliderDValue:(CGFloat)value;
- (UIImage*)applyEffect3:(UIImage*)image sliderDValue:(CGFloat)value;
- (UIImage*)applyEffect4:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR;
- (UIImage*)applyEffect5:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR;
- (UIImage*)applyEffect6:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR;
- (UIImage*)applyEffect7:(UIImage*)image sliderDValue:(CGFloat)value;
- (UIImage*)applyEffect8:(UIImage*)image sliderDValue:(CGFloat)value;
- (UIImage*)applyEffect9:(UIImage*)image sliderDValue:(CGFloat)valueD;
- (UIImage*)applyEffect10:(UIImage*)image sliderDValue:(float)valueD;
- (UIImage*)applyEffect11:(UIImage*)image sliderDValue:(CGFloat)valueD;
- (UIImage*)applyEffect12:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR;
- (UIImage*)applyEffect13:(UIImage*)image;
- (UIImage*)applyEffect14:(UIImage*)image;
- (UIImage*)applyEffect15:(UIImage*)image;
- (UIImage*)applyEffect16:(UIImage*)image;
- (UIImage*)applyEffect17:(UIImage*)image;
- (UIImage*)applyEffect18:(UIImage*)image;
- (UIImage*)applyEffect19:(UIImage*)image sliderDValue:(CGFloat)valueD;
- (UIImage*)applyEffect20:(UIImage*)image sliderDValue:(CGFloat)valueD;
- (UIImage*)applyEffect21:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR sliderLValue:(CGFloat)valueL;
- (UIImage*)applyEffect22:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR;
- (UIImage*)applyEffect23:(UIImage*)image sliderDValue:(CGFloat)valueD;
- (UIImage*)applyEffect24:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR;
- (UIImage*)applyEffect25:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR sliderLValue:(CGFloat)valueL;
- (UIImage*)applyEffect26:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR sliderLValue:(CGFloat)valueL;
- (UIImage*)applyEffect27:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR sliderLValue:(CGFloat)valueL;
- (UIImage*)applyEffect28:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR;
- (UIImage*)applyEffect29:(UIImage*)image sliderDValue:(CGFloat)valueD;
- (UIImage*)applyEffect30:(UIImage*)image sliderDValue:(CGFloat)valueD;
- (UIImage*)applyEffect31:(UIImage*)image sliderDValue:(CGFloat)valueD;
@end

