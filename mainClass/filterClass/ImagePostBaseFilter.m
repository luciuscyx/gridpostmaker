//
//  VideoFilterBaseFilter.m
//  Eraser
//
//  Created by chenteng on 2018/2/27.
//  Copyright © 2018年 BiggerLensStore. All rights reserved.
//


#import "ImagePostBaseFilter.h"
#import "GPUImageAmatorkaFilter.h"
#import "UIImage+EKTilesMaker.h"
//#import "UIImage+Utility.h"
float _alphavalue = 1.0f;
static ImagePostBaseFilter *sharedFilter = nil;

#pragma mark- Default Filters


@interface ImagePostBaseFilter()

@end

@implementation ImagePostBaseFilter
+ (instancetype)sharedFilter {
    @synchronized (self) {
        if (sharedFilter == nil) {
            sharedFilter = [[ImagePostBaseFilter alloc] init];
        }
    }
    return sharedFilter;
}
- (NSDictionary*)defaultFilterInfo
{
    NSDictionary *defaultFilterInfo = nil;
    if(defaultFilterInfo==nil){
        defaultFilterInfo =
        @{
          @"CLDefaultEmptyFilter"    :@{@"name":@"CLDefaultEmptyFilter",   @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultEmptyFilter_DefaultTitle",     @""),        @"version":@(0.0), @"dockedNum":@(0.0)},
          @"CLDefaultLinearFilter"   :@{@"name":@"CISRGBToneCurveToLinear",@"FV":@"0", @"title":NSLocalizedString(@"CLDefaultLinearFilter_DefaultTitle",     @""),      @"version":@(7.0), @"dockedNum":@(1.0)},
          @"CLDefaultVignetteFilter" :@{@"name":@"CIVignetteEffect",       @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultVignetteFilter_DefaultTitle", @""),    @"version":@(7.0), @"dockedNum":@(2.0)},
          @"CLDefaultAmatorkaFilter" :@{@"name":@"CIAmatorkaEffect",       @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultAmatorkaFilter_DefaultTitle", @""),    @"version":@(5.0), @"dockedNum":@(3.0)},
          @"CLDefaultEtikateFilter"  :@{@"name":@"CIEtikateEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultEtikateFilter_DefaultTitle",   @""),     @"version":@(5.0), @"dockedNum":@(4.0)},
          @"CLDefaultEleganceFilter" :@{@"name":@"CIEleganceEffect",       @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultEleganceFilter_DefaultTitle", @""),    @"version":@(5.0), @"dockedNum":@(5.0)},
          @"CLDefaultInstantFilter"  :@{@"name":@"CIPhotoEffectInstant",   @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultInstantFilter_DefaultTitle", @""),     @"version":@(7.0), @"dockedNum":@(6.0)},
          @"CLDefaultProcessFilter"  :@{@"name":@"CIPhotoEffectProcess",   @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultProcessFilter_DefaultTitle",@""),     @"version":@(7.0), @"dockedNum":@(7.0)},
          @"CLDefaultTransferFilter" :@{@"name":@"CIPhotoEffectTransfer",  @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultTransferFilter_DefaultTitle", @""),    @"version":@(7.0), @"dockedNum":@(8.0)},
          @"CLDefaultChromeFilter"   :@{@"name":@"CIPhotoEffectChrome",    @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultChromeFilter_DefaultTitle",     @""),      @"version":@(7.0), @"dockedNum":@(9.0)},
          @"CLDefaultFadeFilter"     :@{@"name":@"CIPhotoEffectFade",      @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultFadeFilter_DefaultTitle", @""),        @"version":@(7.0), @"dockedNum":@(10.0)},
          @"CLDefaultVibranceFilter" :@{@"name":@"CIVibrance",             @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultCIVibranceFilter_DefaultTitle", @""),    @"version":@(5.0), @"dockedNum":@(11.0)},
          @"CLDefaultCurveFilter"    :@{@"name":@"CILinearToSRGBToneCurve",@"FV":@"0", @"title":NSLocalizedString(@"CLDefaultCurveFilter_DefaultTitle",      @""),       @"version":@(7.0), @"dockedNum":@(12.0)},
          @"CLDefaultUnsharpFilter"  :@{@"name":@"CIUnsharpMask",          @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultUnsharpFilter_DefaultTitle",  @""),@"version":@(6.0), @"dockedNum":@(13.0)},
          
          @"CLDefaultK1Filter"       :@{@"name":@"CIK1Effect",            @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultK1Filter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(13.1)},
          @"CLDefaultK2Filter"       :@{@"name":@"CIK2Effect",            @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultK2Filter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(13.2)},
          @"CLDefaultK6Filter"       :@{@"name":@"CIK6Effect",            @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultK6Filter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(13.3)},
          @"CLDefaultKDynamicFilter" :@{@"name":@"CIKDynamicEffect",      @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultKDynamicFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(13.4)},
          @"CLDefaultPola669Filter"  :@{@"name":@"CIPola669Effect",       @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultPola669Filter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(13.41)},
          @"CLDefaultPolaSXFilter"   :@{@"name":@"CIPolaSXEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultPolaSXFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(13.42)},
          @"CLDefaultLomoFilter"     :@{@"name":@"CILomoEffect",          @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultLomoFilter_DefaultTitle", @""),    @"version":@(5.0), @"dockedNum":@(13.43)},
          @"CLDefaultLomo100Filter"  :@{@"name":@"CILomo100Effect",       @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultLomo100Filter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(13.44)},
          @"CLDefaultPro400Filter"   :@{@"name":@"CIPro400Effect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultPro400Filter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(13.45)},
          
          @"CLDefaultFridgeFilter"   :@{@"name":@"CIFridgeEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultFridgeFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(13.5)},
          @"CLDefaultBreezeFilter"   :@{@"name":@"CIBreezeEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultBreezeFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(13.6)},
          @"CLDefaultOrchidFilter"   :@{@"name":@"CIOrchidEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultOrchidFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(13.7)},
          @"CLDefaultChestFilter"    :@{@"name":@"CIChestEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultChestFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(13.8)},
          @"CLDefaultFrontFilter"    :@{@"name":@"CIFrontEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultFrontFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(13.9)},
          @"CLDefaultFixieFilter"    :@{@"name":@"CIFixieEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultFixieFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(14.0)},
          @"CLDefaultLeninFilter"    :@{@"name":@"CILeninEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultLeninFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(14.1)},
          @"CLDefaultQuoziFilter"    :@{@"name":@"CIQuoziEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultQuoziFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(14.2)},
          @"CLDefaultFoodFilter"     :@{@"name":@"CIFoodEffect",          @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultFoodFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(14.5)},
          @"CLDefaultGlamFilter"     :@{@"name":@"CIGlamEffect",          @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultGlamFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(14.6)},
          @"CLDefaultCelsiusFilter"  :@{@"name":@"CICelsiusEffect",       @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultCelsiusFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(14.7)},
          @"CLDefaultTexasFilter"    :@{@"name":@"CITexasEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultTexasFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(14.9)},
          @"CLDefaultGoblinFilter"   :@{@"name":@"CIGoblinEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultGoblinFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(15.0)},
          @"CLDefaultMellowFilter"   :@{@"name":@"CIMellowEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultMellowFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(15.1)},
          @"CLDefaultSoftFilter"     :@{@"name":@"CISoftEffect",          @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultSoftFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(15.2)},
          @"CLDefaultBluesFilter"    :@{@"name":@"CIBluesEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultBluesFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(15.3)},
          @"CLDefaultElderFilter"    :@{@"name":@"CIElderEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultElderFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(15.4)},
          
          @"CLDefaultSteelFilter"    :@{@"name":@"CISteelEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultSteelFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(15.8)},
          @"CLDefaultBleachedFilter" :@{@"name":@"CIBleachedEffect",      @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultBleachedFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(16.0)},
          @"CLDefaultBleachedBlueFilter" :@{@"name":@"CIBleachedBlueEffect", @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultBleachedBlueFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(16.1)},
          
          @"CLDefaultHighContrastFilter" :@{@"name":@"CIHighContrastEffect",@"FV":@"0", @"title":NSLocalizedString(@"CLDefaultHighContrastFilter_DefaultTitle",   @""),  @"version":@(5.0), @"dockedNum":@(16.2)},
          @"CLDefaultHighcarbFilter" :@{@"name":@"CIHighcarbEffect",       @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultHighcarbFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(16.3)},
          @"CLDefaultSeventiesFilter":@{@"name":@"CISeventiesEffect",      @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultSeventiesFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(16.4)},
          @"CLDefaultEightiesFilter" :@{@"name":@"CIEightiesEffect",       @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultEightiesFilter_DefaultTitle", @""),    @"version":@(5.0), @"dockedNum":@(16.5)},
          @"CLDefaultColorfulFilter" :@{@"name":@"CIColorfulEffect",       @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultColorfulFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(16.6)},
          @"CLDefaultTwilightFilter" :@{@"name":@"CITwilightEffect",       @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultTwilightFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(16.9)},
          @"CLDefaultCottonCandyFilter" :@{@"name":@"CICottonCandyEffect", @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultCottonCandyFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(17.0)},
          @"CLDefaultPaleFilter"     :@{@"name":@"CIPaleEffect",           @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultPaleFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(17.1)},
          @"CLDefaultSettledFilter"  :@{@"name":@"CISettledEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultSettledFilter_DefaultTitle", @""),    @"version":@(5.0), @"dockedNum":@(17.2)},
          @"CLDefaultCoolFilter"     :@{@"name":@"CICoolEffect",           @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultCoolFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(17.3)},
          @"CLDefaultLithoFilter"    :@{@"name":@"CILithoEffect",          @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultLithoFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(17.4)},
          @"CLDefaultAncientFilter"  :@{@"name":@"CIAncientEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultAncientFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(17.5)},
          @"CLDefaultPitchedFilter"  :@{@"name":@"CIPitchedEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultPitchedFilter_DefaultTitle",   @""),    @"version":@(5.0), @"dockedNum":@(17.6)},
          @"CLDefaultLucidFilter"    :@{@"name":@"CILucidEffect",          @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultLucidFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(17.7)},
          @"CLDefaultCreamyFilter"   :@{@"name":@"CICreamyEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultCreamyFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(17.8)},
          @"CLDefaultKeenFilter"     :@{@"name":@"CIKeenEffect",           @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultKeenFilter_DefaultTitle", @""),    @"version":@(5.0), @"dockedNum":@(17.9)},
          @"CLDefaultTenderFilter"   :@{@"name":@"CITenderEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultTenderFilter_DefaultTitle", @""),    @"version":@(5.0), @"dockedNum":@(18.0)},
          @"CLDefaultClassicFilter"  :@{@"name":@"CIClassicEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultClassicFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(18.1)},
          @"CLDefaultNoGreenFilter"  :@{@"name":@"CINoGreenEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultNoGreenFilter_DefaultTitle", @""),    @"version":@(5.0), @"dockedNum":@(18.2)},
          @"CLDefaultNeatFilter"     :@{@"name":@"CINeatEffect",           @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultNeatFilter_DefaultTitle", @""),    @"version":@(5.0), @"dockedNum":@(18.3)},
          @"CLDefaultPlateFilter"    :@{@"name":@"CIPlateEffect",          @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultPlateFilter_DefaultTitle",  @""),    @"version":@(5.0), @"dockedNum":@(18.4)},
          
          @"CLDefaultSummerFilter"   :@{@"name":@"CISummerEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultSummerFilter_DefaultTitle", @""),        @"version":@(5.0), @"dockedNum":@(18.5)},
          @"CLDefaultFallFilter"     :@{@"name":@"CIFallEffect",           @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultFallFilter_DefaultTitle",  @""),            @"version":@(5.0), @"dockedNum":@(18.6)},
          @"CLDefaultWinterFilter"   :@{@"name":@"CIWinterEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultWinterFilter_DefaultTitle", @""),        @"version":@(5.0), @"dockedNum":@(18.7)},
          @"CLDefaultSunsetFilter"   :@{@"name":@"CISunsetEffect",         @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultSunsetFilter_DefaultTitle",  @""),        @"version":@(5.0), @"dockedNum":@(18.8)},
          @"CLDefaultEveningFilter"  :@{@"name":@"CIEveningEffect",        @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultEveningFilter_DefaultTitle",   @""),      @"version":@(5.0), @"dockedNum":@(18.9)},
          
          @"CLDefaultX400Filter"     :@{@"name":@"CIX400Effect",           @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultX400Filter_DefaultTitle",   @""),            @"version":@(5.0), @"dockedNum":@(19.0)},
          @"CLDefaultNoirFilter"     :@{@"name":@"CIPhotoEffectNoir",      @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultNoirFilter_DefaultTitle",     @""),        @"version":@(7.0), @"dockedNum":@(19.1)},
          @"CLDefaultTonalFilter"    :@{@"name":@"CIPhotoEffectTonal",     @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultTonalFilter_DefaultTitle",     @""),       @"version":@(7.0), @"dockedNum":@(19.2)},
          @"CLDefaultMonoFilter"     :@{@"name":@"CIPhotoEffectMono",      @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultMonoFilter_DefaultTitle",       @""),        @"version":@(7.0), @"dockedNum":@(19.3)},
          
          @"CLDefaultSinFilter"      :@{@"name":@"CISinEffect",            @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultSinFilter_DefaultTitle",  @""),          @"version":@(5.0), @"dockedNum":@(20.1)},
          @"CLDefaultSin1Filter"     :@{@"name":@"CISin1Effect",           @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultSin1Filter_DefaultTitle",   @""),       @"version":@(5.0), @"dockedNum":@(20.2)},
          @"CLDefaultSin2Filter"     :@{@"name":@"CISin2Effect",           @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultSin2Filter_DefaultTitle",  @""),        @"version":@(5.0), @"dockedNum":@(20.3)},
          @"CLDefaultNightFilter"    :@{@"name":@"CINightVision",          @"FV":@"1", @"title":NSLocalizedString(@"CLDefaultNightFilter_DefaultTitle",     @""),  @"version":@(6.0), @"dockedNum":@(21.0)},
          @"CLDefaultPredatorFilter" :@{@"name":@"CIPredatorVision",       @"FV":@"1", @"title":NSLocalizedString(@"CLDefaultPredatorFilter_DefaultTitle",    @""),  @"version":@(6.0), @"dockedNum":@(22.0)},
          @"CLDefaultArcadeFilter"   :@{@"name":@"CIArcadeVision",         @"FV":@"1", @"title":NSLocalizedString(@"CLDefaultArcadeFilter_DefaultTitle",   @""),      @"version":@(6.0), @"dockedNum":@(22.5)},
          @"CLDefaultXRayFilter"     :@{@"name":@"CIXRayVision",           @"FV":@"1", @"title":NSLocalizedString(@"CLDefaultXRayFilter_DefaultTitle",    @""),         @"version":@(6.0), @"dockedNum":@(23.0)},
          
          @"CLDefaultInvertFilter"   :@{@"name":@"CIColorInvert",          @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultInvertFilter_DefaultTitle",    @""),      @"version":@(6.0), @"dockedNum":@(25.0)},
          @"CLDefaultSepiaFilter"    :@{@"name":@"CISepiaTone",            @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultSepiaFilter_DefaultTitle",     @""),       @"version":@(5.0), @"dockedNum":@(26.0)},
          @"CLDefaultSepiaHighFilter":@{@"name":@"CISepiaHighEffect",      @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultSepiaHighFilter_DefaultTitle", @""),    @"version":@(5.0), @"dockedNum":@(27.0)},
          
          @"CLDefaultGoldFilter"     :@{@"name":@"CIColorMonochrome",      @"FV":@"5", @"title":NSLocalizedString(@"CLDefaultGoldFilter_DefaultTitle",      @""),        @"version":@(5.0), @"dockedNum":@(28.0)},
          @"CLDefaultBronceFilter"   :@{@"name":@"CIColorMonochrome",      @"FV":@"0", @"title":NSLocalizedString(@"CLDefaultBronceFilter_DefaultTitle",    @""),      @"version":@(5.0), @"dockedNum":@(29.0)},
          @"CLDefaultRubyFilter"     :@{@"name":@"CIColorMonochrome",      @"FV":@"1", @"title":NSLocalizedString(@"CLDefaultRubyFilter_DefaultTitle",     @""),        @"version":@(5.0), @"dockedNum":@(30.0)},
          @"CLDefaultEmeraldFilter"  :@{@"name":@"CIColorMonochrome",      @"FV":@"2", @"title":NSLocalizedString(@"CLDefaultEmeraldFilter_DefaultTitle",   @""),     @"version":@(5.0), @"dockedNum":@(31.0)},
          @"CLDefaultAmethystFilter" :@{@"name":@"CIColorMonochrome",      @"FV":@"3", @"title":NSLocalizedString(@"CLDefaultAmethystFilter_DefaultTitle", @""),    @"version":@(5.0), @"dockedNum":@(32.0)},
          @"CLDefaultAquaFilter"     :@{@"name":@"CIColorMonochrome",      @"FV":@"4", @"title":NSLocalizedString(@"CLDefaultAquaFilter_DefaultTitle",       @""),        @"version":@(5.0), @"dockedNum":@(33.0)},
          @"CLDefaultRoseFilter"     :@{@"name":@"CIColorMonochrome",      @"FV":@"6", @"title":NSLocalizedString(@"CLDefaultRoseFilter_DefaultTitle",     @""),        @"version":@(5.0), @"dockedNum":@(34.0)},
          };
    }
    return defaultFilterInfo;
}

//+ (id)defaultInfoForKey:(NSString*)key
//{
//    return self.defaultFilterInfo[NSStringFromClass(self)][key];
//}
//
//+ (NSString*)filterName
//{
//    return [self defaultInfoForKey:@"name"];
//}
//
//+ (NSString*)filterVersion2
//{
//    return [self defaultInfoForKey:@"FV"];
//}

#pragma mark-

//+ (NSString*)defaultTitle
//{
//    return [self defaultInfoForKey:@"title"];
//}
//
//
//+ (CGFloat)defaultDockedNumber
//{
//    return [[self defaultInfoForKey:@"dockedNum"] floatValue];
//}

#pragma mark-

- (UIImage*)filteredImage:(UIImage*)image withFilterName:(NSString*)filterName withFilterVersion2:(NSInteger)index
{
    //NSDictionary *dic = self.defaultFilterInfo[filterName];
//    filterVersion2 = dic[@"FV"];
//    NSLog(@"----- %@",filterVersion2);
    NSString *filterVersion2 = @"0";
    if(index == 77 ||index == 78||index == 79||index == 80 ||index == 86){
        filterVersion2 = @"1";
    }
    if(index == 87){
        filterVersion2 = @"2";
    }
    if(index == 88){
        filterVersion2 = @"3";
    }
    if(index == 89){
        filterVersion2 = @"4";
    }
    if(index == 84){
        filterVersion2 = @"5";
    }
    if(index == 90){
        filterVersion2 = @"6";
    }
    NSString  *currentFilter;
    if([currentFilter isEqualToString:@""]){
        currentFilter = @"CLDefaultEmptyFilter";
    }
    
    if(filterName == currentFilter){
        filterName = @"CLDefaultEmptyFilter";
        currentFilter = filterName;
        return image;
    }
    
    if([filterName isEqualToString:@"CLDefaultEmptyFilter"]){
        currentFilter = filterName;
        return image;
    }
    
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
    
    //NSLog(@"%@", [filter attributes]);
    //NSLog(@"%@", filterVersion2);
    
    if([filterName isEqualToString:@"CIVignetteEffect"]){
        // parameters for CIVignetteEffect
        CGFloat R = MIN(image.size.width, image.size.height)/2;
        CIVector *vct = [[CIVector alloc] initWithX:image.size.width/2 Y:image.size.height/2];
        [filter setValue:vct forKey:@"inputCenter"];
        [filter setValue:[NSNumber numberWithFloat:0.9] forKey:@"inputIntensity"];
        [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
    }
    
    if([filterName isEqualToString:@"CIVibrance"]){
        // parameters for CIVibrance
        [filter setValue:[NSNumber numberWithFloat:1.0] forKey:@"inputAmount"];
    }
    
    if([filterName isEqualToString:@"CIUnsharpMask"]){
        // parameters for CIUnsharpMask
        [filter setValue:[NSNumber numberWithFloat:3] forKey:@"inputRadius"];
        [filter setValue:[NSNumber numberWithFloat:1.5] forKey:@"inputIntensity"];
    }
    
    if([filterName isEqualToString:@"CINightVision"]){
        
        filterName = @"CIColorControls";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
        [filter setValue: [NSNumber numberWithFloat:-0.2] forKey: @"inputBrightness"];
        [filter setValue: [NSNumber numberWithFloat:0.5] forKey: @"inputContrast"];
        CIImage *outputImage = [filter outputImage];
        
        filterName = @"CIColorMonochrome";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, outputImage, nil];
        [filter setValue: [CIColor colorWithRed:0.5f green:1.0f blue:0.1f alpha:1.0f] forKey: @"inputColor"];
        outputImage = [filter outputImage];
        
        filterName = @"CIVignetteEffect";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, outputImage, nil];
        CGFloat R = MIN(image.size.width, image.size.height)/2;
        CIVector *vct = [[CIVector alloc] initWithX:image.size.width/2 Y:image.size.height/2];
        [filter setValue:vct forKey:@"inputCenter"];
        [filter setValue:[NSNumber numberWithFloat:1.0] forKey:@"inputIntensity"];
        [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
        outputImage = [filter outputImage];
        
        filterName = @"CIExposureAdjust";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, outputImage, nil];
        [filter setValue:[NSNumber numberWithFloat:1.3] forKey: @"inputEV"];
        outputImage = [filter outputImage];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CGImageRef cgImage =  [context createCGImage:outputImage fromRect:[outputImage extent]];
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        CGImageRelease(cgImage);
        
        currentFilter = filterName;
        
        return result;
    }
    
    if([filterName isEqualToString:@"CIXRayVision"]){
        
        filterName = @"CIColorControls";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
        [filter setValue: [NSNumber numberWithFloat:0.5] forKey: @"inputBrightness"];
        CIImage *outputImage = [filter outputImage];
        
        filterName = @"CIColorInvert";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, outputImage, nil];
        outputImage = [filter outputImage];
        
        filterName = @"CIPhotoEffectNoir";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, outputImage, nil];
        outputImage = [filter outputImage];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CGImageRef cgImage =  [context createCGImage:outputImage fromRect:[outputImage extent]];
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        CGImageRelease(cgImage);
        
        currentFilter = filterName;
        
        return result;
    }
    
    if([filterName isEqualToString:@"CIArcadeVision"]){
        
        filterName = @"CIColorControls";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
        [filter setValue: [NSNumber numberWithFloat:0.5] forKey: @"inputBrightness"];
        CIImage *outputImage = [filter outputImage];
        
        filterName = @"CIColorInvert";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, outputImage, nil];
        outputImage = [filter outputImage];
        
        CIColor *color = [CIColor colorWithRed:0.45 green:1.0 blue:0.1 alpha:1.0];
        filter = [CIFilter filterWithName:@"CIColorMonochrome"];
        [filter setValue:outputImage forKey:@"inputImage"];
        [filter setValue:[NSNumber numberWithFloat:(int)1] forKey:@"inputIntensity"];
        [filter setValue:color forKey:@"inputColor"];
        outputImage = filter.outputImage;
        
        filterName = @"CIVignetteEffect";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, outputImage, nil];
        CGFloat R = MIN(image.size.width, image.size.height)/2;
        CIVector *vct = [[CIVector alloc] initWithX:image.size.width/2 Y:image.size.height/2];
        [filter setValue:vct forKey:@"inputCenter"];
        [filter setValue:[NSNumber numberWithFloat:0.75] forKey:@"inputIntensity"];
        [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
        outputImage = [filter outputImage];
        
        CIContext *context = [CIContext contextWithOptions:nil];
        CGImageRef cgImage =  [context createCGImage:outputImage fromRect:[outputImage extent]];
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        CGImageRelease(cgImage);
        
        GPUImageEightiesFilter *stillImageFilter = [[GPUImageEightiesFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:result];
        currentFilter = filterName;
        return quickFilteredImage;
        
    }
    
    if([filterName isEqualToString:@"CIPredatorVision"]){
        
        CIContext *context = [CIContext contextWithOptions:nil];
        NSDictionary *opts = @{ CIDetectorAccuracy : CIDetectorAccuracyHigh };
        CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeFace context:context options:opts];
        NSArray *features = [detector featuresInImage:ciImage];
        
        filterName = @"CIToneCurve";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, ciImage, nil];
        [filter setValue:[CIVector vectorWithX:0 Y:0] forKey:@"inputPoint0"];
        [filter setValue:[CIVector vectorWithX:0.25 Y:0.25] forKey:@"inputPoint1"];
        [filter setValue:[CIVector vectorWithX:0.5 Y:0] forKey:@"inputPoint2"];
        [filter setValue:[CIVector vectorWithX:0.75 Y:0.5] forKey:@"inputPoint3"];
        [filter setValue:[CIVector vectorWithX:1 Y:0.5] forKey:@"inputPoint4"];
        context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [filter outputImage];
        
        filterName = @"CIColorMonochrome";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, outputImage, nil];
        [filter setValue: [CIColor colorWithRed:1.0f green:0.1f blue:0.1f alpha:1.0f] forKey: @"inputColor"];
        context = [CIContext contextWithOptions:nil];
        outputImage = [filter outputImage];
        
        filterName = @"CIExposureAdjust";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, outputImage, nil];
        [filter setValue:[NSNumber numberWithFloat:0.75] forKey: @"inputEV"];
        context = [CIContext contextWithOptions:nil];
        outputImage = [filter outputImage];
        
        filterName = @"CIColorControls";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, outputImage, nil];
        [filter setValue: [NSNumber numberWithFloat:1.5] forKey: @"inputSaturation"];
        context = [CIContext contextWithOptions:nil];
        outputImage = [filter outputImage];
        
        filterName = @"CIVignetteEffect";
        filter = [CIFilter filterWithName:filterName keysAndValues:kCIInputImageKey, outputImage, nil];
        CGFloat R = MIN(image.size.width, image.size.height)/2;
        CIVector *vct = [[CIVector alloc] initWithX:image.size.width/2 Y:image.size.height/2];
        [filter setValue:vct forKey:@"inputCenter"];
        [filter setValue:[NSNumber numberWithFloat:0.75] forKey:@"inputIntensity"];
        [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
        context = [CIContext contextWithOptions:nil];
        outputImage = [filter outputImage];
        CGImageRef cgImage =  [context createCGImage:outputImage fromRect:[outputImage extent]];
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        
        UIImage *predator_txtimage = [UIImage imageNamed:@"predator_txt.png"];
        float max_height = image.size.height/1.5;
        float max_width = (image.size.width/1.5)/(image.size.width/1.5/predator_txtimage.size.width);
        CGSize newSize = CGSizeMake(max_width,max_height);
        UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
        [predator_txtimage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        UIGraphicsBeginImageContextWithOptions(image.size, NO, result.scale);
        [result drawAtPoint:CGPointZero];
        if(image.size.width>150){ // do not show in thumbnails
            [newImage drawAtPoint:CGPointMake(5, 5) blendMode:kCGBlendModeNormal alpha:0.66];
        }
        
        if(image.size.width>150){ // do not show in thumbnails
            UIImage *predator_target = [UIImage imageNamed:@"predator_target.png"];
            for (CIFaceFeature *f in features)
            {
                //[predator_target drawInRect:CGRectMake(f.bounds.origin.x, f.bounds.origin.y,f.bounds.size.width,f.bounds.size.height) blendMode:kCGBlendModeNormal alpha:0.66];
                [predator_target drawInRect:CGRectMake(f.bounds.origin.x-(f.bounds.size.width/4), image.size.height-f.bounds.origin.y-(f.bounds.size.height*1.4),f.bounds.size.width*1.5,f.bounds.size.height*1.5) blendMode:kCGBlendModeNormal alpha:0.66];
                
                NSLog(@"Bound: %@",CGRectCreateDictionaryRepresentation(f.bounds));
            }
        }
        
        result = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        CGImageRelease(cgImage);
        
        currentFilter = filterName;
        
        return result;
    }
    
    
    if([filterName isEqualToString:@"CIColorMonochrome"]){
        if([filterVersion2  isEqualToString: @"1"]){
            [filter setValue: [CIColor colorWithRed:0.8f green:0.1f blue:0.1f alpha:0.8f] forKey: @"inputColor"];
        }
        if([filterVersion2  isEqualToString: @"2"]){
            [filter setValue: [CIColor colorWithRed:0.1f green:0.7f blue:0.4f alpha:0.8f] forKey: @"inputColor"];
        }
        if([filterVersion2  isEqualToString: @"3"]){
            [filter setValue: [CIColor colorWithRed:0.4f green:0.3f blue:0.7f alpha:0.8f] forKey: @"inputColor"];
        }
        if([filterVersion2  isEqualToString: @"4"]){
            [filter setValue: [CIColor colorWithRed:0.6f green:0.7f blue:1.0f alpha:1.0f] forKey: @"inputColor"];
        }
        if([filterVersion2  isEqualToString: @"5"]){
            [filter setValue: [CIColor colorWithRed:0.9f green:0.8f blue:0.3f alpha:1.0f] forKey: @"inputColor"];
        }
        if([filterVersion2  isEqualToString: @"6"]){
            [filter setValue: [CIColor colorWithRed:0.8f green:0.5f blue:0.5f alpha:1.0f] forKey: @"inputColor"];
        }
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [filter outputImage];
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        CGImageRelease(cgImage);
        
        currentFilter = filterName;
        
        return result;
    }
    
    
    if([filterName isEqualToString:@"CIAmatorkaEffect"]){
        GPUImageAmatorkaFilter *stillImageFilter = [[GPUImageAmatorkaFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIK1Effect"]){
        GPUImageK1Filter *stillImageFilter = [[GPUImageK1Filter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIK2Effect"]){
        GPUImageK2Filter *stillImageFilter = [[GPUImageK2Filter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIK6Effect"]){
        GPUImageK6Filter *stillImageFilter = [[GPUImageK6Filter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIKDynamicEffect"]){
        GPUImageKDynamicFilter *stillImageFilter = [[GPUImageKDynamicFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIFridgeEffect"]){
        GPUImageFridgeFilter *stillImageFilter = [[GPUImageFridgeFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIBreezeEffect"]){
        GPUImageBreezeFilter *stillImageFilter = [[GPUImageBreezeFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIOrchidEffect"]){
        GPUImageOrchidFilter *stillImageFilter = [[GPUImageOrchidFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIChestEffect"]){
        GPUImageChestFilter *stillImageFilter = [[GPUImageChestFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIFrontEffect"]){
        GPUImageFrontFilter *stillImageFilter = [[GPUImageFrontFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIFixieEffect"]){
        GPUImageFixieFilter *stillImageFilter = [[GPUImageFixieFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CILeninEffect"]){
        GPUImageLeninFilter *stillImageFilter = [[GPUImageLeninFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIQuoziEffect"]){
        GPUImageQuoziFilter *stillImageFilter = [[GPUImageQuoziFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIPola669Effect"]){
        GPUImagePola669Filter *stillImageFilter = [[GPUImagePola669Filter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIPolaSXEffect"]){
        GPUImagePolaSXFilter *stillImageFilter = [[GPUImagePolaSXFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIFoodEffect"]){
        GPUImageFoodFilter *stillImageFilter = [[GPUImageFoodFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIGlamEffect"]){
        GPUImageGlamFilter *stillImageFilter = [[GPUImageGlamFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CICelsiusEffect"]){
        GPUImageCelsiusFilter *stillImageFilter = [[GPUImageCelsiusFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CITexasEffect"]){
        GPUImageTexasFilter *stillImageFilter = [[GPUImageTexasFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CILomoEffect"]){
        GPUImageLomoFilter *stillImageFilter = [[GPUImageLomoFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIGoblinEffect"]){
        GPUImageGoblinFilter *stillImageFilter = [[GPUImageGoblinFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CISinEffect"]){
        GPUImageSinFilter *stillImageFilter = [[GPUImageSinFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CISin1Effect"]){
        GPUImageSin1Filter *stillImageFilter = [[GPUImageSin1Filter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CISin2Effect"]){
        GPUImageSin2Filter *stillImageFilter = [[GPUImageSin2Filter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIHighContrastEffect"]){
        GPUImageHighContrastFilter *stillImageFilter = [[GPUImageHighContrastFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIHighcarbEffect"]){
        GPUImageHighcarbFilter *stillImageFilter = [[GPUImageHighcarbFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIEightiesEffect"]){
        GPUImageEightiesFilter *stillImageFilter = [[GPUImageEightiesFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIColorfulEffect"]){
        GPUImageColorfulFilter *stillImageFilter = [[GPUImageColorfulFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CILomo100Effect"]){
        GPUImageLomo100Filter *stillImageFilter = [[GPUImageLomo100Filter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIPro400Effect"]){
        GPUImagePro400Filter *stillImageFilter = [[GPUImagePro400Filter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CITwilightEffect"]){
        GPUImageTwilightFilter *stillImageFilter = [[GPUImageTwilightFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CICottonCandyEffect"]){
        GPUImageCottonCandyFilter *stillImageFilter = [[GPUImageCottonCandyFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIPaleEffect"]){
        GPUImagePaleFilter *stillImageFilter = [[GPUImagePaleFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CISettledEffect"]){
        GPUImageSettledFilter *stillImageFilter = [[GPUImageSettledFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CICoolEffect"]){
        GPUImageCoolFilter *stillImageFilter = [[GPUImageCoolFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CILithoEffect"]){
        GPUImageLithoFilter *stillImageFilter = [[GPUImageLithoFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIAncientEffect"]){
        GPUImageAncientFilter *stillImageFilter = [[GPUImageAncientFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIPitchedEffect"]){
        GPUImagePitchedFilter *stillImageFilter = [[GPUImagePitchedFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CILucidEffect"]){
        GPUImageLucidFilter *stillImageFilter = [[GPUImageLucidFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CICreamyEffect"]){
        GPUImageCreamyFilter *stillImageFilter = [[GPUImageCreamyFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIKeenEffect"]){
        GPUImageKeenFilter *stillImageFilter = [[GPUImageKeenFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CITenderEffect"]){
        GPUImageTenderFilter *stillImageFilter = [[GPUImageTenderFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIFallEffect"]){
        GPUImageFallFilter *stillImageFilter = [[GPUImageFallFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIWinterEffect"]){
        GPUImageWinterFilter *stillImageFilter = [[GPUImageWinterFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CISepiaHighEffect"]){
        GPUImageSepiaHighFilter *stillImageFilter = [[GPUImageSepiaHighFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CISummerEffect"]){
        GPUImageSummerFilter *stillImageFilter = [[GPUImageSummerFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIClassicEffect"]){
        GPUImageClassicFilter *stillImageFilter = [[GPUImageClassicFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CINoGreenEffect"]){
        GPUImageNoGreenFilter *stillImageFilter = [[GPUImageNoGreenFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CINeatEffect"]){
        GPUImageNeatFilter *stillImageFilter = [[GPUImageNeatFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIPlateEffect"]){
        GPUImagePlateFilter *stillImageFilter = [[GPUImagePlateFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIX400Effect"]){
        GPUImageX400Filter *stillImageFilter = [[GPUImageX400Filter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIMellowEffect"]){
        GPUImageMellowFilter *stillImageFilter = [[GPUImageMellowFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CISoftEffect"]){
        GPUImageSoftFilter *stillImageFilter = [[GPUImageSoftFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIBluesEffect"]){
        GPUImageBluesFilter *stillImageFilter = [[GPUImageBluesFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIElderEffect"]){
        GPUImageElderFilter *stillImageFilter = [[GPUImageElderFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CISunsetEffect"]){
        GPUImageSunsetFilter *stillImageFilter = [[GPUImageSunsetFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIEveningEffect"]){
        GPUImageEveningFilter *stillImageFilter = [[GPUImageEveningFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CISteelEffect"]){
        GPUImageSteelFilter *stillImageFilter = [[GPUImageSteelFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CISeventiesEffect"]){
        GPUImageSeventiesFilter *stillImageFilter = [[GPUImageSeventiesFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIBleachedEffect"]){
        GPUImageBleachedFilter *stillImageFilter = [[GPUImageBleachedFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIBleachedBlueEffect"]){
        GPUImageBleachedBlueFilter *stillImageFilter = [[GPUImageBleachedBlueFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIEtikateEffect"]){
        GPUImageMissEtikateFilter *stillImageFilter = [[GPUImageMissEtikateFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else if([filterName isEqualToString:@"CIEleganceEffect"]){
        GPUImageSoftEleganceFilter *stillImageFilter = [[GPUImageSoftEleganceFilter alloc] init];
        UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
        currentFilter = filterName;
        return quickFilteredImage;
    } else {
        CIContext *context = [CIContext contextWithOptions:nil];
        CIImage *outputImage = [filter outputImage];
        CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
        UIImage *result = [UIImage imageWithCGImage:cgImage];
        CGImageRelease(cgImage);
        currentFilter = filterName;
        return result;
    }
}

- (UIImage*)applyEffect1:(UIImage*)image
{
    NSLog(@"%@",NSStringFromCGSize(image.size));
    CGFloat _X = 0.5;
    CGFloat _Y = 0.5;
    CGFloat _R = 0.5;
    CIImage *ciImage = [CIImage imageWithCGImage:image.CGImage];//[[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:@"CIVignetteEffect" keysAndValues:kCIInputImageKey, ciImage, nil];
    
    [filter setDefaults];
    
    CGFloat R = MIN(image.size.width, image.size.height) * 0.5 * (_R + 0.1);
    CIVector *vct = [[CIVector alloc] initWithX:image.size.width * _X Y:image.size.height * (1 - _Y)];
    [filter setValue:vct forKey:@"inputCenter"];
    [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    return result;
}
- (UIImage*)applyEffect2:(UIImage*)image sliderDValue:(CGFloat)value
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:@"CIHueAdjust" keysAndValues:kCIInputImageKey, ciImage, nil];
    
    //NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    [filter setValue:[NSNumber numberWithFloat:value] forKey:@"inputAngle"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    return result;
}
- (UIImage*)applyEffect3:(UIImage*)image sliderDValue:(CGFloat)value
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:@"CIHighlightShadowAdjust" keysAndValues:kCIInputImageKey, ciImage, nil];
    
    //NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    //[filter setValue:[NSNumber numberWithFloat:_highlightSlider.value] forKey:@"inputHighlightAmount"];
    [filter setValue:[NSNumber numberWithFloat:value] forKey:@"inputShadowAmount"];
    //CGFloat R = MAX(image.size.width, image.size.height) * 0.02 * _radiusSlider.value;
    //[filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    return result;
}

//2
- (UIImage*)applyEffect4:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:@"CIBloom" keysAndValues:kCIInputImageKey, ciImage, nil];
    
    //NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    CGFloat R = valueD * MIN(image.size.width, image.size.height) * 0.05;
    [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
    [filter setValue:[NSNumber numberWithFloat:valueR] forKey:@"inputIntensity"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    CGFloat dW = (result.size.width - image.size.width)/2;
    CGFloat dH = (result.size.height - image.size.height)/2;
    
    CGRect rct = CGRectMake(dW, dH, image.size.width, image.size.height);
    
    return [result crop:rct];
}
//2
- (UIImage*)applyEffect5:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:@"CIGloom" keysAndValues:kCIInputImageKey, ciImage, nil];
    
    //NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    CGFloat R = valueD * MIN(image.size.width, image.size.height) * 0.05;
    [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
    [filter setValue:[NSNumber numberWithFloat:valueR] forKey:@"inputIntensity"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    CGFloat dW = (result.size.width - image.size.width)/2;
    CGFloat dH = (result.size.height - image.size.height)/2;
    
    CGRect rct = CGRectMake(dW, dH, image.size.width, image.size.height);
    
    return [result crop:rct];
}
//2
- (UIImage*)applyEffect6:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR
{
    CGFloat newD ,newR;
    if(valueD<0.67){
        newD = -(1-valueD*1.5);
    }else{
        newD = 1.5*(valueD - 0.67);
    }
    if(valueR<0.67){
        newR = -(1-valueR*1.5);
    }else{
        newR = 1.5*(valueR - 0.67);
    }
    GPUImageHazeFilter *stillImageFilter = [[GPUImageHazeFilter alloc] init];
    [stillImageFilter setDistance:newD];
    [stillImageFilter setSlope:newR];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
//1
- (UIImage*)applyEffect7:(UIImage*)image sliderDValue:(CGFloat)valueD
{
    GPUImageSepiaFilter *stillImageFilter = [[GPUImageSepiaFilter alloc] init];
    [stillImageFilter setIntensity:valueD];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
//1
- (UIImage*)applyEffect8:(UIImage*)image sliderDValue:(CGFloat)valueD
{
    GPUImageKuwaharaFilter *stillImageFilter = [[GPUImageKuwaharaFilter alloc] init];
    [stillImageFilter setRadius:valueD];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect9:(UIImage*)image sliderDValue:(CGFloat)valueD
{
    
    GPUImageEmbossFilter *stillImageFilter = [[GPUImageEmbossFilter alloc] init];
    [stillImageFilter setIntensity:valueD];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect10:(UIImage*)image sliderDValue:(float)valueD
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:@"CIColorPosterize" keysAndValues:kCIInputImageKey, ciImage, nil];
    
    [filter setDefaults];
    [filter setValue:[NSNumber numberWithFloat:valueD*10] forKey:@"inputLevels"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    return result;
}
- (UIImage*)applyEffect11:(UIImage*)image sliderDValue:(CGFloat)valueD
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:@"CIPixellate" keysAndValues:kCIInputImageKey, ciImage, nil];
    [filter setDefaults];
    
    CGFloat R = MIN(image.size.width, image.size.height) * 0.1 * valueD;
    CIVector *vct = [[CIVector alloc] initWithX:image.size.width/2 Y:image.size.height/2];
    [filter setValue:vct forKey:@"inputCenter"];
    [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputScale"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    CGRect clippingRect = [self clippingRectForTransparentSpace:cgImage];
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    return [result crop:clippingRect];
}
- (CGRect)clippingRectForTransparentSpace:(CGImageRef)inImage
{
    CGFloat left=0, right=0, top=0, bottom=0;
    
    CFDataRef m_DataRef = CGDataProviderCopyData(CGImageGetDataProvider(inImage));
    UInt8 * m_PixelBuf = (UInt8 *) CFDataGetBytePtr(m_DataRef);
    
    int width  = (int)CGImageGetWidth(inImage);
    int height = (int)CGImageGetHeight(inImage);
    
    BOOL breakOut = NO;
    for (int x = 0;breakOut==NO && x < width; ++x) {
        for (int y = 0; y < height; ++y) {
            int loc = x + (y * width);
            loc *= 4;
            if (m_PixelBuf[loc + 3] != 0) {
                left = x;
                breakOut = YES;
                break;
            }
        }
    }
    
    breakOut = NO;
    for (int y = 0;breakOut==NO && y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            int loc = x + (y * width);
            loc *= 4;
            if (m_PixelBuf[loc + 3] != 0) {
                top = y;
                breakOut = YES;
                break;
            }
            
        }
    }
    
    breakOut = NO;
    for (int y = height-1;breakOut==NO && y >= 0; --y) {
        for (int x = width-1; x >= 0; --x) {
            int loc = x + (y * width);
            loc *= 4;
            if (m_PixelBuf[loc + 3] != 0) {
                bottom = y;
                breakOut = YES;
                break;
            }
            
        }
    }
    
    breakOut = NO;
    for (int x = width-1;breakOut==NO && x >= 0; --x) {
        for (int y = height-1; y >= 0; --y) {
            int loc = x + (y * width);
            loc *= 4;
            if (m_PixelBuf[loc + 3] != 0) {
                right = x;
                breakOut = YES;
                break;
            }
            
        }
    }
    
    CFRelease(m_DataRef);
    
    return CGRectMake(left, top, right-left, bottom-top);
}
- (UIImage*)applyEffect12:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR
{
    
    GPUImagePolkaDotFilter *stillImageFilter = [[GPUImagePolkaDotFilter alloc] init];
    [stillImageFilter setFractionalWidthOfAPixel:valueD];
    [stillImageFilter setDotScaling:valueR];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect13:(UIImage*)image
{
    CGFloat _X = 0.5;
    CGFloat _Y = 0.5;
    CGFloat _R = 0.5;
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:@"CIHoleDistortion" keysAndValues:kCIInputImageKey, ciImage, nil];
    
    [filter setDefaults];
    
    CGFloat R = MIN(image.size.width, image.size.height) * 0.5 * (_R + 0.1);
    CIVector *vct = [[CIVector alloc] initWithX:image.size.width * _X Y:image.size.height * (1 - _Y)];
    [filter setValue:vct forKey:@"inputCenter"];
    [filter setValue:[NSNumber numberWithFloat:R] forKey:@"inputRadius"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    
    return result;
}
- (UIImage*)applyEffect14:(UIImage*)image
{
    CGFloat _X = 0.5;
    CGFloat _Y = 0.5;
    CGFloat _R = 0.5;
    CGFloat _Ro = 0.5;
    
    CGFloat R = (_R + 0.1);
    GPUImageSwirlFilter *stillImageFilter = [[GPUImageSwirlFilter alloc] init];
    [stillImageFilter setAngle: _Ro];
    [stillImageFilter setRadius:R];
    [stillImageFilter setCenter:CGPointMake(_X,_Y)];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect15:(UIImage*)image
{
    CGFloat _X = 0.5;
    CGFloat _Y = 0.5;
    CGFloat _R = 0.5;
    CGFloat _Ro = 0.5;
    
    CGFloat R = (_R + 0.1);
    GPUImageBulgeDistortionFilter *stillImageFilter = [[GPUImageBulgeDistortionFilter alloc] init];
    [stillImageFilter setScale: _Ro];
    [stillImageFilter setRadius:R];
    [stillImageFilter setCenter:CGPointMake(_X,_Y)];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect16:(UIImage*)image
{
    CGFloat _X = 0.5;
    CGFloat _Y = 0.5;
    CGFloat _R = 1;
    NSLog(@"%f",_R);
    
    GPUImagePinchDistortionFilter *stillImageFilter = [[GPUImagePinchDistortionFilter alloc] init];
    [stillImageFilter setScale: _R];
    [stillImageFilter setRadius:1];
    [stillImageFilter setCenter:CGPointMake(_X,_Y)];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect17:(UIImage*)image
{
    CGFloat _X = 0.5;
    CGFloat _Y = 0.5;
   // CGFloat _R = 1;
    GPUImageStretchDistortionFilter *stillImageFilter = [[GPUImageStretchDistortionFilter alloc] init];
    [stillImageFilter setCenter:CGPointMake(_X,_Y)];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect18:(UIImage*)image
{
    CGFloat _X = 0.5;
    CGFloat _Y = 0.5;
    CGFloat _R = 0.5;
    CGFloat R = (_R + 0.1);
    GPUImageZoomBlurFilter *stillImageFilter = [[GPUImageZoomBlurFilter alloc] init];
    [stillImageFilter setBlurSize:R];
    [stillImageFilter setBlurCenter:CGPointMake(_X,_Y)];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect19:(UIImage*)image sliderDValue:(CGFloat)valueD
{
    
    GPUImageSharpenFilter *stillImageFilter = [[GPUImageSharpenFilter alloc] init];
    [stillImageFilter setSharpness:valueD];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect20:(UIImage*)image sliderDValue:(CGFloat)valueD
{
    
    GPUImageUnsharpMaskFilter *stillImageFilter = [[GPUImageUnsharpMaskFilter alloc] init];
    [stillImageFilter setIntensity:valueD];
    [stillImageFilter setBlurRadiusInPixels:5];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect21:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR sliderLValue:(CGFloat)valueL
{
    GPUImageSmoothToonFilter *stillImageFilter = [[GPUImageSmoothToonFilter alloc] init];
    [stillImageFilter setQuantizationLevels:valueD];
    [stillImageFilter setThreshold:valueR];
    [stillImageFilter setBlurRadiusInPixels:valueL];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect22:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR
{
    GPUImageToonFilter *stillImageFilter = [[GPUImageToonFilter alloc] init];
    [stillImageFilter setQuantizationLevels:valueD];
    [stillImageFilter setThreshold:valueR];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect23:(UIImage*)image sliderDValue:(CGFloat)valueD
{
    
    GPUImageSketchFilter *stillImageFilter = [[GPUImageSketchFilter alloc] init];
    [stillImageFilter setEdgeStrength:valueD];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect24:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR
{
    GPUImageThresholdSketchFilter *stillImageFilter = [[GPUImageThresholdSketchFilter alloc] init];
    [stillImageFilter setEdgeStrength:valueD];
    [stillImageFilter setThreshold:valueR];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect25:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR sliderLValue:(CGFloat)valueL
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:@"CIDotScreen" keysAndValues:kCIInputImageKey, ciImage, nil];
    
    //NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    [filter setValue:[NSNumber numberWithFloat:valueD] forKey:@"inputWidth"];
    [filter setValue:[NSNumber numberWithFloat:valueR] forKey:@"inputSharpness"];
    [filter setValue:[NSNumber numberWithFloat:valueL] forKey:@"inputAngle"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    CGFloat dW = (result.size.width - image.size.width)/2;
    CGFloat dH = (result.size.height - image.size.height)/2;
    
    CGRect rct = CGRectMake(dW, dH, image.size.width, image.size.height);
    
    return [result crop:rct];
}
- (UIImage*)applyEffect26:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR sliderLValue:(CGFloat)valueL
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:@"CIDotScreen" keysAndValues:kCIInputImageKey, ciImage, nil];
    
    //NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    [filter setValue:[NSNumber numberWithFloat:valueD] forKey:@"inputWidth"];
    [filter setValue:[NSNumber numberWithFloat:valueR] forKey:@"inputSharpness"];
    [filter setValue:[NSNumber numberWithFloat:valueL] forKey:@"inputAngle"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    CGFloat dW = (result.size.width - image.size.width)/2;
    CGFloat dH = (result.size.height - image.size.height)/2;
    
    CGRect rct = CGRectMake(dW, dH, image.size.width, image.size.height);
    
    return [result crop:rct];
}
- (UIImage*)applyEffect27:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR sliderLValue:(CGFloat)valueL
{
    CIImage *ciImage = [[CIImage alloc] initWithImage:image];
    CIFilter *filter = [CIFilter filterWithName:@"CIHatchedScreen" keysAndValues:kCIInputImageKey, ciImage, nil];
    
    //NSLog(@"%@", [filter attributes]);
    
    [filter setDefaults];
    
    [filter setValue:[NSNumber numberWithFloat:valueD] forKey:@"inputWidth"];
    [filter setValue:[NSNumber numberWithFloat:valueR] forKey:@"inputSharpness"];
    [filter setValue:[NSNumber numberWithFloat:valueL] forKey:@"inputAngle"];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *outputImage = [filter outputImage];
    CGImageRef cgImage = [context createCGImage:outputImage fromRect:[outputImage extent]];
    
    UIImage *result = [UIImage imageWithCGImage:cgImage];
    
    CGImageRelease(cgImage);
    
    CGFloat dW = (result.size.width - image.size.width)/2;
    CGFloat dH = (result.size.height - image.size.height)/2;
    
    CGRect rct = CGRectMake(dW, dH, image.size.width, image.size.height);
    
    return [result crop:rct];
}
- (UIImage*)applyEffect28:(UIImage*)image sliderDValue:(CGFloat)valueD sliderRValue:(CGFloat)valueR
{
    
    GPUImageCrosshatchFilter *stillImageFilter = [[GPUImageCrosshatchFilter alloc] init];
    [stillImageFilter setCrossHatchSpacing:valueD];
    [stillImageFilter setLineWidth:valueR];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect29:(UIImage*)image sliderDValue:(CGFloat)valueD
{
    GPUImageLuminanceThresholdFilter *stillImageFilter = [[GPUImageLuminanceThresholdFilter alloc] init];
    [stillImageFilter setThreshold:valueD];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect30:(UIImage*)image sliderDValue:(CGFloat)valueD
{
    GPUImagePrewittEdgeDetectionFilter *stillImageFilter = [[GPUImagePrewittEdgeDetectionFilter alloc] init];
    [stillImageFilter setEdgeStrength:valueD];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}
- (UIImage*)applyEffect31:(UIImage*)image sliderDValue:(CGFloat)valueD
{
    GPUImageOpacityFilter *stillImageFilter = [[GPUImageOpacityFilter alloc] init];
    [stillImageFilter setOpacity:valueD];
    UIImage *quickFilteredImage = [stillImageFilter imageByFilteringImage:image];
    return quickFilteredImage;
}

@end


