

#import "EditBackMImageView.h"
#import <QuartzCore/QuartzCore.h>

#define kSPUserResizableViewGlobalInset 5.0
#define kSPUserResizableViewDefaultMinWidth 48.0
#define kSPUserResizableViewInteractiveBorderSize 10.0
#define kEditBackMImageViewControlSize 32.0


@interface EditBackMImageView ()


@property (nonatomic) BOOL preventsLayoutWhileResizing;

@property (nonatomic) CGPoint prevPoint;
@property (nonatomic) CGAffineTransform startTransform;

@property (nonatomic) CGPoint touchStart;
@property(nonatomic,assign)BOOL isTran;

@end

@implementation EditBackMImageView
@synthesize contentView, touchStart;

@synthesize prevPoint;
@synthesize deltaAngle, startTransform; //rotation
@synthesize resizingControl, deleteControl, customControl;
@synthesize preventsPositionOutsideSuperview;
@synthesize preventsResizing;
@synthesize preventsDeleting;
@synthesize preventsCustomButton;
@synthesize minWidth, minHeight;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#ifdef EditBackMImageView_LONGPRESS
-(void)longPress:(UIPanGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        if([_delegate respondsToSelector:@selector(stickerViewDidLongPressed:)]) {
            [_delegate stickerViewDidLongPressed:self];
        }
    }
}
#endif

-(void)singleTap:(UIPanGestureRecognizer *)recognizer
{
    if (NO == self.preventsDeleting) {
        UIView * close = (UIView *)[recognizer view];
        [close.superview removeFromSuperview];
    }
    
    if([_delegate respondsToSelector:@selector(stickerViewDidClose:)]) {
        [_delegate stickerViewDidClose:self];
    }
}

-(void)customTap:(UIPanGestureRecognizer *)recognizer
{
    self.isTran = !self.isTran;
    if (NO == self.preventsCustomButton) {
        if (self.isTran) {
            [UIView animateWithDuration:0.1 animations:^{
                self.contentView.transform = CGAffineTransformMakeScale(-1,1);
            }];
        }
        else
        {
            [UIView animateWithDuration:0.1 animations:^{
                self.contentView.transform = CGAffineTransformIdentity;
            }];
        }
    }
    if([_delegate respondsToSelector:@selector(stickerViewDidCustomButtonTap:)]) {
        [_delegate stickerViewDidCustomButtonTap:self];
    }

}

-(void)resizeTranslate:(UIPanGestureRecognizer *)recognizer
{
    if ([recognizer state]== UIGestureRecognizerStateBegan)
    {
        prevPoint = [recognizer locationInView:self];
        [self setNeedsDisplay];
    }
    else if ([recognizer state] == UIGestureRecognizerStateChanged)
    {
        if (self.bounds.size.width < minWidth || self.bounds.size.height < minHeight)
        {
            self.bounds = CGRectMake(self.bounds.origin.x,
                                     self.bounds.origin.y,
                                     minWidth,
                                     minHeight);
            resizingControl.frame =CGRectMake(self.bounds.size.width-kEditBackMImageViewControlSize,
                                       self.bounds.size.height-kEditBackMImageViewControlSize,
                                              kEditBackMImageViewControlSize,
                                              kEditBackMImageViewControlSize);
            deleteControl.frame = CGRectMake(0, 0,
                                             kEditBackMImageViewControlSize, kEditBackMImageViewControlSize);
            customControl.frame =CGRectMake(0,self.bounds.size.height - kEditBackMImageViewControlSize,
                                              kEditBackMImageViewControlSize,
                                              kEditBackMImageViewControlSize);
            prevPoint = [recognizer locationInView:self];
             
        } else {
            CGPoint point = [recognizer locationInView:self];
            float wChange = 0.0, hChange = 0.0;
            
            wChange = 2*(point.x - prevPoint.x);
            float wRatioChange = (wChange/(float)contentView.bounds.size.width);
            
            hChange = wRatioChange * contentView.bounds.size.height;
            
//            if (ABS(wChange) > 20.0f || ABS(hChange) > 20.0f) {
//                prevPoint = [recognizer locationInView:self];
//                return;
//            }
            
            self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y,
                                     self.bounds.size.width + (wChange),
                                     self.bounds.size.height + (hChange));
            resizingControl.frame =CGRectMake(self.bounds.size.width-kEditBackMImageViewControlSize,
                                              self.bounds.size.height-kEditBackMImageViewControlSize,
                                              kEditBackMImageViewControlSize, kEditBackMImageViewControlSize);
            deleteControl.frame = CGRectMake(0, 0,
                                             kEditBackMImageViewControlSize, kEditBackMImageViewControlSize);
            customControl.frame =CGRectMake(0,self.bounds.size.height - kEditBackMImageViewControlSize,
                                            kEditBackMImageViewControlSize,
                                            kEditBackMImageViewControlSize);
            prevPoint = [recognizer locationInView:self];
        }
        
        /* Rotation */
        float ang = atan2([recognizer locationInView:self.superview].y - self.center.y,
                          [recognizer locationInView:self.superview].x - self.center.x);
        float angleDiff = deltaAngle - ang;
        if (NO == preventsResizing) {
            self.transform = CGAffineTransformMakeRotation(-angleDiff);
            self.angleDiff = _angleDiff;

        }
        
        self.borderView.frame = CGRectInset(self.bounds, kSPUserResizableViewGlobalInset, kSPUserResizableViewGlobalInset);
        [self.borderView setNeedsDisplay];
        if ([self.delegate respondsToSelector:@selector(stickerViewDidTranslateBounds:rotation:)]) {
            [self.delegate stickerViewDidTranslateBounds:self.bounds rotation:-angleDiff];
        }
        [self setNeedsDisplay];
    }
    else if ([recognizer state] == UIGestureRecognizerStateEnded)
    {
        prevPoint = [recognizer locationInView:self];
        [self setNeedsDisplay];
    }
}

- (void)setupDefaultAttributes {
    
    //取消边框 20190401
    self.borderView = [[EditbBackImageDrawView alloc] initWithFrame:CGRectInset(self.bounds, kSPUserResizableViewGlobalInset, kSPUserResizableViewGlobalInset)];
    [self.borderView setHidden:YES];
    //[self addSubview:self.borderView];
    
    if (kSPUserResizableViewDefaultMinWidth > self.bounds.size.width*0.2) {
        self.minWidth = kSPUserResizableViewDefaultMinWidth;
        self.minHeight = self.bounds.size.height * (kSPUserResizableViewDefaultMinWidth/self.bounds.size.width);
    } else {
        self.minWidth = self.bounds.size.width*0.2;
        self.minHeight = self.bounds.size.height*0.2;
    }
    self.preventsPositionOutsideSuperview = NO;
    self.preventsLayoutWhileResizing = YES;
    self.preventsResizing = NO;
    self.preventsDeleting = NO;
    self.preventsCustomButton = NO;
#ifdef EditBackMImageView_LONGPRESS
    UILongPressGestureRecognizer* longpress = [[UILongPressGestureRecognizer alloc]
                                               initWithTarget:self
                                               action:@selector(longPress:)];
    [self addGestureRecognizer:longpress];
#endif

    deleteControl = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,
                                                                 kEditBackMImageViewControlSize, kEditBackMImageViewControlSize)];
    deleteControl.backgroundColor = [UIColor clearColor];
    deleteControl.image = [UIImage imageNamed:@"btn_delete.png" ];
    deleteControl.userInteractionEnabled = YES;
    UITapGestureRecognizer * singleTap = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(singleTap:)];
    [deleteControl addGestureRecognizer:singleTap];
    [self addSubview:deleteControl];
    
    resizingControl = [[UIImageView alloc]initWithFrame:CGRectMake(self.frame.size.width-kEditBackMImageViewControlSize,
                                                                   self.frame.size.height-kEditBackMImageViewControlSize,
                                                                   kEditBackMImageViewControlSize, kEditBackMImageViewControlSize)];
    resizingControl.backgroundColor = [UIColor clearColor];
    resizingControl.userInteractionEnabled = YES;
    resizingControl.image = [UIImage imageNamed:@"btn_size"];
//    resizingControl.layer.cornerRadius = 16;
//    resizingControl.layer.borderColor = [UIColor blackColor].CGColor;
//    resizingControl.layer.borderWidth = 7;

    UIPanGestureRecognizer* panResizeGesture = [[UIPanGestureRecognizer alloc]
                                                initWithTarget:self
                                                action:@selector(resizeTranslate:)];
    [resizingControl addGestureRecognizer:panResizeGesture];
    [self addSubview:resizingControl];
    
    customControl = [[UIImageView alloc]initWithFrame:CGRectMake(0,self.height - kEditBackMImageViewControlSize,
                                                kEditBackMImageViewControlSize, kEditBackMImageViewControlSize)];
    customControl.backgroundColor = [UIColor clearColor];
    customControl.userInteractionEnabled = YES;
    //customControl.image = [UIImage imageNamed:@"btn_mirror.png" ];

    //UITapGestureRecognizer * customTapGesture = [[UITapGestureRecognizer alloc]
   //                                          initWithTarget:self
   //                                          action:@selector(customTap:)];
   // [customControl addGestureRecognizer:customTapGesture];
    [self addSubview:customControl];
    
    
    deltaAngle = atan2(self.frame.origin.y+self.frame.size.height - self.center.y,
                       self.frame.origin.x+self.frame.size.width - self.center.x);
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [self setupDefaultAttributes];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self setupDefaultAttributes];
    }
    return self;
}

+(CGRect)getFrameFromContentRect:(CGRect)contentRect
{
    CGFloat offx = kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2;
    CGFloat offy = kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2;
    return CGRectMake(contentRect.origin.x-offx, contentRect.origin.y-offy, contentRect.size.width+2*offx, contentRect.size.height+2*offy);
}

- (void)setContentView:(UIView *)newContentView {
    [contentView removeFromSuperview];
    contentView = newContentView;
    CGRect f = self.bounds;
    f = CGRectInset(self.bounds, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2);;
    contentView.frame = CGRectInset(self.bounds, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2);
    contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:contentView];
    
    for (UIView* subview in [contentView subviews]) {
        [subview setFrame:CGRectMake(0, 0,
                                     contentView.frame.size.width,
                                     contentView.frame.size.height)];
        subview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    
    [self bringSubviewToFront:self.borderView];
    [self bringSubviewToFront:resizingControl];
    [self bringSubviewToFront:deleteControl];
    [self bringSubviewToFront:customControl];
}

- (void)setFrame:(CGRect)newFrame {
    [super setFrame:newFrame];
    contentView.frame = CGRectInset(self.bounds, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2, kSPUserResizableViewGlobalInset + kSPUserResizableViewInteractiveBorderSize/2);
    contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    for (UIView* subview in [contentView subviews]) {
        [subview setFrame:CGRectMake(0, 0,
                                     contentView.frame.size.width,
                                     contentView.frame.size.height)];
        subview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    
    self.borderView.frame = CGRectInset(self.bounds,
                                   kSPUserResizableViewGlobalInset,
                                   kSPUserResizableViewGlobalInset);
    resizingControl.frame =CGRectMake(self.bounds.size.width-kEditBackMImageViewControlSize,
                                      self.bounds.size.height-kEditBackMImageViewControlSize,
                                      kEditBackMImageViewControlSize,
                                      kEditBackMImageViewControlSize);
    deleteControl.frame = CGRectMake(0, 0,
                                     kEditBackMImageViewControlSize, kEditBackMImageViewControlSize);
    customControl.frame =CGRectMake(0,self.bounds.size.height - kEditBackMImageViewControlSize,
                                      kEditBackMImageViewControlSize,
                                      kEditBackMImageViewControlSize);
    [self.borderView setNeedsDisplay];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    touchStart = [touch locationInView:self.superview];
    if([_delegate respondsToSelector:@selector(stickerViewDidBeginEditing:)]) {
        [_delegate stickerViewDidBeginEditing:self];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    // Notify the delegate we've ended our editing session.
    if([_delegate respondsToSelector:@selector(stickerViewDidEndEditing:)]) {
        [_delegate stickerViewDidEndEditing:self];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    // Notify the delegate we've ended our editing session.
    if([_delegate respondsToSelector:@selector(stickerViewDidCancelEditing:)]) {
        [_delegate stickerViewDidCancelEditing:self];
    }
}

- (void)translateUsingTouchLocation:(CGPoint)touchPoint {
    CGPoint newCenter = CGPointMake(self.center.x + touchPoint.x - touchStart.x,
                                    self.center.y + touchPoint.y - touchStart.y);
    if (self.preventsPositionOutsideSuperview) {
        // Ensure the translation won't cause the view to move offscreen.
        CGFloat midPointX = CGRectGetMidX(self.bounds);
        if (newCenter.x > self.superview.bounds.size.width - midPointX) {
            newCenter.x = self.superview.bounds.size.width - midPointX;
        }
        if (newCenter.x < midPointX) {
            newCenter.x = midPointX;
        }
        CGFloat midPointY = CGRectGetMidY(self.bounds);
        if (newCenter.y > self.superview.bounds.size.height - midPointY) {
            newCenter.y = self.superview.bounds.size.height - midPointY;
        }
        if (newCenter.y < midPointY) {
            newCenter.y = midPointY;
        }
    }
    self.center = newCenter;
    if ([self.delegate respondsToSelector:@selector(stickerViewDidMoveLocationCenter:)]) {
        [self.delegate stickerViewDidMoveLocationCenter:newCenter];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint touchLocation = [[touches anyObject] locationInView:self];
    if (CGRectContainsPoint(resizingControl.frame, touchLocation)) {
        return;
    }
    
    CGPoint touch = [[touches anyObject] locationInView:self.superview];
    [self translateUsingTouchLocation:touch];
    touchStart = touch;
}

- (void)hideDelHandle
{
    deleteControl.hidden = YES;
}

- (void)showDelHandle
{
    deleteControl.hidden = NO;
}

- (void)hideEditingHandles
{
    resizingControl.hidden = YES;
    deleteControl.hidden = YES;
    customControl.hidden = YES;
    [self.borderView setHidden:YES];
    self.contentView.layer.borderWidth = 0;
}

- (void)showEditingHandles
{
    if (NO == preventsCustomButton) {
        customControl.hidden = NO;
    } else {
        customControl.hidden = YES;
    }
    if (NO == preventsDeleting) {
        deleteControl.hidden = NO;
    } else {
        deleteControl.hidden = YES;
    }
    if (NO == preventsResizing) {
        resizingControl.hidden = NO;
    } else {
        resizingControl.hidden = YES;
    }
    [self.borderView setHidden:NO];
    self.contentView.layer.borderWidth = 1;

}

- (void)showCustomHandle
{
    customControl.hidden = NO;
}

- (void)hideCustomHandle
{
    customControl.hidden = YES;
}

- (void)setButton:(EditBackMImageView_BUTTONS)type image:(UIImage*)image
{
    switch (type) {
        case EditBackMImageView_BUTTON_RESIZE:
            resizingControl.image = image;
            break;
        case EditBackMImageView_BUTTON_DEL:
            deleteControl.image = image;
            break;
        case EditBackMImageView_BUTTON_CUSTOM:
            customControl.image = image;
            break;
            
        default:
            break;
    }
}

@end
