//
//  FilePathDtata.m
//  Instagrid Post
//
//  Created by macbookair on 5/8/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import "FilePathDtata.h"

@implementation FilePathDtata

+(NSString *)saveImageFile:(UIImage *)imageThumb  index:(NSInteger )index{
    
    NSString *baseP = [self createUUID];
    NSString *footPath = [NSString stringWithFormat:@"%@_%ld.jpg",baseP,index];
    NSString *filePath = [BASEPATH stringByAppendingPathComponent:footPath];
    //    if ([[aDicItem objectForKey:UIImagePickerControllerMediaType] isEqualToString:ALAssetTypePhoto]) {
    //Image filename
    [UIImageJPEGRepresentation(imageThumb, 1.0) writeToFile: filePath atomically:YES];
    return footPath;
}
+(NSString*) createUUID {
    CFUUIDRef puuid = CFUUIDCreate( nil );
    CFStringRef uuidString = CFUUIDCreateString( nil, puuid );
    NSString * result = (NSString *)CFBridgingRelease(CFStringCreateCopy( NULL, uuidString));
    CFRelease(puuid);
    CFRelease(uuidString);
    return result;
}
@end
