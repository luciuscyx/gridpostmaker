//
//  PostImagehelper.m
//  Instagrid Post
//
//  Created by macbookair on 1/8/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import "PostImagehelper.h"

@implementation PostImagehelper
/**
 *  剪切图片为正方形
 *
 *  @param image   原始图片比如size大小为(400x200)pixels
 *  @param newSize 正方形的size比如400pixels
 *
 *  @return 返回正方形图片(400x400)pixels
 */
+ (UIImage *)squareImageFromImage:(UIImage *)image scaledToSize:(CGFloat)newSize {
    CGAffineTransform scaleTransform;
    CGPoint origin;
    if (image.size.width > image.size.height) {
        // newSize = image.size.height;
        //image原始高度为200，缩放image的高度为400pixels，所以缩放比率为2
        CGFloat scaleRatio = newSize / image.size.height;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        //设置绘制原始图片的画笔坐标为CGPoint(-100, 0)pixels
        origin = CGPointMake(-(image.size.width - image.size.height) / 2.0f, 0);
    } else {
        //  newSize = image.size.width;
        CGFloat scaleRatio = newSize / image.size.width;
        scaleTransform = CGAffineTransformMakeScale(scaleRatio, scaleRatio);
        
        origin = CGPointMake(0, -(image.size.height - image.size.width) / 2.0f);
    }
    
    CGSize size = CGSizeMake(newSize, newSize);
    //创建画板为(400x400)pixels
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(size, NO,[UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(size);
    }
    CGContextRef context = UIGraphicsGetCurrentContext();
    //将image原始图片(400x200)pixels缩放为(800x400)pixels
    CGContextConcatCTM(context, scaleTransform);
    //origin也会从原始(-100, 0)缩放到(-200, 0)
    [image drawAtPoint:origin];
    //获取缩放后剪切的image图片
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
//图片缩放
+ (UIImage *)clipImage:(UIImage *)image ScaleWithsize:(CGSize)asize
{
    UIImage *newimage;
    if (nil == image) {
        newimage = nil;
    }
    else{
        CGSize oldsize = image.size;
        CGRect rect;
        if (asize.width/asize.height > oldsize.width/oldsize.height) {
            rect.size.width = asize.width;
            rect.size.height = asize.width*oldsize.height/oldsize.width;
            rect.origin.x = 0;
            rect.origin.y = (asize.height - rect.size.height)/2;
        }
        else{
            rect.size.width = asize.height*oldsize.width/oldsize.height;
            rect.size.height = asize.height;
            rect.origin.x = (asize.width - rect.size.width)/2;
            rect.origin.y = 0;
        }
        UIGraphicsBeginImageContext(asize);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextClipToRect(context, CGRectMake(0, 0, asize.width, asize.height));
        CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
        UIRectFill(CGRectMake(0, 0, asize.width, asize.height));//clear background
        [image drawInRect:rect];
        newimage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newimage;
}
// 获取视频第一帧
+(UIImage*) getVideoPreViewImage:(NSURL *)path
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:path options:nil];
    AVAssetImageGenerator *assetGen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    assetGen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [assetGen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *videoImage = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return videoImage;
}
+ (UIImage *) image: (UIImage *) image fitInSize: (CGSize) viewsize
{
    UIGraphicsBeginImageContext(viewsize);
    [image drawInRect:[self frameSize:image.size inSize:viewsize]];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
}
+ (CGRect) frameSize: (CGSize)thisSize inSize: (CGSize) aSize
{
    CGSize size = [self fitSize:thisSize inSize: aSize];
    float dWidth = aSize.width - size.width;
    float dHeight = aSize.height - size.height;
    
    return CGRectMake(dWidth / 2.0f, dHeight / 2.0f, size.width, size.height);
}
#pragma mark Base Image Utility
+ (CGSize) fitSize: (CGSize)thisSize inSize: (CGSize) aSize
{
    CGFloat scale;
    CGSize newsize = thisSize;
    
    if (newsize.height && (newsize.height > aSize.height))
    {
        scale = aSize.height / newsize.height;
        newsize.width *= scale;
        newsize.height *= scale;
    }
    
    if (newsize.width && (newsize.width >= aSize.width))
    {
        scale = aSize.width / newsize.width;
        newsize.width *= scale;
        newsize.height *= scale;
    }
    
    return newsize;
}
+ (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2  AddToX:(float )X AddToY:(float )Y{
    UIGraphicsBeginImageContext(image2.size);
    // Draw image1
    [image2 drawInRect:CGRectMake(0, 0, image2.size.width, image2.size.height)];
    
    // Draw image2Y
    [image1 drawInRect:CGRectMake(X,Y, image1.size.width, image1.size.height)];
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultingImage;
}
//- (void)centerImage:(UIImage *)image limtSize:(CGSize)size{
    
//}
+ (UIImage *) image: (UIImage *) image centerInSize: (CGSize) viewsize
{
    CGSize size = image.size;
    
    UIGraphicsBeginImageContext(viewsize);
    float dwidth = (viewsize.width - size.width) / 2.0f;
    float dheight = (viewsize.height - size.height) / 2.0f;
    
    CGRect rect = CGRectMake(dwidth, dheight, size.width, size.height);
    [image drawInRect:rect];
    
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newimg;
}
+(UIImage*)getImageByGridCount:(int)count frame:(CGRect)frame
{
    CGSize size = CGSizeMake(frame.size.width*2, frame.size.height*2);
    CGFloat colwidth = size.width/3;
    CGFloat colheight = size.height/count;
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2);  //线宽
    CGContextSetAllowsAntialiasing(context, true);
    CGContextSetRGBStrokeColor(context, 0.0 , 0.0, 0.0, 1.0);  //线的颜色
    CGContextBeginPath(context);
    for (int i = 1; i < 3; i++) {
        CGContextMoveToPoint(context, colwidth*i, 0);  //起点坐标
        CGContextAddLineToPoint(context, colwidth*i, size.height);   //终点坐标
    }
    for (int i = 1; i < count; i++) {
        CGContextMoveToPoint(context, 0, colheight*i);  //起点坐标
        CGContextAddLineToPoint(context, size.width, colheight*i);   //终点坐标
    }
    CGContextStrokePath(context);
    UIImage *retImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return retImg;
}
//分割全景图片  1*n
+(UIImage*)getHomeImageByGridCount:(int)count frame:(CGRect)frame
{
    CGSize size = CGSizeMake(frame.size.width*2, frame.size.height*2);
    CGFloat colwidth = size.width/count;
    CGFloat colheight = colwidth;
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2);  //线宽
    CGContextSetAllowsAntialiasing(context, true);
    CGContextSetRGBStrokeColor(context, 0.0 , 0.0, 0.0, 1.0);  //线的颜色
    CGContextBeginPath(context);
    for (int i = 1; i < count ; i++) {
        CGContextMoveToPoint(context, colwidth*i, 0);  //起点坐标
        CGContextAddLineToPoint(context, colwidth*i, size.height);   //终点坐标
    }
    for (int i = 1; i < 1; i++) {
        CGContextMoveToPoint(context, 0, colheight*i);  //起点坐标
        CGContextAddLineToPoint(context, size.width, colheight*i);   //终点坐标
    }
    CGContextStrokePath(context);
    UIImage *retImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return retImg;
}
#pragma mark ---- 添加水印
+(UIImage*)createWithWaterMarkonImage:(UIImage*)image{
    CGSize size = image.size;
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage* waterMarkImage = [UIImage imageNamed:@"watermark"];
    
    float scaleFactor = image.size.width / 320;
    float newHeight = waterMarkImage.size.height * scaleFactor;
    float newWidth = waterMarkImage.size.width * scaleFactor;
    if (newHeight > size.height/3) {
        newWidth = (size.height/3)*newWidth/newHeight;
        newHeight = size.height/3;
    }
    [waterMarkImage drawInRect:CGRectMake(size.width - newWidth,size.height-newHeight, newWidth,newHeight)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
+ (UIImage *) imageFromView: (UIView *) theView
{
    UIGraphicsBeginImageContext(theView.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [theView.layer renderInContext:context];
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}
+(UIImage *)addImageLogo:(UIImage *)image image:(UIImage *)logoImage rect:(CGRect)rectReal
{
    CGFloat w = image.size.width;
    CGFloat h = image.size.height;
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0, 0, w, h)];
    [logoImage drawInRect:rectReal];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newimg;
}
@end
