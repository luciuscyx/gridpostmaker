
#import <UIKit/UIKit.h>
#import "EditbBackImageDrawView.h"
#import "UIView+Frame.h"

typedef enum {
    EditBackMImageView_BUTTON_NULL,
    EditBackMImageView_BUTTON_DEL,
    EditBackMImageView_BUTTON_RESIZE,
    EditBackMImageView_BUTTON_CUSTOM,
    EditBackMImageView_BUTTON_MAX
} EditBackMImageView_BUTTONS;

@protocol EditBackMImageViewDelegate;

@interface EditBackMImageView : UIView
{
}

@property (assign, nonatomic) UIView *contentView;
@property (nonatomic) BOOL preventsPositionOutsideSuperview; //default = YES
@property (nonatomic) BOOL preventsResizing; //default = NO
@property (nonatomic) BOOL preventsDeleting; //default = NO
@property (nonatomic) BOOL preventsCustomButton; //default = YES
@property (nonatomic) CGFloat minWidth;
@property (nonatomic) CGFloat minHeight;
@property(nonatomic,strong)UIImage * orgimage;

@property (nonatomic) float deltaAngle;

@property (strong, nonatomic) UIImageView *resizingControl;
@property (strong, nonatomic) UIImageView *deleteControl;
@property (strong, nonatomic) UIImageView *customControl;

@property (strong, nonatomic) id <EditBackMImageViewDelegate> delegate;
@property(nonatomic,strong)EditbBackImageDrawView *borderView;
@property(nonatomic,assign)CGFloat angleDiff;


- (void)hideDelHandle;
- (void)showDelHandle;
- (void)hideEditingHandles;
- (void)showEditingHandles;
- (void)showCustomHandle;
- (void)hideCustomHandle;
- (void)setButton:(EditBackMImageView_BUTTONS)type image:(UIImage*)image;
+(CGRect)getFrameFromContentRect:(CGRect)contentRect;

@end

@protocol EditBackMImageViewDelegate <NSObject>
@required
@optional
- (void)stickerViewDidBeginEditing:(EditBackMImageView *)sticker;
- (void)stickerViewDidEndEditing:(EditBackMImageView *)sticker;
- (void)stickerViewDidCancelEditing:(EditBackMImageView *)sticker;
- (void)stickerViewDidClose:(EditBackMImageView *)sticker;
#ifdef EditBackMImageView_LONGPRESS
- (void)stickerViewDidLongPressed:(EditBackMImageView *)sticker;
#endif
- (void)stickerViewDidCustomButtonTap:(EditBackMImageView *)sticker;
-(void)stickerViewDidMoveLocationCenter:(CGPoint)center;
-(void)stickerViewDidTranslateBounds:(CGRect )bounds rotation:(CGFloat)Angle;

@end


