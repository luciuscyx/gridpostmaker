//
//  FilePathDtata.h
//  Instagrid Post
//
//  Created by macbookair on 5/8/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FilePathDtata : NSObject
+(NSString *)saveImageFile:(UIImage *)imageThumb  index:(NSInteger )index;
+(NSString*) createUUID;
@end

NS_ASSUME_NONNULL_END
