//
//  AppDelegate.m
//  Instagrid Post
//
//  Created by Mangal on 29/06/15.
//  Copyright (c) 2015 RJLabs. All rights reserved.
//

#import "AppDelegate.h"
#import <AdSupport/ASIdentifierManager.h>
#import "MainPostViewController.h"
#import <UMCommon/UMCommon.h>

@interface AppDelegate ()
@property (nonatomic, strong)NSDate* currentTime;
@end

@implementation AppDelegate

@synthesize window = _window;


+ (void)initialize
{
    //set the bundle ID. normally you wouldn't need to do this
    //as it is picked up automatically from your Info.plist file
    //but we want to test with an app that's actually on the store
    [iRate sharedInstance].applicationBundleID = @"com.instagridpost.rsigp";
    [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    [[iRate sharedInstance] setUsesPerWeekForPrompt:5];
    //enable preview mode
    //[iRate sharedInstance].previewMode = YES;
}

#pragma mark ---- 判断是否是中文
- (void)getCurrentLanguage
{
    self.isDomestic = [[NSUserDefaults standardUserDefaults] integerForKey:@"isDomestic"];
    if (!self.isDomestic) {
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *currentLanguage = [languages objectAtIndex:0];
    NSString *str = [currentLanguage substringToIndex:2];
    if([str isEqualToString:@"zh"]){
        self.isDomestic = 1;
    }else{
        self.isDomestic = 2;
    }
    [[NSUserDefaults standardUserDefaults] setInteger:self.isDomestic forKey:@"isDomestic"];
    }
  //  self.isDomestic = [[[NSUserDefaults standardUserDefaults] objectForKey:@"isDomestic"] boolValue];
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    //记录进入次数
    NSUserDefaults *useDef = [NSUserDefaults standardUserDefaults];
    NSInteger enterSum = [useDef integerForKey:@"enterSum"];
    if(enterSum){
        enterSum++;
    }else{
        enterSum = 1;
    }
    [useDef setInteger:enterSum forKey:@"enterSum"];
    
    //友盟统计
    [UMConfigure setEncryptEnabled:NO];//打开加密传输
    [UMConfigure setLogEnabled:NO];//设置打开日志
    [UMConfigure initWithAppkey:@"5d526a074ca3576f200001d1" channel:@"App Store"];
    
    
    //判断是否是中文
    [self getCurrentLanguage];
    NSString *strtemp =GET_USER_DEFAULTS(SHARED_IMAGE_COUNT);
    int i = [strtemp intValue];
    if (i==0)
    {
        i++;
        NSString *strSet = [NSString stringWithFormat:@"%i",i];
        SET_USER_DEFAULTS(SHARED_IMAGE_COUNT,strSet);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }

    // Register for Push Notitications
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    MainPostViewController *MainVC = [[MainPostViewController alloc] init];
    BOOL isExistingUser = ([[NSUserDefaults standardUserDefaults] objectForKey:@"openCount"] != nil);
    if (isExistingUser) {
     //   [Tune setExistingUser:YES];
    }
    UINavigationController *objNavigationController = [[UINavigationController alloc] initWithRootViewController:MainVC];
    objNavigationController.navigationBarHidden = YES;
    [_window setRootViewController:objNavigationController];
    [_window makeKeyAndVisible];
    
    self.oneSignal = [OneSignal initWithLaunchOptions:launchOptions appId:@"79abfcbd-23c6-4b94-9b90-433072a4abee"];
//    self.oneSignal = [[OneSignal alloc] initWithLaunchOptions:launchOptions
//                                                        appId:@"b985f045-6e2e-4efa-ba30-e9188399b9af"
//                                           handleNotification:nil];
    
//    [OneSignal promptForPushNotificationsWithUserResponse:^(BOOL accepted) {
//        NSLog(@"User accepted notifications: %d", accepted);
//    }];
    return YES;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
   // [Tune applicationDidOpenURL:[url absoluteString] sourceApplication:sourceApplication];
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
  
    
}
- (void)applicationWillResignActive:(UIApplication *)application
{

}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    //NSLog(@"%@",deviceToken);

}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler {
    if (userInfo == nil) {
        return;
    }
    NSDictionary *aps = [userInfo valueForKey:@"aps"];
    if (aps == nil) {
        return;
    }
     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"enterFromPush"];
    
}
/*
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSDictionary *dic = (NSDictionary *)[userInfo objectForKey:@"aps"];
    if (!dic) {
        return;
    }
    NSString *custom = (NSString *)[dic objectForKey:@"alert"];
    //title,,msg,,operator,,confirmButton,,operationContent:::get the app now;;get the app now;;cross;;get it;;https://itunes.apple.com/app/id835915067
    //title,,msg,,operator,,confirmButton,,operationContent:::get the app now;;get the app now;;iap;;get it;;com.bestimage.ghostlens.prolayouts
    if (custom) {
        NSArray * cut1 = [custom componentsSeparatedByString:@":::"];
        if (cut1.count < 2) {
            return;
        }
        NSString * cut21 = (NSString*)[cut1 objectAtIndex:0];
        NSString * cut22 = (NSString*)[cut1 objectAtIndex:1];
        NSArray * cut31 = [cut21 componentsSeparatedByString:@",,"];
        NSArray * cut32 = [cut22 componentsSeparatedByString:@";;"];
        if (cut31.count != cut32.count) {
            return;
        }
        NSDictionary * aps = [NSDictionary dictionaryWithObjects:cut32 forKeys:cut31];
        NSString * msg = [aps objectForKey:@"msg"];
        NSString * title = [aps objectForKey:@"title"];
        NSString * operator = [aps objectForKey:@"operator"];
        
        operationContent = [aps objectForKey:@"operationContent"];
        NSString * confirmButton = [aps objectForKey:@"confirmButton"];
        
        UIAlertView *baseAlert = [[UIAlertView alloc]
                                  initWithTitle:title message:msg
                                  delegate:self cancelButtonTitle:nil
                                  otherButtonTitles:confirmButton,@"Cancel",nil];
        if ([@"cross" isEqualToString:operator]) {
            baseAlert.tag = 1;
        }else if ([@"iap" isEqualToString:operator]) {
            baseAlert.tag = 2;
        }
        
        [baseAlert show];
    }
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        if (buttonIndex == 0){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:operationContent]];
        }
    }else if (alertView.tag == 2) {

    }
}
*/

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.instagridpostdev.Instagrid_Post" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Instagrid_Post" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Instagrid_Post.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}
- (BOOL)IsCH{
    NSUserDefaults* defs = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [defs objectForKey:@"AppleLanguages"];
    NSString* preferredLang = [languages objectAtIndex:0];
    if ([preferredLang containsString:@"zh-Han"]) {
        return YES;
    }
    return NO;
}
@end
