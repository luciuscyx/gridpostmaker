//
//  AppDelegate.h
//  Instagrid Post
//
//  Created by Mangal on 29/06/15.
//  Copyright (c) 2015 RJLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <OneSignal/OneSignal.h>

#define APPGate (AppDelegate*)[UIApplication sharedApplication].delegate

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSString * operationContent;
}

@property (strong, nonatomic) IBOutlet UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, assign) BOOL isRateAppPressed;
@property (strong, nonatomic) OneSignal *oneSignal;
@property (nonatomic,assign) NSInteger isDomestic;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

//是否是中文
- (BOOL)IsCH;

@end

