//


//#import "KnockOutCLImageToolBase.h"
//#import "ViewController.h"
#import "CLImageEditorTheme+Private.h"
#import "UIView+Frame.h"
@protocol GhostLensTextCustomToolDelegate
-(void)addNewTextView:(UIView *)txtView;
-(void)unlockallfont;
@end
@interface GhostLensTextCustomTool : UIView <UIActionSheetDelegate>
@property (nonatomic, assign) BOOL isUsing;
@property (nonatomic, retain) id <GhostLensTextCustomToolDelegate> delegate;
- (void)setup:(CGRect)menuScrollRect menuColor:(UIColor*)menuColor workingView:(UIView *)workingView SuperView:(UIView *)superview;
-(void)setActiveOfTextView:(BOOL)isbool;
- (id)initWithImageEditor:(UIViewController*)editor withToolInfo:(CLImageToolInfo*)info;
+(void)setTxtViewActive:(UIView*)txtView active:(BOOL)active;
- (void)cleanup;
-(void)hiddenSettingView;
-(void)setTextViewFront;
-(void)setAllTextViewFront;
-(void)setTextViewSend;
-(void)setAllTextViewSend;
-(void)setTextUsingBool:(BOOL)isbool;
-(BOOL)getTextUsingBool;
-(void)showTextView;
-(void)turnOffSettingView:(CGPoint)pt fromView:(UIView*)fromView;
-(BOOL)isShowingMenu;
-(void)hideTextView;
+ (BOOL)checkIfTextCustomView:(UIView*)view;
@end
