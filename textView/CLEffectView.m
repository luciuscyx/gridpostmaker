//  FontVC
//  BeautyHour
//
//  Created by Johnny Xu(徐景周) on 7/23/14.
//  Copyright (c) 2014 Future Studio. All rights reserved.
//

#import "CLEffectView.h"
#import "FXLabel.h"
@interface CLEffectView ()<UIScrollViewDelegate>
{
    UIScrollView *scrollView;
}

@end

@implementation CLEffectView
#define kLightBlue [UIColor colorWithRed:155/255.0f green:188/255.0f blue:220/255.0f alpha:1]
#define kShadowOffset        CGSizeMake(0.0, UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 4.0 : 2.0)
#define kShadowBlur            (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 10.0 : 5.0)

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setup];
        
    }
    return self;
}
-(void)setup
{
    scrollView = [[UIScrollView alloc]initWithFrame:self.bounds];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.userInteractionEnabled = YES;
    scrollView.delegate = self;
    NSArray *customFontArr = @[@"Default",@"Shadow",@"Emboss",@"Gradient",@"Rainbow",@"Clouds"];;
    CGFloat x = 0;
    for (int i = 0; i < customFontArr.count ; i++) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 44 * i, self.frame.size.width, 44)];
        view.backgroundColor =[UIColor clearColor];
        view.tag = 100 + i;
        if (i == 0) {
            view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
        }
        NSString * fontName = customFontArr[i];
        FXLabel *titleLabel = [[FXLabel alloc] initWithFrame:view.bounds];
        x = x + 44;
        titleLabel.text = NSLocalizedString(fontName,"");
        titleLabel.tag = 2000;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        // Effect Font
        switch (i + 1)
        {
            case 1:
            {
                titleLabel.shadowColor = kLightBlue;
                titleLabel.shadowOffset = kShadowOffset;
                titleLabel.shadowBlur = 8.0f;
                
                break;
            }
            case 2:
            {
                titleLabel.shadowOffset = CGSizeMake(0.0f, 2.0f);
                titleLabel.shadowColor = [UIColor colorWithWhite:0.0f alpha:0.75f];
                titleLabel.shadowBlur = 5.0f;
                
                break;
            }
            case 3:
            {
                titleLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:0.8f];
                titleLabel.shadowOffset = CGSizeMake(1.0f, 1.0f);
                titleLabel.shadowBlur = 2.0f;
                titleLabel.innerShadowBlur = 2.0f;
                titleLabel.innerShadowColor = [UIColor colorWithWhite:0.0f alpha:0.8f];
                titleLabel.innerShadowOffset = CGSizeMake(1.0f, 1.0f);
                
                break;
            }
            case 4:
            {
                titleLabel.gradientStartColor = [UIColor yellowColor];
                titleLabel.gradientEndColor = [UIColor blueColor];
                
                break;
            }
            case 5:
            {
                titleLabel.gradientStartPoint = CGPointMake(0.0f, 0.0f);
                titleLabel.gradientEndPoint = CGPointMake(1.0f, 1.0f);
                titleLabel.gradientColors = @[[UIColor redColor],
                                              [UIColor yellowColor],
                                              [UIColor greenColor],
                                              [UIColor cyanColor],
                                              [UIColor blueColor],
                                              [UIColor purpleColor],
                                              [UIColor redColor]];
                
                break;
            }
            case 6:
            {
                titleLabel.shadowColor = [UIColor blackColor];
                titleLabel.shadowOffset = kShadowOffset;
                titleLabel.shadowBlur = kShadowBlur;
                titleLabel.innerShadowBlur = 2.0f;
                titleLabel.innerShadowColor = [UIColor whiteColor];
                titleLabel.innerShadowOffset = CGSizeMake(1.0f, 1.0f);
                titleLabel.gradientStartColor = [UIColor redColor];
                titleLabel.gradientEndColor = [UIColor yellowColor];
                titleLabel.gradientStartPoint = CGPointMake(0.0f, 0.5f);
                titleLabel.gradientEndPoint = CGPointMake(1.0f, 0.5f);
                titleLabel.oversampling = 2;
                
                break;
            }
            default:
            {
                break;
            }
        }
        [view addSubview:titleLabel];
        UIButton * cateBtn = [[UIButton alloc]initWithFrame:view.bounds];
        cateBtn.backgroundColor = [UIColor clearColor];
        [cateBtn addTarget:self action:@selector(refreshLabel:) forControlEvents:UIControlEventTouchUpInside];
        cateBtn.tag = i;
        [view addSubview:titleLabel];
        [view addSubview:cateBtn];
        [scrollView addSubview:view];
        
    }
    scrollView.contentSize = CGSizeMake(0, x);
    [self addSubview:scrollView];
}

-(void)refreshLabel:(UIButton *)sender
{
    for (UIView *view in scrollView.subviews) {
        if ([view isKindOfClass:[UIView class]]) {
            if (view.tag == sender.tag + 100) {
                view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
            }else{
                view.backgroundColor = [UIColor clearColor];
            }
        }
    }
    if (self.fontSuccessBlock)
    {
        self.fontSuccessBlock(TRUE, (int)sender.tag);
    }
}

-(void)changeEffectButSelect
{
    for (UIView *view in scrollView.subviews) {
        if ([view isKindOfClass:[UIView class]]) {
            if (view.tag == 100) {
                view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
            }else{
                view.backgroundColor = [UIColor clearColor];
            }
        }
    }
}

@end
