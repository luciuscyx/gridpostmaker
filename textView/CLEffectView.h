


#import <UIKit/UIKit.h>
typedef void(^FontCallback)(BOOL success, int result);
@interface CLEffectView : UIView
@property (copy, nonatomic) FontCallback fontSuccessBlock;
-(void)changeEffectButSelect;
@end
