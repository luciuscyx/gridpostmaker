//
//  GhostLensTextCustomSettingView.m
//
//  Created by Kevin Siml - Appzer.de on 2015/12/18.
//  Copyright (c) 2015 Appzer.de. All rights reserved.
//

#import "GhostLensTextCustomSettingView.h"
#import "Utility.h"
#import "UIView+Frame.h"
#import "CLImageEditorTheme.h"
#import "CLColorPickerView.h"
#import "CLFontPickerView.h"
#import "CLCircleView.h"
#import "CLEffectView.h"
@interface GhostLensTextCustomSettingView()
<CLColorPickerViewDelegate, CLFontPickerViewDelegate, UITextViewDelegate>
@property (nonatomic, strong) UIView *selectedMode;
@end


@implementation GhostLensTextCustomSettingView
{
    UIScrollView *_scrollView;
    CLColorPickerView *_colorPickerView;
    CLFontPickerView *_fontPickerView;
    CLEffectView *_effectPickerView;
    UIView *_colorPanel;
    CLCircleView *_fillCircle;
    CLCircleView *_pathCircle;
    UISlider *_pathSlider;
    BOOL keyboardShown;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

- (UIImage*)SliderBackground
{
    CGSize size = _pathSlider.frame.size;
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIColor *color = [[[CLImageEditorTheme theme] toolbarTextColor] colorWithAlphaComponent:0.33];
    
    CGFloat strRadius = size.height/2 * 0.4;;
    CGFloat endRadius = size.height/2 * 0.4;
    
    CGPoint strPoint = CGPointMake(strRadius, size.height/2);
    CGPoint endPoint = CGPointMake(size.width-endRadius - 1, strPoint.y);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddArc(path, NULL, strPoint.x, strPoint.y, strRadius, -M_PI/2, M_PI-M_PI/2, YES);
    CGPathAddLineToPoint(path, NULL, endPoint.x, endPoint.y + endRadius);
    CGPathAddArc(path, NULL, endPoint.x, endPoint.y, endRadius, M_PI/2, M_PI+M_PI/2, YES);
    CGPathAddLineToPoint(path, NULL, strPoint.x, strPoint.y - strRadius);
    
    CGPathCloseSubpath(path);
    
    CGContextAddPath(context, path);
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillPath(context);
    
    UIImage *tmp = UIGraphicsGetImageFromCurrentImageContext();
    
    CGPathRelease(path);
    
    UIGraphicsEndImageContext();
    
    return tmp;
}

- (void)setColorPanel
{
    _colorPickerView = [[CLColorPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 160)];
    _colorPickerView.delegate = self;
    _colorPickerView.center = CGPointMake(_colorPanel.width/2 - 10, _colorPickerView.height/2 - 5);
    [_colorPanel addSubview:_colorPickerView];
    
    _pathSlider = [[UISlider alloc] initWithFrame:CGRectMake(0, 0, _colorPickerView.width*0.8, 34)];
    _pathSlider.center = CGPointMake(_colorPickerView.center.x, _colorPickerView.bottom + 5);
    _pathSlider.minimumValue = 0;
    _pathSlider.maximumValue = 0.2;
    _pathSlider.value = 0;
    _pathSlider.backgroundColor = [UIColor colorWithPatternImage:[self SliderBackground]];
    [_pathSlider addTarget:self action:@selector(pathSliderDidChange:) forControlEvents:UIControlEventValueChanged];
    [_colorPanel addSubview:_pathSlider];
    
    _pathCircle = [[CLCircleView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    _pathCircle.right = _colorPanel.width - 10;
    _pathCircle.bottom = _pathSlider.center.y;
    _pathCircle.radius = 0.6;
    _pathCircle.borderWidth = 2;
    _pathCircle.borderColor = [UIColor blackColor];
    _pathCircle.color = [UIColor clearColor];
    [_colorPanel addSubview:_pathCircle];
    
    _fillCircle = [[CLCircleView alloc] initWithFrame:_pathCircle.frame];
    _fillCircle.bottom = _pathCircle.top;
    _fillCircle.radius = 0.6;
    [_colorPanel addSubview:_fillCircle];
    
    [_pathCircle addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(modeViewTapped:)]];
    [_fillCircle addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(modeViewTapped:)]];
    
    _fillCircle.tag = 0;
    _pathCircle.tag = 1;
    self.selectedMode = _fillCircle;
}

- (void)customInit
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    _scrollView.pagingEnabled = YES;
    _scrollView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.scrollEnabled = NO;
    [self addSubview:_scrollView];
    
    _textView = [[UITextView alloc] initWithFrame:CGRectMake(10, 0, self.width-42, 80)];
    _textView.delegate = self;
    _textView.backgroundColor = [UIColor clearColor];
    _textView.textColor = [UIColor blackColor];
    [_scrollView addSubview:_textView];
    
    _colorPanel = [[UIView alloc] initWithFrame:CGRectMake(self.width, 0, self.width, self.height)];
    _colorPanel.backgroundColor = [UIColor clearColor];
    [_scrollView addSubview:_colorPanel];
    [self setColorPanel];
    
    _fontPickerView = [[CLFontPickerView alloc] initWithFrame:CGRectMake(self.width * 2, 0, self.width, self.height)];
    _fontPickerView.delegate = self;
    _fontPickerView.sizeComponentHidden = YES;
    [_scrollView addSubview:_fontPickerView];
    
    if (_effectPickerView) {
        return;
    }
    __weak __typeof(self)weakself = self;
    _effectPickerView = [[CLEffectView alloc] initWithFrame:CGRectMake(self.width * 3, 0, self.width, self.height)];
    // Callback
    [_effectPickerView setFontSuccessBlock:^(BOOL success, int result)
     {
         if (success)
         {
             NSLog(@"%d", result);
             
             if([weakself.delegate respondsToSelector:@selector(textSettingView:didChangeEffect:)]){
                 [weakself.delegate textSettingView:weakself didChangeEffect:result];
             }
         }
     }];
    [_scrollView addSubview:_effectPickerView];
    
    _scrollView.contentSize = CGSizeMake(self.width * 4, self.height);
}
-(void)changeEffectButSelect
{
   // [_effectPickerView changeEffectButSelect];
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setTextColor:(UIColor*)textColor
{
   // _fontPickerView.textColor = textColor;
   // _textView.textColor = textColor;
}

- (BOOL)isFirstResponder
{
    return _textView.isFirstResponder;
}

- (BOOL)becomeFirstResponder
{
    return [_textView becomeFirstResponder];
}

- (BOOL)resignFirstResponder
{
    return [_textView resignFirstResponder];
}

- (void)modeViewTapped:(UITapGestureRecognizer*)sender
{
    self.selectedMode = sender.view;
}

#pragma mark - Properties

- (void)setSelectedMode:(UIView *)selectedMode
{
    if(selectedMode != _selectedMode){
        _selectedMode.backgroundColor = [UIColor clearColor];
        _selectedMode = selectedMode;
        _selectedMode.backgroundColor = [[CLImageEditorTheme theme] toolbarSelectedButtonColor];
        
        if(_selectedMode==_fillCircle){
            _colorPickerView.color = _fillCircle.color;
        }
        else{
            _colorPickerView.color = _pathCircle.borderColor;
        }
    }
}

- (void)setSelectedText:(NSString *)selectedText
{
    _textView.text = selectedText;
}

- (NSString*)selectedText
{
    return _textView.text;
}

- (void)setSelectedFillColor:(UIColor *)selectedFillColor
{
    _fillCircle.color = selectedFillColor;
    
    if(self.selectedMode==_fillCircle){
        _colorPickerView.color = _fillCircle.color;
    }
}

- (UIColor*)selectedFillColor
{
    return _fillCircle.color;
}

- (void)setSelectedBorderColor:(UIColor *)selectedBorderColor
{
    _pathCircle.borderColor = selectedBorderColor;
    
    if(self.selectedMode==_pathCircle){
        _colorPickerView.color = _pathCircle.borderColor;
    }
}

- (UIColor*)selectedBorderColor
{
    return _pathCircle.borderColor;
}

- (void)setSelectedBorderWidth:(CGFloat)selectedBorderWidth
{
    _pathSlider.value = selectedBorderWidth;
}

- (CGFloat)selectedBorderWidth
{
    return _pathSlider.value;
}

- (void)setSelectedFont:(UIFont *)selectedFont
{
    _fontPickerView.font = selectedFont;
}

- (UIFont*)selectedFont
{
    return _fontPickerView.font;
}

- (void)setFontPickerForegroundColor:(UIColor*)foregroundColor
{
   // _fontPickerView.foregroundColor = foregroundColor;
}

- (void)showSettingMenuWithIndex:(NSInteger)index animated:(BOOL)animated
{
    [_scrollView setContentOffset:CGPointMake(index * self.width, 0) animated:animated];
}

#pragma mark - keyboard events

- (void)keyBoardWillShow:(NSNotification *)notificatioin
{
    if (keyboardShown) {
        return;
    }
    keyboardShown = YES;
    [self keyBoardWillChange:notificatioin withTextViewHeight:self.height - 20];
    [_textView scrollRangeToVisible:_textView.selectedRange];
}

- (void)keyBoardWillHide:(NSNotification *)notificatioin
{
    keyboardShown = NO;
    [self keyBoardWillChange:notificatioin withTextViewHeight:self.height - 20];
}

- (void)keyBoardWillChange:(NSNotification *)notificatioin withTextViewHeight:(CGFloat)height
{
    CGRect keyboardFrame = [[notificatioin.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrame = [self.superview convertRect:keyboardFrame fromView:self.window];
    CGFloat deltaY = keyboardFrame.size.height;
    UIViewAnimationCurve animationCurve = [[notificatioin.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    double duration = [[notificatioin.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
   // if (self.isPhotoEdit) {
        [UIView animateWithDuration:duration
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState | (animationCurve<<16)
                         animations:^{
                             _textView.height = height;
                             CGFloat dy = MIN(0, keyboardFrame.origin.y - _textView.height - self.top);
                             //CGFloat dy = MIN(0, keyboardFrame.origin.y - self.top - 40);
                             self.transform = CGAffineTransformMakeTranslation(0,dy);
                         } completion:^(BOOL finished) {
                             
                         }
         ];
//    }else{
//        [UIView animateWithDuration:duration
//                              delay:0
//                            options:UIViewAnimationOptionBeginFromCurrentState | (animationCurve<<16)
//                         animations:^{
//                             _textView.height = height;
//                             CGFloat dy = MIN(0, (keyboardFrame.origin.y - _textView.height) - self.top);
//                             if (height == 80) {
//                                 self.transform = CGAffineTransformMakeTranslation(0, - 80 + 49);
//                             }else{
//                                 self.transform = CGAffineTransformMakeTranslation(0, dy);
//                             }
//                         } completion:^(BOOL finished) {
//                             
//                         }
//         ];
//    }
    
}

#pragma mark- Color picker delegate

- (void)colorPickerView:(CLColorPickerView *)picker colorDidChange:(UIColor *)color
{
    if(self.selectedMode==_fillCircle){
        _fillCircle.color = color;
        if([self.delegate respondsToSelector:@selector(textSettingView:didChangeFillColor:)]){
            [self.delegate textSettingView:self didChangeFillColor:color];
        }
    }
    else{
        _pathCircle.borderColor = color;
        if([self.delegate respondsToSelector:@selector(textSettingView:didChangeBorderColor:)]){
            [self.delegate textSettingView:self didChangeBorderColor:color];
        }
    }
}

#pragma mark- PathSlider event

- (void)pathSliderDidChange:(UISlider*)sender
{
    if([self.delegate respondsToSelector:@selector(textSettingView:didChangeBorderWidth:)]){
        [self.delegate textSettingView:self didChangeBorderWidth:_pathSlider.value];
    }
}

#pragma mark- Font picker delegate

- (void)fontPickerView:(CLFontPickerView *)pickerView didSelectFont:(UIFont *)font fontIndex:(int)fontIndex
{
    if([self.delegate respondsToSelector:@selector(textSettingView:didChangeFont:fontIndex:)]){
        [self.delegate textSettingView:self didChangeFont:font fontIndex:fontIndex];
    }
}

- (void)charSliderDidChange:(CGFloat)value
{
    if([self.delegate respondsToSelector:@selector(textSettingView:didChangeCharSpace:)]){
        [self.delegate textSettingView:self didChangeCharSpace:value];
    }
}
- (void)lineSliderDidChange:(CGFloat)value
{
    if([self.delegate respondsToSelector:@selector(textSettingView:didChangeLineSpace:)]){
        [self.delegate textSettingView:self didChangeLineSpace:value];
    }
}

#pragma mark- UITextView delegate

- (void)textViewDidChange:(UITextView*)textView
{
    NSRange selection = textView.selectedRange;
    if(selection.location+selection.length == textView.text.length && [textView.text characterAtIndex:textView.text.length-1] == '\n') {
        [textView layoutSubviews];
        [textView scrollRectToVisible:CGRectMake(0, textView.contentSize.height - 1, 1, 1) animated:YES];
    }
    else {
        [textView scrollRangeToVisible:textView.selectedRange];
    }
    if (textView.text && textView.text.length > 800) {
        [self popWarning:self.superview labelText:NSLocalizedString(@"Max 100 Chars", "")  fontSize:13];
        return;
    }
    if([self.delegate respondsToSelector:@selector(textSettingView:didChangeText:)]){
        [self.delegate textSettingView:self didChangeText:textView.text];
    }
}
- (void)popWarning:(UIView *)bgView labelText:(NSString *)text fontSize:(float)fontsize
{
    float textWitdh = 120;
    if (text.length>15&text.length<=25) {
        textWitdh = 140;
    } else if(text.length>25){
        textWitdh = 180;
    }
    
    CGRect frame = [text boundingRectWithSize:CGSizeMake(textWitdh,MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Avenir-Book" size:fontsize]} context:nil];
    
    UIView *warningView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, textWitdh+20, frame.size.height+25+35)];
    warningView.center = CGPointMake(bgView.center.x, bgView.center.y);
    warningView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
    warningView.layer.masksToBounds = YES;
    warningView.layer.cornerRadius = 10.0;
    
    UIImageView *wImageView = [[UIImageView alloc]initWithFrame:CGRectMake(warningView.frame.size.width/2-15, 10, 30, 30)];
    wImageView.image = [UIImage imageNamed:@"BLSImage.bundle/warning"];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 45, textWitdh, frame.size.height)];
    label.numberOfLines = 0;
    label.font = [UIFont fontWithName:@"Avenir-Book" size:fontsize];
    label.text = text;
    label.textColor = [UIColor whiteColor];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.textAlignment = NSTextAlignmentCenter;
    
    [warningView addSubview:wImageView];
    [warningView addSubview:label];
    [bgView addSubview:warningView];
    
    [UIView animateWithDuration:0.2
                          delay:0.2
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         warningView.transform = CGAffineTransformMakeScale(0.9,0.9);
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.5
                                               delay:0.5
                                             options: UIViewAnimationOptionCurveEaseIn
                                          animations:^{
                                              warningView.alpha = 0;
                                          }
                                          completion:^(BOOL finished){
                                              [warningView removeFromSuperview];
                                          }];
                         
                     }];
}
@end
