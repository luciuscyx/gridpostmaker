//
//  CLFontPickerView.m
//
//  Created by Kevin Siml - Appzer.de on 2015/12/14.
//  Copyright (c) 2015 Appzer.de. All rights reserved.
//

#import "CLFontPickerView.h"
//#import "SHKActivityIndicator.h"
#import "UIView+Frame.h"
#import "CLPickerView.h"
#import "Utility.h"

const CGFloat kCLFontPickerViewConstantFontSize = 14;

@interface CLFontPickerView()
<CLPickerViewDelegate, CLPickerViewDataSource>
@end

@implementation CLFontPickerView
{
    CLPickerView *_pickerView;
    NSInteger startIndex;
}

+ (NSArray*)allFontList
{
    NSMutableArray *list = [NSMutableArray array];
    NSArray * oftenList = [[NSUserDefaults standardUserDefaults]objectForKey:@"oftenFontList"];
    if (oftenList&&oftenList.count > 0) {
        for(NSString *fontName in oftenList){
            if (![fontName containsString:@"GujaratiSangamMN"]) {
                [list addObject:[UIFont fontWithName:fontName size:kCLFontPickerViewConstantFontSize]];
            }
            
        }
        NSMutableArray *list2 = [NSMutableArray array];
        for(NSString *familyName in [UIFont familyNames]){
            for(NSString *fontName in [UIFont fontNamesForFamilyName:familyName]){
                if (![fontName containsString:@"GujaratiSangamMN"]&&![oftenList containsObject:fontName]) {
                    [list2 addObject:[UIFont fontWithName:fontName size:kCLFontPickerViewConstantFontSize]];
                }
            }
        }
        return [list arrayByAddingObjectsFromArray:[list2 sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"fontName" ascending:YES]]]];
    }else
    {
        for(NSString *familyName in [UIFont familyNames]){
            for(NSString *fontName in [UIFont fontNamesForFamilyName:familyName]){
                if (![fontName containsString:@"GujaratiSangamMN"]) {
                    [list addObject:[UIFont fontWithName:fontName size:kCLFontPickerViewConstantFontSize]];
                }
                
                //NSLog(@"%@", fontName);
            }
        }
        return [list sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"fontName" ascending:YES]]];
    }
    
}

+ (NSArray*)defaultSizes
{
    return @[@8, @10, @12, @14, @16, @18, @20, @24, @28, @32, @38, @44, @50];
}

+ (UIFont*)defaultFont
{
    NSArray * oftenList = [[NSUserDefaults standardUserDefaults]objectForKey:@"oftenFontList"];
    if (oftenList&&oftenList.count > 0) {
        NSString *fontName = [oftenList firstObject];
        return [UIFont fontWithName:fontName size:kCLFontPickerViewConstantFontSize];
    }
    return [UIFont fontWithName:@"Helvetica"size:kCLFontPickerViewConstantFontSize];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
        float sliderheight = self.bounds.size.height/4;
        _pickerView = [[CLPickerView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-sliderheight-20)];
        _pickerView.center = CGPointMake(self.width/2, self.height/2);
        _pickerView.backgroundColor = [UIColor clearColor];
        _pickerView.dataSource = self;
        _pickerView.delegate = self;
        [self addSubview:_pickerView];
        
        UIImageView * icon1 = [[UIImageView alloc] initWithFrame:CGRectMake(10, _pickerView.frame.origin.y+_pickerView.frame.size.height+(sliderheight-25)/2, 25, 25)];
        icon1.image = [UIImage imageNamed:@"icon_char_space"];
        [self addSubview:icon1];
        
        float orgx = 5+icon1.frame.origin.x+icon1.frame.size.width;
        UISlider *slider = [[UISlider alloc] initWithFrame:CGRectMake(orgx, 0, self.bounds.size.width/2-orgx-30, 30)];
        
        slider.continuous = YES;
        [slider addTarget:self action:@selector(charSliderDidChange:) forControlEvents:UIControlEventValueChanged];
        slider.center = CGPointMake(slider.center.x, icon1.center.y);
        slider.maximumValue = 100;
        slider.minimumValue = 0;
        slider.value = 0;
       // [Utility setSliderImage:slider];
        [self addSubview:slider];
        
        icon1 = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width/2+10, _pickerView.frame.origin.y+_pickerView.frame.size.height+(sliderheight-25)/2, 25, 25)];
        icon1.image = [UIImage imageNamed:@"icon_line_height"];
        [self addSubview:icon1];
        
        orgx = 5+icon1.frame.origin.x+icon1.frame.size.width;
        slider = [[UISlider alloc] initWithFrame:CGRectMake(orgx, 0, self.bounds.size.width-orgx-30, 30)];
        slider.center = CGPointMake(slider.center.x, icon1.center.y);
        slider.continuous = YES;
        [slider addTarget:self action:@selector(lineSliderDidChange:) forControlEvents:UIControlEventValueChanged];
        
        slider.maximumValue = 160;
        slider.minimumValue = 0;
        slider.value = 0;
        //[Utility setSliderImage:slider];
        [self addSubview:slider];
        
        self.fontList = [self.class allFontList];
        self.fontSizes = [self.class defaultSizes];
        self.font = [self.class defaultFont];
        self.foregroundColor = [UIColor colorWithWhite:1 alpha:0.5];
        self.textColor = [UIColor blackColor];
        startIndex = [_pickerView selectedRowInComponent:0];
    }
    return self;
}

-(IBAction)charSliderDidChange:(id)sender
{
    [self.delegate charSliderDidChange:((UISlider*)sender).value];
}

-(IBAction)lineSliderDidChange:(id)sender
{
    [self.delegate lineSliderDidChange:((UISlider*)sender).value];
}

- (void)setForegroundColor:(UIColor *)foregroundColor
{
    _pickerView.foregroundColor = foregroundColor;
}

- (UIColor*)foregroundColor
{
    return _pickerView.foregroundColor;
}

- (void)setFontList:(NSArray *)fontList
{
    if(fontList != _fontList){
        _fontList = fontList;
        [_pickerView reloadComponent:0];
    }
}

- (void)setFontSizes:(NSArray *)fontSizes
{
    if(fontSizes != _fontSizes){
        _fontSizes = fontSizes;
        [_pickerView reloadComponent:1];
    }
}

- (void)setFont:(UIFont *)font
{
    UIFont *tmp = [font fontWithSize:kCLFontPickerViewConstantFontSize];
    
    NSInteger fontIndex = [self.fontList indexOfObject:tmp];
    if(fontIndex==NSNotFound){ fontIndex = 0; }
    
    NSInteger sizeIndex = 0;
    for(sizeIndex=0; sizeIndex<self.fontSizes.count; sizeIndex++){
        if(font.pointSize<=[self.fontSizes[sizeIndex] floatValue]){
            break;
        }
    }
    
    [_pickerView selectRow:fontIndex inComponent:0 animated:NO];
    [_pickerView selectRow:sizeIndex inComponent:1 animated:NO];
}

- (UIFont*)font
{
    UIFont *font = self.fontList[[_pickerView selectedRowInComponent:0]];
    CGFloat size = [self.fontSizes[[_pickerView selectedRowInComponent:1]] floatValue];
    return [font fontWithSize:size];
}

- (void)setSizeComponentHidden:(BOOL)sizeComponentHidden
{
    _sizeComponentHidden = sizeComponentHidden;
    
    [_pickerView setNeedsLayout];
}

#pragma mark- UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(CLPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(CLPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return self.fontList.count;
        case 1:
            return self.fontSizes.count;
    }
    return 0;
}

#pragma mark- UIPickerViewDelegate

- (CGFloat)pickerView:(CLPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return self.height/4;
}

- (CGFloat)pickerView:(CLPickerView *)pickerView widthForComponent:(NSInteger)component
{
    CGFloat ratio = self.sizeComponentHidden ? 1 : 0.8;
    switch (component) {
        case 0:
            return self.width*ratio;
        case 1:
            return self.width*(1-ratio);
    }
    return 0;
}

-(BOOL)checkIfOftenList:(NSString*)fontName
{
    NSArray * oftenList = [[NSUserDefaults standardUserDefaults]objectForKey:@"oftenFontList"];
    if (oftenList) {
        if ([oftenList containsObject:fontName]) {
            return YES;
        }
    }
    return NO;
}

-(IBAction)oftenSwitchChange:(id)sender
{
    UISwitch * switchbtn = sender;
    if (switchbtn.tag >=0 && switchbtn.tag < self.fontList.count) {
        UIFont * cfont = self.fontList[switchbtn.tag];
        NSString * fontName = cfont.fontName;
        NSArray * list = [[NSUserDefaults standardUserDefaults]objectForKey:@"oftenFontList"];
        NSMutableArray * oftenList = [NSMutableArray arrayWithArray:list];
        if (!oftenList) {
            oftenList = [NSMutableArray new];
        }
        if (switchbtn.on) {
            if (![oftenList containsObject:fontName]) {
                [oftenList addObject:fontName];
            }
        }else
        {
            if ([oftenList containsObject:fontName]) {
                [oftenList removeObject:fontName];
            }
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:[oftenList copy] forKey:@"oftenFontList"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (UIView*)pickerView:(CLPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *lbl = nil;
    UIView * retView;
    
    switch (component) {
        case 0:
        {
            UIView *pview = view;
            UISwitch * often;
            if (!pview) {
                CGFloat W = [self pickerView:pickerView widthForComponent:component];
                CGFloat H = [self pickerView:pickerView rowHeightForComponent:component];
                CGFloat dx = 10;
                pview = [[UIView alloc] initWithFrame:CGRectMake(dx, 0, W-2*dx, H)];
                pview.backgroundColor = [UIColor clearColor];
                lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pview.frame.size.width*3/4, H)];
                lbl.backgroundColor = [UIColor clearColor];
                lbl.adjustsFontSizeToFitWidth = YES;
                lbl.minimumScaleFactor = 0.5;
                lbl.textAlignment = NSTextAlignmentCenter;
                lbl.textColor = self.textColor;
                [pview addSubview:lbl];
                often = [[UISwitch alloc] initWithFrame:CGRectMake(lbl.frame.size.width+lbl.frame.origin.x, H/8, pview.frame.size.width/8, H)];
                [pview addSubview:often];
                often.transform = CGAffineTransformMakeScale(0.7, 0.7);
                often.tag = row;
                [often addTarget:self action:@selector(oftenSwitchChange:) forControlEvents:UIControlEventValueChanged];
                UILabel * oftenlbl = [[UILabel alloc] initWithFrame:CGRectMake(often.frame.size.width+often.frame.origin.x, 0, pview.frame.size.width/8, H)];
                oftenlbl.text = NSLocalizedString(@"Often",nil);
                oftenlbl.tag = 999;
                oftenlbl.transform = CGAffineTransformMakeScale(0.7, 0.7);
                oftenlbl.adjustsFontSizeToFitWidth = YES;
                [pview addSubview:oftenlbl];
            }else
            {
                for (UIView * subview in pview.subviews) {
                    if ([subview isKindOfClass:[UILabel class]]&&subview.tag != 999) {
                        lbl = (UILabel*)subview;
                    }
                    if ([subview isKindOfClass:[UISwitch class]]) {
                        often = (UISwitch*)subview;
                    }
                }
            }
            UIFont * cfont = self.fontList[row];
            often.on = [self checkIfOftenList:cfont.fontName];
            often.tag = row;
            lbl.font = cfont;
            if(self.text.length>0){
                lbl.text = self.text;
            }
            else{
                lbl.text = [NSString stringWithFormat:@"%@", lbl.font.fontName];
            }
            retView = pview;
            
            break;
        }
        case 1:
        {
            if([view isKindOfClass:[UILabel class]]){
                lbl = (UILabel*)view;
            }
            else{
                CGFloat W = [self pickerView:pickerView widthForComponent:component];
                CGFloat H = [self pickerView:pickerView rowHeightForComponent:component];
                CGFloat dx = 10;
                lbl = [[UILabel alloc] initWithFrame:CGRectMake(dx, 0, W-2*dx, H)];
                lbl.backgroundColor = [UIColor clearColor];
                lbl.adjustsFontSizeToFitWidth = YES;
                lbl.minimumScaleFactor = 0.5;
                lbl.textAlignment = NSTextAlignmentCenter;
                lbl.textColor = self.textColor;
            }
            lbl.font = [UIFont systemFontOfSize:kCLFontPickerViewConstantFontSize];
            lbl.text = [NSString stringWithFormat:@"%@", self.fontSizes[row]];
            retView = lbl;
            break;
        }
        default:
            break;
    }
    
    return retView;
}

-(int)checkIfInIAPRound:(NSInteger)index
{
    if (index >= startIndex-3&&index <=startIndex+10) {
        return 1;
    }else
    {
        return 0;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([self.delegate respondsToSelector:@selector(fontPickerView:didSelectFont:fontIndex:)]){
       // [[SHKActivityIndicator currentIndicator] displayActivity:NSLocalizedString(@"Working...",nil)];
        [self performSelector:@selector(realPickFont:) withObject:[NSNumber numberWithInteger:[_pickerView selectedRowInComponent:0]] afterDelay:0.1];
        
    }
}
-(void)realPickFont:(NSNumber*)index
{
    [self.delegate fontPickerView:self didSelectFont:self.font fontIndex:[self checkIfInIAPRound:[index intValue]]];
   // [[SHKActivityIndicator currentIndicator] hide];
}
- (BOOL)anySubViewScrolling:(UIView *)view{
    if ([view isKindOfClass:[UIScrollView class]]) {
        UIScrollView *scrollView = (UIScrollView *)view;
        if (scrollView.dragging || scrollView.decelerating) {
            return YES;
        }
    }
    for (UIView *theSubView in view.subviews) {
        if ([self anySubViewScrolling:theSubView]) {
            return YES;
        }
    }
    return NO;
}

@end
