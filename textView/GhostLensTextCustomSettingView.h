//
//  GhostLensTextCustomSettingView.h
//
//  Created by Kevin Siml - Appzer.de on 2015/12/18.
//  Copyright (c) 2015 Appzer.de. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GhostLensTextCustomSettingViewDelegate;

@interface GhostLensTextCustomSettingView : UIView

@property (nonatomic, weak) id<GhostLensTextCustomSettingViewDelegate> delegate;
@property (nonatomic, strong) NSString *selectedText;
@property (nonatomic, strong) UIColor *selectedFillColor;
@property (nonatomic, strong) UIColor *selectedBorderColor;
@property (nonatomic, assign) CGFloat selectedBorderWidth;
@property (nonatomic, strong) UIFont *selectedFont;
@property (nonatomic, assign) BOOL isPhotoEdit;
@property (nonatomic,strong)UITextView *textView;

- (void)setTextColor:(UIColor*)textColor;
- (void)setFontPickerForegroundColor:(UIColor*)foregroundColor;
-(void)changeEffectButSelect;
- (void)showSettingMenuWithIndex:(NSInteger)index animated:(BOOL)animated;

@end



@protocol GhostLensTextCustomSettingViewDelegate <NSObject>
@optional
- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeText:(NSString*)text;
- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeFillColor:(UIColor*)fillColor;
- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeBorderColor:(UIColor*)borderColor;
- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeBorderWidth:(CGFloat)borderWidth;
- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeFont:(UIFont*)font fontIndex:(int)fontIndex;
- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeEffect:(int)index;
- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeCharSpace:(CGFloat)value;
- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeLineSpace:(CGFloat)value;
@end
