//
//  EraserCLTextView.h
//  Eraser
//
//  Created by macbookair on 25/6/19.
//  Copyright © 2019年 BiggerLensStore. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLImageEditorTheme+Private.h"
#import "UIView+Frame.h"
#import "CLCircleView.h"
#import "CLColorPickerView.h"
#import "CLFontPickerView.h"
//#import "LocationTools.h"
#import "GhostLensTextCustomSettingView.h"
#import "THLabel.h"
#import <sys/utsname.h>
#import "CLToolbarMenuItem.h"
#include "CLImageEditorTheme.h"
#import "UIDevice+SystemVersion.h"
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN
static NSString* const GhostLensTextCustomViewActiveViewDidChangeNotification = @"GhostLensTextCustomViewActiveViewDidChangeNotificationString";
static NSString* const GhostLensTextCustomViewActiveViewDidTapNotification = @"GhostLensTextCustomViewActiveViewDidTapNotificationString";

@protocol EraserCLTextViewDelegate <NSObject>

- (void)tapSelectTextView:(UIView *)textView; //点击选择某一个编辑框

- (void)deleteTextView:(UIView *)textView; //删除某一个编辑框

@end
@interface EraserCLTextView : UIView<GhostLensTextCustomSettingViewDelegate,UIGestureRecognizerDelegate>
{
    NSString *_imageDate;
    NSString *_imageTime;
    NSString *_imageLocation;
    BOOL TextWithDateLoc;
   // GhostLensTextCustomSettingView *_settingView;
}
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *fillColor;
@property (nonatomic, strong) UIColor *strokeColor;
@property (nonatomic, assign) CGFloat strokeWidth;
@property (nonatomic,strong)THLabel *label;
@property (nonatomic,strong)UIButton *deleteButton;
@property (nonatomic,strong)UIButton *mirrorButton;
@property (nonatomic,strong)CLCircleView *circleView;
@property (nonatomic,weak)id <EraserCLTextViewDelegate> delegate;

@property (nonatomic, assign) NSTextAlignment textAlignment;
//- (void)initWithFrame:(CGRect)frame baseView:(UIView *)baseView;
- (void)clearLabelStyle;
- (void)setActiveTextView;
//- (void)setActiveTextView;
- (void)setScale:(CGFloat)scale;
- (void)sizeToFitWithMaxWidth:(CGFloat)width lineHeight:(CGFloat)lineHeight;
-(void)showWhichLabel:(int)index;
-(void)setTextViewAvtive:(BOOL)active;
-(void)setTextViewLabelStyleIndex:(int)index;
-(void)drawLabel;
-(void)setLetterSpace:(CGFloat)value;
-(void)setLineSpace:(CGFloat)value;
-(void)setUsingBool:(BOOL)isbool;
-(BOOL)getUsingBool;
- (void)setAvtive:(BOOL)active;
- (BOOL)active;
- (void)setTextAlpha:(CGFloat )alpha;
@end

NS_ASSUME_NONNULL_END
