//
//  EraserCLTextView.m
//  Eraser
//
//  Created by macbookair on 25/6/19.
//  Copyright © 2019年 BiggerLensStore. All rights reserved.
//

#import "EraserCLTextView.h"
#define kStrokeColor        [UIColor blackColor]
#define kStrokeSize            (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 6.0 : 3.0)
#define kShadowOffset        CGSizeMake(0.0, UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 4.0 : 2.0)
#define kShadowBlur            (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 10.0 : 5.0)
static const CGFloat kCLImageToolAnimationDuration = 0.5;
static const CGFloat kCLImageToolFadeoutDuration   = 0.5;

@implementation EraserCLTextView
{
//    THLabel *_label;
//    UIButton *_deleteButton;
//    UIButton *_mirrorButton;
//    CLCircleView *_circleView;
    
    CGFloat _scale;
    CGFloat _scalex;
    CGFloat _scaley;
    CGFloat _arg;
    
    CGPoint _initialPoint;
    CGFloat _initialArg;
    CGFloat _initialScale;
    CGFloat _MaininitialScale;
    BOOL isUsing;
    UITextView *textTempView;
}

- (void)setActiveTextView
{
        //[self setAvtive:NO];
        //activeView = view;
        [self setAvtive:YES];
        //[self.superview bringSubviewToFront:self];
        NSNotification *n = [NSNotification notificationWithName:GhostLensTextCustomViewActiveViewDidChangeNotification object:self userInfo:nil];
        [[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
}

-(void)setTextViewAvtive:(BOOL)active
{
    [self setAvtive:active];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    //self = [super initWithFrame:CGRectMake(0, 0, 132, 132)];
    if(self){
    self.frame = frame;
    NSDate *date = [NSDate date];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    
    format.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    NSString *string = [format stringFromDate:date];
    NSArray* DateTime = [string componentsSeparatedByString: @" "];
    _imageDate = [[DateTime objectAtIndex: 0] stringByReplacingOccurrencesOfString:@":" withString:@"-"];
    _imageTime = [DateTime objectAtIndex: 1];
    
    /* [LocationTools getUserLocation:^(NSString *cityName) {
     _imageLocation = cityName;
     
     }];*/
    //添加键盘
//    _settingView = [[GhostLensTextCustomSettingView alloc] initWithFrame:CGRectMake(0,kDeviceHeight - 100 - 60 - KDNavH, kDeviceWidth, 100)];
//    _settingView.backgroundColor = [CLImageEditorTheme toolbarColor];
//    _settingView.textColor = [CLImageEditorTheme toolbarTextColor];
//    _settingView.fontPickerForegroundColor = _settingView.backgroundColor;
//    _settingView.delegate = self;
//    [baseView addSubview:_settingView];
//    _settingView.hidden = YES;
//
//    UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    UIImage * img = [UIImage imageNamed:@"btn_delete.png"];
//    [okButton setImage:img forState:UIControlStateNormal];
//    okButton.frame = CGRectMake(_settingView.width-32, 0, 32, 32);
//    [okButton addTarget:self action:@selector(pushedButton:) forControlEvents:UIControlEventTouchUpInside];
//    [_settingView addSubview:okButton];
    
    _label = [[THLabel alloc] init];
    [_label setTextColor:[CLImageEditorTheme toolbarTextColor]];
    _label.numberOfLines = 0;
    _label.backgroundColor = [UIColor clearColor];
    _label.layer.borderColor = [mainColor CGColor];
    _label.layer.cornerRadius = 5;
    _label.font = [UIFont systemFontOfSize:200];
    _label.minimumScaleFactor = 0.001;
    _label.adjustsFontSizeToFitWidth = YES;
    _label.textAlignment = NSTextAlignmentCenter;
    _label.strokeColor = kStrokeColor;
    //_label.strokeSize = kStrokeSize;
    
    if(TextWithDateLoc==TRUE){
        _label.textAlignment = NSTextAlignmentLeft;
        self.text = [NSString stringWithFormat:@"%@ %@\n%@", _imageDate, _imageTime, _imageLocation];
    } else {
        self.text = @"";
    }
    
    [self addSubview:_label];
    
    CGSize size = [_label sizeThatFits:CGSizeMake(FLT_MAX, FLT_MAX)];
    _label.frame = CGRectMake(16, 16, size.width, size.height);
    self.frame = CGRectMake(0, 0, size.width + 32, size.height + 32);
    
    _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_deleteButton setImage:[UIImage imageNamed:@"btn_Textdelete.png"] forState:UIControlStateNormal];
    _deleteButton.frame = CGRectMake(0, 0, 32, 32);
    _deleteButton.center = _label.frame.origin;
    [_deleteButton addTarget:self action:@selector(pushedDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_deleteButton];
    
    _mirrorButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_mirrorButton setImage:[UIImage imageNamed:@"btn_Textmirror.png"] forState:UIControlStateNormal];
    _mirrorButton.frame = CGRectMake(0, 0, 32, 32);
    _mirrorButton.center = CGPointMake(_label.frame.origin.x, _label.frame.origin.y+_label.height);
    _mirrorButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [_mirrorButton addTarget:self action:@selector(pushedMirrorBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_mirrorButton];
    
    _circleView = [[CLCircleView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    _circleView.center = CGPointMake(_label.width + _label.left, _label.height + _label.top);
    _circleView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
    _circleView.radius = 0.7;
    _circleView.color = [UIColor whiteColor];
        _circleView.borderColor = mainColor;//RGB(255, 182,0);//[UIColor blackColor];
    _circleView.borderWidth = 5;
    [self addSubview:_circleView];
    _arg = 0;
    [self setScale:1];
    _scale = 1;
    _scalex = 1;
    _scaley = 1;
    [self initGestures];
    
    self.fillColor = [UIColor blackColor];
     }
     return self;
}
#pragma mark- Setting view delegate

- (void)textSettingView:(GhostLensTextCustomSettingView *)settingView didChangeText:(NSString *)text
{
//    self.isUsing = YES;
    self.text = text;
    [self sizeToFitWithMaxWidth:self.superview.width lineHeight:0.2*self.superview.height];
    [self drawLabel];
}

//- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeFillColor:(UIColor*)fillColor
//{
////    self.isUsing = YES;
////    _colorBtn.iconView.backgroundColor = fillColor;
////   self.fillColor = fillColor;
////   [self clearLabelStyle];
////   [self drawLabel];
//}
//
//- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeBorderColor:(UIColor*)borderColor
//{
////    self.isUsing = YES;
////    _colorBtn.iconView.layer.borderColor = borderColor.CGColor;
//    self.strokeColor = borderColor;
//    [self showWhichLabel:0];
//    [self clearLabelStyle];
//    [self drawLabel];
//}
//
//- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeBorderWidth:(CGFloat)borderWidth
//{
////    self.isUsing = YES;
////    _colorBtn.iconView.layer.borderWidth = MAX(2, 10*borderWidth);
//    self.strokeWidth = borderWidth;
//    [self clearLabelStyle];
//    [self drawLabel];
//}
//
//- (void)textSettingView:(GhostLensTextCustomSettingView *)settingView didChangeFont:(UIFont *)font fontIndex:(int)fontIndex
//{
////    if (fontIndex == 0&&![GhostLensStoreIAPManager featureFPurchased]) {
////        [self.delegate unlockallfont];
////    }else
////    {
////        self.isUsing = YES;
//        self.font = font;
//        [self sizeToFitWithMaxWidth:self.superview.width lineHeight:0.2*self.superview.height];
//        [self clearLabelStyle];
//        [self drawLabel];
////    }
////
//}
//- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeEffect:(int)index
//{
////    self.isUsing = YES;
////    NSLog(@"---------------------- effect num %d",index);
////    if (self.text && self.text.length > 100 &&[self isLowDevice]) {
////        [[SigtonUserInfo shareUserInfoArray] popWarning:self.superview labelText:NSLocalizedString(@"Only Support Up to 100 Chars", "")  fontSize:13];
////        return;
////    }
////    [_GhostLensTextCustomView setTextViewLabelStyle:_selectedTextView Index:index];
////    [self.selectedTextView drawLabel];
//}
//- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeCharSpace:(CGFloat)value
//{
////    self.isUsing = YES;
//    [self setLetterSpace:value];
//    [self sizeToFitWithMaxWidth:self.superview.width lineHeight:0.2*self.superview.height];
//    [self drawLabel];
//}
//- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeLineSpace:(CGFloat)value
//{
////    self.isUsing = YES;
//    [self setLineSpace:value];
//    [self sizeToFitWithMaxWidth:self.superview.width lineHeight:0.2*self.superview.height];
//    [self drawLabel];
//}


-(void)setTextViewLabelStyleIndex:(int)index
{
    [self setLabelStyle:index];
}
- (void)setLabelStyle:(int)styleID
{
    [self clearLabelStyle];
    // _label.hidden = no;
    //  _fxlabel.hidden = NO;
    switch (styleID)
    {
        case 1:
        {
            _label.shadowOffset = CGSizeMake(0.0f, 2.0f);
            _label.shadowColor = [UIColor colorWithWhite:0.0f alpha:0.75f];
            _label.shadowBlur = 5.0f;
            break;
        }
        case 2:
        {
            _label.shadowColor = [UIColor colorWithWhite:1.0f alpha:0.8f];
            _label.shadowOffset = CGSizeMake(1.0f, 1.0f);
            _label.shadowBlur = 2.0f;
            _label.innerShadowBlur = 2.0f;
            _label.innerShadowColor = [UIColor colorWithWhite:0.0f alpha:0.8f];
            _label.innerShadowOffset = CGSizeMake(1.0f, 1.0f);
            break;
        }
        case 3:
        {
            _label.gradientStartColor = [UIColor yellowColor];
            _label.gradientEndColor = [UIColor blueColor];
            break;
        }
        case 4:
        {
            _label.gradientStartPoint = CGPointMake(0.0f, 0.0f);
            _label.gradientEndPoint = CGPointMake(1.0f, 1.0f);
            _label.gradientColors = @[[UIColor redColor],
                                      [UIColor yellowColor],
                                      [UIColor greenColor],
                                      [UIColor cyanColor],
                                      [UIColor blueColor],
                                      [UIColor purpleColor],
                                      [UIColor redColor]];
            break;
        }
        case 5:
        {
            _label.shadowColor = [UIColor blackColor];
            _label.shadowOffset = kShadowOffset;
            _label.shadowBlur = kShadowBlur;
            _label.innerShadowBlur = 2.0f;
            _label.innerShadowColor = [UIColor whiteColor];
            _label.innerShadowOffset = CGSizeMake(1.0f, 1.0f);
            _label.gradientStartColor = [UIColor redColor];
            _label.gradientEndColor = [UIColor yellowColor];
            _label.gradientStartPoint = CGPointMake(0.0f, 0.5f);
            _label.gradientEndPoint = CGPointMake(1.0f, 0.5f);
            //   _label.oversampling = 2;
            
            break;
        }
        default:
        {
            
            //  _label.isEffect = NO;
            _label.shadowColor = nil;
            _label.shadowOffset = kShadowOffset;
            _label.shadowBlur = kShadowBlur;
            //            _label.hidden = YES;
            //            _label.hidden = NO;
            break;
        }
    }
    NSLog(@"setLabelStyle invoked");
}

- (void)clearLabelStyle
{
    _label.shadowColor = nil;
    _label.shadowOffset = CGSizeZero;
    _label.shadowBlur = 0.0f;
    _label.innerShadowBlur = 0.0f;
    _label.innerShadowColor = nil;
    _label.innerShadowOffset = CGSizeZero;
    _label.innerShadowBlur = 0.0f;
    
    _label.gradientStartColor = nil;
    _label.gradientEndColor = nil;
    _label.gradientStartPoint = CGPointMake(0.5f, 0.0f);
    _label.gradientEndPoint = CGPointMake(0.5f, 0.75f);
    // _label.oversampling = 0;
}



- (void)initGestures
{
    _label.userInteractionEnabled = YES;
    //移动
    [_label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidTap:)]];
   // [_label addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidPan:)]];
    UIPanGestureRecognizer *startPan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidPan:)];
    startPan.maximumNumberOfTouches = 1;
    [_label addGestureRecognizer:startPan];
    [_circleView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(circleViewDidPan:)]];
    //缩放
    //[_label addGestureRecognizer:[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidPinch:)]];
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidPinch:)];
    [pinchRecognizer setDelegate:self];
     //pinchRecognizer.numberOfTouches = 2;
    [_label addGestureRecognizer:pinchRecognizer];
    
    //    _fxlabel.userInteractionEnabled = YES;
    //    [_fxlabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidTap:)]];
    //    [_fxlabel addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidPan:)]];
    //    [_circleView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(circleViewDidPan:)]];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView* view= [super hitTest:point withEvent:event];
    if(view==self){
        return nil;
    }
    return view;
}

#pragma mark- Properties

- (void)setAvtive:(BOOL)active
{
    _deleteButton.hidden = !active;
    _mirrorButton.hidden = !active;
    _circleView.hidden = !active;
    _label.layer.borderWidth = (active) ? 1/_scale : 0;
    //  _fxlabel.layer.borderWidth = (active) ? 1/_scale : 0;
}

- (BOOL)active
{
    return !_deleteButton.hidden;
}

-(void)showWhichLabel:(int)index
{
    //    if (index == 1) {
    //        _label.hidden = YES;
    //      //  _fxlabel.hidden = NO;
    //    }else{
    //      //  _fxlabel.hidden = YES;
    //        _label.hidden = NO;
    //       // changeEffectButSelect
    //    }
}

- (void)sizeToFitWithMaxWidth:(CGFloat)width lineHeight:(CGFloat)lineHeight
{
    self.transform = CGAffineTransformIdentity;
    _label.transform = CGAffineTransformIdentity;
    UILabel *lab = [[UILabel alloc]initWithFrame:_label.frame];
    lab.numberOfLines = 0;
    //lab.minimumScaleFactor = 0.001;
    lab.adjustsFontSizeToFitWidth = YES;
    lab.textAlignment = self.textAlignment;
    lab.text = self.text;
    
    NSString *str = [_label.font fontName];
    UIFont *font = [UIFont fontWithName:str size:_label.font.pointSize];
    lab.font = font;
    CGFloat maxwidth = width / (15/200.0);
    CGSize size = [lab sizeThatFits:CGSizeMake(maxwidth, FLT_MAX)];
    size = [self getSpaceLabelHeight:lab.text withFont:lab.font withWidth:maxwidth];
    CGFloat addh = size.height*0.5;
    if (addh > 600) {
        addh = 600;
    }
    _label.frame = CGRectMake(16, 16, size.width+100, size.height+addh);
    
    CGFloat viewW = (_label.width + 32);
    CGFloat viewH = _label.font.lineHeight;
    
    CGFloat ratio = MIN(width / viewW, lineHeight / viewH);
    [self setScale:ratio];
}

-(CGSize)getSpaceLabelHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paraStyle.alignment = self.textAlignment;
    paraStyle.lineSpacing = _label.lineSpacing; //设置行间距
    NSNumber * charspace = [NSNumber numberWithFloat:_label.letterSpacing];
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:charspace
                          };
    
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size;
}
-(void)setUsingBool:(BOOL)isbool
{
    isUsing = isbool;
}
-(BOOL)getUsingBool
{
    return isUsing;
}
- (void)setScale:(CGFloat)scale
{
    _scale = scale;
    if(_scalex<0){
        _scalex=scale*-1;
    } else {
        _scalex = scale;
    }
    _scaley = scale;
    
    self.transform = CGAffineTransformIdentity;
    
    _label.transform = CGAffineTransformMakeScale(_scalex, _scaley);
    
    CGRect rct = self.frame;
    rct.origin.x += (rct.size.width - (_label.width + 32)) / 2;
    rct.origin.y += (rct.size.height - (_label.height + 32)) / 2;
    rct.size.width  = _label.width + 32;
    rct.size.height = _label.height + 32;
    self.frame = rct;
    
    _label.center = CGPointMake(rct.size.width/2, rct.size.height/2);
    
    self.transform = CGAffineTransformMakeRotation(_arg);
    
    _label.layer.borderWidth = 1/_scale;
    _label.layer.cornerRadius = 3/_scale;
    isUsing = YES;
}

- (void)setFillColor:(UIColor *)fillColor
{
    _label.textColor = fillColor;
    //    _fxlabel.textColor = fillColor;
    //    _fxlabel.gradientStartColor = fillColor;
}

- (UIColor*)fillColor
{
    return _label.textColor;
}

- (void)setStrokeColor:(UIColor *)strokeColor
{
    //_label.outlineColor = borderColor;
    _label.strokeColor = strokeColor;
}

- (UIColor*)strokeColor
{
    //return _label.outlineColor;
    return _label.strokeColor;
}

- (void)setStrokeWidth:(CGFloat)strokeWidth
{
    //_label.outlineWidth = borderWidth;
    _label.strokeSize = strokeWidth * 20;
}

- (CGFloat)strokeWidth
{
    // return _label.outlineWidth;
    return _label.strokeSize;
}

- (void)setFont:(UIFont *)font
{
    _label.font = [font fontWithSize:200];
    //  _fxlabel.font = [font fontWithSize:200];
}


-(void)setLetterSpace:(CGFloat)value
{
    _label.letterSpacing = value;
}

-(void)setLineSpace:(CGFloat)value
{
    _label.lineSpacing = value;
}

- (UIFont*)font
{
    return _label.font;
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment
{
    _label.textAlignment = textAlignment;
    // _fxlabel.textAlignment = textAlignment;
}
- (void)setTextAlpha:(CGFloat )alpha{
    _label.alpha = alpha;
}
- (NSTextAlignment)textAlignment
{
    return _label.textAlignment;
}

- (void)setText:(NSString *)text
{
    if(![text isEqualToString:_text]){
        _text = text;
        _label.textStr = (_text.length>0) ? _text : @"";
        // _fxlabel.text = (_text.length>0) ? _text : NSLocalizedStringWithDefaultValue(@"GhostLensTextCustomTool_DefaultTitle", nil, [CLImageEditorTheme bundle], @"Text", @"");
    }
}

#pragma mark- gesture events

- (void)pushedMirrorBtn:(id)sender
{
    [UIView animateWithDuration:kCLImageToolAnimationDuration animations:^{ _label.transform = CGAffineTransformMakeScale(_scalex*-1, _scale);}];
    
    //  [UIView animateWithDuration:kCLImageToolAnimationDuration animations:^{ _fxlabel.transform = CGAffineTransformMakeScale(_scalex*-1, _scale);}];
    _scalex=_scalex*-1;
    isUsing = YES;
}

- (void)pushedDeleteBtn:(id)sender
{
    [self.delegate deleteTextView:self];
//    EraserCLTextView *nextTarget = nil;
//
//    const NSInteger index = [self.superview.subviews indexOfObject:self];
//
//    for(NSInteger i=index+1; i<self.superview.subviews.count; ++i){
//        UIView *view = [self.superview.subviews objectAtIndex:i];
//        if([view isKindOfClass:[EraserCLTextView class]]){
//            nextTarget = (EraserCLTextView*)view;
//            break;
//        }
//    }
//
//    if(nextTarget==nil){
//        for(NSInteger i=index-1; i>=0; --i){
//            UIView *view = [self.superview.subviews objectAtIndex:i];
//            if([view isKindOfClass:[EraserCLTextView class]]){
//                nextTarget = (EraserCLTextView*)view;
//                break;
//            }
//        }
//    }
//
//    [[self class] setActiveTextView:nextTarget];
    [self removeFromSuperview];
    //[_settingView removeFromSuperview];
}
//- (void)activeTextViewDidTap:(NSNotification*)notification
//{
//    [self beginTextEditting];
//}
//- (void)beginTextEditting
//{
//    [self showSettingViewWithMenuIndex:0];
//    [_settingView becomeFirstResponder];
//}
//- (void)showSettingViewWithMenuIndex:(NSInteger)index
//{
//    if(_settingView.hidden){
//        _settingView.hidden = NO;
//        [_settingView showSettingMenuWithIndex:index animated:NO];
//    }
//    else{
//        [_settingView showSettingMenuWithIndex:index animated:YES];
//    }
//}

//- (void)pushedButton:(UIButton*)button
//{
////    if(_settingView.isFirstResponder){
////        [_settingView resignFirstResponder];
////    }
////    else{
//        [_settingView resignFirstResponder];
//        _settingView.hidden = YES;
//       // [_settingView removeFromSuperview];
//       // [self hideSettingView];
//       // [self hideTextView];
////    }
//}
- (void)viewDidTap:(UITapGestureRecognizer*)sender
{
    //点击代理
    [self.delegate tapSelectTextView:self];
//    if(_settingView.hidden){
//    _settingView.hidden = NO;
//    [self beginTextEditting];
//    }
    
    
   // _settingView.hidden = NO;
   // [_settingView canResignFirstResponder];
//    if(self.active){
//        NSNotification *n = [NSNotification notificationWithName:GhostLensTextCustomViewActiveViewDidTapNotification object:self userInfo:nil];
//        [[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
//    }
//    [self setActiveTextView];
}

- (void)viewDidPan:(UIPanGestureRecognizer*)sender
{
//    [self  setTextViewAvtive:YES];
//    [self  setActiveTextView];
    CGPoint p = [sender translationInView:self.superview];
    if(sender.state == UIGestureRecognizerStateBegan){
        _initialPoint = self.center;
    }
    self.center = CGPointMake(_initialPoint.x + p.x, _initialPoint.y + p.y);
    isUsing = YES;
}
-(void)viewDidPinch:(UIPinchGestureRecognizer*)sender{
    if (sender.state == UIGestureRecognizerStateBegan || sender.state == UIGestureRecognizerStateChanged) {
        _label.transform = CGAffineTransformScale(_label.transform, sender.scale, sender.scale);
        //_label.layer.borderWidth = 1/sender.scale;
        //_label.layer.cornerRadius = 1/sender.scale;
        //self.totalScale *= scale;
        //[self setScale:sender.scale];
        self.transform = CGAffineTransformIdentity;
        //_label.transform = CGAffineTransformMakeScale(sender.scale, sender.scale);
        
        CGRect rct = self.frame;
        rct.origin.x += (rct.size.width - (_label.width + 32)) / 2;
        rct.origin.y += (rct.size.height - (_label.height + 32)) / 2;
        rct.size.width  = _label.width + 32;
        rct.size.height = _label.height + 32;
        self.frame = rct;
        
        _label.center = CGPointMake(rct.size.width/2, rct.size.height/2);
        
        //self.transform = CGAffineTransformMakeRotation(_arg);
        
        _label.layer.borderWidth = 1/_scale;
        _label.layer.cornerRadius = 3/_scale;
        //isUsing = YES;
        //[self setTextViewAvtive:YES];
        //[self setActiveTextView];
        sender.scale = 1;
    }
}
-(void)drawLabel
{
    [_label drawViewNowRect];
}

- (void)circleViewDidPan:(UIPanGestureRecognizer*)sender
{
    CGPoint p = [sender translationInView:self.superview];
    
    static CGFloat tmpR = 1;
    static CGFloat tmpA = 0;
    if(sender.state == UIGestureRecognizerStateBegan){
        _initialPoint = [self.superview convertPoint:_circleView.center fromView:_circleView.superview];
        
        CGPoint p = CGPointMake(_initialPoint.x - self.center.x, _initialPoint.y - self.center.y);
        tmpR = sqrt(p.x*p.x + p.y*p.y);
        tmpA = atan2(p.y, p.x);
        
        _initialArg = _arg;
        _initialScale = _scale;
    }
    
    if(_MaininitialScale<=0){
        _MaininitialScale = _scale;
    }
    
    p = CGPointMake(_initialPoint.x + p.x - self.center.x, _initialPoint.y + p.y - self.center.y);
    CGFloat R = sqrt(p.x*p.x + p.y*p.y);
    CGFloat arg = atan2(p.y, p.x);
    
    _arg   = _initialArg + arg - tmpA;
    //[self setScale:MAX(_initialScale * R / tmpR, 15/200.0)];
    [self setScale:MAX(_initialScale * R / tmpR, (_MaininitialScale*0.1))];
    
    [self setTextViewAvtive:YES];
    [self setActiveTextView];
}

@end
