//
//  GhostLensTextCustomTool.m
//
//  Created by Kevin Siml - Appzer.de on 2015/12/15.
//  Copyright (c) 2015 Appzer.de. All rights reserved.
//

#import "GhostLensTextCustomTool.h"
//#import "SigtonUserInfo.h"
//#import "CutoutAppDelegate.h"
#import "CLCircleView.h"
#import "CLColorPickerView.h"
#import "CLFontPickerView.h"
//#import "LocationTools.h"
#import "GhostLensTextCustomSettingView.h"
#import "THLabel.h"
#import <sys/utsname.h>
#import "CLToolbarMenuItem.h"
#include "CLImageEditorTheme.h"
#import "UIDevice+SystemVersion.h"
//#import "GhostLensStoreIAPManager.h"
static const CGFloat kCLImageToolAnimationDuration = 0.5;
static const CGFloat kCLImageToolFadeoutDuration   = 0.5;
static NSString* const GhostLensTextCustomViewActiveViewDidChangeNotification = @"GhostLensTextCustomViewActiveViewDidChangeNotificationString";
static NSString* const GhostLensTextCustomViewActiveViewDidTapNotification = @"GhostLensTextCustomViewActiveViewDidTapNotificationString";

BOOL TextWithDateLoc = FALSE;

@interface _GhostLensTextCustomView : UIView
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *fillColor;
@property (nonatomic, strong) UIColor *strokeColor;
@property (nonatomic, assign) CGFloat strokeWidth;

@property (nonatomic, assign) NSTextAlignment textAlignment;
- (void)clearLabelStyle;
+ (void)setActiveTextView:(_GhostLensTextCustomView*)view;
- (void)setScale:(CGFloat)scale;
- (void)sizeToFitWithMaxWidth:(CGFloat)width lineHeight:(CGFloat)lineHeight;
-(void)showWhichLabel:(int)index;
+(void)setTextView:(_GhostLensTextCustomView*)view Avtive:(BOOL)active;
+(void)setTextViewLabelStyle:(_GhostLensTextCustomView*)view Index:(int)index;
-(void)drawLabel;
-(void)setLetterSpace:(CGFloat)value;
-(void)setLineSpace:(CGFloat)value;
-(void)setUsingBool:(BOOL)isbool;
-(BOOL)getUsingBool;
- (void)setAvtive:(BOOL)active;
- (BOOL)active;
@end


@interface GhostLensTextCustomTool()
<CLColorPickerViewDelegate, CLFontPickerViewDelegate, UITextViewDelegate, GhostLensTextCustomSettingViewDelegate>
@property (nonatomic, strong) _GhostLensTextCustomView *selectedTextView;
@property (nonatomic, strong) UIView* editorView;
@end
NSString *_imageDate;
NSString *_imageTime;
NSString *_imageLocation;
#define kStrokeColor        [UIColor blackColor]
#define kStrokeSize            (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 6.0 : 3.0)
#define kShadowOffset        CGSizeMake(0.0, UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 4.0 : 2.0)
#define kShadowBlur            (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 10.0 : 5.0)

@implementation GhostLensTextCustomTool
{
   // UIImage *_originalImage;
    UIView *_workingView;
    GhostLensTextCustomSettingView *_settingView;

    CLToolbarMenuItem *_textBtn;
    CLToolbarMenuItem *_colorBtn;
    CLToolbarMenuItem *_fontBtn;
    CLToolbarMenuItem *_effectBtn;
    
    CLToolbarMenuItem *_alignLeftBtn;
    CLToolbarMenuItem *_alignCenterBtn;
    CLToolbarMenuItem *_alignRightBtn;
    
    UIScrollView *_menuScroll;
    

}

+ (NSArray*)subtools
{
    return nil;
}

+ (NSString*)defaultTitle
{
    return @"";
}

+ (BOOL)isAvailable
{
    return ([UIDevice iosVersion] >= 5.0);
}
+ (BOOL)checkIfTextCustomView:(UIView*)view
{
    return [view isKindOfClass:[_GhostLensTextCustomView class]];
}
+ (CGFloat)defaultDockedNumber
{
    return 4.7;
}
-(void)setTextUsingBool:(BOOL)isbool
{
    self.isUsing = isbool;
    [_selectedTextView setUsingBool:isbool];
}
-(BOOL)getTextUsingBool
{
    BOOL isbool = _isUsing;
    if (!isbool) {
        isbool = [_selectedTextView getUsingBool];
    }
    return isbool;
}
#pragma mark- implementation

- (void)setup:(CGRect)menuScrollRect menuColor:(UIColor*)menuColor workingView:(UIView *)workingView SuperView:(UIView *)superview
{
    self.frame = menuScrollRect;
    self.editorView = superview;
    _workingView = workingView;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(activeTextViewDidChange:) name:GhostLensTextCustomViewActiveViewDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(activeTextViewDidTap:) name:GhostLensTextCustomViewActiveViewDidTapNotification object:nil];
    
    _menuScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, menuScrollRect.size.width, menuScrollRect.size.height)];
    _menuScroll.backgroundColor = menuColor;
    _menuScroll.showsHorizontalScrollIndicator = NO;
    _menuScroll.hidden = YES;
    [self addSubview:_menuScroll];
    
    _settingView = [[GhostLensTextCustomSettingView alloc] initWithFrame:CGRectMake(0, menuScrollRect.origin.y -  180, self.width, 180)];
    _settingView.backgroundColor = [CLImageEditorTheme toolbarColor];
    _settingView.textColor = [CLImageEditorTheme toolbarTextColor];
    _settingView.fontPickerForegroundColor = _settingView.backgroundColor;
    _settingView.delegate = self;
    _settingView.hidden = YES;
    [superview addSubview:_settingView];
    
    UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage * img = [UIImage imageNamed:@"btn_delete.png"];//[CLImageEditorTheme imageNamed:[self class] image:@"btn_delete.png"];
    [okButton setImage:img forState:UIControlStateNormal];
    okButton.frame = CGRectMake(_settingView.width-32, 0, 32, 32);
    [okButton addTarget:self action:@selector(pushedButton:) forControlEvents:UIControlEventTouchUpInside];
    [_settingView addSubview:okButton];
    
    [self setMenu];
    
    self.selectedTextView = nil;
    
    _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.height-_menuScroll.top);
    [UIView animateWithDuration:kCLImageToolAnimationDuration
                     animations:^{
                         _menuScroll.transform = CGAffineTransformIdentity;
                     }];
    TextWithDateLoc=FALSE;
    [self addNewText];
    [self.editorView addSubview:self];
}
-(void)showTextView
{
    [self addSubview:_menuScroll];
    [self.editorView addSubview:_settingView];
    [self.editorView addSubview:self];
    _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editorView.height-_menuScroll.top);
    [UIView animateWithDuration:kCLImageToolAnimationDuration
                     animations:^{
                         _menuScroll.transform = CGAffineTransformIdentity;
                     }];
    _GhostLensTextCustomView * nextTarget;
    _GhostLensTextCustomView * activeTarget;
    for(UIView *view in _workingView.subviews){
        if([view isKindOfClass:[_GhostLensTextCustomView class]]){
            nextTarget = (_GhostLensTextCustomView*)view;
            if (nextTarget.active) {
                if (!activeTarget) {
                    activeTarget = nextTarget;
                }
            }
        }
    }
    if (activeTarget) {
        [_GhostLensTextCustomView setActiveTextView:activeTarget];
        [self beginTextEditting];
    }else if (nextTarget)
    {
        [_GhostLensTextCustomView setActiveTextView:nextTarget];
        [self beginTextEditting];
    }
}
-(void)hideTextView
{
   // [self addSubview:_menuScroll];
    [_settingView removeFromSuperview];
    [UIView animateWithDuration:kCLImageToolAnimationDuration
                     animations:^{
                         _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.editorView.height);
                         [_menuScroll removeFromSuperview];
                         [self removeFromSuperview];
                     }];
}
-(BOOL)isShowingMenu
{
    return _menuScroll.superview != nil;
}
-(void)turnOffSettingView:(CGPoint)pt fromView:(UIView*)fromView
{
    CGPoint convertedLocation = [_settingView.layer convertPoint:pt fromLayer:fromView.layer];
    CGPoint convertedLocation1 = [_menuScroll.layer convertPoint:pt fromLayer:fromView.layer];
    
    if (![_settingView.layer containsPoint:convertedLocation]&&![_menuScroll.layer containsPoint:convertedLocation1])
    {
        if(_settingView.isFirstResponder){
            [_settingView resignFirstResponder];
        }
    }
    
}
-(void)hiddenSettingView
{
    _settingView.hidden = YES;
}
- (void)cleanup
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_settingView endEditing:YES];
    [_settingView removeFromSuperview];
  //  [_workingView removeFromSuperview];
    for (UIView *view in _workingView.subviews) {
        if([view isKindOfClass:[_GhostLensTextCustomView class]])
        {
            [view removeFromSuperview];
        }
    }
    [UIView animateWithDuration:kCLImageToolAnimationDuration
                     animations:^{
                         _menuScroll.transform = CGAffineTransformMakeTranslation(0, self.height-_menuScroll.top);
                     }
                     completion:^(BOOL finished) {
                         [_menuScroll removeFromSuperview];
                     }];
}

- (void)executeWithCompletionBlock:(void (^)(UIImage *, NSError *, NSDictionary *))completionBlock
{
    [_GhostLensTextCustomView setActiveTextView:nil];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //    UIImage *image = [self buildImage:_originalImage];
        
        dispatch_async(dispatch_get_main_queue(), ^{
         //   completionBlock(image, nil, nil);
        });
    });
}

#pragma mark-

- (UIImage*)buildImage:(UIImage*)image
{
    UIGraphicsBeginImageContext(image.size);
    
    [image drawAtPoint:CGPointZero];
    
    CGFloat scale = image.size.width / _workingView.width;
    CGContextScaleCTM(UIGraphicsGetCurrentContext(), scale, scale);
    [_workingView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *tmp = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return tmp;
}

- (void)setMenuBtnEnabled:(BOOL)enabled
{
    _textBtn.userInteractionEnabled =
    _colorBtn.userInteractionEnabled =
    _fontBtn.userInteractionEnabled =
    _effectBtn.userInteractionEnabled =
    _alignLeftBtn.userInteractionEnabled =
    _alignCenterBtn.userInteractionEnabled =
    _alignRightBtn.userInteractionEnabled = enabled;
}

- (void)setSelectedTextView:(_GhostLensTextCustomView *)selectedTextView
{
    if(selectedTextView != _selectedTextView){
        _selectedTextView = selectedTextView;
    }
    
    [self setMenuBtnEnabled:(_selectedTextView!=nil)];
    
    if(_selectedTextView==nil){
        [self hideSettingView];
        
        _colorBtn.iconView.backgroundColor = _settingView.selectedFillColor;
        _alignLeftBtn.selected = _alignCenterBtn.selected = _alignRightBtn.selected = NO;
    }
    else{
        _colorBtn.iconView.backgroundColor = selectedTextView.fillColor;
        _colorBtn.iconView.layer.borderColor = selectedTextView.strokeColor.CGColor;
        _colorBtn.iconView.layer.borderWidth = MAX(2, 10*selectedTextView.strokeWidth);
        
        _settingView.selectedText = selectedTextView.text;
        _settingView.selectedFillColor = selectedTextView.fillColor;
        _settingView.selectedBorderColor = selectedTextView.strokeColor;
        _settingView.selectedBorderWidth = selectedTextView.strokeWidth;
        _settingView.selectedFont = selectedTextView.font;
        [self setTextAlignment:selectedTextView.textAlignment];
    }
}

- (void)activeTextViewDidChange:(NSNotification*)notification
{
    self.selectedTextView = notification.object;
}

- (void)activeTextViewDidTap:(NSNotification*)notification
{
    [self beginTextEditting];
}

- (void)setMenu
{
    CGFloat W = 70;
    CGFloat H = _menuScroll.height;
    CGFloat x = 0;
    
    NSArray *_menu = @[
                       @{@"title":NSLocalizedStringWithDefaultValue(@"GhostLensTextCustomTool_MenuItemNew", nil, [CLImageEditorTheme bundle], @"New", @""), @"icon":[UIImage imageNamed:@"btn_add.png"]},
                       @{@"title":NSLocalizedStringWithDefaultValue(@"GhostLensTextCustomTool_MenuItemAlignLeft", nil, [CLImageEditorTheme bundle], @" ", @""), @"icon":[UIImage imageNamed:@"btn_align_left.png"]},
                       
                       @{@"title":NSLocalizedStringWithDefaultValue(@"GhostLensTextCustomTool_MenuItemAlignCenter", nil, [CLImageEditorTheme bundle], @" ", @""), @"icon":[UIImage imageNamed:@"btn_align_center.png"]},
                       
                       @{@"title":NSLocalizedStringWithDefaultValue(@"GhostLensTextCustomTool_MenuItemAlignRight", nil, [CLImageEditorTheme bundle], @" ", @""), @"icon":[UIImage imageNamed:@"btn_align_right.png"]},
                       @{@"title":NSLocalizedStringWithDefaultValue(@"GhostLensTextCustomTool_DefaultTitle", nil, [CLImageEditorTheme bundle], @"Text", @""), @"icon":[UIImage imageNamed:@"icon.png"]},
                       @{@"title":NSLocalizedStringWithDefaultValue(@"GhostLensTextCustomTool_MenuItemColor", nil, [CLImageEditorTheme bundle], @"Color", @"")},
					   
                       @{@"title":NSLocalizedStringWithDefaultValue(@"GhostLensTextCustomTool_MenuItemFont", nil, [CLImageEditorTheme bundle], @"Font", @""), @"icon":[UIImage imageNamed:@"btn_font.png"]},
                       @{@"title":NSLocalizedStringWithDefaultValue(@"GhostLensTextCustomTool_MenuItemEffect", nil, [CLImageEditorTheme bundle], @"Effect", @""), @"icon":[UIImage imageNamed:@"btn_effect.png"]},
                       @{@"title":@"", @"icon":[UIImage imageNamed:@"btn_delete.png"]},
                       
                       ];
    
    NSInteger tag = 0;
    for(NSDictionary *obj in _menu){
        CLToolbarMenuItem *view = [CLImageEditorTheme menuItemWithFrame:CGRectMake(x, 0, W, H) target:self action:@selector(tappedMenuPanel:) toolInfo:nil];
        view.tag = tag++;
        view.title = obj[@"title"];
        view.iconImage = obj[@"icon"];
        switch (view.tag) {
            case 1:
                _alignLeftBtn = view;
                break;
            case 2:
                _alignCenterBtn = view;
                break;
            case 3:
                _alignRightBtn = view;
            case 4:
                _textBtn = view;
                break;
            case 5:
                _colorBtn = view;
                _colorBtn.iconView.layer.borderWidth = 2;
                _colorBtn.iconView.layer.borderColor = [[UIColor blackColor] CGColor];
                break;
            case 6:
                _fontBtn = view;
                break;
            case 7:
                _effectBtn = view;
                break;
                break;
        }
        
        [_menuScroll addSubview:view];
        x += W;
    }
    _menuScroll.contentSize = CGSizeMake(MAX(x, _menuScroll.frame.size.width+1), 0);
}

- (void)tappedMenuPanel:(UITapGestureRecognizer*)sender
{
    UIView *view = sender.view;
    
    switch (view.tag) {
        case 0:
            [self openSheetText];
            break;
        case 1:
            [self setTextAlignment:NSTextAlignmentLeft];
            break;
        case 2:
            [self setTextAlignment:NSTextAlignmentCenter];
            break;
        case 3:
            [self setTextAlignment:NSTextAlignmentRight];
            break;
        case 4:
        case 5:
        case 6:
        case 7:
            [self showSettingViewWithMenuIndex:view.tag-1-3];
            break;
        case 8:
            [self pushedButton:nil];
            break;
        
    }
    
    view.alpha = 0.2;
    [UIView animateWithDuration:kCLImageToolAnimationDuration
                     animations:^{
                         view.alpha = 1;
                     }
     ];
}

- (void) openSheetText
{
    TextWithDateLoc=FALSE;

    /*if(_imageDate.length>0 || _imageLocation.length>0){
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedStringWithDefaultValue(@"TXT_Cancel", nil, [CLImageEditorTheme bundle], NSLocalizedString(@"Cancel",nil), @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedStringWithDefaultValue(@"CLImageEditor_Text", nil, [CLImageEditorTheme bundle], NSLocalizedString(@"Text Only",nil), @""), NSLocalizedStringWithDefaultValue(@"CLImageEditor_DateLocation", nil, [CLImageEditorTheme bundle], NSLocalizedString(@"DateLocation",nil), @""), nil];
        CGRect cellRect = _workingView.bounds;
        cellRect.size.width = _workingView.frame.size.width * 2;
        cellRect.origin.x = -(_workingView.frame.size.width)-80;
        cellRect.origin.y = (_workingView.frame.size.height + 30);
        [sheet showFromRect:cellRect inView: _workingView animated:YES];
    } else {*/
        [self addNewText];
   // }
}

#pragma mark- Actionsheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex;  // after animation

{
    if(buttonIndex==actionSheet.cancelButtonIndex){
        TextWithDateLoc=FALSE;
        return;
    }
    
    if(buttonIndex==0){
        TextWithDateLoc=FALSE;
        [self addNewText];
    }
    if(buttonIndex==1){
        TextWithDateLoc=TRUE;
        [self addNewText];
    }
}

- (void)addNewText
{
    _GhostLensTextCustomView *view = [_GhostLensTextCustomView new];

        view.fillColor = [UIColor blackColor];
        view.strokeColor = _settingView.selectedBorderColor;
        view.strokeWidth = _settingView.selectedBorderWidth;
        view.font = _settingView.selectedFont;
        CGFloat ratio = MIN( (0.2 * _workingView.width) / view.width, (0.05 * _workingView.height) / view.height);
        [view setScale:ratio];
        view.center = CGPointMake(_workingView.width/2, _workingView.height/2 + 10);

    
    [_workingView addSubview:view];
    [_GhostLensTextCustomView setActiveTextView:view];
    if (self.delegate) {
        [self.delegate addNewTextView:view];
    }
    
    [self beginTextEditting];
}
-(void)setActiveOfTextView:(BOOL)isbool
{
   // [self.selectedTextView setAvtive:NO];
    [_GhostLensTextCustomView setTextView:self.selectedTextView Avtive:isbool];
    
}
+(void)setTxtViewActive:(UIView*)txtView active:(BOOL)active
{
    if ([txtView isKindOfClass:[_GhostLensTextCustomView class]]) {
        [(_GhostLensTextCustomView*)txtView setAvtive:active];
    }
}
-(void)setTextViewSend
{
    [_workingView sendSubviewToBack:self.selectedTextView];
}
-(void)setAllTextViewSend
{
    for (UIView * subView in _workingView.subviews) {
        if ([subView isKindOfClass:[_GhostLensTextCustomView class]]) {
            if (subView != self.selectedTextView) {
                [_workingView sendSubviewToBack:subView];
            }
        }
    }
}
- (void)hideSettingView
{
    [_settingView endEditing:YES];
    _settingView.hidden = YES;
}

- (void)showSettingViewWithMenuIndex:(NSInteger)index
{
    if(_settingView.hidden){
        _settingView.hidden = NO;
        [_settingView showSettingMenuWithIndex:index animated:NO];
    }
    else{
        [_settingView showSettingMenuWithIndex:index animated:YES];
    }
}

- (void)beginTextEditting
{
    [self showSettingViewWithMenuIndex:0];
    [_settingView becomeFirstResponder];
}

- (void)setTextAlignment:(NSTextAlignment)alignment
{
    self.selectedTextView.textAlignment = alignment;
    
    
    _alignLeftBtn.selected = _alignCenterBtn.selected = _alignRightBtn.selected = NO;
    switch (alignment) {
        case NSTextAlignmentLeft:
            _alignLeftBtn.selected = YES;
            break;
        case NSTextAlignmentCenter:
            _alignCenterBtn.selected = YES;
            break;
        case NSTextAlignmentRight:
            _alignRightBtn.selected = YES;
            break;
        default:
            break;
    }
    [self.selectedTextView drawLabel];
}

- (void)pushedButton:(UIButton*)button
{
    if(_settingView.isFirstResponder){
        [_settingView resignFirstResponder];
    }
    else{
        [self hideSettingView];
        [self hideTextView];
    }
}
-(void)setTextViewFront
{
    [_workingView bringSubviewToFront:self.selectedTextView];
}
-(void)setAllTextViewFront
{
    for (UIView * subView in _workingView.subviews) {
        if ([subView isKindOfClass:[_GhostLensTextCustomView class]]) {
            if (subView != self.selectedTextView) {
                [_workingView bringSubviewToFront:subView];
            }
        }
    }
}
#pragma mark- Setting view delegate

- (void)textSettingView:(GhostLensTextCustomSettingView *)settingView didChangeText:(NSString *)text
{
    self.isUsing = YES;
    self.selectedTextView.text = text;
    [self.selectedTextView sizeToFitWithMaxWidth:_workingView.width lineHeight:0.2*_workingView.height];
    [self.selectedTextView drawLabel];
}

- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeFillColor:(UIColor*)fillColor
{
    self.isUsing = YES;
    _colorBtn.iconView.backgroundColor = fillColor;
    self.selectedTextView.fillColor = fillColor;
    [self.selectedTextView clearLabelStyle];
    [self.selectedTextView drawLabel];
}

- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeBorderColor:(UIColor*)borderColor
{
    self.isUsing = YES;
    _colorBtn.iconView.layer.borderColor = borderColor.CGColor;
    self.selectedTextView.strokeColor = borderColor;
    [self.selectedTextView showWhichLabel:0];
    [self.selectedTextView clearLabelStyle];
    [self.selectedTextView drawLabel];
}

- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeBorderWidth:(CGFloat)borderWidth
{
    self.isUsing = YES;
    _colorBtn.iconView.layer.borderWidth = MAX(2, 10*borderWidth);
    self.selectedTextView.strokeWidth = borderWidth;
    [self.selectedTextView clearLabelStyle];
    [self.selectedTextView drawLabel];
}

- (void)textSettingView:(GhostLensTextCustomSettingView *)settingView didChangeFont:(UIFont *)font fontIndex:(int)fontIndex
{
        self.isUsing = YES;
        self.selectedTextView.font = font;
        [self.selectedTextView sizeToFitWithMaxWidth:_workingView.width lineHeight:0.2*_workingView.height];
        [self.selectedTextView clearLabelStyle];
        [self.selectedTextView drawLabel];
}
- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeEffect:(int)index
{
    self.isUsing = YES;
    NSLog(@"---------------------- effect num %d",index);
    if (self.selectedTextView.text && self.selectedTextView.text.length > 100 &&[self isLowDevice]) {
        //[[SigtonUserInfo shareUserInfoArray] popWarning:self.superview labelText:NSLocalizedString(@"Only Support Up to 100 Chars", "")  fontSize:13];
        return;
    }
    [_GhostLensTextCustomView setTextViewLabelStyle:_selectedTextView Index:index];
    [self.selectedTextView drawLabel];
}
- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeCharSpace:(CGFloat)value
{
    self.isUsing = YES;
    [self.selectedTextView setLetterSpace:value];
    [self.selectedTextView sizeToFitWithMaxWidth:_workingView.width lineHeight:0.2*_workingView.height];
    [self.selectedTextView drawLabel];
}
- (void)textSettingView:(GhostLensTextCustomSettingView*)settingView didChangeLineSpace:(CGFloat)value
{
    self.isUsing = YES;
    [self.selectedTextView setLineSpace:value];
    [self.selectedTextView sizeToFitWithMaxWidth:_workingView.width lineHeight:0.2*_workingView.height];
    [self.selectedTextView drawLabel];
}

-(BOOL)isLowDevice
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *model = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    NSRange range1 = [model rangeOfString:@"iPad"];
    NSRange range2 = [model rangeOfString:@"iPhone"];
    NSRange range = [model rangeOfString:@","];
    if (range1.length > 0&&range.length > 0) {
        NSRange newRange;
        newRange.location = range1.length;
        newRange.length = range.location - range1.length;
        NSString * num = [model substringWithRange:newRange];
        if ([num intValue] >= 6) {
            return NO;
        }
    }else if (range2.length > 0&&range.length > 0)
    {
        NSRange newRange;
        newRange.location = range2.length;
        newRange.length = range.location - range2.length;
        NSString * num = [model substringWithRange:newRange];
        if ([num intValue] >= 8) {
            return NO;
        }
    }
    return YES;
}


@end






#pragma mark- _GhostLensTextCustomView

@implementation _GhostLensTextCustomView
{
    THLabel *_label;
 //   FXLabel     *_fxlabel;
    UIButton *_deleteButton;
    UIButton *_mirrorButton;
    CLCircleView *_circleView;
    
    CGFloat _scale;
    CGFloat _scalex;
    CGFloat _scaley;
    CGFloat _arg;
    
    CGPoint _initialPoint;
    CGFloat _initialArg;
    CGFloat _initialScale;
    CGFloat _MaininitialScale;
    BOOL isUsing;
}

+ (void)setActiveTextView:(_GhostLensTextCustomView*)view
{
    static _GhostLensTextCustomView *activeView = nil;
    if(view != activeView){
        [activeView setAvtive:NO];
        activeView = view;
        [activeView setAvtive:YES];
        
        [activeView.superview bringSubviewToFront:activeView];
        
        NSNotification *n = [NSNotification notificationWithName:GhostLensTextCustomViewActiveViewDidChangeNotification object:view userInfo:nil];
        [[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
    }
}

+(void)setTextView:(_GhostLensTextCustomView*)view Avtive:(BOOL)active
{
    [view setAvtive:active];
}
- (id)init
{
    self = [super initWithFrame:CGRectMake(0, 0, 132, 132)];
    if(self){
        NSDate *date = [NSDate date];
        
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        
        format.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        
        NSString *string = [format stringFromDate:date];
        NSArray* DateTime = [string componentsSeparatedByString: @" "];
        _imageDate = [[DateTime objectAtIndex: 0] stringByReplacingOccurrencesOfString:@":" withString:@"-"];
        _imageTime = [DateTime objectAtIndex: 1];
        
       /* [LocationTools getUserLocation:^(NSString *cityName) {
            _imageLocation = cityName;
            
        }];*/
        _label = [[THLabel alloc] init];
        [_label setTextColor:[CLImageEditorTheme toolbarTextColor]];
        _label.numberOfLines = 0;
        _label.backgroundColor = [UIColor clearColor];
        _label.layer.borderColor = [[UIColor blackColor] CGColor];
        _label.layer.cornerRadius = 5;
        _label.font = [UIFont systemFontOfSize:200];
        _label.minimumScaleFactor = 0.001;
        _label.adjustsFontSizeToFitWidth = YES;
        _label.textAlignment = NSTextAlignmentCenter;
        _label.strokeColor = kStrokeColor;
        _label.strokeSize = kStrokeSize;
        
        if(TextWithDateLoc==TRUE){
            _label.textAlignment = NSTextAlignmentLeft;
            self.text = [NSString stringWithFormat:@"%@ %@\n%@", _imageDate, _imageTime, _imageLocation];
        } else {
            self.text = @"";
        }
        
        [self addSubview:_label];
        
        CGSize size = [_label sizeThatFits:CGSizeMake(FLT_MAX, FLT_MAX)];
        _label.frame = CGRectMake(16, 16, size.width, size.height);
        self.frame = CGRectMake(0, 0, size.width + 32, size.height + 32);
        
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton setImage:[UIImage imageNamed:@"btn_delete.png"] forState:UIControlStateNormal];
        _deleteButton.frame = CGRectMake(0, 0, 32, 32);
        _deleteButton.center = _label.frame.origin;
        [_deleteButton addTarget:self action:@selector(pushedDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_deleteButton];
        
        _mirrorButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_mirrorButton setImage:[UIImage imageNamed:@"btn_mirror.png"] forState:UIControlStateNormal];
        _mirrorButton.frame = CGRectMake(0, 0, 32, 32);
        _mirrorButton.center = CGPointMake(_label.frame.origin.x, _label.frame.origin.y+_label.height);
        _mirrorButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        [_mirrorButton addTarget:self action:@selector(pushedMirrorBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_mirrorButton];
        
        _circleView = [[CLCircleView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        _circleView.center = CGPointMake(_label.width + _label.left, _label.height + _label.top);
        _circleView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
        _circleView.radius = 0.7;
        _circleView.color = [UIColor whiteColor];
        _circleView.borderColor = [UIColor blackColor];
        _circleView.borderWidth = 5;
        [self addSubview:_circleView];
        
        _arg = 0;
        [self setScale:1];
        
        _scale = 1;
        _scalex = 1;
        _scaley = 1;
        
        [self initGestures];
    }
    return self;
}
+(void)setTextViewLabelStyle:(_GhostLensTextCustomView*)view Index:(int)index
{
    [view setLabelStyle:index];
}
- (void)setLabelStyle:(int)styleID
{
    [self clearLabelStyle];
   // _label.hidden = no;
  //  _fxlabel.hidden = NO;
    switch (styleID)
    {
        case 1:
        {
            _label.shadowOffset = CGSizeMake(0.0f, 2.0f);
            _label.shadowColor = [UIColor colorWithWhite:0.0f alpha:0.75f];
            _label.shadowBlur = 5.0f;
            break;
        }
        case 2:
        {
            _label.shadowColor = [UIColor colorWithWhite:1.0f alpha:0.8f];
            _label.shadowOffset = CGSizeMake(1.0f, 1.0f);
            _label.shadowBlur = 2.0f;
            _label.innerShadowBlur = 2.0f;
            _label.innerShadowColor = [UIColor colorWithWhite:0.0f alpha:0.8f];
            _label.innerShadowOffset = CGSizeMake(1.0f, 1.0f);
            break;
        }
        case 3:
        {
            _label.gradientStartColor = [UIColor yellowColor];
            _label.gradientEndColor = [UIColor blueColor];
            break;
        }
        case 4:
        {
            _label.gradientStartPoint = CGPointMake(0.0f, 0.0f);
            _label.gradientEndPoint = CGPointMake(1.0f, 1.0f);
            _label.gradientColors = @[[UIColor redColor],
                                      [UIColor yellowColor],
                                      [UIColor greenColor],
                                      [UIColor cyanColor],
                                      [UIColor blueColor],
                                      [UIColor purpleColor],
                                      [UIColor redColor]];
            break;
        }
        case 5:
        {
            _label.shadowColor = [UIColor blackColor];
            _label.shadowOffset = kShadowOffset;
            _label.shadowBlur = kShadowBlur;
            _label.innerShadowBlur = 2.0f;
            _label.innerShadowColor = [UIColor whiteColor];
            _label.innerShadowOffset = CGSizeMake(1.0f, 1.0f);
            _label.gradientStartColor = [UIColor redColor];
            _label.gradientEndColor = [UIColor yellowColor];
            _label.gradientStartPoint = CGPointMake(0.0f, 0.5f);
            _label.gradientEndPoint = CGPointMake(1.0f, 0.5f);
         //   _label.oversampling = 2;
            
            break;
        }
        default:
        {
            
          //  _label.isEffect = NO;
            _label.shadowColor = nil;
            _label.shadowOffset = kShadowOffset;
            _label.shadowBlur = kShadowBlur;
//            _label.hidden = YES;
//            _label.hidden = NO;
            break;
        }
    }
    NSLog(@"setLabelStyle invoked");
}

- (void)clearLabelStyle
{
    _label.shadowColor = nil;
    _label.shadowOffset = CGSizeZero;
    _label.shadowBlur = 0.0f;
    _label.innerShadowBlur = 0.0f;
    _label.innerShadowColor = nil;
    _label.innerShadowOffset = CGSizeZero;
    _label.innerShadowBlur = 0.0f;
    
    _label.gradientStartColor = nil;
    _label.gradientEndColor = nil;
    _label.gradientStartPoint = CGPointMake(0.5f, 0.0f);
    _label.gradientEndPoint = CGPointMake(0.5f, 0.75f);
   // _label.oversampling = 0;
}



- (void)initGestures
{
    _label.userInteractionEnabled = YES;
    [_label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidTap:)]];
    [_label addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidPan:)]];
    [_circleView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(circleViewDidPan:)]];
    
//    _fxlabel.userInteractionEnabled = YES;
//    [_fxlabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidTap:)]];
//    [_fxlabel addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(viewDidPan:)]];
//    [_circleView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(circleViewDidPan:)]];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView* view= [super hitTest:point withEvent:event];
    if(view==self){
        return nil;
    }
    return view;
}

#pragma mark- Properties

- (void)setAvtive:(BOOL)active
{
    _deleteButton.hidden = !active;
    _mirrorButton.hidden = !active;
    _circleView.hidden = !active;
    _label.layer.borderWidth = (active) ? 1/_scale : 0;
  //  _fxlabel.layer.borderWidth = (active) ? 1/_scale : 0;
}

- (BOOL)active
{
    return !_deleteButton.hidden;
}

-(void)showWhichLabel:(int)index
{
//    if (index == 1) {
//        _label.hidden = YES;
//      //  _fxlabel.hidden = NO;
//    }else{
//      //  _fxlabel.hidden = YES;
//        _label.hidden = NO;
//       // changeEffectButSelect
//    }
}

- (void)sizeToFitWithMaxWidth:(CGFloat)width lineHeight:(CGFloat)lineHeight
{
    self.transform = CGAffineTransformIdentity;
    _label.transform = CGAffineTransformIdentity;
    UILabel *lab = [[UILabel alloc]initWithFrame:_label.frame];
    lab.numberOfLines = 0;
    lab.minimumScaleFactor = 0.001;
    lab.adjustsFontSizeToFitWidth = YES;
    lab.textAlignment = self.textAlignment;
    lab.text = self.text;
    
    NSString *str = [_label.font fontName];
    UIFont *font = [UIFont fontWithName:str size:_label.font.pointSize];
    lab.font = font;
    CGFloat maxwidth = width / (15/200.0);
    CGSize size = [lab sizeThatFits:CGSizeMake(maxwidth, FLT_MAX)];
    size = [self getSpaceLabelHeight:lab.text withFont:lab.font withWidth:maxwidth];
    CGFloat addh = size.height*0.5;
    if (addh > 600) {
        addh = 600;
    }
    _label.frame = CGRectMake(16, 16, size.width+100, size.height+addh);
    
    CGFloat viewW = (_label.width + 32);
    CGFloat viewH = _label.font.lineHeight;
    
    CGFloat ratio = MIN(width / viewW, lineHeight / viewH);
    [self setScale:ratio];
}

-(CGSize)getSpaceLabelHeight:(NSString*)str withFont:(UIFont*)font withWidth:(CGFloat)width {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paraStyle.alignment = self.textAlignment;
    paraStyle.lineSpacing = _label.lineSpacing; //设置行间距
    NSNumber * charspace = [NSNumber numberWithFloat:_label.letterSpacing];
    NSDictionary *dic = @{NSFontAttributeName:font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:charspace
                          };
    
    CGSize size = [str boundingRectWithSize:CGSizeMake(width, FLT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size;
}
-(void)setUsingBool:(BOOL)isbool
{
    isUsing = isbool;
}
-(BOOL)getUsingBool
{
    return isUsing;
}
- (void)setScale:(CGFloat)scale
{
    _scale = scale;
    if(_scalex<0){
        _scalex=scale*-1;
    } else {
        _scalex = scale;
    }
    _scaley = scale;
    
    self.transform = CGAffineTransformIdentity;
    
    _label.transform = CGAffineTransformMakeScale(_scalex, _scaley);
    
    CGRect rct = self.frame;
    rct.origin.x += (rct.size.width - (_label.width + 32)) / 2;
    rct.origin.y += (rct.size.height - (_label.height + 32)) / 2;
    rct.size.width  = _label.width + 32;
    rct.size.height = _label.height + 32;
    self.frame = rct;
    
    _label.center = CGPointMake(rct.size.width/2, rct.size.height/2);
    
    self.transform = CGAffineTransformMakeRotation(_arg);
    
    _label.layer.borderWidth = 1/_scale;
    _label.layer.cornerRadius = 3/_scale;
    isUsing = YES;
}

- (void)setFillColor:(UIColor *)fillColor
{
    _label.textColor = fillColor;
//    _fxlabel.textColor = fillColor;
//    _fxlabel.gradientStartColor = fillColor;
}

- (UIColor*)fillColor
{
    return _label.textColor;
}

- (void)setStrokeColor:(UIColor *)strokeColor
{
    //_label.outlineColor = borderColor;
    _label.strokeColor = strokeColor;
}

- (UIColor*)strokeColor
{
    //return _label.outlineColor;
    return _label.strokeColor;
}

- (void)setStrokeWidth:(CGFloat)strokeWidth
{
    //_label.outlineWidth = borderWidth;
    _label.strokeSize = strokeWidth * 20;
}

- (CGFloat)strokeWidth
{
   // return _label.outlineWidth;
    return _label.strokeSize;
}

- (void)setFont:(UIFont *)font
{
    _label.font = [font fontWithSize:200];
  //  _fxlabel.font = [font fontWithSize:200];
}

-(void)setLetterSpace:(CGFloat)value
{
    _label.letterSpacing = value;
}

-(void)setLineSpace:(CGFloat)value
{
    _label.lineSpacing = value;
}

- (UIFont*)font
{
    return _label.font;
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment
{
    _label.textAlignment = textAlignment;
   // _fxlabel.textAlignment = textAlignment;
}

- (NSTextAlignment)textAlignment
{
    return _label.textAlignment;
}

- (void)setText:(NSString *)text
{
    if(![text isEqualToString:_text]){
        _text = text;
        _label.textStr = (_text.length>0) ? _text : @"";
       // _fxlabel.text = (_text.length>0) ? _text : NSLocalizedStringWithDefaultValue(@"GhostLensTextCustomTool_DefaultTitle", nil, [CLImageEditorTheme bundle], @"Text", @"");
    }
}

#pragma mark- gesture events

- (void)pushedMirrorBtn:(id)sender
{
    [UIView animateWithDuration:kCLImageToolAnimationDuration animations:^{ _label.transform = CGAffineTransformMakeScale(_scalex*-1, _scale);}];
    
  //  [UIView animateWithDuration:kCLImageToolAnimationDuration animations:^{ _fxlabel.transform = CGAffineTransformMakeScale(_scalex*-1, _scale);}];
    _scalex=_scalex*-1;
    isUsing = YES;
}

- (void)pushedDeleteBtn:(id)sender
{
    _GhostLensTextCustomView *nextTarget = nil;
    
    const NSInteger index = [self.superview.subviews indexOfObject:self];
    
    for(NSInteger i=index+1; i<self.superview.subviews.count; ++i){
        UIView *view = [self.superview.subviews objectAtIndex:i];
        if([view isKindOfClass:[_GhostLensTextCustomView class]]){
            nextTarget = (_GhostLensTextCustomView*)view;
            break;
        }
    }
    
    if(nextTarget==nil){
        for(NSInteger i=index-1; i>=0; --i){
            UIView *view = [self.superview.subviews objectAtIndex:i];
            if([view isKindOfClass:[_GhostLensTextCustomView class]]){
                nextTarget = (_GhostLensTextCustomView*)view;
                break;
            }
        }
    }
    
    [[self class] setActiveTextView:nextTarget];
    [self removeFromSuperview];
}

- (void)viewDidTap:(UITapGestureRecognizer*)sender
{
    if(self.active){
        NSNotification *n = [NSNotification notificationWithName:GhostLensTextCustomViewActiveViewDidTapNotification object:self userInfo:nil];
        [[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotification:) withObject:n waitUntilDone:NO];
    }
    [[self class] setActiveTextView:self];
}

- (void)viewDidPan:(UIPanGestureRecognizer*)sender
{
    [[self class] setTextView:self Avtive:YES];
    [[self class] setActiveTextView:self];
    
    CGPoint p = [sender translationInView:self.superview];
    
    if(sender.state == UIGestureRecognizerStateBegan){
        _initialPoint = self.center;
    }
    self.center = CGPointMake(_initialPoint.x + p.x, _initialPoint.y + p.y);
    isUsing = YES;
}

-(void)drawLabel
{
    [_label drawViewNowRect];
}

- (void)circleViewDidPan:(UIPanGestureRecognizer*)sender
{
    CGPoint p = [sender translationInView:self.superview];
    
    static CGFloat tmpR = 1;
    static CGFloat tmpA = 0;
    if(sender.state == UIGestureRecognizerStateBegan){
        _initialPoint = [self.superview convertPoint:_circleView.center fromView:_circleView.superview];
        
        CGPoint p = CGPointMake(_initialPoint.x - self.center.x, _initialPoint.y - self.center.y);
        tmpR = sqrt(p.x*p.x + p.y*p.y);
        tmpA = atan2(p.y, p.x);
        
        _initialArg = _arg;
        _initialScale = _scale;
    }
    
    if(_MaininitialScale<=0){
        _MaininitialScale = _scale;
    }
    
    p = CGPointMake(_initialPoint.x + p.x - self.center.x, _initialPoint.y + p.y - self.center.y);
    CGFloat R = sqrt(p.x*p.x + p.y*p.y);
    CGFloat arg = atan2(p.y, p.x);
    
    _arg   = _initialArg + arg - tmpA;
    //[self setScale:MAX(_initialScale * R / tmpR, 15/200.0)];
    [self setScale:MAX(_initialScale * R / tmpR, (_MaininitialScale*0.1))];

    [[self class] setTextView:self Avtive:YES];
    [[self class] setActiveTextView:self];
}

@end


