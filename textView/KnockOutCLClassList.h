

#import <Foundation/Foundation.h>

@interface KnockOutCLClassList : NSObject

+ (NSArray*)subclassesOfClass:(Class)parentClass;

@end
