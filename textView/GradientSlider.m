//
//  XqSlider.m
//  ProgressBarWithColors
//
//  Created by 田相强 on 2017/12/23.
//  Copyright © 2017年 田相强. All rights reserved.
//

#import "GradientSlider.h"
#import "UIColor+Extensions.h"
@implementation GradientSlider

-(id)initWithFrame:(CGRect)frame
{
    if (self=[super initWithFrame:frame]) {
        [self normalSettings];
        [self loadGradientLayers];
    }
    return self;
}
-(void)normalSettings{
    self.minimumTrackTintColor=[UIColor clearColor];
    self.maximumTrackTintColor=[UIColor clearColor];
}
-(void)loadGradientLayers{
   self.colorArray = @[(id)[[UIColor colorWithHex:0xFF6347] CGColor],
                       (id)[[UIColor colorWithHex:0xFFEC8B] CGColor],
                      (id)[[UIColor colorWithHex:0x98FB98] CGColor],
                       (id)[[UIColor colorWithHex:0x00B2EE] CGColor],
                       (id)[[UIColor colorWithHex:0x9400D3] CGColor]];
   self.colorLocationArray = @[@0.1, @0.3, @0.5, @0.7, @1.0];
    self.gradientLayer =  [CAGradientLayer layer];
    self.gradientLayer.frame = CGRectMake(2,-5,self.frame.size.width-2,20);
    self.gradientLayer.masksToBounds = YES;
    self.gradientLayer.cornerRadius = 10;
    [self.gradientLayer setLocations:self.colorLocationArray];
    [self.gradientLayer setColors:self.colorArray];
    [self.gradientLayer setStartPoint:CGPointMake(0, 0)];
    [self.gradientLayer setEndPoint:CGPointMake(1.0, 0)];
    [self.layer addSublayer:self.gradientLayer];
}
@end
