

#import <UIKit/UIKit.h>

//! Project version number for THLabel.
FOUNDATION_EXPORT double THLabelVersionNumber;

//! Project version string for THLabel.
FOUNDATION_EXPORT const unsigned char THLabelVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <THLabel/PublicHeader.h>


typedef NS_ENUM(NSInteger, THLabelStrokePosition) {
	THLabelStrokePositionOutside,
	THLabelStrokePositionCenter,
	THLabelStrokePositionInside
};

typedef NS_OPTIONS(NSUInteger, THLabelFadeTruncatingMode) {
	THLabelFadeTruncatingModeNone = 0,
	THLabelFadeTruncatingModeTail = 1 << 0,
	THLabelFadeTruncatingModeHead = 1 << 1,
	THLabelFadeTruncatingModeHeadAndTail = THLabelFadeTruncatingModeHead | THLabelFadeTruncatingModeTail
};

@interface THLabel : UILabel

@property (nonatomic, assign) CGFloat letterSpacing;
@property (nonatomic, assign) CGFloat lineSpacing;

@property (nonatomic, assign) CGFloat shadowBlur;

@property (nonatomic, assign) CGFloat innerShadowBlur;
@property (nonatomic, assign) CGSize innerShadowOffset;
@property (nonatomic, strong) UIColor *innerShadowColor;
@property (nonatomic, strong) NSString *textStr;
@property (nonatomic, assign) CGFloat strokeSize;
@property (nonatomic, strong) UIColor *strokeColor;
@property (nonatomic, assign) THLabelStrokePosition strokePosition;
@property (nonatomic, strong) UIImageView *textImageView;
@property (nonatomic, strong) UIColor *gradientStartColor;
@property (nonatomic, strong) UIColor *gradientEndColor;
@property (nonatomic, copy) NSArray *gradientColors;
@property (nonatomic, assign) CGPoint gradientStartPoint;
@property (nonatomic, assign) CGPoint gradientEndPoint;

@property (nonatomic, assign) THLabelFadeTruncatingMode fadeTruncatingMode;

@property (nonatomic, assign) UIEdgeInsets textInsets;
@property (nonatomic, assign) BOOL automaticallyAdjustTextInsets;
- (void)drawViewNowRect;
@end
