

#import <Foundation/Foundation.h>

#import "CLImageToolSettings.h"

static const CGFloat kCLImageToolAnimationDuration = 0.5;
static const CGFloat kCLImageToolFadeoutDuration   = 0.5;



@interface CLImageToolBase : NSObject<CLImageToolProtocol>
{
    
}
@property (nonatomic, weak) UIViewController *editor;
@property (nonatomic, weak) CLImageToolInfo *toolInfo;

- (id)initWithImageEditor:(UIViewController*)editor withToolInfo:(CLImageToolInfo*)info;

- (void)setup:(CGRect)menuScrollRect workView:(UIView*)workView menuColor:(UIColor*)menuColor;
- (void)cleanup;
- (void)executeWithCompletionBlock:(void(^)(UIImage *image, NSError *error, NSDictionary *userInfo))completionBlock;

@end
