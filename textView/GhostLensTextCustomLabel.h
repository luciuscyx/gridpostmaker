

#import <UIKit/UIKit.h>

@interface GhostLensTextCustomLabel : UILabel

@property (nonatomic, strong) UIColor *outlineColor;
@property (nonatomic, assign) CGFloat outlineWidth;

@end
