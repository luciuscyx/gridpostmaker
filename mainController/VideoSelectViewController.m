//
//  InstaGridShareViewController.m
//  Instagrid Post
//
//  Created by Mangal on 02/07/15.
//  Copyright (c) 2015 RJLabs. All rights reserved.
//

#import "VideoSelectViewController.h"
#import "UIImage+EKTilesMaker.h"
#import "SHKActivityIndicator.h"
#import "UpgradeViewController.h"
#import "ImageHelper.h"
#import "ChangeVideoGridViewController.h"
//#import "HomeChangeVideoGridViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "UIView+MBProgressHUD.h"
//#import "MBProgressHUD.h"
#import "HandlerVideo.h"
#import <AssetsLibrary/AssetsLibrary.h>  // 必须导入
#import <MobileCoreServices/MobileCoreServices.h>
#import "CutVideoViewController.h"
#import "PostTabButton.h"
#import "guideHelpView.h"
#import "AppDelegate.h"
@interface VideoSelectViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UpgradeViewControllerDelegate,guideHelpViewDelegate>
{
    UpgradeViewController * objUpgradeViewController;
    NSMutableArray *selectArray;//存储选择后的数组坐标
    MPMoviePlayerController * player;
    BOOL loadsucess;//是否加载成功
    BOOL isprocessing; //加载时等待是否加载完判断
    AVPlayerLayer *playerLayer;
    NSURL *firstUrls;
    UIImagePickerController *imagePicker;
    UIImagePickerController *vodeoPicker;
}
@property (nonatomic,assign)int selectRow;
@property (nonatomic,strong)NSString *theVideoPath;
@property(nonatomic,retain)AVAsset * firstAsset;
@property(nonatomic,retain)AVAsset * secondAsset;
@property(nonatomic,retain)AVAsset * audioAsset;
@property (nonatomic,strong)NSMutableDictionary *dicUrl;//存储最后拼接后的视屏
@property (nonatomic,strong)NSMutableDictionary *dicImage;//存储图片转成的视频
@property (nonatomic,strong)NSMutableArray *buttonArray;
@property (nonatomic,assign)int  imagSize;
@property (nonatomic,strong)NSMutableArray *playArray;//存储已经创建的play
@property (nonatomic,assign)BOOL playStact;
@property (nonatomic,strong)NSMutableDictionary *playDiction;
@property (nonatomic,strong)AVPlayer *players;
@property(nonatomic,strong)MBProgressHUD * mbProgress;
@property (nonatomic,assign)BOOL homeBool;

@property (nonatomic,strong)UIView *natView;
@property (nonatomic,strong)UIImageView *tabImageView;

@property (nonatomic,strong)PostTabButton *addAlbumVButton; //从相册中选择视频
@property (nonatomic,strong)PostTabButton *addVideoButton; //录制视频
@property (nonatomic,strong)PostTabButton *deletevideoButton ;  //删除
@property (nonatomic,strong)PostTabButton *playVideoButton;  //播放

@property (nonatomic,strong)guideHelpView *guideView;//指引页

@end

@implementation VideoSelectViewController

@synthesize imageSource,orgimageSource;

@synthesize gridCount;
@synthesize docController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (arrayImageTiles)
    {
        arrayImageTiles=nil;
    }
    
    UIImageView *mainImage = [[UIImageView alloc]initWithFrame:self.view.bounds];
    mainImage.backgroundColor = [UIColor whiteColor];
    //mainImage.image = [UIImage imageNamed:@"bg"];
    [self.view addSubview:mainImage];
    
    arrayImageTiles = [[NSMutableArray alloc] init];
    self.playDiction = [[NSMutableDictionary alloc]init];
    self.dicUrl = [[NSMutableDictionary alloc]init];
    self.dicImage = [[NSMutableDictionary alloc]init];;
//    if (![[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]){
//        self.orgimageSource = imageSource;
//        imageSource = [self createWithWaterMarkonImage:imageSource];
//    }
    
    self.natView = [[UIView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth,60+KDNavH)];
    [self.view addSubview:self.natView];
    //添加左按钮
    UIButton *btnBacks = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBacks setFrame:CGRectMake(10, 15+KDNavH, 35,35)];
    [btnBacks setImage:[UIImage imageNamed:@"backBlack_icn"] forState:UIControlStateNormal];
    // [btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
    btnBacks.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [btnBacks addTarget:self action:@selector(btnBackPresseds) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:btnBacks];
    
    self.playArray = [[NSMutableArray alloc]init];
    //添加右按钮
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(ScreenWidth - 45, 15+KDNavH, 35, 35)];
    [btnNext setImage:[UIImage imageNamed:@"doneSel_icn"] forState:UIControlStateNormal];
    // [btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
    btnNext.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [btnNext addTarget:self action:@selector(nextAction) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:btnNext];
    
    UILabel * instructionLabel = [[UILabel alloc] init];
    //CGFloat orgx = btnBacks.frame.origin.x+btnBacks.frame.size.width+10;
    instructionLabel.frame = CGRectMake(VIEW_XW(btnBacks)+20,KDNavH+10,ScreenWidth - (VIEW_XW(btnBacks)+20)*2,50);//CGRectMake(orgx, btnBacks.frame.origin.y, btnNext.frame.origin.x-orgx-10, btnBacks.frame.size.height+10);
    instructionLabel.textColor = [UIColor blackColor];
    instructionLabel.text = NSLocalizedString(@"TAP EACH PIC TO ADD VIDEO",nil);
    instructionLabel.numberOfLines = 0;
    instructionLabel.textAlignment = NSTextAlignmentCenter;
    instructionLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    [self.natView addSubview:instructionLabel];
    
    self.tabImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, ScreenHeight - 50 - KDNavH,ScreenWidth,50 + KDNavH)];
    self.tabImageView.image = [UIImage imageNamed:@"finishIg"];
    self.tabImageView.userInteractionEnabled = YES;
    [self.view addSubview:self.tabImageView];
//    UIButton *popbutton = [[UIButton alloc]initWithFrame:CGRectMake(10,10, 30, 30)];
//    [popbutton setImage:[UIImage imageNamed:@"video_pop"] forState:UIControlStateNormal];
//    [popbutton addTarget:self action:@selector(btnBackPresseds) forControlEvents:UIControlEventTouchUpInside];
//    [self.tabImageView addSubview:popbutton];
    CGFloat tabLinW = (kDeviceWidth - 40*4)/4;
    CGFloat tabButW = 40;

    self.addAlbumVButton = [[PostTabButton alloc]initWithFrame:CGRectMake(tabLinW/2, 5, tabButW, tabButW)];
    [self.addAlbumVButton setImage:[UIImage imageNamed:@"album_sel"] forState:UIControlStateNormal];
    [self.addAlbumVButton setTitle:NSLocalizedString(@"Album",nil) forState:UIControlStateNormal];
    [self.addAlbumVButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.addAlbumVButton addTarget:self action:@selector(selectAlbumVideo) forControlEvents:UIControlEventTouchUpInside];
    self.addAlbumVButton.enabled = NO;
    [self.tabImageView addSubview:self.addAlbumVButton];
    
    self.addVideoButton = [[PostTabButton alloc]initWithFrame:CGRectMake(VIEW_XW(self.addAlbumVButton)+tabLinW, 5, tabButW, tabButW)];
    [self.addVideoButton setImage:[UIImage imageNamed:@"addVideo_icn"] forState:UIControlStateNormal];
    [self.addVideoButton setTitle:NSLocalizedString(@"Record",nil) forState:UIControlStateNormal];
    [self.addVideoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.addVideoButton addTarget:self action:@selector(addVideoPresseds) forControlEvents:UIControlEventTouchUpInside];
    self.addVideoButton.enabled = NO;
    [self.tabImageView addSubview:self.addVideoButton];
    
    self.deletevideoButton = [[PostTabButton alloc]initWithFrame:CGRectMake(VIEW_XW(self.addVideoButton)+tabLinW , 5, tabButW, tabButW)];
    [self.deletevideoButton setImage:[UIImage imageNamed:@"deleteIg_icn"] forState:UIControlStateNormal];
    [self.deletevideoButton setTitle:NSLocalizedString(@"Delete",nil) forState:UIControlStateNormal];
    [self.deletevideoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.deletevideoButton addTarget:self action:@selector(deleteVideoPresseds) forControlEvents:UIControlEventTouchUpInside];
    self.deletevideoButton.enabled = NO;
    [self.tabImageView addSubview:self.deletevideoButton];
    
    self.playVideoButton = [[PostTabButton alloc]initWithFrame:CGRectMake(VIEW_XW(self.deletevideoButton)+tabLinW, 5, tabButW, tabButW)];
    [self.playVideoButton setImage:[UIImage imageNamed:@"displayVideo_icn"] forState:UIControlStateNormal];
    [self.playVideoButton setImage:[UIImage imageNamed:@"playVideo_icn"] forState:UIControlStateSelected];
    [self.playVideoButton setTitle:NSLocalizedString(@"Play",nil) forState:UIControlStateNormal];
    [self.playVideoButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.playVideoButton addTarget:self action:@selector(playVideoPresseds:) forControlEvents:UIControlEventTouchUpInside];
    self.playVideoButton.enabled = NO;
    [self.tabImageView addSubview:self.playVideoButton];
    
    
    //for(int i = 0;i < )
    
//    UILabel * instructionLabel = [[UILabel alloc] init];
//    CGFloat orgx = btnBacks.frame.origin.x+btnBacks.frame.size.width+10;
//    instructionLabel.frame = CGRectMake(orgx, btnBacks.frame.origin.y, btnNext.frame.origin.x-orgx-10, btnBacks.frame.size.height+10);
//    instructionLabel.textColor = [UIColor whiteColor];
//    instructionLabel.text = NSLocalizedString(@"TAP EACH PIC TO ADD VIDEO",nil);
//    instructionLabel.numberOfLines = 0;
//    instructionLabel.textAlignment = NSTextAlignmentCenter;
//    instructionLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
//    [self.view addSubview:instructionLabel];
//    CGFloat height = btnBacks.frame.size.height+10;
//
//    if (![[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]){
//        UIButton * upgradeButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        upgradeButton.frame = CGRectMake(0, 0, self.view.frame.size.width/3, height-10);
//        upgradeButton.layer.cornerRadius = 10;
//        upgradeButton.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height-height/2-2);
//        upgradeButton.backgroundColor = [UIColor darkGrayColor];
//        [upgradeButton addTarget:self action:@selector(btnUpgradePressed:) forControlEvents:UIControlEventTouchUpInside];
//        [upgradeButton setBackgroundImage:[UIImage imageNamed:@"startover"] forState:UIControlStateNormal];
//        upgradeButton.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
//        [upgradeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [upgradeButton setTitle:NSLocalizedString(@"NO WATERMARK",nil) forState:UIControlStateNormal];
//        [self.view addSubview:upgradeButton];
//    }
    //    else
    //    {
    //        instructionLabel = [[UILabel alloc] init];
    //        orgx = btnBacks.frame.origin.x+btnBacks.frame.size.width+10;
    //
    //        instructionLabel.frame = CGRectMake(orgx, self.view.frame.size.height-height-10, btnNext.frame.origin.x-orgx-10, height);
    //        instructionLabel.textColor = [UIColor whiteColor];
    //        instructionLabel.text = @"TAP EACH PIC TO ADD VIDEO";
    //        instructionLabel.numberOfLines = 0;
    //        instructionLabel.textAlignment = NSTextAlignmentCenter;
    //
    //        instructionLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
    //        [self.view addSubview:instructionLabel];
    //    }
}
-(void)viewWillAppear:(BOOL)animated
{
    while (isprocessing) {
        sleep(0.1);
    }
    if(self.selectHome){
        [self createHomeTilesForZoomLevel];
    }else{
        [self createTilesForZoomLevel];
    }
    NSUserDefaults *useDef = [NSUserDefaults standardUserDefaults];
    NSInteger enterSum = [[useDef objectForKey:@"enterGuideSum"] integerValue];
    enterSum  = enterSum+1;
    [useDef setInteger:enterSum forKey:@"enterGuideSum"];
    [useDef synchronize];
    if(self.guideView){
        [self.guideView dismissMaskView];
    }
    if(enterSum == 1){
        [self createGuideHelpView];
    }
}
#pragma mark ---- 加载指引页
- (void)createGuideHelpView{
    CGRect maskRect = CGRectMake(self.addAlbumVButton.frame.origin.x-5,kDeviceHeight - VIEW_H(self.tabImageView),VIEW_XW(self.addVideoButton) - self.addAlbumVButton.frame.origin.x+10 ,VIEW_H(self.addAlbumVButton)+10);//self.textLabel.frame;
    UIImage *guideImage ;
    CGFloat guideImageW;
    if([APPGate IsCH]){
        guideImage = [UIImage imageNamed:@"imageGuide0"];
        guideImageW = 200;
    }else{
        guideImage = [UIImage imageNamed:@"imageGuide0_En"];
        guideImageW = 200;
    }
    CGRect imageRect = CGRectMake(maskRect.origin.x+maskRect.size.width/2,maskRect.origin.y- (guideImageW*guideImage.size.height/guideImage.size.width),guideImageW,guideImageW*guideImage.size.height/guideImage.size.width);
    
    CGRect buttonRect = CGRectMake(ScreenWidth - 120,KDNavH+30,100,40);
    self.guideView = [guideHelpView new];
    self.guideView.delegate = self;
    [self.guideView addImageViewImage:guideImage helpImageRect:imageRect buttonText:NSLocalizedString(@"I See",nil) buttonRect:buttonRect maskViewRect:maskRect isFinish:NO];
    [self.guideView showMaskViewInView:self.view];
}
#pragma mark ---- maskGuideViewDelegate
- (void)finishGuideHeip{
    [self.guideView dismissMaskView];
}
//- (IBAction)btnUpgradePressed:(id)sender
//{
//    objUpgradeViewController = [[UpgradeViewController alloc] initWithNibName:@"UpgradeViewController" bundle:nil];
//    objUpgradeViewController.delegate = self;
//    [RJAnimation bounceAddSubViewToParentView:self.view ChildView:objUpgradeViewController.view];
//}

//-(void)upgradeSuccess
//{
//    if (self.orgimageSource) {
//        self.imageSource = self.orgimageSource;
//        [self createTilesForZoomLevel];
//    }
//}
//-(UIImage*)createWithWaterMarkonImage:(UIImage*)image{
//    CGSize size = image.size;
//    UIGraphicsBeginImageContext(size);
//    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
//    UIImage* waterMarkImage = [UIImage imageNamed:@"watermark"];
//
//    float scaleFactor = image.size.width / 320;
//    float newHeight = waterMarkImage.size.height * scaleFactor;
//    float newWidth = waterMarkImage.size.width * scaleFactor;
//    if (newHeight > size.height/3) {
//        newWidth = (size.height/3)*newWidth/newHeight;
//        newHeight = size.height/3;
//    }
//    [waterMarkImage drawInRect:CGRectMake(size.width - newWidth,size.height-newHeight, newWidth,newHeight)];
//    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return destImage;
//}
- (void)createTilesForZoomLevel
{
    if (arrayImageTiles) {
        [arrayImageTiles removeAllObjects];
    }
    UIImage *sourceImage = imageSource;
    NSUInteger row = 0;
    CGFloat offsetY = 0;
    CGFloat singlew= sourceImage.size.width/3;
    int cmph = sourceImage.size.height;
    int a = cmph/(int)singlew;
    // while (((int)offsetY+1) < cmph) {
    for(int i=0;i<a;i++){
        NSUInteger column = 0;
        CGFloat offsetX = 0;
        
        while (offsetX < sourceImage.size.width) {
            CGRect tileFrame = CGRectMake(offsetX, offsetY, singlew,singlew);
            
            UIImage *tileImage = [imageSource imageInRect:tileFrame];
            
            NSData *imageData =  UIImageJPEGRepresentation(tileImage, 1);
            
            [arrayImageTiles addObject:imageData];
            column += 1;
            offsetX += singlew;
        }
        row += 1;
        offsetY += singlew;
    }
    //数组反序
    arrayImageTiles=(NSMutableArray *)[[arrayImageTiles reverseObjectEnumerator] allObjects];
    [self updateView];
}
- (void)createHomeTilesForZoomLevel
{
    if (arrayImageTiles) {
        [arrayImageTiles removeAllObjects];
    }
    UIImage *sourceImage = imageSource;
    NSUInteger row = 0;
    CGFloat offsetY = 0;
    CGFloat singlew = sourceImage.size.width/gridCount;
    int cmph = sourceImage.size.height;
    int a = cmph/(int)singlew;
    // while (((int)offsetY+1) < cmph) {
    for(int i=0;i<a;i++){
        NSUInteger column = 0;
        CGFloat offsetX = 0;
        
        while (offsetX < sourceImage.size.width) {
            CGRect tileFrame = CGRectMake(offsetX, offsetY, singlew,singlew);
            
            UIImage *tileImage = [imageSource imageInRect:tileFrame];
            
            NSData *imageData =  UIImageJPEGRepresentation(tileImage, 1);
            
            [arrayImageTiles addObject:imageData];
            column += 1;
            offsetX += singlew;
        }
        row += 1;
        offsetY += singlew;
    }
    //数组反序
    arrayImageTiles=(NSMutableArray *)[[arrayImageTiles reverseObjectEnumerator] allObjects];
    [self updateHomeView];
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)updateView
{
    NSInteger enterSum = [[[NSUserDefaults standardUserDefaults] objectForKey:@"enterGuideSum"] integerValue];
    selectArray = [[NSMutableArray alloc]init];
    for (UIView * subView in self.view.subviews) {
        if (subView.tag >= 100) {
            [subView removeFromSuperview];
        }
    }
    CGFloat maxh = ScreenHeight-60-65;
    CGSize showSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.width*self.imageSource.size.height/self.imageSource.size.width);
    if (showSize.height > maxh) {
        showSize = CGSizeMake(self.view.frame.size.width*maxh/showSize.height, maxh);
    }
    CGFloat beginX = (self.view.frame.size.width-showSize.width)*0.5;
    CGFloat beginY = (self.view.frame.size.height-showSize.height)*0.5;
    CGFloat singleW = showSize.width/3;
    self.imagSize = singleW;
    UIFont *tmp1 = [UIFont fontWithName:@"HelveticaNeue" size:18.0f];
    //取出字典里的Key
    NSArray* arr = [self.dicUrl allKeys];
    //图片视图分块
    for (int i = 0; i < arrayImageTiles.count; i ++ ) {
        UIImageView * imgView = [self.view viewWithTag:100+i];
        if (imgView) {
            [imgView removeFromSuperview];
        }
        imgView = [[UIImageView alloc] init];
        imgView.image = [UIImage imageWithData:[arrayImageTiles objectAtIndex:arrayImageTiles.count-1-i]];
        imgView.frame = CGRectMake(beginX+(i%3)*singleW, beginY+((int)(i/3))*singleW, singleW, singleW);
        imgView.tag = 100+i;
        [self.view addSubview:imgView];
        //选择拼接视频后，取出字典对应的key,显示为视屏状态
        UIImageView *roundImg = [[UIImageView alloc]init];
        for(NSString* str in arr){
            int row = [str intValue];
            if(i == row){
                roundImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stop"]];
                roundImg.frame = CGRectMake(imgView.frame.origin.x+imgView.frame.size.width/3, imgView.frame.origin.y+imgView.frame.size.height/3, imgView.frame.size.width/3, imgView.frame.size.height/3);
                roundImg.tag = 300+i;
                [self.view addSubview:roundImg];
                // 初始化播放器item
                NSURL *videoURL = [NSURL fileURLWithPath:self.dicUrl[str]];
                AVPlayerItem *playerItem = [[AVPlayerItem alloc] initWithURL:videoURL];
                self.players = [[AVPlayer alloc] initWithPlayerItem:playerItem];
                // 初始化播放器的Layer
                playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.players];
                //取出添加视频的视图
                playerLayer.frame = imgView.bounds;
                playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                // 把Layer加到底部View上<可以>
                [imgView.layer insertSublayer:playerLayer atIndex:0];
                //把AVPlayer存储到字典里
                self.playDiction[str] =  self.players;
                //self.playDiction[]
            }
        }
        //可点击的按钮
        UIButton * btn = [self.view viewWithTag:200+i];
        if (btn) {
            [btn removeFromSuperview];
        }
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.titleLabel.font = tmp1;
        btn.frame = imgView.frame;
        btn.tag = 200+i;
        CALayer * layer = [btn layer];
        //在引导页的时候默认选择第一个
        if(enterSum == 0 && i == 0){
            layer.borderColor = mainColor.CGColor;
            self.selectRow = 200+i;
        }else{
            self.addVideoButton.enabled = NO;
            self.addAlbumVButton.enabled = NO;
            layer.borderColor =  [[UIColor colorWithRed:61/255.0 green:39/255.0 blue:63/255.0 alpha:1.0] CGColor];//[[UIColor whiteColor] CGColor];
        }
        layer.borderWidth = 1.0f;
        [btn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
    if(enterSum == 0){
       self.addVideoButton.enabled = YES;
       self.addAlbumVButton.enabled = YES;
    }
}
-(void)updateHomeView
{
    selectArray = [[NSMutableArray alloc]init];
    for (UIView * subView in self.view.subviews) {
        if (subView.tag >= 100) {
            [subView removeFromSuperview];
        }
    }
    CGFloat maxh = ScreenHeight-60-65;
    CGSize showSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.width*self.imageSource.size.height/self.imageSource.size.width);
    if (showSize.height > maxh) {
        showSize = CGSizeMake(self.view.frame.size.width*maxh/showSize.height, maxh);
    }
    CGFloat beginX = (self.view.frame.size.width-showSize.width)*0.5;
    CGFloat beginY = (self.view.frame.size.height-showSize.height)*0.5;
    CGFloat singleW = showSize.width/gridCount;
    self.imagSize = singleW;
    UIFont *tmp1 = [UIFont fontWithName:@"HelveticaNeue" size:18.0f];
    //取出字典里的Key
    NSArray* arr = [self.dicUrl allKeys];
    //图片视图分块
    for (int i = 0; i < arrayImageTiles.count; i ++ ) {
        UIImageView * imgView = [self.view viewWithTag:100+i];
        if (imgView) {
            [imgView removeFromSuperview];
        }
        imgView = [[UIImageView alloc] init];
        imgView.image = [UIImage imageWithData:[arrayImageTiles objectAtIndex:arrayImageTiles.count-1-i]];
        imgView.frame = CGRectMake(beginX+(i%gridCount)*singleW, beginY+((int)(i/gridCount))*singleW, singleW, singleW);
        imgView.tag = 100+i;
        [self.view addSubview:imgView];
        //选择拼接视频后，取出字典对应的key,显示为视屏状态
        UIImageView *roundImg = [[UIImageView alloc]init];
        for(NSString* str in arr){
            int row = [str intValue];
            if(i == row){
                roundImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stop"]];
                roundImg.frame = CGRectMake(imgView.frame.origin.x+imgView.frame.size.width/3, imgView.frame.origin.y+imgView.frame.size.height/3, imgView.frame.size.width/3, imgView.frame.size.height/3);
                roundImg.tag = 300+i;
                [self.view addSubview:roundImg];
                // 初始化播放器item
                NSURL *videoURL = [NSURL fileURLWithPath:self.dicUrl[str]];
                AVPlayerItem *playerItem = [[AVPlayerItem alloc] initWithURL:videoURL];
                self.players = [[AVPlayer alloc] initWithPlayerItem:playerItem];
                // 初始化播放器的Layer
                playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.players];
                //取出添加视频的视图
                playerLayer.frame = imgView.bounds;
                playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                // 把Layer加到底部View上<可以>
                [imgView.layer insertSublayer:playerLayer atIndex:0];
                //把AVPlayer存储到字典里
                self.playDiction[str] =  self.players;
                //self.playDiction[]
            }
        }
        //可点击的按钮
        UIButton * btn = [self.view viewWithTag:200+i];
        if (btn) {
            [btn removeFromSuperview];
        }
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.titleLabel.font = tmp1;
        btn.frame = imgView.frame;
        btn.tag = 200+i;
        CALayer * layer = [btn layer];
        layer.borderColor =  mainColor.CGColor;//[[UIColor colorWithRed:61/255.0 green:39/255.0 blue:63/255.0 alpha:1.0] CGColor];//[[UIColor whiteColor] CGColor];
        layer.borderWidth = 1.0f;
        [btn addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
}
////选择框
//- (void)addView:(UIButton *)imaeBut withindex:(int)index{
//    //选择的时候来添加视频
//    NSArray *arrayKey =  [self.dicUrl allKeys];
//    NSString *strKey = [NSString stringWithFormat:@"%d",index-200];
//    //判断是否是视频
//    if([arrayKey containsObject:strKey]){
//        //调用用录屏按钮
//        UIView *selectView = [[UIView alloc]init];
//        selectView.tag = index+400;//i+600
//        selectView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.6];
//        //int b = 35
//        int u = (200-imaeBut.frame.size.width)/2;
//        int x;
//        if(imaeBut.frame.origin.x-u<0){
//            x  = imaeBut.frame.origin.x;
//        }else if(imaeBut.frame.origin.x+imaeBut.frame.size.width+u>ScreenWidth){
//            x = imaeBut.frame.origin.x-2*u;
//        }else{
//            x =imaeBut.frame.origin.x-u;
//        }
//        selectView.frame = CGRectMake(x,imaeBut.frame.origin.y + imaeBut.frame.size.height,2*u+imaeBut.frame.size.width,45);
//        selectView.layer.cornerRadius = selectView.frame.size.height/2;
//        selectView.layer.masksToBounds = YES;
//        [self.view addSubview:selectView];
//
//        //添加相册按钮（只调用Video）
//        UIButton *selectVideo = [UIButton buttonWithType:UIButtonTypeCustom];
//        [selectVideo setFrame:CGRectMake(10,5,35,35)];
//        [selectVideo setImage:[UIImage imageNamed:@"picture_"] forState:UIControlStateNormal];
//        //[selectVideo setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
//        selectVideo.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
//        [selectVideo addTarget:self action:@selector(selectVideo) forControlEvents:UIControlEventTouchUpInside];
//        [selectView addSubview:selectVideo];
//
//        //录制视频按钮
//        UIButton *video = [UIButton buttonWithType:UIButtonTypeCustom];
//        [video setFrame:CGRectMake(VIEW_XW(selectVideo)+10,5,35,35)];
//        [video setImage:[UIImage imageNamed:@"video_"] forState:UIControlStateNormal];
//        //[btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
//        video.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
//        [video addTarget:self action:@selector(addVideo) forControlEvents:UIControlEventTouchUpInside];
//        [selectView addSubview:video];
//        //删除按钮
//        UIButton *delete = [UIButton buttonWithType:UIButtonTypeCustom];
//        [delete setFrame:CGRectMake(VIEW_XW(video)+10,5,35,35)];
//        [delete setImage:[UIImage imageNamed:@"trash"] forState:UIControlStateNormal];
//        delete.tag = index+300;//i+500
//        //[btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
//        delete.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
//        [delete addTarget:self action:@selector(deleteVideo:) forControlEvents:UIControlEventTouchUpInside];
//        [selectView addSubview:delete];
//
//        //调用播放按钮
//        UIButton *selectPlay = [UIButton buttonWithType:UIButtonTypeCustom];
//        [selectPlay setFrame:CGRectMake(VIEW_XW(delete)+10,5,35,35)];
//        [selectPlay setImage:[UIImage imageNamed:@"play_"] forState:UIControlStateNormal];
//        [selectPlay setImage:[UIImage imageNamed:@"suspend_"] forState:UIControlStateSelected];
//        selectPlay.tag = index+200;//i+400
//        //[selectVideo setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
//        //selectPlay.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
//        [selectPlay addTarget:self action:@selector(selectPlay:) forControlEvents:UIControlEventTouchUpInside];
//        [selectView addSubview:selectPlay];
//
//    }else{
//        //调用用录屏按钮
//        UIView *selectView = [[UIView alloc]init];
//        selectView.tag = index+400;//i+600
//        selectView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.6];
//        //需要考虑平板适配问题
//        int u = (110-imaeBut.frame.size.width)/2;
//        int x;
//        if(imaeBut.frame.origin.x-u<0){
//            x  = imaeBut.frame.origin.x;
//        }else if(imaeBut.frame.origin.x+imaeBut.frame.size.width+u>ScreenWidth){
//            x = imaeBut.frame.origin.x-2*u;
//        }else{
//            x =imaeBut.frame.origin.x-u;
//        }
//        selectView.frame = CGRectMake(x,imaeBut.frame.origin.y + imaeBut.frame.size.height,2*u+imaeBut.frame.size.width,45);
//        selectView.layer.cornerRadius = selectView.frame.size.height/2;
//        selectView.layer.masksToBounds = YES;
//        [self.view addSubview:selectView];
//
//        //添加相册按钮（只调用Video）
//        UIButton *selectVideo = [UIButton buttonWithType:UIButtonTypeCustom];
//        [selectVideo setFrame:CGRectMake(10,5,35,35)];
//        [selectVideo setImage:[UIImage imageNamed:@"picture_"] forState:UIControlStateNormal];
//        //[selectVideo setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
//        selectVideo.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
//        [selectVideo addTarget:self action:@selector(selectVideo) forControlEvents:UIControlEventTouchUpInside];
//        [selectView addSubview:selectVideo];
//
//        //录制视频按钮
//        UIButton *video = [UIButton buttonWithType:UIButtonTypeCustom];
//        [video setFrame:CGRectMake(VIEW_XW(selectVideo)+20,5,35,35)];
//        [video setImage:[UIImage imageNamed:@"video_"] forState:UIControlStateNormal];
//        //[btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
//        video.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
//        [video addTarget:self action:@selector(addVideo) forControlEvents:UIControlEventTouchUpInside];
//        [selectView addSubview:video];
//    }}

#pragma mark --- 删除视频事件
- (void)deleteVideoPresseds{
    NSString *deleteStr = [NSString stringWithFormat:@"%d",self.selectRow-200];
    [self.dicUrl removeObjectForKey:deleteStr];
    [self createTilesForZoomLevel];
}
#pragma mark ---- 进入下一个页面
- (void)nextAction{
    if(self.dicUrl){
        ChangeVideoGridViewController *change = [[ChangeVideoGridViewController alloc]init];
        change.imageArray = (NSMutableArray *)[[arrayImageTiles reverseObjectEnumerator] allObjects];
        change.rowArray = [self.dicUrl allKeys];
        change.homeBool = self.selectHome;
        change.gridCount = self.gridCount;
        change.imageSelect = self.imageSource;
        change.dicUrl = self.dicUrl;
        [self.navigationController pushViewController:change animated:YES];
    }else{
        
    }
}
#pragma mark --- 选择播放按钮
- (void)playVideoPresseds:(UIButton *)sender{
    // self.playVideoButton.enabled = YES;
    WeakSelf;
    for(NSString *str in [self.dicUrl allKeys]){
        if(self.selectRow-200 == [str intValue]){
            
        }else{
            self.players = self.playDiction[str];
            [self.players seekToTime:CMTimeMake(0, 1)];
            [self.players pause];
            UIImageView *roundImgs= [self.view viewWithTag:[str intValue]+300];
            roundImgs.hidden = NO;
        }
    }
    sender.selected =! sender.selected;
    //    if(sender.selected){
    //       [sender setImage:[UIImage imageNamed:@"suspend_"] forState:UIControlStateNormal];
    //    }else{
    //       [sender setImage:[UIImage imageNamed:@"play_"] forState:UIControlStateNormal];
    //    }
    NSString *str = [NSString stringWithFormat:@"%d",self.selectRow-200];
    //同步视图上的播放状态
    UIImageView *roundImg = [self.view viewWithTag:self.selectRow+100];
    self.players = self.playDiction[str];
    [self.players addPeriodicTimeObserverForInterval:CMTimeMake(1.0, 1.0) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        float current=CMTimeGetSeconds(time);
        float total=CMTimeGetSeconds([weakSelf.players.currentItem duration]);
        // NSLog(@"当前已经播放%.2fs.",current);
        if (current == total) {
            //   NSLog(@"mp3播放完毕");
            sender.selected = NO;
            //[sender setImage:[UIImage imageNamed:@"play_"] forState:UIControlStateNormal];
            [weakSelf.players pause];
            roundImg.hidden = NO;
            [weakSelf.players seekToTime:CMTimeMake(0, 1)];
        }
    }];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:AVPlayerItemDidPlayToEndTimeNotification  object:self.players.currentItem];
    if(sender.selected){
        roundImg.hidden = YES;
        [self.players play];
    }else{
        [self.players pause];
        roundImg.hidden = NO;
    }
}
/**
 *  播放完成通知
 *
 *  @param notification 通知对象
 */
-(void)playbackFinished:(NSNotificationCenter *)notification{
    NSLog(@"视频播放完成.");
    // 播放完成后重复播放
    // 跳到最新的时间点开始播放
    self.playStact = NO;
    [self.players seekToTime:CMTimeMake(0, 1)];
    [self.players pause];
}
#pragma mark ------- 调用相册添加视频
- (void)selectAlbumVideo{
    //在选择视频时，把图片转成视频，节省时间，避免堵塞，否则会出错了
    //选择模块
    // WeakSelf;
    // [self.view showWaiting:NO message:@"加载中.."];
    if(self.selectRow != 0){
        NSString *key = [NSString stringWithFormat:@"%d",self.selectRow-200];
        NSLog(@"%@",self.dicUrl);
        //如果已经存在就不需要再次创建
        if([[self.dicUrl allKeys] containsObject:key]){
            firstUrls =  self.dicImage[key];
        }else{
            //图片转视频
            UIImage *image = [UIImage imageWithData:[arrayImageTiles objectAtIndex:arrayImageTiles.count-1-self.selectRow+200]];
            [[HandlerVideo sharedInstance] testCompressionSession:self.selectRow-100 image:image videoSize:CGSizeMake(ScreenWidth,ScreenWidth) composesBlock:^(BOOL success, NSString *path){
                //                dispatch_async(dispatch_get_main_queue(), ^{
                //                    [weakSelf.view hiddenWaiting];
                //                });
                if(success){
                    NSLog(@"图片合成成功");
                    firstUrls = [NSURL fileURLWithPath:path];
                    self.dicImage[key] = firstUrls;
                }else{
                    NSLog(@"合成失败");
                }
            }];
        }
    }
    //归零
    //        self.selectRow = 0;
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    // imagePicker.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    imagePicker.allowsEditing = YES;
    imagePicker.videoMaximumDuration = 59;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = @[(NSString *)kUTTypeMovie];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

#pragma mark ----- 录像添加视频
//录像
- (void)addVideoPresseds{
    //在录视频时，把图片转成视频，节省时间，避免堵塞，否则会出错了
    //选择模块
    //    WeakSelf;
    //    [self.view showWaiting:NO message:@"加载中.."];
    if(self.selectRow != 0){
        NSString *key = [NSString stringWithFormat:@"%d",self.selectRow-200];
        NSLog(@"%@",self.dicUrl);
        if([[self.dicUrl allKeys] containsObject:key]){
            firstUrls = self.dicImage[key];
        }else{
            //图片转视频
            UIImage *image = [UIImage imageWithData:[arrayImageTiles objectAtIndex:arrayImageTiles.count-1-self.selectRow+200]];
            [[HandlerVideo sharedInstance] testCompressionSession:self.selectRow-100 image:image videoSize:CGSizeMake(ScreenWidth,ScreenWidth) composesBlock:^(BOOL success, NSString *path){
                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    [weakSelf.view hiddenWaiting];
                });
                if(success){
                    firstUrls = [NSURL fileURLWithPath:path];
                    self.dicImage[key] = firstUrls;
                }else{
                    NSLog(@"合成失败");
                }
            }];
        }
    }
    vodeoPicker = [[UIImagePickerController alloc] init];
    vodeoPicker.delegate = self;
    vodeoPicker.allowsEditing = YES;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        vodeoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        vodeoPicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        vodeoPicker.videoQuality = UIImagePickerControllerQualityTypeMedium; //录像质量
        vodeoPicker.videoMaximumDuration = 600.0f; //录像最长时间
        vodeoPicker.mediaTypes = [NSArray arrayWithObjects:@"public.movie", nil];
        [self presentViewController:vodeoPicker animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"当前设备不支持录像功能" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
    }
    //跳转到拍摄页面
    // [self presentViewController:vodeoPicker animated:YES completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate
//拍摄完成后要执行的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    loadsucess = NO;
    NSString *key = [NSString stringWithFormat:@"%d",self.selectRow-200];
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
        //图片（不选择）
        [picker.view showToast:@"Don't add pictures"];
    }else {
        //视频
        //视频的存储
        NSString *videoPath = [self  getSavePathWithFileSuffix:@"Vodeo.mov"];
        success = [fileManager fileExistsAtPath:videoPath];
        if (success) {
            [fileManager removeItemAtPath:videoPath error:nil];
        }
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        NSData *videlData = [NSData dataWithContentsOfURL:videoURL];
        [videlData writeToFile:videoPath atomically:YES]; //写入本地
        //存储数据
        success = [fileManager fileExistsAtPath:videoPath];
        if (success) {
            //跳转到视频剪切页面
            [picker dismissViewControllerAnimated:YES completion:^{
                CutVideoViewController *videoCropVC = [[CutVideoViewController alloc] init];
                videoCropVC.videoPath = videoPath;
                videoCropVC.imageVurl = firstUrls;
                videoCropVC.lastPathBlock = ^(NSString *lastVPath){
                    self.dicUrl[key] = lastVPath;
                };
                [self.navigationController pushViewController:videoCropVC animated:NO];
            }];
            
            
            //拼接视频
            //NSMutableArray *conbineVideos = [NSMutableArray arrayWithObjects:firstUrls,[NSURL fileURLWithPath:videoPath],nil];
            //设置mov存储路径
//            NSString *processVodeoPath = [self  getSavePathWithFileSuffix:@"ProcessVodeo.mov"];
//            NSLog(@"新路径：  %@",processVodeoPath);
//            [picker.view showWaiting:NO message:@"加载中.."];
//            isprocessing = YES;
//            [[HandlerVideo sharedInstance]compositeVideosAndAudios:[NSURL fileURLWithPath:videoPath] audioUrl:firstUrls videoFullPath:processVodeoPath completedBlock:^(BOOL success, NSString *errorMsg) {
//                //回到主线程中
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [picker.view hiddenWaiting];
//                    //  [picker.view showToast:@"加载成功"];
//                });
//                isprocessing = NO;
//                if (success) {
//                    NSLog(@"---->  SUCCESS");
//                    loadsucess = YES;//加载成功
//                    //把拼接好的视频URL存在字典里
//                    self.dicUrl[key] = processVodeoPath;
//                } else {
//                    NSLog(@"---->> %@",errorMsg);
//                    //[picker.view showToast:@"加载失败"];
//                    loadsucess = NO;//加载失败
//                }
//            }];
//        }
//        [picker dismissViewControllerAnimated:YES completion:nil];
        }
    }
}
#pragma mark ---- 文件存储路经
- (NSString *)getSavePathWithFileSuffix:(NSString *)suffix
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths objectAtIndex:0];
    //NSString *moviePath =[[paths objectAtIndex:0]stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.mov",index]];
    NSDate *date = [NSDate date];
    //获取当前时间
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
    NSString *curretDateAndTime = [dateFormat stringFromDate:date];
    //获取用户userID
    // NSDictionary *userDic= [[NSUserDefaults standardUserDefaults] objectForKey:UserInformation];
    //命名文件
    //NSString *fileName = [NSString stringWithFormat:@"%@%@.%@",userDic[@"UserID"],curretDateAndTime,suffix];
    NSString *fileName = [NSString stringWithFormat:@"%@%@",curretDateAndTime,suffix];
    //指定文件存储路径
    NSString *filePath = [documentPath stringByAppendingPathComponent:fileName];
    
    return filePath;
}

//进入拍摄页面点击取消按钮
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
    // [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)tilePhotoWithcompletion:(void (^)(NSMutableArray *array))success
{
    EKTilesMaker *tilesMaker = [EKTilesMaker new];
    [tilesMaker setSourceImage:imageSource];
    [tilesMaker setZoomLevels:@[@1]];
    [tilesMaker setTileSize:CGSizeMake(1200,1200)];
    [tilesMaker setOutputFileType:OutputFileTypeJPG];
    [tilesMaker setCompletionBlock:success];
    [tilesMaker createTiles];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIViewController *)viewControllerForPresentingModalView
{
    return self;
}
#pragma mark ---- 模块选择按钮事件
-(void)selectAction:(UIButton *)sender{
    //单选
    self.addVideoButton.enabled = YES;
    self.addAlbumVButton.enabled = YES;
    self.selectRow = (int)sender.tag;
    int a = (int)sender.tag-200;
    for (int i = 0;i<arrayImageTiles.count;i++){
        if(i==a){
            UIButton *btnV = (UIButton*)[self.view viewWithTag:a+200];
            CALayer  *layer = [btnV layer];
            layer.borderColor = [mainColor CGColor];
            layer.borderWidth = 2.0f;
            NSArray *arrayKey =  [self.dicUrl allKeys];
            NSString *strKey = [NSString stringWithFormat:@"%d",a];
            //判断是否是视频
            if([arrayKey containsObject:strKey]){
               //有视频
                self.deletevideoButton.enabled = YES;
                self.playVideoButton.enabled = YES;
            }else{
               //无
                self.deletevideoButton.enabled = NO;
                self.playVideoButton.enabled = NO;
            }
            //[self addView:sender withindex:sender.tag];
        }else{
            UIButton *btnV = (UIButton*)[self.view viewWithTag:i+200];
            CALayer * layer = [btnV layer];
            layer.borderColor = [[UIColor colorWithRed:61/255.0 green:39/255.0 blue:63/255.0 alpha:1.0] CGColor];
            layer.borderWidth = 1.0f;
        }
    }
}

#pragma mark ---- 返回按钮
- (void)btnBackPresseds
{
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
    
    
    
    
    
    
}

#pragma mark ---- 实现页面的跳转
//- (void)btnStartOverPressed:(UIButton *)sender
//{
//    ChangeVideoGridViewController *ChangeVideo = [[ChangeVideoGridViewController alloc]init];
//    ChangeVideo.rowArray = [selectArray copy];
//    ChangeVideo.imageArray = arrayImageTiles;
//    ChangeVideo.imageSelect = imageSource;
//    //[self presentViewController:ChangeVideo animated:NO completion: nil];
//    [self.navigationController pushViewController:ChangeVideo animated:NO];
//}


- (void)documentInteractionController:(UIDocumentInteractionController *)controller
        willBeginSendingToApplication:(NSString *)application
{
    UIButton * nextBut = [self.view viewWithTag:self.btnSelected.tag+1];
    if (nextBut) {
        nextBut.userInteractionEnabled = YES;
    }
}

- (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)filePathString
{
    NSURL *fileURL = [NSURL fileURLWithPath:filePathString];
    
    assert([[NSFileManager defaultManager] fileExistsAtPath: [fileURL path]]);
    
    NSError *error = nil;
    
    BOOL success = [fileURL setResourceValue:[NSNumber numberWithBool: YES]
                                      forKey: NSURLIsExcludedFromBackupKey
                                       error: &error];
    return success;
}

@end

