//
//  SudokuViewController.h
//  Instagrid Post
//
//  Created by chenteng on 2018/5/10.
//  Copyright © 2018年 RJLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SudokuViewController : UIViewController
@property (nonatomic,strong)UIImage *imageSource;
@property (nonatomic,strong)UIImage *showImage;
@property (nonatomic,assign)NSInteger intShareType;
@property (nonatomic,strong)NSURL *shareVideoUrl;
@property (nonatomic, assign) int gridCount;
@end
