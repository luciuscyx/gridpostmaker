//
//  SudokuViewController.m
//  Instagrid Post
//
//  Created by chenteng on 2018/5/10.
//  Copyright © 2018年 RJLabs. All rights reserved.
//

#import "SudokuViewController.h"
#import "UIImage+EKTilesMaker.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "SHKActivityIndicator.h"
#import <AVFoundation/AVFoundation.h>
#import <Social/Social.h>
@interface SudokuViewController ()
{
    MBProgressHUD *_progressHUD;
    int currentCount;
    NSInteger intWxSave;//判断是否是点击微信或者扣扣保存图片
}
@property (nonatomic,strong)UIView *natView;
@property (nonatomic,strong)UIView *saveView;
@property (nonatomic,strong)UIView *saveDidView;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)NSMutableArray *arrayImageTiles;

@end

@implementation SudokuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImageView *mainImage = [[UIImageView alloc]initWithFrame:self.view.bounds];
    mainImage.image = [UIImage imageNamed:@"finishIg"];
    [self.view addSubview:mainImage];
    //self.view.backgroundColor = RGB(97, 97, 97);
    [self createTilesForZoomLevel];
    [self loadNat];
}
- (void)loadNat{
    CGFloat natH ;
    // CGFloat tabH;
    if(IPHONEX_SERIES){
        natH = 20;
    }else{
        natH = 0;
        
    }
    //添加左按钮
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setFrame:CGRectMake(8, 20+natH, 40, 40)];
    [btnBack setImage:[UIImage imageNamed:@"back_su"] forState:UIControlStateNormal];
    // [btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
    btnBack.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];
    
    //添加右按钮
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(self.view.frame.size.width-45-8, 25+natH, 40, 40)];
    [btnNext setImage:[UIImage imageNamed:@"backHome_Wechat"] forState:UIControlStateNormal];
    // [btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
    btnNext.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [btnNext addTarget:self action:@selector(homeAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnNext];
    self.natView = [[UIView alloc]initWithFrame:CGRectMake(0,VIEW_YH(btnNext),ScreenWidth,160)];
    self.natView.backgroundColor = [UIColor clearColor];
    [self.view addSubview: self.natView];
 
    //保存到相册
    self.saveView = [[UIView alloc]initWithFrame:CGRectMake((ScreenWidth-200)/2, 10,200, 40)];
    self.saveView.backgroundColor = [UIColor colorWithRed:39/255.0 green:111/255.0 blue:243/255.0 alpha:1.0];
    self.saveView.layer.cornerRadius = 5;
    UIImageView *tiIgView = [[UIImageView alloc]initWithFrame:CGRectMake(40,5,30, 30)];
    tiIgView.image = [UIImage imageNamed:@"save_al.png"];
    [self.saveView addSubview:tiIgView];
    UILabel *tilabel = [[UILabel alloc]initWithFrame:CGRectMake(VIEW_XW(tiIgView), 5,110,30)];
    tilabel.text = NSLocalizedString(@"Save to Album",nil);
    tilabel.font = [UIFont systemFontOfSize:15];
    tilabel.textColor = [UIColor whiteColor];
    [self.saveView addSubview:tilabel];
    UIControl *saveControl = [[UIControl alloc]initWithFrame:CGRectMake(0, 0,200,40)];
    [self.saveView addSubview:saveControl];
    [saveControl addTarget:self action:@selector(saveControl) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:self.saveView];
    //已保存到相册
    self.saveDidView = [[UIView alloc]initWithFrame:CGRectMake((ScreenWidth-200)/2, 10,200, 40)];
    self.saveDidView.backgroundColor = [UIColor clearColor];
    self.saveDidView.layer.cornerRadius = 5;
    UIImageView *tiIgDidView = [[UIImageView alloc]initWithFrame:CGRectMake(40,10,20, 20)];
    tiIgDidView.image = [UIImage imageNamed:@"save_did.png"];
    [self.saveDidView addSubview:tiIgDidView];
    UILabel *tiDlabel = [[UILabel alloc]initWithFrame:CGRectMake(VIEW_XW(tiIgView), 5,100,30)];
    tiDlabel.text = NSLocalizedString(@"Saved to Album",nil);
    tiDlabel.font = [UIFont systemFontOfSize:15];
    tiDlabel.textColor = [UIColor whiteColor];
    [self.saveDidView addSubview:tiDlabel];
    [self.natView addSubview:self.saveDidView];
    self.saveDidView.hidden = YES;

    //微信，微博，qq
    NSArray *titleArray = @[NSLocalizedString(@"ShareTo Weibo",nil),NSLocalizedString(@"ShareTo WeChat",nil),NSLocalizedString(@"ShareTo QQ",nil)];
    CGFloat space = (ScreenWidth-48*3)/6;
    CGFloat width = 48;
    for (int i=0; i<3; i++) {
        
        UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        shareButton.frame = CGRectMake(space+(width+space*2)*i,VIEW_YH(self.saveView)+20, width, width);
        NSString *imageName = [NSString stringWithFormat:@"cm%d",i];
        shareButton.tag = 100+i;
        [shareButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [shareButton addTarget:self action:@selector(shareControl:) forControlEvents:UIControlEventTouchUpInside];
        [self.natView addSubview:shareButton];
    
        UILabel *shareLabel = [[UILabel alloc]initWithFrame:CGRectMake(space+(width+space*2)*i-10, shareButton.frame.origin.y+shareButton.frame.size.height, width+20, 40)];
        shareLabel.textColor = [UIColor whiteColor];
        shareLabel.font = [UIFont systemFontOfSize:12];
        shareLabel.text = [titleArray objectAtIndex:i];
        shareLabel.numberOfLines = 0;
        shareLabel.textAlignment = NSTextAlignmentCenter;
        [self.natView addSubview:shareLabel];
    }
    self.mainScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,VIEW_YH(self.natView),ScreenWidth,ScreenHeight-VIEW_YH(self.natView))];
    self.mainScrollView.contentSize = CGSizeMake(ScreenWidth,500);
    self.mainScrollView.contentOffset = CGPointMake(0, 0);
    self.mainScrollView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.mainScrollView];
    
    UILabel *infolabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenWidth-200)/2,5, 200,30)];
    infolabel.textColor = [UIColor blackColor];
    infolabel.textAlignment = NSTextAlignmentCenter;
    infolabel.font = [UIFont systemFontOfSize:12];

    infolabel.text = NSLocalizedString(@"How to share",nil);
    [self.mainScrollView addSubview:infolabel];
    if(self.intShareType == 1){
        self.gridCount = self.gridCount*3;
    [self updateView];
    }else if(self.intShareType == 2){
        [self updateView];
    }else{
        [self updateOneView];
    }
}

- (void)updateOneView{
    CGFloat igW = 50*2+5;
    CGFloat shW = 50*3+6;
    UIImageView *IgView = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-igW)/2,30+10,igW,igW)];
    IgView.image =  [UIImage imageWithData:[self.arrayImageTiles objectAtIndex:0]];
    if(self.shareVideoUrl){
    UIImageView *playView = [[UIImageView alloc]initWithFrame:CGRectMake((igW-20)/2, (igW-20)/2, 20, 20)];
    playView.image = [UIImage imageNamed:@"play_su.png"];
    [IgView addSubview:playView];
    }
    [self.mainScrollView addSubview:IgView];
    UILabel *infoIgLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,30+10+igW+10,ScreenWidth,30)];
    infoIgLabel.textColor = [UIColor lightGrayColor];
    infoIgLabel.textAlignment = NSTextAlignmentCenter;
    infoIgLabel.font = [UIFont systemFontOfSize:10];
    
    if(self.shareVideoUrl){
    infoIgLabel.text = NSLocalizedString(@"Share video as following result",nil);
    }else{
    infoIgLabel.text = NSLocalizedString(@"Share photos as following result",nil);
    }
    [self.mainScrollView addSubview:infoIgLabel];
    UIImageView *infoImageV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-30)/2,VIEW_YH(infoIgLabel), 30, 30)];
    infoImageV.image = [UIImage imageNamed:@"bottow_su.png"];
    [self.mainScrollView addSubview:infoImageV];
    
    UIView *showView  = [[UIView alloc]initWithFrame:CGRectMake(0,VIEW_YH(infoImageV)+10,ScreenWidth, shW+50+30)];
    showView.backgroundColor = RGB(244, 244, 244);
    [self.mainScrollView addSubview:showView];
    
    UIImageView *showImageV = [[UIImageView alloc]initWithFrame:CGRectMake(20, 10, 50,50)];
    showImageV.image = [UIImage imageNamed:@"person_su"];
    [showView addSubview:showImageV];
    
    UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(VIEW_XW(showImageV)+5,10,100,20)];
    
    nameLabel.text = NSLocalizedString(@"Grid Post",nil);
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.font = [UIFont systemFontOfSize:12];
    nameLabel.textColor = RGB(112, 205,250);
    [showView addSubview:nameLabel];
    
    UILabel *contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(VIEW_XW(showImageV)+5,VIEW_YH(nameLabel),300,20)];
    contentLabel.text = NSLocalizedString(@"Sharing pictures (or videos) to friends circle",nil);
    contentLabel.textAlignment = NSTextAlignmentLeft;
    contentLabel.adjustsFontSizeToFitWidth = YES;
    contentLabel.font = [UIFont systemFontOfSize:10];
    contentLabel.textColor = [UIColor blackColor];
    [showView addSubview:contentLabel];
    
    UIImageView *igShowView = [[UIImageView alloc]initWithFrame:CGRectMake(VIEW_XW(showImageV)+10,VIEW_YH(contentLabel)+10,shW,shW)];
    igShowView.image =  [UIImage imageWithData:[self.arrayImageTiles objectAtIndex:0]];
    if(self.shareVideoUrl){
        UIImageView *playbigView = [[UIImageView alloc]initWithFrame:CGRectMake((shW-30)/2, (shW-30)/2, 30, 30)];
        playbigView.image = [UIImage imageNamed:@"play_su.png"];
        [igShowView addSubview:playbigView];
    }
    [showView addSubview:igShowView];
}
-(void)updateView
{
    CGFloat igW = 50;
    CGFloat linW = (ScreenWidth-igW*5-5*4)/2;
    CGFloat laW = 20;
    //图片视图分块
    for (int i = 0; i < self.gridCount; i ++ ) {
        UIImageView *IgView = [[UIImageView alloc]initWithFrame:CGRectMake(linW+(igW+5)*(i%5),30+10+(igW+5)*(int)(i/5),igW,igW)];
        if (i < self.arrayImageTiles.count) {
            IgView.image =  [UIImage imageWithData:[self.arrayImageTiles objectAtIndex:i]];
        }
        
        UILabel *linLabel = [[UILabel alloc]initWithFrame:CGRectMake(igW-laW, igW-laW, laW, laW)];
        linLabel.text = [NSString stringWithFormat:@"%d",i+1];
        linLabel.textAlignment = NSTextAlignmentCenter;
        linLabel.textColor = [UIColor whiteColor];
        linLabel.font = [UIFont systemFontOfSize:10];
        linLabel.backgroundColor =  RGB(112, 205,250);
        linLabel.alpha = 0.7;
        [IgView addSubview:linLabel];
        [self.mainScrollView addSubview:IgView];
    }
    UILabel *infoIgLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,30+10+igW*2+5+10,ScreenWidth,30)];
    infoIgLabel.textColor = [UIColor lightGrayColor];
    infoIgLabel.textAlignment = NSTextAlignmentCenter;
    infoIgLabel.font = [UIFont systemFontOfSize:10];
    
    NSString *textst = [NSString stringWithFormat:NSLocalizedString(@"Please follow the order 1~%d to share these photos，you will get the amazing result",nil),self.gridCount];
    infoIgLabel.text = textst;
    [self.mainScrollView addSubview:infoIgLabel];
    UIImageView *infoImageV = [[UIImageView alloc]initWithFrame:CGRectMake((ScreenWidth-30)/2,VIEW_YH(infoIgLabel), 30, 30)];
    infoImageV.image = [UIImage imageNamed:@"bottow_su.png"];
    [self.mainScrollView addSubview:infoImageV];
    
    UIView *showView  = [[UIView alloc]initWithFrame:CGRectMake(0,VIEW_YH(infoImageV)+10,ScreenWidth, (igW+3)*3+igW+30)];
    showView.backgroundColor = RGB(244, 244, 244);
    [self.mainScrollView addSubview:showView];
    
    UIImageView *showImageV = [[UIImageView alloc]initWithFrame:CGRectMake(20, 10, igW,igW)];
    showImageV.image = [UIImage imageNamed:@"person_su"];
    [showView addSubview:showImageV];
    
    UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(VIEW_XW(showImageV)+5,10,100,20)];
    nameLabel.text = NSLocalizedString(@"Grid Post",nil);
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.font = [UIFont systemFontOfSize:12];
    nameLabel.textColor = RGB(112, 205,250);
    [showView addSubview:nameLabel];
    
    UILabel *contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(VIEW_XW(showImageV)+5,VIEW_YH(nameLabel),300,20)];
    contentLabel.text = NSLocalizedString(@"Sharing pictures (or videos) to friends circle",nil);
    contentLabel.textAlignment = NSTextAlignmentLeft;
    contentLabel.font = [UIFont systemFontOfSize:10];
    contentLabel.textColor = [UIColor blackColor];
    [showView addSubview:contentLabel];
    
    for(int x = 0 ;x<self.gridCount;x++){
        UIImageView *IgView = [[UIImageView alloc]initWithFrame:CGRectMake(VIEW_XW(showImageV)+10+(igW+3)*(x%3),VIEW_YH(contentLabel)+10+(igW+3)*(int)(x/3),igW,igW)];
        IgView.image =  [UIImage imageWithData:[self.arrayImageTiles objectAtIndex:x]];
        [showView addSubview:IgView];
    }
    
}
- (void)createTilesForZoomLevel
{
    self.arrayImageTiles = [[NSMutableArray alloc]init];
    if(self.intShareType == 1){
    UIImage *sourceImage = self.imageSource;
    NSUInteger row = 0;
    CGFloat offsetY = 0;
    CGFloat singlew= sourceImage.size.width/3;
    int cmph = sourceImage.size.height;
    int a = cmph/(int)singlew;
    // while (((int)offsetY+1) < cmph) {
    for(int i=0;i<a;i++){
        NSUInteger column = 0;
        CGFloat offsetX = 0;
        
        while (offsetX < sourceImage.size.width) {
            CGRect tileFrame = CGRectMake(offsetX, offsetY, singlew,singlew);
            
            UIImage *tileImage = [self.imageSource imageInRect:tileFrame];
            
            NSData *imageData =  UIImageJPEGRepresentation(tileImage, 1);
            
            [self.arrayImageTiles addObject:imageData];
            column += 1;
            offsetX += singlew;
        }
        row += 1;
        offsetY += singlew;
    }
    //数组反序
   // self.arrayImageTiles=(NSMutableArray *)[[self.arrayImageTiles reverseObjectEnumerator] allObjects];
    //[self updateView];
    }else if(self.intShareType == 2){
        UIImage *sourceImage = self.imageSource;
        NSUInteger row = 0;
        CGFloat offsetY = 0;
        CGFloat singlew = sourceImage.size.width/self.gridCount;
        //int cmph = sourceImage.size.height;
        int a = 1;
        // while (((int)offsetY+1) < cmph) {
        for(int i=0;i<a;i++){
            NSUInteger column = 0;
            CGFloat offsetX = 0;
            
            while (offsetX < sourceImage.size.width) {
                CGRect tileFrame = CGRectMake(offsetX, offsetY, singlew,singlew);
                
                UIImage *tileImage = [self.imageSource imageInRect:tileFrame];
                
                NSData *imageData =  UIImageJPEGRepresentation(tileImage, 1);
                
                [self.arrayImageTiles addObject:imageData];
                column += 1;
                offsetX += singlew;
            }
            row += 1;
            offsetY += singlew;
        }
    }else{
      NSData *imageData =  UIImageJPEGRepresentation(self.imageSource, 1);
      [self.arrayImageTiles addObject:imageData];
    }
}


- (void)shareControl:(UIButton *)sender{
    switch (sender.tag) {
        case 100:
            [self shareWeiBoAction];
            break;
        case 101:
            [self shareWeiXinAction];
            break;
        case 102:
            [self shareQqAction];
            break;
        default:
            break;
    }
}

- (void)shareWeiBoAction{
    if(self.saveView.hidden){
        [self openWeiBo];
    }else{
        intWxSave = 3;
        [self saveControl];
    }
}

- (void)openWeiBo{
    NSURL *url = [NSURL URLWithString:@"sinaweibo://"];
    if([[UIApplication sharedApplication] canOpenURL:url]){
        [[UIApplication sharedApplication] openURL:url];
    } else {
        UIAlertView*ale=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Hint",nil) message:NSLocalizedString(@"Please install Sina Weibo and try again。",nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Sure",nil), nil];
        [ale show];
    }
}
- (void)shareWeiXinAction{
    if(self.saveView.hidden){
        [self openWeixin];
    }else{
    intWxSave = 1;
    [self saveControl];
    }
}
- (void)openWeixin{
    NSURL *url = [NSURL URLWithString:@"weixin://"];
    if([[UIApplication sharedApplication] canOpenURL:url]){
        [[UIApplication sharedApplication] openURL:url];
    } else {
        UIAlertView*ale=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Hint",nil) message:NSLocalizedString(@"Please install WeChat and try again。",nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Sure",nil), nil];
        [ale show];
    }
}
- (void)shareQqAction{
    if(self.saveView.hidden){
        [self openQQ];
    }else{
        intWxSave = 2;
        [self saveControl];
    }
}
- (void)openQQ{
    NSURL *url = [NSURL URLWithString:@"mqq://"];
    if([[UIApplication sharedApplication] canOpenURL:url]){
        [[UIApplication sharedApplication] openURL:url];
    } else {
        UIAlertView*ale=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Hint",nil) message:NSLocalizedString(@"Please install QQ and try again。",nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Sure",nil), nil];
        [ale show];
    }
}
- (void)btnBackPressed:(UIButton *)sender
{
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
//返回首页
- (void)homeAction{
    [self.navigationController popToRootViewControllerAnimated:YES];
    if([SKStoreReviewController respondsToSelector:@selector(requestReview)]) {
        [[UIApplication sharedApplication].keyWindow endEditing:YES];
        [SKStoreReviewController requestReview];
    }
}
- (void)saveControl{
    [[SHKActivityIndicator currentIndicator] displayActivity:NSLocalizedString(@"Saving...", nil)];
    if(self.shareVideoUrl){
        [self exportDidFinish:self.shareVideoUrl];
    }else{
    currentCount = 0;
    //添加两张提示图片
        UIImage *startImage = [UIImage imageNamed:@"startTipImage"];
        UIImage *endImage = [UIImage imageNamed:@"endTipImage"];
        NSData *startData =  UIImageJPEGRepresentation(startImage, 1);
        NSData *endData =  UIImageJPEGRepresentation(endImage, 1);
        [self.arrayImageTiles insertObject:startData atIndex:0];
        [self.arrayImageTiles addObject:endData];
        
    UIImage * image = [UIImage imageWithData:[self.arrayImageTiles objectAtIndex:currentCount]];
    currentCount++;
    UIImageWriteToSavedPhotosAlbum(image,self,  @selector(imageSavedToPhotosAlbum: didFinishSavingWithError: contextInfo:), nil);
    }
}
//视频保存到相册
- (void)exportDidFinish:(NSURL*)outputURL
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
        [library writeVideoAtPathToSavedPhotosAlbum:outputURL
                                    completionBlock:^(NSURL *assetURL, NSError *error){
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            if (error) {
                                                NSLog(@"Video Saving Failed");
                                            }else{
                                                [[SHKActivityIndicator currentIndicator] hide];
                                                [[SHKActivityIndicator currentIndicator] displayCompleted:NSLocalizedString(@"Saved!", nil)];
                                                self.saveView.hidden = YES;
                                                self.saveDidView.hidden = NO;
                                                if(intWxSave == 1){
                                                    [self openWeixin];
                                                }else if(intWxSave == 2){
                                                    [self openQQ];
                                                }else if(intWxSave == 3){
                                                    [self openWeiBo];
                                                }else{
                                                    
                                                }
                                                NSLog(@"Video Saved");
                                            }
                                            
                                        });
                                    }];
    }
}
//实现类中实现（图片保存到相册）
-(void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *) contextInfo {
    NSString *message;
    
    if (!error) {
        // Notify user
        if (currentCount < self.arrayImageTiles.count) {
            UIImage * image = [UIImage imageWithData:[self.arrayImageTiles objectAtIndex:currentCount]];
            currentCount++;
            UIImageWriteToSavedPhotosAlbum(image,self,  @selector(imageSavedToPhotosAlbum: didFinishSavingWithError: contextInfo:), nil);
        }else
        {
            [[SHKActivityIndicator currentIndicator] hide];
            [[SHKActivityIndicator currentIndicator] displayCompleted:NSLocalizedString(@"Saved!", nil)];
            self.saveView.hidden = YES;
            self.saveDidView.hidden = NO;
            if(intWxSave == 1){
                [self openWeixin];
            }else if(intWxSave == 2){
                [self openQQ];
            }else if(intWxSave == 3){
                [self openWeiBo];
            }else{
                
            }
        }
        
    } else {
        intWxSave = 0;
        [[SHKActivityIndicator currentIndicator] hide];
        NSString *title;
        title = NSLocalizedString(@"Error!", nil);
        message =NSLocalizedString(@"DeviceSettingAlert", nil);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
