 //
//  InstaGridShareViewController.m
//  Instagrid Post
//
//  Created by Mangal on 02/07/15.
//  Copyright (c) 2015 RJLabs. All rights reserved.
//

#import "ChangeVideoGridViewController.h"
#import "UIImage+EKTilesMaker.h"
#import "SHKActivityIndicator.h"
#import "UpgradeViewController.h"
#import "ImageHelper.h"
#import "AppDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "MBProgressHUD.h"
#import "RSKImageCropViewController.h"
@interface ChangeVideoGridViewController ()<UpgradeViewControllerDelegate>
{
    UpgradeViewController * objUpgradeViewController;
    NSMutableArray *newArray;//不包括视频的图片
    MBProgressHUD *_progressHUD;
}
@property (nonatomic,strong)NSString *theVideoPath;
@property (nonatomic) UIActivityViewController *activityViewController;
@property (nonatomic,strong)UIView *natView;
@property (nonatomic,strong)UIView *tabView;

@end
@implementation ChangeVideoGridViewController

@synthesize orgimageSource;

@synthesize gridCount;

@synthesize btnSelected;


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    //UIImageView *mainImage = [[UIImageView alloc]initWithFrame:self.view.bounds];
    //mainImage.image = [UIImage imageNamed:@"bg"];
    //[self.view addSubview:mainImage];
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]){
        self.orgimageSource = self.imageSelect;
        self.imageSelect = [self createWithWaterMarkonImage:self.imageSelect];
    }
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.natView = [[UIView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth, 60+KDNavH)];
    [self.view addSubview:self.natView];
    //添加左按钮
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setFrame:CGRectMake(8,15+KDNavH, 35, 35)];
    [btnBack setImage:[UIImage imageNamed:@"backBlack_icn"] forState:UIControlStateNormal];
    // [btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
    btnBack.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:btnBack];
    
    //添加右按钮
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(ScreenWidth - 45,15+KDNavH, 35, 35)];
    [btnNext setImage:[UIImage imageNamed:@"backHome"] forState:UIControlStateNormal];
    // [btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
    btnNext.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [btnNext addTarget:self action:@selector(homeAction) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:btnNext];
    
    UILabel * instructionLabel = [[UILabel alloc] init];
    //CGFloat orgx = btnBack.frame.origin.x+btnBack.frame.size.width+10;
    
    instructionLabel.frame = CGRectMake(VIEW_XW(btnBack)+20,KDNavH+10,ScreenWidth - (VIEW_XW(btnBack)+20)*2,50);//CGRectMake(orgx, btnBack.frame.origin.y-(IPHONEX_SERIES ? 0 : 10), btnNext.frame.origin.x-orgx-10, btnBack.frame.size.height+10);
    instructionLabel.textColor = [UIColor blackColor];
    instructionLabel.text = NSLocalizedString(@"TAP EACH PIC IN THE FOLLWOING ORDER TO SHARE IT ON INSTAGRAM",nil);
    instructionLabel.numberOfLines = 0;
    instructionLabel.textAlignment = NSTextAlignmentCenter;
   // [instructionLabel sizeToFit];
    instructionLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:13];
    [self.natView addSubview:instructionLabel];
    
    self.tabView = [[UIView alloc]initWithFrame:CGRectMake(0,ScreenHeight - 55 - KDNavH,ScreenWidth, 55+KDNavH)];
    [self.view addSubview:self.tabView];
    UIImageView *LineIg = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, ScreenWidth,2)];
    LineIg.image = [UIImage imageNamed:@"tabLine"];//[UIColor lightGrayColor];
    [self.tabView addSubview:LineIg];
    
    UIButton *removeWatterBut = [[UIButton alloc]initWithFrame:CGRectMake(0, 2, ScreenWidth/2,52+KDNavH)];
    [removeWatterBut setTitle:NSLocalizedString(@"NO WATERMARK",nil) forState:UIControlStateNormal];
    removeWatterBut.backgroundColor = [UIColor whiteColor];
    [removeWatterBut setTitleColor:mainColor forState:UIControlStateNormal];
    removeWatterBut.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [removeWatterBut addTarget:self action:@selector(btnUpgradePressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.tabView addSubview:removeWatterBut];
    
    UIImageView *finishImageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2, 2, ScreenWidth/2,52+KDNavH)];
    finishImageView.userInteractionEnabled = YES;
    finishImageView.image = [UIImage imageNamed:@"finishIg"];
    [self.tabView addSubview:finishImageView];
    
    UIButton *finishBut = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth/2, 2, ScreenWidth/2,52+KDNavH)];
    finishBut.backgroundColor = [UIColor clearColor];
    //finishBut.imageView.image = [UIImage imageNamed:@"finishIg"];
    [finishBut setTitle:NSLocalizedString(@"SAVE TO ALBUM",nil) forState:UIControlStateNormal];
    //[finishBut setImage:[UIImage imageNamed:@"finishIg"] forState:UIControlStateNormal]; ;
    [finishBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [finishBut addTarget:self action:@selector(savetophotoalbum:) forControlEvents:UIControlEventTouchUpInside];
    finishBut.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [self.tabView addSubview:finishBut];
    
//    //底部左按钮
//    UIButton *bottomleft = [UIButton buttonWithType:UIButtonTypeCustom];
//    [bottomleft setFrame:CGRectMake(20,self.view.frame.size.height-55, 130,45)];
//    [bottomleft setTitle:NSLocalizedString(@"NO WATERMARK",nil) forState:UIControlStateNormal];
//    [bottomleft setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    bottomleft.backgroundColor = [UIColor blackColor];
//    bottomleft.titleLabel.font = [UIFont systemFontOfSize:15];
//    bottomleft.layer.cornerRadius = 10;
//    bottomleft.layer.masksToBounds = YES;
//    [bottomleft addTarget:self action:@selector(btnUpgradePressed:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:bottomleft];
//    //底部右按钮
//    UIButton *bottomRight = [UIButton buttonWithType:UIButtonTypeCustom];
//    [bottomRight setFrame:CGRectMake(self.view.frame.size.width-20-130,self.view.frame.size.height-55, 130,45)];
//    [bottomRight setTitle:NSLocalizedString(@"SAVE TO ALBUM",nil) forState:UIControlStateNormal];
//    [bottomRight setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    bottomRight.backgroundColor = [UIColor blackColor];
//    bottomRight.titleLabel.font = [UIFont systemFontOfSize:15];
//    bottomRight.layer.cornerRadius = 10;
//    bottomRight.layer.masksToBounds = YES;
//    [bottomRight addTarget:self action:@selector(savetophotoalbum:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:bottomRight];
    
    [self createTilesForZoomLevel];
}
-(UIImage*)createWithWaterMarkonImage:(UIImage*)image{
    CGSize size = image.size;
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage* waterMarkImage = [UIImage imageNamed:@"watermark"];
    
    float scaleFactor = image.size.width / 320;
    float newHeight = waterMarkImage.size.height * scaleFactor;
    float newWidth = waterMarkImage.size.width * scaleFactor;
    if (newHeight > size.height/3) {
        newWidth = (size.height/3)*newWidth/newHeight;
        newHeight = size.height/3;
    }
    [waterMarkImage drawInRect:CGRectMake(size.width - newWidth,size.height-newHeight, newWidth,newHeight)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
- (void)createTilesForZoomLevel
{
    if(self.homeBool){
     [self updateHomeView];
    }else{
   [self updateView];
   // [self updateHomeView];
    }
}

-(void)updateView
{
   // self.imageArray=(NSMutableArray *)[[self.imageArray reverseObjectEnumerator] allObjects];
    for (UIView * subView in self.view.subviews) {
        if (subView.tag >= 100) {
            [subView removeFromSuperview];
        }
    }
    CGFloat maxh = ScreenHeight-60-65;
    CGSize showSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.width*self.imageSelect.size.height/self.imageSelect.size.width);
    if (showSize.height > maxh) {
        showSize = CGSizeMake(self.view.frame.size.width*maxh/showSize.height, maxh);
    }
    CGFloat beginX = (self.view.frame.size.width-showSize.width)*0.5;
    CGFloat beginY = (self.view.frame.size.height-showSize.height)*0.5;
    CGFloat singleW = showSize.width/3;

    UIFont *tmp1 = [UIFont fontWithName:@"HelveticaNeue" size:18.0f];
    for (int i = 0; i < self.imageArray.count; i ++ ) {
        NSString *row = [NSString stringWithFormat:@"%d",self.imageArray.count-1-i];
        UIImageView * imgView = [self.view viewWithTag:100+i];
        if (imgView) {
            [imgView removeFromSuperview];
        }
        imgView = [[UIImageView alloc] init];
        imgView.image = [UIImage imageWithData:[self.imageArray objectAtIndex:self.imageArray.count-1-i]];
        imgView.frame = CGRectMake(beginX+(i%3)*singleW, beginY+((int)(i/3))*singleW, singleW, singleW);
        imgView.tag = 100+i;
        [self.view addSubview:imgView];
        
        UIImageView * roundImg = [self.view viewWithTag:300+i];
        if (roundImg) {
            [roundImg removeFromSuperview];
        }
        roundImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"round"]];
        roundImg.frame = CGRectMake(imgView.frame.origin.x+imgView.frame.size.width*3/8, imgView.frame.origin.y+imgView.frame.size.height*3/8, imgView.frame.size.width/4, imgView.frame.size.height/4);
        roundImg.tag = 300+i;
        [self.view addSubview:roundImg];
        if([self.rowArray containsObject:row]){
           // NSLog(@"-------%@",self.rowArray);
            UIImageView *playImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stop"]];
            playImg.frame = CGRectMake(imgView.frame.origin.x+imgView.frame.size.width/3, imgView.frame.origin.y+imgView.frame.size.height/3, imgView.frame.size.width/3, imgView.frame.size.height/3);
            playImg.tag = 400+i;
            [self.view addSubview:playImg];
        }
            UIButton * btn = [self.view viewWithTag:200+i];
            if (btn) {
                [btn removeFromSuperview];
            }
            btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.titleLabel.font = tmp1;
            btn.frame = imgView.frame;
            btn.tag = 200+i;
            [btn setTitle:[NSString stringWithFormat:@"%d",i+1] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(playAction:) forControlEvents:UIControlEventTouchUpInside];
        if (i == 0) {
            btn.userInteractionEnabled = YES;
        }else
        {
            btn.userInteractionEnabled = NO;
        }
            [self.view addSubview:btn];
    }

}
-(void)updateHomeView
{
    // self.imageArray=(NSMutableArray *)[[self.imageArray reverseObjectEnumerator] allObjects];
    for (UIView * subView in self.view.subviews) {
        if (subView.tag >= 100) {
            [subView removeFromSuperview];
        }
    }
    CGFloat maxh = ScreenHeight-60-55 - KDNavH*2;
    CGSize showSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.width*self.imageSelect.size.height/self.imageSelect.size.width);
    if (showSize.height > maxh) {
        showSize = CGSizeMake(self.view.frame.size.width*maxh/showSize.height, maxh);
    }
    CGFloat beginX = (self.view.frame.size.width-showSize.width)*0.5;
    CGFloat beginY = (self.view.frame.size.height-showSize.height)*0.5;
    CGFloat singleW = showSize.width/self.gridCount;
    
    UIFont *tmp1 = [UIFont fontWithName:@"HelveticaNeue" size:18.0f];
    for (int i = 0; i < self.imageArray.count; i ++ ) {
        NSString *row = [NSString stringWithFormat:@"%d",self.imageArray.count-1-i];
        UIImageView * imgView = [self.view viewWithTag:100+i];
        if (imgView) {
            [imgView removeFromSuperview];
        }
        imgView = [[UIImageView alloc] init];
        imgView.image = [UIImage imageWithData:[self.imageArray objectAtIndex:self.imageArray.count-1-i]];
        imgView.frame = CGRectMake(beginX+(i%self.gridCount)*singleW, beginY+((int)(i/self.gridCount))*singleW, singleW, singleW);
        imgView.tag = 100+i;
        [self.view addSubview:imgView];
        
        UIImageView * roundImg = [self.view viewWithTag:300+i];
        if (roundImg) {
            [roundImg removeFromSuperview];
        }
        roundImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"round"]];
        roundImg.frame = CGRectMake(imgView.frame.origin.x+imgView.frame.size.width*3/8, imgView.frame.origin.y+imgView.frame.size.height*3/8, imgView.frame.size.width/4, imgView.frame.size.height/4);
        roundImg.tag = 300+i;
        [self.view addSubview:roundImg];
        if([self.rowArray containsObject:row]){
            // NSLog(@"-------%@",self.rowArray);
            UIImageView *playImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stop"]];
            playImg.frame = CGRectMake(imgView.frame.origin.x+imgView.frame.size.width/3, imgView.frame.origin.y+imgView.frame.size.height/3, imgView.frame.size.width/3, imgView.frame.size.height/3);
            playImg.tag = 400+i;
            [self.view addSubview:playImg];
        }
        UIButton * btn = [self.view viewWithTag:200+i];
        if (btn) {
            [btn removeFromSuperview];
        }
        btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.titleLabel.font = tmp1;
        btn.frame = imgView.frame;
        btn.tag = 200+i;
        [btn setTitle:[NSString stringWithFormat:@"%d",i+1] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(playAction:) forControlEvents:UIControlEventTouchUpInside];
        if (i == 0) {
            btn.userInteractionEnabled = YES;
        }else
        {
            btn.userInteractionEnabled = NO;
        }
        [self.view addSubview:btn];
    }
    
}
#pragma mark ----分享按钮
- (void)playAction:(UIButton *)sender{
    
    UIButton *btn = (UIButton *)sender;
    int row = btn.tag - 200;
    UIImageView * imgView = [self.view viewWithTag:row+100];
    
    self.btnSelected = btn;
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if (![[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        CIError(@"Download Instagram app from app store firstly please!");
    }
    else
    {
        [btn setImage:[UIImage imageNamed:@"InstaShare"] forState:UIControlStateNormal];
        [btn setTitle:@"" forState:UIControlStateNormal];
    }
    //上传图片
    if(![self.rowArray containsObject:[NSString stringWithFormat:@"%d",self.imageArray.count-1-row]]){
        UIImage *image = imgView.image;
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"Image.igo"];
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        [imageData writeToFile:savedImagePath atomically:YES];
        
        [self addSkipBackupAttributeToItemAtPath:savedImagePath];
        
        NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", savedImagePath]];
        
        //NSURL *imageUrl = [NSURL fileURLWithPath:savedImagePath];
        NSString *caption = @"Uploaded from #instagridpost";
        if([[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]){
            caption = @"";
        }
        self.docController = [UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
        self.docController.UTI = @"com.instagram.exclusivegram";
        self.docController.delegate = self;
        self.docController.annotation = [NSDictionary dictionaryWithObject:caption forKey:@"InstagramCaption"];
        [self.docController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
        
    }//上传视频
    else{
        NSString *pathRow = [NSString stringWithFormat:@"%d",self.imageArray.count-1-row];
        NSURL *pathUrl = [NSURL fileURLWithPath:self.dicUrl[pathRow]];
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library writeVideoAtPathToSavedPhotosAlbum:pathUrl
                                    completionBlock:^(NSURL *assetURL, NSError *error) {
                                        if (error) {
                                            NSLog(@"Save video fail:%@",error);
                                        } else {
                                            NSURL *instagramURL = [NSURL URLWithString:@"instagram://camera"];
                                            [[UIApplication sharedApplication]openURL:instagramURL];
                                        }
                                        UIButton * nextBut = [self.view viewWithTag:self.btnSelected.tag+1];
                                        if (nextBut) {
                                            nextBut.userInteractionEnabled = YES;
                                        }
                                    }];
        //        NSMutableArray *items = [NSMutableArray arrayWithObject:pathUrl];
        //        self.activityViewController = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:nil];
        //        if ( [self.activityViewController respondsToSelector:@selector(popoverPresentationController)] ) {
        //            // iOS8
        //            self.activityViewController.popoverPresentationController.sourceView =
        //            self.view;
        //            CGRect frame = [UIScreen mainScreen].bounds;
        //            frame.size.height /= 2;
        //            self.activityViewController.popoverPresentationController.sourceRect = frame;
        //      }
        //         [self presentViewController:self.activityViewController animated:YES completion:nil];
    }
}
#pragma mark ----保存按钮
- (void)savetophotoalbum:(UIButton *)sender
{

    //保存相册
    newArray = [[NSMutableArray alloc]init];
    for(int i = 0 ;i<self.imageArray.count;i++){
        if([self.rowArray containsObject:[NSString stringWithFormat:@"%d",i]]){
            //如果是视频就不用保存
        }else{
            [newArray addObject:self.imageArray[i]];
        }
    }
    if (newArray.count > 0 || self.rowArray.count > 0) {
        [[SHKActivityIndicator currentIndicator] displayActivity:NSLocalizedString(@"Saving...", nil)];
    }
    currentCount = 0;
    UIImage *startImage = [UIImage imageNamed:@"startTipImage_En"];
    UIImage *endImage = [UIImage imageNamed:@"endTipImage_En"];
    NSData *startData =  UIImageJPEGRepresentation(startImage, 1);
    NSData *endData =  UIImageJPEGRepresentation(endImage, 1);
    [newArray insertObject:endData atIndex:0];
    [newArray addObject:startData];
    if (newArray.count > 0) {
        UIImage * image = [UIImage imageWithData:[newArray objectAtIndex:currentCount]];
        currentCount++;
        UIImageWriteToSavedPhotosAlbum(image,self,  @selector(imageSavedToPhotosAlbum: didFinishSavingWithError: contextInfo:), nil);
    }
    
    //保存视频
    for(NSString *str in self.rowArray){
        [self exportDidFinish:[NSURL fileURLWithPath:self.dicUrl[str]]];
    }
}

//实现类中实现（图片保存到相册）
-(void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *) contextInfo {
    NSString *message;
    
    if (!error) {
        // Notify user
        if (currentCount < newArray.count) {
            UIImage * image = [UIImage imageWithData:[newArray objectAtIndex:currentCount]];
            currentCount++;
            UIImageWriteToSavedPhotosAlbum(image,self,  @selector(imageSavedToPhotosAlbum: didFinishSavingWithError: contextInfo:), nil);
        }else
        {
            [[SHKActivityIndicator currentIndicator] hide];
            [[SHKActivityIndicator currentIndicator] displayCompleted:NSLocalizedString(@"Saved!", nil)];

            NSString*str = [[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY];
            
            if (str&&[str isEqualToString:@"YES"])
            {

            }else
            {
                if([SKStoreReviewController respondsToSelector:@selector(requestReview)]) {
                    [[UIApplication sharedApplication].keyWindow endEditing:YES];
                    [SKStoreReviewController requestReview];
                }
            }

        }
        
    } else {
        [[SHKActivityIndicator currentIndicator] hide];
        NSString *title;
        title = NSLocalizedString(@"Error!", nil);
        message =NSLocalizedString(@"DeviceSettingAlert", nil);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
}
//视频保存到相册
- (void)exportDidFinish:(NSURL*)outputURL
{
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
            [library writeVideoAtPathToSavedPhotosAlbum:outputURL
                                        completionBlock:^(NSURL *assetURL, NSError *error){
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                if (!newArray || newArray.count == 0) {
                                                    [[SHKActivityIndicator currentIndicator] hide];
                                                }
                                                if (error) {
                                                    NSLog(@"Video Saving Failed");
                                                    if (!newArray || newArray.count == 0) {
                                                        [[SHKActivityIndicator currentIndicator] displayCompleted:NSLocalizedString(@"Error!", nil)];
                                                    }
                                                 //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil,nil];
                                                  //  [alert show];
                                                }else{
                                                    NSLog(@"Video Saved");
                                                    if (!newArray || newArray.count == 0) {
                                                        [[SHKActivityIndicator currentIndicator] displayCompleted:NSLocalizedString(@"Saved!", nil)];
                                                    }
                                                   // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil,nil];
                                                  //  [alert show];
                                                }

                                            });

                                        }];
        }
}

- (void)saveshowrateus
{

}

- (void)btnUpgradePressed:(UIButton *)sender
{
    objUpgradeViewController = [[UpgradeViewController alloc] initWithNibName:@"UpgradeViewController" bundle:nil];
    objUpgradeViewController.delegate = self;
    [RJAnimation bounceAddSubViewToParentView:self.view ChildView:objUpgradeViewController.view];
}

- (void)tilePhotoWithcompletion:(void (^)(NSMutableArray *array))success
{
    EKTilesMaker *tilesMaker = [EKTilesMaker new];
    [tilesMaker setSourceImage:self.imageSelect];
    [tilesMaker setZoomLevels:@[@1]];
    [tilesMaker setTileSize:CGSizeMake(1200,1200)];
    [tilesMaker setOutputFileType:OutputFileTypeJPG];
    [tilesMaker setCompletionBlock:success];
    [tilesMaker createTiles];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIViewController *)viewControllerForPresentingModalView
{
    return self;
}


    /*
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        NSString *caption = @"Uploaded from Insta Grid Post app";
        //NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        //NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"Image.igo"];
        //NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
        //[imageData writeToFile:savedImagePath atomically:YES];
        //NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", savedImagePath]];
     
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library writeImageToSavedPhotosAlbum:[image CGImage] orientation:(ALAssetOrientation)[image imageOrientation] completionBlock:^(NSURL *assetURL, NSError *error) {
            NSString *escapedString   = [assetURL.absoluteString urlencodedString];
            NSString *escapedCaption  = [caption urlencodedString];
            NSURL *instagramURL       = [NSURL URLWithString:[NSString stringWithFormat:@"instagram://library?AssetPath=%@&InstagramCaption=%@", escapedString, escapedCaption]];
            if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
            {
                [[UIApplication sharedApplication] openURL:instagramURL];
            }
        }];
     
    }
    else
    {
        CIAlert(@"Download Instagram app", @"Please download instagram app first to share video");
    }
     */

- (void)btnBackPressed:(UIButton *)sender
{
     //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

//返回首页
- (void)homeAction{
    [self.navigationController popToRootViewControllerAnimated:YES];
    NSNumber * ccc = [[NSUserDefaults standardUserDefaults] objectForKey:@"openCount"];
    if (!ccc) {
        ccc = [NSNumber numberWithInt:0];
    }
    
    if ([ccc intValue] > 1) {

        NSString*str = [[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY];
        
        if (str&&[str isEqualToString:@"YES"])
        {

        }else
        {
            if([SKStoreReviewController respondsToSelector:@selector(requestReview)]) {
                [[UIApplication sharedApplication].keyWindow endEditing:YES];
                [SKStoreReviewController requestReview];
            }
        }

    }
}



-(void)upgradeSuccess
{
    if (self.orgimageSource) {
        self.imageSelect = self.orgimageSource;
        [self createTilesForZoomLevel];
    }
}

- (void)documentInteractionController:(UIDocumentInteractionController *)controller
        willBeginSendingToApplication:(NSString *)application
{
    UIButton * nextBut = [self.view viewWithTag:self.btnSelected.tag+1];
    if (nextBut) {
        nextBut.userInteractionEnabled = YES;
    }
//    if (btnSelected.tag==1)
//    {
//        self.btnTwo.userInteractionEnabled = YES;
//    }
//    else if (btnSelected.tag==2)
//    {
//        self.btnThree.userInteractionEnabled = YES;
//    }
//    else if (btnSelected.tag==3)
//    {
//        self.btnFour.userInteractionEnabled = YES;
//    }
//    else if (btnSelected.tag==4)
//    {
//        self.btnFive.userInteractionEnabled = YES;
//    }
//    else if (btnSelected.tag==5)
//    {
//        self.btnSix.userInteractionEnabled = YES;
//    }
//    else if (btnSelected.tag==6)
//    {
//        self.btnSeven.userInteractionEnabled = YES;
//    }
//    else if (btnSelected.tag==7)
//    {
//        self.btnEight.userInteractionEnabled = YES;
//    }
//    else if (btnSelected.tag==8)
//    {
//        self.btnNine.userInteractionEnabled = YES;
//    }
}

- (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)filePathString
{
    NSURL *fileURL = [NSURL fileURLWithPath:filePathString];
    
    assert([[NSFileManager defaultManager] fileExistsAtPath: [fileURL path]]);
    
    NSError *error = nil;
    
    BOOL success = [fileURL setResourceValue:[NSNumber numberWithBool: YES]
                                      forKey: NSURLIsExcludedFromBackupKey
                                       error: &error];
    return success;
}

@end
