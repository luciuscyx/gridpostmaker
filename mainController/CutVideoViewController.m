//
//  CutVideoViewController.m
//  Instagrid Post
//
//  Created by chenteng on 2018/4/24.
//  Copyright © 2018年 RJLabs. All rights reserved.
//

#import "CutVideoViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "HandlerVideo.h"
#import <AssetsLibrary/AssetsLibrary.h>  // 必须导入
#import "MBProgressHUD.h"
#import "UIView+MBProgressHUD.h"
#import "CutShareViewController.h"
#import "SudokuViewController.h"
#import "ILColorPickerDualExampleController.h"
#import "PostTabButton.h"
@interface CutVideoViewController ()<UIGestureRecognizerDelegate,ILColorPickerDualExampleControllerDelegate>
{
    MPMoviePlayerController * player;
    UIColor * selectColor;
    UIImageView *selectImage;
    NSArray* colorList;
    UILabel *selectlable;
    NSURL *videoURL;
    ILColorPickerDualExampleController * ilColorPicker;
}
@property (nonatomic,strong)UIImageView *mainView;
@property (nonatomic,strong)AVPlayer *players;
@property (nonatomic,strong)AVPlayerLayer *playerLayer;
@property (nonatomic,strong)UIImageView *bootView;
@property (nonatomic,assign)CGFloat rotation;
@property (nonatomic,assign)CGPoint originalCenter;
@property (nonatomic,assign)CGRect  originalRect;
@property (nonatomic,strong)MBProgressHUD * mbProgress;
@property (nonatomic,strong)PostTabButton *allBut;
@property (nonatomic,assign)CGFloat *rangeP;
@property (nonatomic,strong)UIScrollView *colorScrollView;
@property (nonatomic,strong)UIView *natView;
//@property (nonatomic,strong)BOOL
@end

@implementation CutVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createNat];
    [self loadUI];
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)viewDidDisappear:(BOOL)animated{
    [self.players pause];
}
- (void)viewWillAppear:(BOOL)animated{
    [self.players play];
}
- (void)createNat{
//    CGFloat natH ;
//    CGFloat tabH;
//    if(IPHONEX_SERIES){
//        natH = 40;
//        tabH = 20;
//    }else{
//        natH = 0;
//        tabH = 0;
//    }
    UIImageView *mainImage = [[UIImageView alloc]initWithFrame:self.view.bounds];
    //mainImage.image = [UIImage imageNamed:@"bg"];
    mainImage.backgroundColor = [UIColor whiteColor];
    mainImage.userInteractionEnabled = YES;
    [self.view addSubview:mainImage];
    
    self.natView = [[UIView alloc]initWithFrame:CGRectMake(0,0, ScreenWidth,60+KDNavH)];
    [self.view addSubview:self.natView];
    //添加左按钮
    UIButton *cancelBut = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBut setFrame:CGRectMake(10, 15+KDNavH, 35, 35)];
    [cancelBut setImage:[UIImage imageNamed:@"backBlack_icn"] forState:UIControlStateNormal];
    //[cancelBut setTitle: forState:<#(UIControlState)#>:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
    cancelBut.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [cancelBut addTarget:self action:@selector(cancelButPresseds) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:cancelBut];
    //添加右按钮
    UIButton *doneBut = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneBut setFrame:CGRectMake(ScreenWidth - 40, 15+KDNavH, 35, 35)];
    [doneBut setImage:[UIImage imageNamed:@"doneSel_icn"] forState:UIControlStateNormal];
    // [btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
    doneBut.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [doneBut addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:doneBut];
    
    //添加文字说明
    UILabel * instructionLabel = [[UILabel alloc] init];
    instructionLabel.frame = CGRectMake(ScreenWidth/2 - 50,15+KDNavH,100,35);
    instructionLabel.textColor = [UIColor blackColor];
    instructionLabel.text = NSLocalizedString(@"CROP SIZE",nil);
    instructionLabel.numberOfLines = 0;
    instructionLabel.textAlignment = NSTextAlignmentCenter;
    instructionLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:16];
    [self.natView addSubview:instructionLabel];
    
    
    //添加左下按钮
    self.allBut = [PostTabButton buttonWithType:UIButtonTypeCustom];
    [self.allBut setFrame:CGRectMake(20,ScreenHeight-55-KDNavH, 40, 40)];
    [self.allBut setImage:[UIImage imageNamed:@"all_v"] forState:UIControlStateNormal];
    [self.allBut setImage:[UIImage imageNamed:@"allselect_v"] forState:UIControlStateSelected];
    [self.allBut setTitle:NSLocalizedString(@"Fill",nil) forState:UIControlStateNormal];
    [self.allBut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.allBut setTitleColor:mainColor forState:UIControlStateSelected];
    self.allBut.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [self.allBut addTarget:self action:@selector(allButButPresseds:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.allBut];
    //添加右下按钮
    PostTabButton *restoreBut = [PostTabButton buttonWithType:UIButtonTypeCustom];
    [restoreBut setFrame:CGRectMake(ScreenWidth - 60, ScreenHeight-55 - KDNavH, 40,40)];
    [restoreBut setImage:[UIImage imageNamed:@"restore_v"] forState:UIControlStateNormal];
    // [btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
    [restoreBut setTitle:NSLocalizedString(@"Restore",nil) forState:UIControlStateNormal];
    [restoreBut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    restoreBut.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [restoreBut addTarget:self action:@selector(restoreButAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:restoreBut];
    
    
}

- (void)loadUI{
    self.mainView = [[UIImageView alloc]initWithFrame:CGRectMake(0,(ScreenHeight-ScreenWidth-80)/2,ScreenWidth, ScreenWidth)];
    self.mainView.backgroundColor = mainColor;//[UIColor whiteColor];
    self.mainView.userInteractionEnabled = YES;
    [self.view addSubview:self.mainView];
    
    CGSize videoSize = CGSizeZero;
    
    if(self.selectImage){
        videoSize = self.selectImage.size;
    }else{
        if(self.selectVideoUrl){
            videoURL = self.selectVideoUrl;
        }else{
            if (self.videoPath) {
                videoURL = [NSURL fileURLWithPath:self.videoPath];
            }
        }
        if (videoURL) {
            AVPlayerItem *playerItem = [[AVPlayerItem alloc] initWithURL:videoURL];
            self.players = [[AVPlayer alloc] initWithPlayerItem:playerItem];
            AVAsset *asset = [AVAsset assetWithURL:videoURL];
            NSArray *tracks = [asset tracksWithMediaType:AVMediaTypeVideo];
            BOOL isFirstAssetPortrait_ = NO;
            if([tracks count] > 0) {
                AVAssetTrack *videoTrack = [tracks objectAtIndex:0];
                CGAffineTransform t = videoTrack.preferredTransform;//这里的矩阵有旋转角度，转换一下即可
                if(t.a == 0 && t.b == 1.0 && t.c == -1.0 && t.d == 0)  { isFirstAssetPortrait_ = YES;}
                if(t.a == 0 && t.b == -1.0 && t.c == 1.0 && t.d == 0)  { isFirstAssetPortrait_ = YES;}
                if(isFirstAssetPortrait_){
                    videoSize = CGSizeMake(videoTrack.naturalSize.height, videoTrack.naturalSize.width);
                }else{
                    videoSize = CGSizeMake(videoTrack.naturalSize.width, videoTrack.naturalSize.height);
                }
            }
        }
        
    }
    NSLog(@"%@",NSStringFromCGSize(videoSize));
    CGFloat playW = ScreenWidth;
    //CGFloat playH = ScreenHeight-20;
    CGFloat playlH = playW/videoSize.width*videoSize.height;
    CGFloat playlW = playW/videoSize.height*videoSize.width;
    if(videoSize.width >videoSize.height){
        self.originalRect = CGRectMake(0,(playW-playlH)/2,playW,playlH);
    }else{
        self.originalRect = CGRectMake((playW-playlW)/2,0,playlW,playW);
    }
    self.bootView = [[UIImageView alloc]initWithFrame: self.originalRect];
    self.originalCenter = self.bootView.center;
    self.mainView.clipsToBounds = YES;
    [self.mainView addSubview:self.bootView];
    
    if(self.selectImage){
        self.bootView.image = self.selectImage;
    }else{
        [self.players play];
        // 初始化播放器的Layer
        self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.players];
        //取出添加视频的视图
        self.playerLayer.frame = self.bootView.bounds;
        self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        // 把Layer加到底部View上<可以>
        [self.bootView.layer insertSublayer:self.playerLayer atIndex:0];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(runLoopTheMovie:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    }
    [self addGestureRecognizerToView:self.mainView];
    [self createSlideColorView];
    //
}
- (void)createSlideColorView{
    CGFloat butWidth = 40;
    int split_width = 5;
    CGFloat mulitper = 1.5;
    self.colorScrollView = [[UIScrollView alloc] init];
    self.colorScrollView.frame = CGRectMake(10,VIEW_YH(self.mainView), WINDOW.bounds.size.width-20, butWidth+40);
    //self.colorScrollView.backgroundColor = [UIColor clearColor];
    self.colorScrollView.showsHorizontalScrollIndicator = NO;
    self.colorScrollView.userInteractionEnabled = YES;
    self.colorScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.colorScrollView];
    //self.colorScrollView.hidden = YES;
    colorList =@[
                [UIColor colorWithRed:1.0 green: 1.0 blue:1.0 alpha: 1.0],
                [UIColor blackColor],
                [UIColor colorWithRed:204.0/255.0 green: 208.0/255.0 blue: 217.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:101.0/255.0 green: 109.0/255.0 blue: 120.0/255.0 alpha: 1.0],
                mainColor,
                [UIColor colorWithRed:236.0/255.0 green: 136/255.0 blue: 192.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:172.0/255.0 green: 146.0/255.0 blue: 237.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:93.0/255.0 green: 155.0/255.0 blue: 236.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:79.0/255.0 green: 192.0/255.0 blue: 232.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:72.0/255.0 green: 207.0/255.0 blue: 174.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:160.0/255.0 green: 212.0/255.0 blue: 104.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:253.0/255.0 green: 207.0/255.0 blue: 85.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:251.0/255.0 green: 110.0/255.0 blue: 82.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:237.0/255.0 green: 85.0/255.0 blue: 100.0/255.0 alpha: 1.0],
                [UIColor redColor],
                [UIColor greenColor],
                [UIColor blueColor],
                [UIColor cyanColor],
                [UIColor yellowColor],
                [UIColor magentaColor],
                [UIColor orangeColor],
                [UIColor purpleColor],
                [UIColor brownColor],
                ];
    // selectColor = colorList[0];
    // Variables for setting the Color Buttons
    CGFloat xCoord  = split_width;
    CGFloat yCoord = split_width+40;//下移40；按钮大小（30，30）
    CGFloat buttonWidth = butWidth-10;
    CGFloat buttonHeight = butWidth-10;
    CGFloat gapBetweenButtons = 15;
    xCoord += gapBetweenButtons;
    // Counter for Fonts
    int colorsCount = 0;
    UIButton * colorButt;
    int colorTag = 0;
    selectColor = colorList[4];
    for (colorsCount = 0; colorsCount <= colorList.count; ++colorsCount){
        colorTag = colorsCount;
        
        // Create a Button for each Color ==========
        colorButt = [UIButton buttonWithType:UIButtonTypeCustom];
        colorButt.frame = CGRectMake(xCoord, yCoord, buttonWidth, buttonHeight);
        
        if (colorsCount == 0) {
            colorButt.backgroundColor = [UIColor clearColor];
            [colorButt setImage:[UIImage imageNamed:@"colorpicker"] forState:UIControlStateNormal];
            colorButt.tag = 9999;  // Assign a tag to each button
        }else
        {
            colorButt.backgroundColor = colorList[colorsCount-1];
            colorButt.tag = 200+colorTag;  // Assign a tag to each button
            
        }
        
        
        // if (!selectColor) {
        //     selectColor = colorButt.backgroundColor;
        // }
        if(colorsCount == 1){
            colorButt.layer.masksToBounds = YES;
            colorButt.layer.borderColor = [UIColor lightGrayColor].CGColor;
            colorButt.layer.borderWidth = 1;
        }
        colorButt.layer.cornerRadius = colorButt.frame.size.width/2;
        colorButt.showsTouchWhenHighlighted = NO;
        [colorButt addTarget:self action:@selector(colorButtTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        xCoord +=  buttonWidth + gapBetweenButtons;
        [self.colorScrollView addSubview:colorButt ];
        //颜色初始化
        if (colorsCount > 0) {
            if (selectColor == colorList[colorsCount-1]) {
                //修改
//                colorButt.layer.borderColor = [UIColor whiteColor].CGColor;
//                colorButt.layer.borderWidth = 3;
                selectImage  = [[UIImageView alloc]initWithFrame:CGRectMake(colorButt.frame.origin.x-5,colorButt.frame.origin.y-45 ,40, 40)];
                UIImage *image = [UIImage imageNamed:@"selectColor"];
                selectImage.image = image;
                [self.colorScrollView addSubview:selectImage];
                selectlable = [[UILabel alloc]initWithFrame:CGRectMake(10,6, 20, 20)];
                selectlable.backgroundColor = selectColor;
                selectlable.layer.cornerRadius = selectlable.frame.size.width/2;
                selectlable.layer.masksToBounds = YES ;
                [selectImage addSubview:selectlable];
                 colorButt.transform = CGAffineTransformMakeScale(0.8, 0.8);
            }else{
              colorButt.transform = CGAffineTransformIdentity;
            }
        }else
        {
            xCoord +=  gapBetweenButtons;
        }
        
    } // END LOOP ================================
    
    // Place Buttons into the ScrollView =====
    self.colorScrollView.contentSize = CGSizeMake((buttonWidth+gapBetweenButtons) * (CGFloat)colorsCount+10+2*gapBetweenButtons, yCoord);
    self.colorScrollView.backgroundColor = [UIColor clearColor];
    
}
#pragma mark --- 颜色选择事件
-(void)colorButtTapped:(UIButton *)sender
{
    UIButton * but = sender;
    if (but&&but.tag != 9999) {
        selectColor = but.backgroundColor;
        for(int i = 0;i<colorList.count;i++){
            UIButton * allbut = [self.view viewWithTag:200+i];
            allbut.transform = CGAffineTransformIdentity;
            //allbut.layer.borderWidth = 0;
            // allbut.layer.borderColor = [UIColor whiteColor].CGColor;
        }
//        but.layer.borderColor = [UIColor whiteColor].CGColor;
//        but.layer.borderWidth = 3;
        selectImage.frame = CGRectMake(but.frame.origin.x-5,but.frame.origin.y-45 ,40, 40);
        selectlable.backgroundColor = selectColor;
        but.transform = CGAffineTransformMakeScale(0.8, 0.8);
        self.mainView.image = nil;
        self.mainView.backgroundColor = selectColor;
    }else{
        if (!ilColorPicker) {
            ilColorPicker = [[ILColorPickerDualExampleController alloc] init];
            ilColorPicker.delegate = self;
        }
        if(!self.selectImage){
        ilColorPicker.isVideo = YES;
        }else{
        ilColorPicker.isVideo = NO;
        }
        // [UIColor colorWithPatternImage:<#(nonnull UIImage *)#>]
        CGRect frameRect;
        frameRect = CGRectMake(0, (WINDOW.bounds.size.height-WINDOW.bounds.size.width)*0.5f, WINDOW.bounds.size.width, WINDOW.bounds.size.width);
        ilColorPicker.initSize = frameRect.size;
        [self.navigationController pushViewController:ilColorPicker
                                             animated:YES];
    }
}
#pragma mark ---- ILColorPickerDualExampleControllerDeletage

- (void)setSelectedColor:(UIColor *)color{
     selectColor = color;
     self.mainView.image = nil;
     self.mainView.backgroundColor = selectColor;
}
- (void)enterStartcolor:(UIImage *)hairImage
{
    selectColor = nil;
    self.mainView.image = hairImage;
}
//-(void)selectColor:(UIImage*)gadientColor
//{
//    if (!selectColor&&!gadientColor) {
//        return;
//    }
//    int ** array11 = listarray[selectIndex];
//
//    int indexc = 1;
//    for (int i = 0; i < 1; i++) {
//        for (int j = 0; j < gridCount; j++) {
//            UIImageView * subView = [self.viewimg viewWithTag:indexc];
//            if (subView) {
//                if (array11[i][j] != 1) {
//                    if (gadientColor) {
//                        subView.image = [ImageHelper getSplitImageFromMask:gadientColor rowCount:gridCount indexx:j indexy:i];
//                    }else
//                    {
//                        subView.backgroundColor = selectColor;
//                        subView.image = nil;
//                    }
//
//                }else
//                {
//                    subView.backgroundColor = [UIColor clearColor];
//                    subView.image = nil;
//                }
//            }
//            indexc++;
//        }
//    }
//}

#pragma mark ----- 循环播放
- (void)runLoopTheMovie:(NSNotification *)n{
    //注册的通知  可以自动把 AVPlayerItem 对象传过来，只要接收一下就OK
    
    AVPlayerItem * p = [n object];
    //关键代码
    [p seekToTime:kCMTimeZero];
    
    [self.players play];
    NSLog(@"重播");
}
#pragma mark - 手势
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return  YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}

- (void) addGestureRecognizerToView:(UIView *)view
{
    // 缩放手势
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchView:)];
    pinchGestureRecognizer.delegate =self;
    [view addGestureRecognizer:pinchGestureRecognizer];
    // 移动手势
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panView:)];
    panGestureRecognizer.delegate =self;
    [panGestureRecognizer setMaximumNumberOfTouches:2];
    [view addGestureRecognizer:panGestureRecognizer];
    //旋转手势
//    UIRotationGestureRecognizer *rotaGestureRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotaView:)];
//     rotaGestureRecognizer.delegate =self;
//    [view addGestureRecognizer:rotaGestureRecognizer];
    
}
// 处理缩放手势
-(void) pinchView:(UIPinchGestureRecognizer *)pinchGestureRecognizer{
    if(!self.allBut.selected){
    if (pinchGestureRecognizer.state == UIGestureRecognizerStateBegan || pinchGestureRecognizer.state == UIGestureRecognizerStateChanged ) {
        if ( (self.bootView.frame.size.width <= (self.mainView.frame.size.width * 10))) {
            self.bootView.transform = CGAffineTransformScale(self.bootView.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
            pinchGestureRecognizer.scale = 1;
        }else{
            self.bootView.transform = CGAffineTransformScale(self.bootView.transform, self.mainView.frame.size.width* 10/self.bootView.frame.size.width,  self.mainView.frame.size.width* 10/self.bootView.frame.size.width);
        }
    }
    }
}
// 处理拖拉手势
- (void) panView:(UIPanGestureRecognizer *)panGestureRecognizer{
    if (panGestureRecognizer.state == UIGestureRecognizerStateBegan || panGestureRecognizer.state == UIGestureRecognizerStateChanged) {
        if(self.allBut.selected){
            if(self.bootView.frame.size.width > self.bootView.frame.size.height){
                CGFloat beyondW = (self.bootView.frame.size.width-self.bootView.frame.size.height)/2;
                CGPoint translation = [panGestureRecognizer translationInView:self.bootView.superview];
                CGFloat a = self.originalCenter.x-beyondW;
                CGFloat b = self.originalCenter.x+beyondW;
                CGFloat c = self.bootView.center.x;

                if(a<=c  && c <= b){
                    if(self.bootView.center.x+translation.x>b){
                      [self.bootView setCenter:(CGPoint){b, self.bootView.center.y}];
                    }else if(self.bootView.center.x+translation.x< a){
                     [self.bootView setCenter:(CGPoint){a, self.bootView.center.y}];
                    }else{
                     [self.bootView setCenter:(CGPoint){self.bootView.center.x + translation.x, self.bootView.center.y}];
                    }
                    [panGestureRecognizer setTranslation:CGPointZero inView:self.bootView.superview];
                }
            }else{
                CGFloat beyondH = (self.bootView.frame.size.height-self.bootView.frame.size.width)/2;
                CGPoint translation = [panGestureRecognizer translationInView:self.bootView.superview];
                CGFloat a = self.originalCenter.y-beyondH;
                CGFloat b = self.originalCenter.y+beyondH;
                CGFloat c = self.bootView.center.y;
                
                if(a<=c  && c <= b){
                    if(self.bootView.center.y+translation.y>b){
                        [self.bootView setCenter:(CGPoint){self.bootView.center.x,b}];
                    }else if(self.bootView.center.y+translation.y< a){
                        [self.bootView setCenter:(CGPoint){self.bootView.center.x,a}];
                    }else{
                        [self.bootView setCenter:(CGPoint){self.bootView.center.x, self.bootView.center.y + translation.y}];
                    }
                    [panGestureRecognizer setTranslation:CGPointZero inView:self.bootView.superview];
                }
            }
        }else{
            CGPoint translation = [panGestureRecognizer translationInView:self.bootView.superview];
            [self.bootView setCenter:(CGPoint){self.bootView.center.x + translation.x, self.bootView.center.y + translation.y}];
            [panGestureRecognizer setTranslation:CGPointZero inView:self.bootView.superview];
        }
    }
}
#pragma mark --- 按钮点击事件
- (void)cancelButPresseds{
   [self.navigationController popViewControllerAnimated:YES];
}
- (void)allButButPresseds:(UIButton *)sender{
    sender.selected = !sender.selected;
    if(sender.selected){
        CGFloat rate ;
        if(self.bootView.frame.size.width > self.bootView.frame.size.height){
            rate = ScreenWidth/self.bootView.frame.size.height;
        }else{
            rate = ScreenWidth/self.bootView.frame.size.width;
        }
        self.bootView.transform = CGAffineTransformScale(self.bootView.transform,rate,rate);
        self.bootView.center = self.originalCenter;
       // [self.allBut setImage:[UIImage imageNamed:@"allselect_v"] forState:UIControlStateNormal];
    }else{
       // [self.allBut setImage:[UIImage imageNamed:@"all_v"] forState:UIControlStateNormal];
    }
}
- (void)restoreButAction{
    self.allBut.selected = NO;
   // [self.allBut setImage:[UIImage imageNamed:@"all_v"] forState:UIControlStateNormal];
    
    CGFloat rate ;
     if(self.bootView.frame.size.width > self.bootView.frame.size.height){
         rate = ScreenWidth/self.bootView.frame.size.width;
     }else{
         rate = ScreenWidth/self.bootView.frame.size.height;
     }
    self.bootView.transform = CGAffineTransformScale(self.bootView.transform,rate,rate);
    self.bootView.center = self.originalCenter;
}
- (void)doneAction{
    if(self.selectImage){
       //图片处理(1600 *1600)
        if(self.isDomestic == 1){
            SudokuViewController *sudoku = [[SudokuViewController alloc]init];
            UIImage *cutImage = [self imageFromView:self.mainView];
            sudoku.intShareType = 3;
            sudoku.imageSource = cutImage;
            [self.navigationController pushViewController:sudoku animated:YES];
        }else{
        //[self saveImageToPhotos:cutImage];
        CutShareViewController *cutshare = [[CutShareViewController alloc]init];
        UIImage *cutImage = [self imageFromView:self.mainView];
        cutshare.shareImage = cutImage;
        [self.navigationController pushViewController:cutshare animated:YES];
        }
    }else{
        //1:保存分享视频    2:拼接视频后返回（刷新）
    [self.view showWaiting:NO message:@"Loading..."];
     CGFloat videoRadians = atan2f(self.bootView.transform.b, self.bootView.transform.a);//旋转的角度
     CGFloat rate = 1080/ScreenWidth;
     CGPoint videoCp = CGPointMake(self.bootView.frame.origin.x*rate,self.bootView.frame.origin.y*rate);//位移
    //CGPoint  videoCp = CGPointMake((self.bootView.center.x-self.bootView.frame.size.width/2)*rate,(self.bootView.center.y-self.bootView.frame.size.height/2)*rate);
    CGFloat videoW ;
    // NSLog(@" ------- %@",NSStringFromCGRect(self.bootView.frame));
    
    if(self.bootView.frame.size.width > self.bootView.frame.size.height){
        videoW = self.bootView.frame.size.width/ScreenWidth*1080;
    }else{
        videoW = self.bootView.frame.size.height/ScreenWidth*1080;
    }
    NSString *processVodeoPath = [self  getSavePathWithFileSuffix:@"_cutVodeo.mov"];//记得删除 过度
    NSString *lastVodeoPath = [self  getSavePathWithFileSuffix:@"_lastVodeo.mov"];  //总的路径 保存
        
        [[HandlerVideo sharedInstance]videosOneWithVideoPath:videoURL lastVideoPath:nil videoFullPath:processVodeoPath videoScale:YES VideocuPoint:videoCp VideoW:videoW VideoAngle:videoRadians VideoColor:selectColor VideoImage:self.mainView.image completedBlock:^(BOOL success, NSString *errorMsg) {
        if (success) {
            if(self.isType){
                 dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hiddenWaiting];
                     if(self.isDomestic == 1){
                         SudokuViewController *sudoku = [[SudokuViewController alloc]init];
                         sudoku.intShareType = 3;
                         sudoku.imageSource = [self getVideoPreViewImage:[NSURL fileURLWithPath:processVodeoPath]];
                         sudoku.shareVideoUrl = [NSURL fileURLWithPath:processVodeoPath];
                         [self.navigationController pushViewController:sudoku animated:YES];
                     }else{
                     //[self exportDidFinish:[NSURL fileURLWithPath:processVodeoPath]];
                     CutShareViewController *cutshare = [[CutShareViewController alloc]init];
                     cutshare.shareVurl = [NSURL fileURLWithPath:processVodeoPath] ;
                     [self.navigationController pushViewController:cutshare animated:YES];
                     }
                     });
            //跳转
            }else{
            [[HandlerVideo sharedInstance]compositeVideosAndAudios:[NSURL fileURLWithPath:processVodeoPath] audioUrl:self.imageVurl videoFullPath:lastVodeoPath completedBlock:^(BOOL success, NSString *errorMsg) {
                //回到主线程中
                if (success) {
                    NSLog(@"---->  SUCCESS");
                    //把拼接好的视频URL存在字典里
                    //self.dicUrl[key] = processVodeoPath;
                    //回调数据
                    if(self.lastPathBlock){
                        self.lastPathBlock(lastVodeoPath);
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.view hiddenWaiting];
                        [self.navigationController popViewControllerAnimated:YES];
                    });
                } else {
                    NSLog(@"---->> %@",errorMsg);
                 [self.view showToast:@"Load failed"];
                }
            }];
            }
        } else {
            [self.view hiddenWaiting];
            [self.view showToast:@"Load failed"];
            NSLog(@"---->> %@",errorMsg);
        }
    }];
    }
}
//把view转成UIImage
-(UIImage*)imageFromView:(UIView*)view{
    CGSize igSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.width);
    // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了，关键就是第三个参数。
    UIGraphicsBeginImageContextWithOptions(igSize, NO, [UIScreen mainScreen].scale);
    
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

// 获取视频第一帧图片
- (UIImage*) getVideoPreViewImage:(NSURL *)path
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:path options:nil];
    AVAssetImageGenerator *assetGen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    assetGen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [assetGen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *videoImage = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return videoImage;
}
#pragma mark ---- 文件存储路经
- (NSString *)getSavePathWithFileSuffix:(NSString *)suffix
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths objectAtIndex:0];
    NSDate *date = [NSDate date];
    //获取当前时间
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
    NSString *curretDateAndTime = [dateFormat stringFromDate:date];
    NSString *fileName = [NSString stringWithFormat:@"%@%@",curretDateAndTime,suffix];
    //指定文件存储路径
    NSString *filePath = [documentPath stringByAppendingPathComponent:fileName];
    return filePath;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
