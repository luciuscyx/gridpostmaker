//
//  CutShareViewController.h
//  Instagrid Post
//
//  Created by chenteng on 2018/4/28.
//  Copyright © 2018年 RJLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CutShareViewController : UIViewController
@property (nonatomic,strong)UIImage *shareImage;
@property (nonatomic,strong)NSURL *shareVurl;
@end
