//
//  InstaGridShareViewController.h
//  Instagrid Post
//
//  Created by Mangal on 02/07/15.
//  Copyright (c) 2015 RJLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChangeVideoGridViewControllerDelegate;
@interface ChangeVideoGridViewController : UIViewController<UIDocumentInteractionControllerDelegate>
{
    int currentCount;

}
@property (nonatomic, strong)  UIButton *btnSelected;
@property (nonatomic, assign) int gridCount;
//@property (nonatomic, strong) UIImage *imageSource;
@property (nonatomic, strong) UIImage *orgimageSource;

@property (nonatomic, strong)  UIDocumentInteractionController *docController;
@property (nonatomic, retain) id <ChangeVideoGridViewControllerDelegate> delegate;

@property (nonatomic, strong)NSArray *rowArray;
@property (nonatomic,strong)NSMutableArray *imageArray;
@property (nonatomic,strong)UIImage *imageSelect;
@property (nonatomic,strong)NSMutableDictionary *dicUrl;
@property (nonatomic, assign) BOOL homeBool;
-(void)rateSuccess;
-(void)updateView;
@end
@protocol  ChangeVideoGridViewControllerDelegate
- (void)checkifshowrateus;
@end

