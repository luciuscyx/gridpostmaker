//
//  SelectImageViewController.m
//  Instagrid Post
//
//  Created by macbookair on 30/7/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import "SelectImageViewController.h"
#import "HomeCollectionViewCells.h"
#import "HomeImageCropViewController.h"
#import "RSKImageCropViewController.h"
#import <Photos/Photos.h>
#import <MessageUI/MessageUI.h>
#import "CCAutoScrollView.h"

#import "CutVideoViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "NSDate+TimeInterval.h"
#import "PostImagehelper.h"
@interface SelectImageViewController ()
<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate> {
    CGFloat cellSize;
    CGFloat separation;
    UIImagePickerController * _cameraPicker;
    UIImagePickerController * _imagePicker;
    UIView *instaView;
}
@property (nonatomic,strong)UIView *natView;
@property (nonatomic,strong)UIScrollView *showScrollView;
@property (nonatomic,strong)UIView *tabView;

@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic,strong)  NSArray *segArray;
@property (nonatomic, strong) PHFetchResult *result;
@property (nonatomic, strong) PHImageRequestOptions *requestOptions;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic,assign)NSInteger count;
@property (nonatomic,strong)NSMutableArray *finishImageArray;
@end

@implementation SelectImageViewController

@synthesize interstitial = interstitial_;
#define INSTERTITIAL_BANNER_UNIT_ID @"ca-app-pub-7766793965493521/4994281446"
- (void)interstitial:(GADInterstitial *)interstitial
didFailToReceiveAdWithError:(GADRequestError *)error {
    // Alert the error.
    self.interstitial.delegate = nil;
    self.interstitial = nil;
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)interstitial {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]){
        self.interstitial.delegate = nil;
        self.interstitial = nil;
        return;
    }
    if (interstitial) {
        @try {
            UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
            while (topController.presentedViewController) {
                topController = topController.presentedViewController;
            }
            
            [interstitial presentFromRootViewController:topController];
            
        }
        
        @catch (NSException *e) {
            NSLog(@"%@",[e reason]);
        }
        @finally {
            
        }
        interstitial.delegate = nil;
        
    }
    self.interstitial.delegate = nil;
    self.interstitial = nil;
}
- (void)showgoogleinitial
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]){
        return;
    }
    self.interstitial = [[GADInterstitial alloc] initWithAdUnitID:INSTERTITIAL_BANNER_UNIT_ID];
    self.interstitial.delegate = self;
    
    // Note: Edit InterstitialExampleAppDelegate.m to update
    // INTERSTITIAL_AD_UNIT_ID with your interstitial ad unit id.
    //self.interstitial.adUnitID = ;
    
    [self.interstitial loadRequest: [GADRequest request]];
    
}
//-(BOOL)prefersStatusBarHidden { return YES; }
- (void)checkifshowrateus
{
    NSNumber * checkCount = [[NSUserDefaults standardUserDefaults] objectForKey:@"checkCount"];
    if (checkCount&&[checkCount intValue] > 2) {
        if([SKStoreReviewController respondsToSelector:@selector(requestReview)]) {
            [[UIApplication sharedApplication].keyWindow endEditing:YES];
            [SKStoreReviewController requestReview];
        }
    }
    if (!checkCount) {
        [[NSUserDefaults standardUserDefaults]  setObject:[NSNumber numberWithInt:0] forKey:@"checkCount"];
    }else
    {
        [[NSUserDefaults standardUserDefaults]  setObject:[NSNumber numberWithInt:[checkCount intValue]+1] forKey:@"checkCount"];
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadData];
    [self  loadUI];
}
- (void)loadData{
    //加载已处理过的图片 ---- 默认加载6个
    NSArray *array= [[NSUserDefaults standardUserDefaults] objectForKey:@"UserFinishedImageinfo"];
    self.finishImageArray = [NSMutableArray arrayWithArray:array];
    
    NSNumber * ccc = [[NSUserDefaults standardUserDefaults] objectForKey:@"openCount"];
    if (!ccc) {
        ccc = [NSNumber numberWithInt:0];
    }
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:[ccc intValue]+1] forKey:@"openCount"];
    //self.isDomestic = [[NSUserDefaults standardUserDefaults] integerForKey:@"isDomestic"];
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusAuthorized:
                [self getAllPhotosFromCamera];
                break;
            case PHAuthorizationStatusRestricted:
                break;
            case PHAuthorizationStatusDenied:
                break;
            default:
                break;
        }
    }];
}
- (void)loadUI{
    [self createNatView];
    [self showFinishedImageView];
    [self createTabView];
    [self showImageListView];
}

-(void)createNatView{
    self.view.backgroundColor = RGB(245,245,245);
    self.natView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,ScreenWidth,KDNavH+60)];
    [self.view addSubview:self.natView];
    UIButton *backButton = [[UIButton alloc]initWithFrame:CGRectMake(10,15+KDNavH, 35, 35)];
    [backButton setImage:[UIImage imageNamed:@"selectBack"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backMainViewAction) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:backButton];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2-50, 15,100, 35)];
    titleLabel.text = NSLocalizedString(@"Select",nil);//@"选择布局";
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:15];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.natView addSubview:titleLabel];
}

-(void)showFinishedImageView{
     CGFloat loadedLine = 10;
     CGFloat loadedImageH = (ScreenWidth - 50)/2.5;
    self.showScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,VIEW_YH(self.natView),ScreenWidth,loadedImageH)];
    self.showScrollView.contentSize = CGSizeMake((loadedImageH+loadedLine)*self.finishImageArray.count, loadedImageH);
    [self.view addSubview:self.showScrollView];
    if(self.finishImageArray.count >0){
        for(int i=0;i<self.finishImageArray.count;i++){
            UIButton *imageBut = [[UIButton alloc]initWithFrame:CGRectMake(10+(loadedImageH+20)*i,0,loadedImageH,loadedImageH)];
            NSString * DocumentsPath = [NSHomeDirectory()stringByAppendingPathComponent:@"Documents/finishAllFile"];
            NSString *itempath  = self.finishImageArray[i];
            NSString * filePath = [[NSString alloc] initWithFormat:@"%@%@",DocumentsPath,itempath];
            //取出图片 等待 你的使用
            UIImage *img;
            if ([[itempath pathExtension] isEqualToString:@"jpg"]){
                img = [UIImage imageWithContentsOfFile:filePath];
            }else{
                img = [PostImagehelper getVideoPreViewImage:[NSURL fileURLWithPath:filePath]];
                UIImageView *playBut = [[UIImageView alloc]initWithFrame:CGRectMake(loadedImageH/2-15,loadedImageH/2-15, 30, 30)];
                playBut.image  = [UIImage imageNamed:@"play_su"];
                [imageBut addSubview:playBut];
            }
            UIImage *nearImage  = [PostImagehelper squareImageFromImage:img scaledToSize:400];
            [imageBut setImage:nearImage forState:UIControlStateNormal];
            imageBut.tag = 500+i;
            [imageBut addTarget:self action:@selector(selectnearImageAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.showScrollView addSubview:imageBut];
        }
    }
}

- (void)createTabView{
    self.tabView  = [[UIView alloc]initWithFrame:CGRectMake(0,ScreenHeight - KDNavH  - 60,ScreenWidth, 60+KDNavH)];
    self.tabView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tabView];
    UIImageView *albumIgview = [[UIImageView alloc]initWithFrame:CGRectMake(15,15, 30, 30)];
    albumIgview.image = [UIImage imageNamed:@"allAlbum_icn"];
    [self.tabView addSubview:albumIgview];
    UILabel *allAlLabel = [[UILabel alloc]initWithFrame:CGRectMake(VIEW_XW(albumIgview), 15,80, 30)];
    allAlLabel.text = NSLocalizedString(@"Select album",nil);//@"选择布局";
    allAlLabel.textColor = [UIColor blackColor];
    allAlLabel.font = [UIFont systemFontOfSize:12];
    allAlLabel.textAlignment = NSTextAlignmentLeft;
    [self.tabView addSubview:allAlLabel];
    UIImageView *pushIgview = [[UIImageView alloc]initWithFrame:CGRectMake(VIEW_XW(allAlLabel)-5,15, 30, 30)];
    pushIgview.image = [UIImage imageNamed:@"pushAlbum_icn"];
    [self.tabView addSubview:pushIgview];
    UIControl *albumButton = [[UIControl alloc]initWithFrame:CGRectMake(15,10,100, 40)];
    [albumButton addTarget:self action:@selector(selectAlbumAction) forControlEvents:UIControlEventTouchUpInside];
     [self.tabView addSubview:albumButton];
    
    UIButton *cameraButton = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 55, 10,40, 40)];
    [cameraButton setImage:[UIImage imageNamed:@"newCamera"] forState:UIControlStateNormal];
    [cameraButton addTarget:self action:@selector(selectCameraAction) forControlEvents:UIControlEventTouchUpInside];
    [self.tabView addSubview:cameraButton];
    
}
- (void)showImageListView{
    [self loadCollectionView:self.modelIndex];
}
- (void)loadCollectionView:(BOOL)select{
    CGRect collectRect;
    if(self.finishImageArray.count >0){
        collectRect = CGRectMake(0,VIEW_YH(self.showScrollView), ScreenWidth,ScreenHeight-VIEW_YH(self.showScrollView)-VIEW_H(self.tabView));
    }else{
        collectRect = CGRectMake(0,VIEW_YH(self.natView), ScreenWidth,ScreenHeight-VIEW_H(self.tabView)-VIEW_YH(self.natView));
    }
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc]init];
    //flow.headerReferenceSize =CGSizeMake(ScreenWidth,45);
    cellSize = floor(([UIScreen mainScreen].bounds.size.width-9)/4);
    separation = ([UIScreen mainScreen].bounds.size.width - cellSize*4)/5;
    if(select){
        flow.minimumLineSpacing = 10;
        //水平间隔
        flow.minimumInteritemSpacing = 10;
        //item的大小
        //flow.itemSize = CGSizeMake(ScreenWidth,80);
        //滑动的方向
        flow.scrollDirection = UICollectionViewScrollDirectionVertical;
        //边距
        flow.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }else{
        flow.minimumLineSpacing = 10;
        //水平间隔
        flow.minimumInteritemSpacing = 10;
        //item的大小
        //flow.itemSize = CGSizeMake(50,50);
        //滑动的方向
        flow.scrollDirection = UICollectionViewScrollDirectionVertical;
        //边距
        flow.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    self.collectionView = [[UICollectionView alloc]initWithFrame:collectRect collectionViewLayout:flow];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.view addSubview:self.collectionView];
    //注册
    // [self.collectionView registerNib:[UINib nibWithNibName:@"HomeCollectionViewCells" bundle:nil] forCellWithReuseIdentifier:@"HomeCollectionViewCells"];
    [self.collectionView registerClass:[UICollectionReusableView class]forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
    [self.collectionView registerClass:[HomeCollectionViewCells class] forCellWithReuseIdentifier:@"HomeCollectionViewCells"];
}
-(void)getAllPhotosFromCamera {
    [self.imageArray removeAllObjects];
    self.requestOptions = [[PHImageRequestOptions alloc] init];
    self.requestOptions.resizeMode   = PHImageRequestOptionsResizeModeFast;
    self.requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    self.requestOptions.synchronous = true;
    PHFetchOptions *fetchOptions = [PHFetchOptions new];
    fetchOptions.sortDescriptors =  @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    if(self.modelIndex == 1){
        // 获得相机胶卷
        PHAssetCollection *cameraRoll = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumPanoramas options:nil].lastObject;
        self.result = [PHAsset fetchAssetsInAssetCollection:cameraRoll options:fetchOptions];
    }else if(self.modelIndex == 0){
        self.result = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
    }else{
        PHAssetCollection *cameraRoll = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary options:nil].lastObject;
        self.result = [PHAsset fetchAssetsInAssetCollection:cameraRoll options:fetchOptions];
        //添加
    }
    self.count = self.result.count;
    self.imageArray = [NSMutableArray arrayWithCapacity:self.count];
    
    for (int i=0; i<self.count; i++) {
        [self.imageArray addObject:[NSNull null]];
    }
    
    dispatch_async(dispatch_get_main_queue(),^{
        [self.collectionView reloadData];
        if (self.count <= 0) {
            if(self.modelIndex == 1){
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"There are no panoramas in your album.", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }else
            {
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"There are no photos in your album.", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
        }
    });
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(self.modelIndex == 1){
        return CGSizeMake(cellSize, 80);
    }else{
        return CGSizeMake(cellSize, cellSize);
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
        return self.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return separation;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return separation;
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(separation, separation, separation, separation); // top, left, bottom, right
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeCollectionViewCells *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeCollectionViewCells" forIndexPath:indexPath];
    id object;
    if(self.modelIndex == 1){
        object = self.imageArray[indexPath.row];
    }else {
        object = self.imageArray[indexPath.row];
    }
    CGSize size;
    
    if([object isKindOfClass:[UIImage class]]) {
        if(self.modelIndex == 1){
            cell.imgView.image = self.imageArray[indexPath.row];
        }else{
            cell.imgView.image = self.imageArray[indexPath.row];
            cell.videoView.hidden = YES;
        }
    }
    else {
        if(self.modelIndex == 1){
            PHAsset *set = self.result[indexPath.row];
            size = CGSizeMake(cellSize, 80);
            [[PHImageManager defaultManager] requestImageForAsset:set
                                                       targetSize:size
                                                      contentMode:PHImageContentModeDefault
                                                          options:self.requestOptions
                                                    resultHandler:^void(UIImage *image, NSDictionary *info) {
                                                        
                                                        image = [PostImagehelper clipImage:image ScaleWithsize:CGSizeMake(cellSize,80)];
                                                        if (indexPath.row < self.imageArray.count) {
                                                            [self.imageArray replaceObjectAtIndex:indexPath.row withObject:image];
                                                            cell.imgView.image = self.imageArray[indexPath.row];
                                                        }else
                                                        {
                                                            [self.imageArray addObject:image];
                                                            cell.imgView.image = image;
                                                        }
                                                        
                                                    }];
        }else {
                PHAsset *set = self.result[indexPath.row];
                if(set.mediaType == PHAssetMediaTypeVideo){
                    cell.videoView.hidden = NO;
                    cell.videoTime.text = [NSDate timeDescriptionOfTimeInterval:set.duration];
                }else{
                    cell.videoView.hidden = YES;
                    // [cell clear];
                }
                size = CGSizeMake(cellSize, cellSize);
                [[PHImageManager defaultManager] requestImageForAsset:set
                                                           targetSize:size
                                                          contentMode:PHImageContentModeDefault
                                                              options:self.requestOptions
                                                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                                                            if(set.mediaType == PHAssetMediaTypeVideo){
                                                                cell.imgView.image = image;
                                                            }else{
                                                                image = [PostImagehelper squareImageFromImage:image scaledToSize:cellSize];
                                                                if (indexPath.row < self.imageArray.count) {
                                                                    [self.imageArray replaceObjectAtIndex:indexPath.row withObject:image];
                                                                    cell.imgView.image = self.imageArray[indexPath.row];
                                                                }else
                                                                {
                                                                    [self.imageArray addObject:image];
                                                                    cell.imgView.image = image;
                                                                }
                                                                
                                                            }
                                                        }];
        }
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //20180906 ---- 添加一个随机几率，用来判定广告  ---  设置20%的几率出现广告
    int random = (arc4random() % 10);
    //-----
    PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeNone;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.synchronous = true;
    if(self.modelIndex == 1){
        [[PHImageManager defaultManager] requestImageForAsset:self.result[indexPath.row]
                                                   targetSize:PHImageManagerMaximumSize
                                                  contentMode:PHImageContentModeDefault
                                                      options:requestOptions
                                                resultHandler:^void(UIImage *image, NSDictionary *info) {
                                                    if(image.size.width >1024){
                                                        image = [PostImagehelper image:image fitInSize:CGSizeMake(1024, image.size.height/image.size.width*1024)];
                                                    }
                                                    HomeImageCropViewController *imageCropVC = [[HomeImageCropViewController alloc] initWithImage:image cropMode:RSKImageCropModeSquare];
                                                    imageCropVC.isDomestic = self.isDomestic;
                                                    imageCropVC.needAutofix = YES;
                                                    //[self presentViewController:imageCropVC animated:YES completion:nil];
                                                    [self.navigationController pushViewController:imageCropVC animated:YES];
                                                    //20180906
                                                    if(random < 2){
                                                          [self performSelector:@selector(showgoogleinitial) withObject:nil afterDelay:0.5];
                                                    }
                                                }];
        
        
        
    }else{
            PHAsset *phAsset = self.result[indexPath.row];
            if (phAsset.mediaType == PHAssetMediaTypeVideo) {
                PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc] init];
                options.version = PHImageRequestOptionsVersionCurrent;
                options.deliveryMode = PHVideoRequestOptionsDeliveryModeAutomatic;
                PHImageManager *manager = [PHImageManager defaultManager];
                [manager requestAVAssetForVideo:phAsset options:options resultHandler:^(AVAsset * _Nullable asset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        AVURLAsset *urlAsset = (AVURLAsset *)asset;
                        NSURL *url = urlAsset.URL;
                        CutVideoViewController *cutV = [[CutVideoViewController alloc]init];
                        cutV.isType = YES;
                        cutV.isDomestic = self.isDomestic;
                        cutV.selectVideoUrl = url;
                        //[self presentViewController:cutV animated:YES completion:nil];
                        [self.navigationController pushViewController:cutV animated:YES];
                        //20180906
                        if(random < 2){
                            [self performSelector:@selector(showgoogleinitial) withObject:nil afterDelay:0.5];
                        }
                        //---
                    });
                }];
            }else{
                [[PHImageManager defaultManager] requestImageForAsset:phAsset
                                                           targetSize:PHImageManagerMaximumSize
                                                          contentMode:PHImageContentModeDefault
                                                              options:requestOptions
                                                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                                                            if(self.modelIndex == 2){
                                                                CutVideoViewController *cut = [[CutVideoViewController alloc]init];
                                                                cut.isType = YES;
                                                                cut.isDomestic = self.isDomestic;
                                                                cut.selectImage = image;
                                                                //[self presentViewController:cut animated:YES completion:nil];
                                                                [self.navigationController pushViewController:cut animated:YES];
                                                                //20180906
                                                                if(random < 2){
                                                                     [self performSelector:@selector(showgoogleinitial) withObject:nil afterDelay:0.5];
                                                                }
                                                            }else{
                                                                RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:image cropMode:RSKImageCropModeSquare];
                                                                imageCropVC.isDomestic = self.isDomestic;
                                                                imageCropVC.needAutofix = YES;
                                                                //[self presentViewController:imageCropVC animated:YES completion:nil];
                                                                [self.navigationController pushViewController:imageCropVC animated:YES];
                                                                //20180906
                                                                if(random < 2){
                                                                        [self performSelector:@selector(showgoogleinitial) withObject:nil afterDelay:0.5];
                                                                }
                                                            }
                                                        }];
            }
    }
    //        else{
    //        //添加
    //    }
    
}
#pragma mark --- 选择相机
-(void)selectCameraAction{
    if(self.modelIndex == 0 || self.modelIndex == 1){
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            _cameraPicker = [UIImagePickerController new];
            _cameraPicker.delegate = self;
            _cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            _cameraPicker.allowsEditing = NO;
            [self presentViewController:_cameraPicker animated:YES completion:nil];
        }
    }else{
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            //Create and show picker
            UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
            cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
            cameraUI.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:
                                   UIImagePickerControllerSourceTypeCamera];//[[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage,kUTTypeVideo, nil];
            cameraUI.allowsEditing = NO;
            cameraUI.delegate = self;
            [self presentViewController:cameraUI animated:true completion:nil];
        }
    }
}
#pragma mark --- 选择相册
- (void)selectAlbumAction{
    if(self.modelIndex == 0 || self.modelIndex == 1){
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            _imagePicker = [UIImagePickerController new];
            _imagePicker.delegate = self;
            _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            _imagePicker.allowsEditing = NO;
            [self presentViewController:_imagePicker animated:YES completion:nil];
        }
    }else{
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            _imagePicker = [UIImagePickerController new];
            _imagePicker.delegate = self;
            _imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            _imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:
                                       UIImagePickerControllerSourceTypePhotoLibrary];
            _imagePicker.allowsEditing = NO;
            [self presentViewController:_imagePicker animated:YES completion:nil];
        }
    }
}

#pragma mark --- 返回
- (void)backMainViewAction{
    [self.navigationController popToRootViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark --- 选择最近处理的图片
- (void)selectnearImageAction:(UIButton *)sender{
    
}
#pragma mark image picker delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //20180906 ---- 添加一个随机几率，用来判定广告
    int random = (arc4random() % 10);
    //-----
    //获取相册图片(从摄像机进入相册)
    if(self.modelIndex == 2){
        NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){
            UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
            [picker dismissViewControllerAnimated:YES completion:^{
                CutVideoViewController *videoCropVC = [[CutVideoViewController alloc] init];
                videoCropVC.selectImage = image;
                videoCropVC.isDomestic = self.isDomestic;
                videoCropVC.isType = YES;
                //[self presentViewController:videoCropVC animated:YES completion:nil];
                [self.navigationController pushViewController:videoCropVC animated:YES];
                //20180906
                if(random < 2){
                      [self performSelector:@selector(showgoogleinitial) withObject:nil afterDelay:0.5];
                }
            }];
            //图片
        }else {
            //视频
            //视频的存储
            NSString *videoPath = [self  getSavePathWithFileSuffix:@"CutShareVodeo.mov"];
            success = [fileManager fileExistsAtPath:videoPath];
            if (success) {
                [fileManager removeItemAtPath:videoPath error:nil];
            }
            NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
            NSData *videlData = [NSData dataWithContentsOfURL:videoURL];
            [videlData writeToFile:videoPath atomically:YES]; //写入本地
            //存储数据
            success = [fileManager fileExistsAtPath:videoPath];
            if (success) {
                //跳转到视频剪切页面
                [picker dismissViewControllerAnimated:YES completion:^{
                    CutVideoViewController *videoCropVC = [[CutVideoViewController alloc] init];
                    videoCropVC.selectVideoUrl = [NSURL fileURLWithPath:videoPath];
                    videoCropVC.isDomestic = self.isDomestic;
                    videoCropVC.isType = YES;
                    // [self presentViewController:videoCropVC animated:YES completion:nil];
                    [self.navigationController pushViewController:videoCropVC animated:NO];
                    //20180906
                    if(random < 2){
                           [self performSelector:@selector(showgoogleinitial) withObject:nil afterDelay:0.5];
                    }
                }];
            }
        }
    }else{
        UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
        [picker dismissViewControllerAnimated:YES completion:^{
            RSKImageCropViewController *imageCropVC = [[RSKImageCropViewController alloc] initWithImage:image cropMode:RSKImageCropModeSquare];
            imageCropVC.isDomestic = self.isDomestic;
            //imageCropVC.delegate = self;
            imageCropVC.needAutofix = YES;
             //[self presentViewController:imageCropVC animated:YES completion:nil];
            [self.navigationController pushViewController:imageCropVC animated:YES];
            //20180906
            if(random < 2){
                  [self performSelector:@selector(showgoogleinitial) withObject:nil afterDelay:0.5];
            }
        }];
    }
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}
#pragma mark ---- 文件存储路经
- (NSString *)getSavePathWithFileSuffix:(NSString *)suffix
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentPath = [paths objectAtIndex:0];
    //NSString *moviePath =[[paths objectAtIndex:0]stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.mov",index]];
    NSDate *date = [NSDate date];
    //获取当前时间
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
    NSString *curretDateAndTime = [dateFormat stringFromDate:date];
    //获取用户userID
    // NSDictionary *userDic= [[NSUserDefaults standardUserDefaults] objectForKey:UserInformation];
    //命名文件
    //NSString *fileName = [NSString stringWithFormat:@"%@%@.%@",userDic[@"UserID"],curretDateAndTime,suffix];
    NSString *fileName = [NSString stringWithFormat:@"%@%@",curretDateAndTime,suffix];
    //指定文件存储路径
    NSString *filePath = [documentPath stringByAppendingPathComponent:fileName];
    
    return filePath;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
