//
//  AddImageEditViewController.h
//  Instagrid Post
//
//  Created by macbookair on 3/8/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddImageEditViewController : UIViewController
@property (nonatomic,strong)UIImage *editImage;
@property (nonatomic,assign)int gridCount;
@property (nonatomic,assign)BOOL selectHome;
@end

NS_ASSUME_NONNULL_END
