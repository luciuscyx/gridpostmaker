//
//  HomeCollectionViewCells.h
//  MyInstagrids
//
//  Created by chenteng on 2017/10/17.
//  Copyright © 2017年 fh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionViewCells : UICollectionViewCell
@property (strong, nonatomic)  UIImageView *imgView;
@property (strong,nonatomic)   UIView *videoView;
@property (strong, nonatomic)  UIImageView *videoTypeView;
@property (strong,nonatomic)   UILabel *videoTime;

-(void)clear;
@end
