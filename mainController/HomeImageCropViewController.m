//
// RSKImageCropViewController.m
//
// Copyright (c) 2014 Ruslan Skorb, http://ruslanskorb.com/
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "HomeImageCropViewController.h"
#import "RSKTouchView.h"
#import "RSKImageScrollView.h"
#import "UIImage+RSKImageCropper.h"
#import "CGGeometry+RSKImageCropper.h"
#import "ImageHelper.h"

#import "ILColorPickerDualExampleController.h"
#import "HomeVideoSelectViewController.h"
#import "UpgradeViewController.h"
#import "SudokuViewController.h"
#import "AddImageEditViewController.h"
//static const CGFloat kPortraitCircleMaskRectInnerEdgeInset = 15.0f;
static const CGFloat kPortraitSquareMaskRectInnerEdgeInset = 20.0f;
static const CGFloat kPortraitMoveAndScaleLabelVerticalMargin = 64.0f;
static const CGFloat kPortraitCancelAndChooseButtonsHorizontalMargin = 13.0f;
static const CGFloat kPortraitCancelAndChooseButtonsVerticalMargin = 21.0f;

//static const CGFloat kLandscapeCircleMaskRectInnerEdgeInset = 45.0f;
static const CGFloat kLandscapeSquareMaskRectInnerEdgeInset = 45.0f;
static const CGFloat kLandscapeMoveAndScaleLabelVerticalMargin = 12.0f;
static const CGFloat kLandscapeCancelAndChooseButtonsVerticalMargin = 12.0f;

static const CGFloat kResetAnimationDuration = 0.4;
static const CGFloat kLayoutImageScrollViewAnimationDuration = 0.25;
#define layoutcount 7
#define buttonWidthNor 50
@interface HomeImageCropViewController () <UIGestureRecognizerDelegate,UIActionSheetDelegate,ILColorPickerDualExampleControllerDelegate,UpgradeViewControllerDelegate>
{
    int gridCount;
    int ** listarray[layoutcount];
    int rowcount[layoutcount];
    UIColor * selectColor;
    int selectIndex;

    ILColorPickerDualExampleController * ilColorPicker;
    UILabel *slideLable;
    float slideWth;
    UpgradeViewController * objUpgradeViewController;
}
@property (strong, nonatomic) UIColor *originalNavigationControllerViewBackgroundColor;
@property (assign, nonatomic) BOOL originalNavigationControllerNavigationBarHidden;
@property (assign, nonatomic) BOOL originalStatusBarHidden;

@property (strong, nonatomic) RSKImageScrollView *imageScrollView;
@property (strong, nonatomic) RSKTouchView *overlayView;
@property (strong, nonatomic) CAShapeLayer *maskLayer;
@property (assign, nonatomic) CGRect maskRect;
@property (strong, nonatomic) UIBezierPath *maskPath;
@property (strong, nonatomic) UILabel *moveAndScaleLabel;
@property (strong, nonatomic) UIButton *cancelButton;
@property (strong, nonatomic) UIButton *chooseButton;
@property (nonatomic) UIActionSheet *actionSheet;
@property (strong, nonatomic) UIButton *btnBack;
@property (strong, nonatomic) UIButton *btnNext;
@property (strong, nonatomic) UIButton *btnGrid;
@property (strong, nonatomic) UIButton *btnFrame;
@property (strong, nonatomic) UIScrollView *layoutScrollView;
@property (strong, nonatomic) UIScrollView *colorScrollView;
@property (strong, nonatomic) UIImageView *viewimg;
@property (strong, nonatomic) UITapGestureRecognizer *doubleTapGestureRecognizer;
@property (strong, nonatomic) UIRotationGestureRecognizer *rotationGestureRecognizer;
@property (strong, nonatomic) UIImage * gardientImage;
@property (assign, nonatomic) BOOL didSetupConstraints;
@property (strong, nonatomic) NSLayoutConstraint *moveAndScaleLabelTopConstraint;
@property (strong, nonatomic) NSLayoutConstraint *cancelButtonBottomConstraint;
@property (strong, nonatomic) NSLayoutConstraint *chooseButtonBottomConstraint;

@end

@implementation HomeImageCropViewController

#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        _avoidEmptySpaceAroundImage = NO;
        _applyMaskToCroppedImage = NO;
        _rotationEnabled = NO;
        _cropMode = RSKImageCropModeSquare;
    }
    return self;
}

- (instancetype)initWithImage:(UIImage *)originalImage
{
    self = [self init];
    if (self) {
        _originalImage = originalImage;
    }
    return self;
}

- (instancetype)initWithImage:(UIImage *)originalImage cropMode:(RSKImageCropMode)cropMode
{
    self = [self initWithImage:originalImage];
    if (self) {
        _cropMode = cropMode;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    /*
     if (![[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"])
     {
     ObjAdViewController = [[AdViewController alloc] initWithNibName:@"AdViewController" bundle:nil];
     ObjAdViewController.delegate = self;
     [self presentViewController:ObjAdViewController animated:YES completion:nil];
     }
     */
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    int wh = (self.view.frame.size.width-45*4-16)/3;
    //返回按钮
    self.btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnBack setFrame:CGRectMake(10, 15+KDNavH, 35, 35)];
    [self.btnBack setImage:[UIImage imageNamed:@"backBlack_icn"] forState:UIControlStateNormal];
    //[self.btnBack setImage:[UIImage imageNamed:@"back_sel"] forState:UIControlStateHighlighted];
    self.btnBack.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self.btnBack addTarget:self action:@selector(cancelCrop) forControlEvents:UIControlEventTouchUpInside];
    //图片尺寸大小选择
    self.btnFrame = [[UIButton alloc]init];
    [self.btnFrame setFrame:CGRectMake(self.btnBack.frame.origin.x+self.btnBack.frame.size.width+wh,(IPHONEX_SERIES ? 30 : 20),45,45)];
    [self.btnFrame setImage:[UIImage imageNamed:@"aspectNew"] forState:UIControlStateNormal];
    self.btnFrame.hidden = YES;
    [self.btnFrame addTarget:self action:@selector(constrain:) forControlEvents:UIControlEventTouchUpInside];
    
    self.btnGrid = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnGrid setFrame:CGRectMake(self.btnFrame.frame.origin.x+self.btnFrame.frame.size.width+wh, (IPHONEX_SERIES ? 30 : 20),45,45)];
    UIImage *rands = [UIImage imageNamed:@"randNew"];
    [self.btnGrid setImage:rands forState:UIControlStateNormal];
    // [self.btnGrid setImage:[UIImage imageNamed:@"random_sel"] forState:UIControlStateHighlighted];
    self.btnGrid.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    self.btnGrid.hidden = YES;
    [self.btnGrid addTarget:self action:@selector(gridChange) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnNext setFrame:CGRectMake(kDeviceWidth - 45, 15+KDNavH, 35, 35)];
    [self.btnNext setImage:[UIImage imageNamed:@"nextBlack_icn"] forState:UIControlStateNormal];
    //[self.btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
    self.btnNext.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [self.btnNext addTarget:self action:@selector(cropImage) forControlEvents:UIControlEventTouchUpInside];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.clipsToBounds = YES;
    [self.imageScrollView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.imageScrollView];
    [self.view addSubview:self.overlayView];
    
//    [self.view addSubview:self.cancelButton];
//    [self.view addSubview:self.chooseButton];
    
    [self.view addGestureRecognizer:self.doubleTapGestureRecognizer];
    [self.view addGestureRecognizer:self.rotationGestureRecognizer];
    CGRect frameRect;
    
    frameRect = CGRectMake(0, (WINDOW.bounds.size.height-WINDOW.bounds.size.width)*0.5f, WINDOW.bounds.size.width, WINDOW.bounds.size.width);
    self.viewimg =[[UIImageView alloc] initWithFrame:frameRect];
    self.viewimg.image = [self getImageByGridCount:3 frame:self.viewimg.frame] ;
    self.viewimg.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    
    [self.view addSubview:self.viewimg];
    [self.view addSubview:self.btnBack];
    [self.view addSubview:self.btnGrid];
    [self.view addSubview:self.btnNext];
    [self.view addSubview:self.btnFrame];
    [self initListCount];
    [self setupTwoScrollView:buttonWidthNor];
    if (self.needAutofix) {
        [self performSelector:@selector(autofix) withObject:nil afterDelay:0.5];
    }
}

-(void)initListCount
{
    int a0[1][2] = {{1,1}};
    [self setNewListValue:a0 rowCount:2 index:0];
    int a1[1][3] = {{1,1,1}};
    [self setNewListValue:a1 rowCount:3 index:1];
    
    int a2[1][4] = {{1,1,1,1}};
    [self setNewListValue:a2 rowCount:4 index:2];
    
    int a3[1][5] = {{1,1,1,1,1}};
    [self setNewListValue:a3 rowCount:5 index:3];
    
    int a4[1][6] = {{1,1,1,1,1,1}};
    [self setNewListValue:a4 rowCount:6 index:4];
    
    int a5[1][7] = {{1,1,1,1,1,1,1}};
    [self setNewListValue:a5 rowCount:7 index:5];
    
    int a6[1][8] = {{1,1,1,1,1,1,1,1}};
    [self setNewListValue:a6 rowCount:8 index:6];
    
//    int a7[7][3] = {{1,1,1},{1,1,1},{1,1,1},{1,1,1},{1,1,1},{1,1,1},{1,1,1}};
//    [self setListValue:a7 rowCount:7 index:6];
//
//    int a8[3][3] = {{1,0,1},
//        {0,1,0},
//        {1,0,1}};
//    [self setListValue:a8 rowCount:3 index:7];
//
//    int a11[1][3] = {{1,0,1}};
//    [self setListValue:a11 rowCount:1 index:8];
//
//    int a12[1][3] = {{0,0,1}};
//    [self setListValue:a12 rowCount:1 index:9];
//
//    int a13[1][3] = {{0,1,1}};
//    [self setListValue:a13 rowCount:1 index:10];
//
//    int a14[1][3] = {{1,1,0}};
//    [self setListValue:a14 rowCount:1 index:11];
//    int a15[1][3] = {{1,0,0}};
//    [self setListValue:a15 rowCount:1 index:12];
//
//    int a16[2][3] = {{0,1,1},
//        {0,1,1}};
//    [self setListValue:a16 rowCount:2 index:13];
//
//    int a17[2][3] = {{1,1,0},
//        {1,1,0}};
//    [self setListValue:a17 rowCount:2 index:14];
//
//    int a18[2][3] = {{1,0,1},
//        {1,0,1}};
//    [self setListValue:a18 rowCount:2 index:15];
//
//    int a19[2][3] = {{0,1,0},
//        {1,0,1}};
//    [self setListValue:a19 rowCount:2 index:16];
//
//    int a110[2][3] = {{1,0,1},
//        {0,1,0}};
//    [self setListValue:a110 rowCount:2 index:17];
//
//    int a31[3][3] = {{0,0,0},
//        {1,1,1},
//        {0,0,0}};
//    [self setListValue:a31 rowCount:3 index:18];
//
//    int a32[3][3] = {{0,0,0},
//        {1,1,1},
//        {1,1,1}};
//    [self setListValue:a32 rowCount:3 index:19];
//
//    int a33[3][3] = {{1,1,1},
//        {0,0,0},
//        {1,1,1}};
//    [self setListValue:a33 rowCount:3 index:20];
//
//    int a34[3][3] = {{1,1,1},
//        {0,0,0},
//        {0,0,0}};
//    [self setListValue:a34 rowCount:3 index:21];
//
//    int a35[3][3] = {{1,1,1},
//        {1,1,1},
//        {0,0,0}};
//    [self setListValue:a35 rowCount:3 index:22];
//
//    int a36[3][3] = {{1,1,0},
//        {1,1,0},
//        {1,1,0}};
//    [self setListValue:a36 rowCount:3 index:23];
//
//    int a37[3][3] = {{1,0,0},
//        {1,0,0},
//        {1,0,0}};
//    [self setListValue:a37 rowCount:3 index:24];
//
//    int a38[3][3] = {{1,0,1},
//        {1,0,1},
//        {1,0,1}};
//    [self setListValue:a38 rowCount:3 index:25];
//
//    int a39[3][3] = {{0,1,1},
//        {0,1,1},
//        {0,1,1}};
//    [self setListValue:a39 rowCount:3 index:26];
//
//    int a310[3][3] = {{0,1,0},
//        {0,1,0},
//        {0,1,0}};
//    [self setListValue:a310 rowCount:3 index:27];
//
//    int a311[3][3] = {{0,1,0},
//        {1,0,1},
//        {0,1,0}};
//    [self setListValue:a311 rowCount:3 index:28];
//
//    int a312[3][3] = {{0,0,0},
//        {0,1,0},
//        {0,0,0}};
//    [self setListValue:a312 rowCount:3 index:29];
//
//    int a313[3][3] = {{1,1,1},
//        {1,0,1},
//        {1,1,1}};
//    [self setListValue:a313 rowCount:3 index:30];
//
//    int a314[3][3] = {{0,0,1},
//        {0,1,0},
//        {1,0,0}};
//    [self setListValue:a314 rowCount:3 index:31];
//
//    int a315[3][3] = {{1,0,0},
//        {0,1,0},
//        {0,0,1}};
//    [self setListValue:a315 rowCount:3 index:32];
//
//    int a316[3][3] = {{1,1,0},
//        {1,1,0},
//        {0,0,0}};
//    [self setListValue:a316 rowCount:3 index:33];
//
//    int a317[3][3] = {{0,1,1},
//        {0,1,1},
//        {0,0,0}};
//    [self setListValue:a317 rowCount:3 index:34];
//
//    int a318[3][3] = {{0,0,0},
//        {0,1,1},
//        {0,1,1}};
//    [self setListValue:a318 rowCount:3 index:35];
//
//    int a319[3][3] = {{0,0,0},
//        {1,1,0},
//        {1,1,0}};
//    [self setListValue:a319 rowCount:3 index:36];
//
//    int a320[3][3] = {{1,0,1},
//        {0,1,0},
//        {1,0,1}};
//    [self setListValue:a320 rowCount:3 index:37];
//
//    int a321[3][3] = {{1,0,1},
//        {0,0,0},
//        {1,0,1}};
//    [self setListValue:a321 rowCount:3 index:38];
//
//    int a41[4][3] = {{1,1,1},{0,0,0},{0,0,0},{0,0,0}};
//    [self setListValue:a41 rowCount:4 index:39];
//
//    int a42[4][3] = {{1,1,1},{1,1,1},{0,0,0},{0,0,0}};
//    [self setListValue:a42 rowCount:4 index:40];
//
//    int a43[4][3] = {{0,0,0},{1,1,1},{1,1,1},{0,0,0}};
//    [self setListValue:a43 rowCount:4 index:41];
//
//    int a44[4][3] = {{0,0,0},{0,0,0},{1,1,1},{1,1,1}};
//    [self setListValue:a44 rowCount:4 index:42];
//
//    int a45[4][3] = {{1,0,1},{0,1,0},{0,1,0},{1,0,1}};
//    [self setListValue:a45 rowCount:4 index:43];
//
//    int a46[4][3] = {{1,1,0},{1,1,0},{1,1,0},{1,1,0}};
//    [self setListValue:a46 rowCount:4 index:44];
//
//    int a47[4][3] = {{1,1,1},{1,0,1},{1,0,1},{1,1,1}};
//    [self setListValue:a47 rowCount:4 index:45];
//
//    int a48[4][3] = {{0,1,1},{0,1,1},{0,1,1},{0,0,0}};
//    [self setListValue:a48 rowCount:4 index:46];
//
//    int a51[5][3] = {{1,1,1},{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
//    [self setListValue:a51 rowCount:5 index:47];
//
//    int a52[5][3] = {{1,1,1},{1,1,1},{0,0,0},{0,0,0},{0,0,0}};
//    [self setListValue:a52 rowCount:5 index:48];
//
//    int a53[5][3] = {{0,0,0},{1,1,1},{1,1,1},{0,0,0},{0,0,0}};
//    [self setListValue:a53 rowCount:5 index:49];
//
//    int a54[5][3] = {{0,0,0},{0,0,0},{1,1,1},{1,1,1},{0,0,0}};
//    [self setListValue:a54 rowCount:5 index:50];
//
//    int a55[5][3] = {{1,0,1},{0,1,0},{0,1,0},{0,1,0},{1,0,1}};
//    [self setListValue:a55 rowCount:5 index:51];
//
//    int a56[5][3] = {{1,1,0},{1,1,0},{1,1,0},{1,1,0},{1,1,0}};
//    [self setListValue:a56 rowCount:5 index:52];
//
//    int a57[5][3] = {{1,1,1},{1,0,1},{1,0,1},{1,0,1},{1,1,1}};
//    [self setListValue:a57 rowCount:5 index:53];
//
//    int a58[5][3] = {{0,1,1},{0,1,1},{0,1,1},{0,1,1},{0,0,0}};
//    [self setListValue:a58 rowCount:5 index:54];
//
//    int a59[5][3] = {{0,0,0},{0,0,0},{0,0,0},{1,1,1},{1,1,1}};
//    [self setListValue:a59 rowCount:5 index:55];
//
//    int a61[6][3] = {{1,1,1},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
//    [self setListValue:a61 rowCount:6 index:56];
//
//    int a62[6][3] = {{1,1,1},{1,1,1},{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
//    [self setListValue:a62 rowCount:6 index:57];
//
//    int a63[6][3] = {{0,0,0},{1,1,1},{1,1,1},{0,0,0},{0,0,0},{0,0,0}};
//    [self setListValue:a63 rowCount:6 index:58];
//
//    int a64[6][3] = {{0,0,0},{0,0,0},{1,1,1},{1,1,1},{0,0,0},{0,0,0}};
//    [self setListValue:a64 rowCount:6 index:59];
//
//    int a65[6][3] = {{1,0,1},{0,1,0},{0,1,0},{0,1,0},{0,1,0},{1,0,1}};
//    [self setListValue:a65 rowCount:6 index:60];
//
//    int a66[6][3] = {{1,1,0},{1,1,0},{1,1,0},{1,1,0},{1,1,0},{1,1,0}};
//    [self setListValue:a66 rowCount:6 index:61];
//
//    int a67[6][3] = {{1,1,1},{1,0,1},{1,0,1},{1,0,1},{1,0,1},{1,1,1}};
//    [self setListValue:a67 rowCount:6 index:62];
//
//    int a68[6][3] = {{0,1,1},{0,1,1},{0,1,1},{0,1,1},{0,1,1},{0,0,0}};
//    [self setListValue:a68 rowCount:6 index:63];
//
//    int a69[6][3] = {{0,0,0},{0,0,0},{0,0,0},{1,1,1},{1,1,1},{0,0,0}};
//    [self setListValue:a69 rowCount:6 index:64];
//
//    int a610[6][3] = {{0,0,0},{0,0,0},{0,0,0},{1,1,1},{1,1,1},{1,1,1}};
//    [self setListValue:a610 rowCount:6 index:65];
//
//    int a71[7][3] = {{1,1,1},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
//    [self setListValue:a71 rowCount:7 index:66];
//
//    int a72[7][3] = {{1,1,1},{1,1,1},{0,0,0},{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
//    [self setListValue:a72 rowCount:7 index:67];
//
//    int a73[7][3] = {{0,0,0},{1,1,1},{1,1,1},{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
//    [self setListValue:a73 rowCount:7 index:68];
//
//    int a74[7][3] = {{0,0,0},{0,0,0},{1,1,1},{1,1,1},{0,0,0},{0,0,0},{0,0,0}};
//    [self setListValue:a74 rowCount:7 index:69];
//
//    int a75[7][3] = {{1,0,1},{0,1,0},{0,1,0},{0,1,0},{0,1,0},{0,1,0},{1,0,1}};
//    [self setListValue:a75 rowCount:7 index:70];
//
//    int a76[7][3] = {{1,1,0},{1,1,0},{1,1,0},{1,1,0},{1,1,0},{1,1,0},{1,1,0}};
//    [self setListValue:a76 rowCount:7 index:71];
//
//    int a77[7][3] = {{1,1,1},{1,0,1},{1,0,1},{1,0,1},{1,0,1},{1,0,1},{1,1,1}};
//    [self setListValue:a77 rowCount:7 index:72];
//
//    int a78[7][3] = {{0,1,1},{0,1,1},{0,1,1},{0,1,1},{0,1,1},{0,1,1},{0,0,0}};
//    [self setListValue:a78 rowCount:7 index:73];
//
//    int a79[7][3] = {{0,0,0},{0,0,0},{0,0,0},{1,1,1},{1,1,1},{0,0,0},{0,0,0}};
//    [self setListValue:a79 rowCount:7 index:74];
//
//    int a710[7][3] = {{0,0,0},{0,0,0},{0,0,0},{1,1,1},{1,1,1},{1,1,1},{0,0,0}};
//    [self setListValue:a710 rowCount:7 index:75];
//
//    int a711[7][3] = {{0,0,0},{0,0,0},{0,0,0},{0,0,0},{1,1,1},{1,1,1},{1,1,1}};
//    [self setListValue:a711 rowCount:7 index:76];
}

-(void)setListValue:(int[][3])a rowCount:(int)rowCount index:(int)index
{
    int **array;
    array=(int **)malloc(rowCount*sizeof(int*));
    if(array==NULL)//分配失败
    {
        //printf("内存分配失败!\n");
        exit(0);
    }
    for(int i=0;i<rowCount;i++)
    {
        array[i]=(int *)malloc(3*sizeof(int));
        if(array[i]==NULL)//分配失败
        {
            //printf("内存分配失败!\n");
            for(int j=0;j<i;j++)
                free(array[j]);
            free(array);
            exit(0);
        }
    }
    for (int i = 0; i < rowCount; i++) {
        for (int j = 0; j < 3; j++) {
            array[i][j] = a[i][j];
        }
    }
    
    listarray[index] = array;
    //NSLog(@"----------%@",listarray[index]);
    rowcount[index] = rowCount;
    //NSLog(@"**********%@",listarray[index]);
}
-(void)setNewListValue:(int[1][10])a rowCount:(int)rowCount index:(int)index
{
    int **array;
    array=(int **)malloc(sizeof(int*));
    if(array==NULL)//分配失败
    {
        //printf("内存分配失败!\n");
        exit(0);
    }
        array[0]=(int *)malloc(rowCount*sizeof(int));
        if(array[0]==NULL)//分配失败
        {
            //printf("内存分配失败!\n");
            for(int j=0;j<1;j++)
                free(array[j]);
            free(array);
            exit(0);
        }
        for (int j = 0; j < rowCount; j++) {
            array[0][j] = a[0][j];
    }
    
    listarray[index] = array;
    
    rowcount[index] = rowCount;
}
-(void)setupTwoScrollView:(CGFloat)butWidth{
    if (!self.layoutScrollView) {
        self.layoutScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    }
    if (!self.colorScrollView) {
        self.colorScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    }
    int split_width = 5;
    CGFloat mulitper = 1.5;
    self.layoutScrollView.frame = CGRectMake(split_width,WINDOW.bounds.size.height-butWidth*mulitper+5-(IPHONEX_SERIES ? 30 : 0), WINDOW.bounds.size.width-2*split_width, butWidth*mulitper-5);
    self.layoutScrollView.backgroundColor = [UIColor clearColor];
    self.layoutScrollView.userInteractionEnabled = YES;
    self.layoutScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.layoutScrollView];
    
    UIView * lineView = [[UIView alloc] initWithFrame:CGRectMake(0, self.layoutScrollView.frame.origin.y-5, WINDOW.bounds.size.width, 1)];
    lineView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:lineView];
    
    //    UILabel * alphaLabel = [[UILabel alloc] init];
    //    alphaLabel.frame = CGRectMake(split_width, lineView.frame.origin.y-35, 55, 35);
    //    alphaLabel.text = @"Alpha";
    //    alphaLabel.textColor = [UIColor whiteColor];
    //    alphaLabel.tag = 108887;
    //    [self.view addSubview:alphaLabel];
    //    alphaLabel.hidden = YES;
    //    CGFloat orgx = alphaLabel.frame.origin.x+alphaLabel.frame.size.width;
    
    //---------滑动条
    UISlider * alphaSlider = [[UISlider alloc] initWithFrame:CGRectMake(split_width, lineView.frame.origin.y-35, WINDOW.bounds.size.width/2-split_width*2, 35)];
    slideWth =  WINDOW.bounds.size.width/2-split_width*2;
    alphaSlider.minimumValue = 0;
    alphaSlider.maximumValue = 1;
    alphaSlider.value = 0.6;
    alphaSlider.tag = 108888;
    [alphaSlider addTarget:self action:@selector(alphaChange:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:alphaSlider];
    alphaSlider.hidden = YES;
    //--------修改添加按钮上的百分比
    slideLable = [[UILabel alloc]initWithFrame:CGRectMake(split_width+slideWth*(float)alphaSlider.value+20/2-35*(float)alphaSlider.value,lineView.frame.origin.y-55, 40,23)];
    slideLable.text = @"60%";
    slideLable.font = [UIFont systemFontOfSize:12];
    slideLable.textColor = [UIColor whiteColor];
    slideLable.hidden = YES;
    [self.view addSubview:slideLable];
    //--------颜色按钮 (注意butwith的高度可变 现在是40)
    self.colorScrollView.frame = CGRectMake(WINDOW.bounds.size.width/2,lineView.frame.origin.y-40, WINDOW.bounds.size.width/2, butWidth);
    self.colorScrollView.backgroundColor = [UIColor clearColor];
    self.colorScrollView.userInteractionEnabled = YES;
    self.colorScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.colorScrollView];
    self.colorScrollView.hidden = YES;
    NSArray* colorList=@[
                         [UIColor colorWithRed:1.0 green: 1.0 blue:1.0 alpha: 1.0],
                         [UIColor blackColor],
                         [UIColor colorWithRed:204.0/255.0 green: 208.0/255.0 blue: 217.0/255.0 alpha: 1.0],
                         [UIColor colorWithRed:101.0/255.0 green: 109.0/255.0 blue: 120.0/255.0 alpha: 1.0],
                         [UIColor colorWithRed:236.0/255.0 green: 136/255.0 blue: 192.0/255.0 alpha: 1.0],
                         [UIColor colorWithRed:172.0/255.0 green: 146.0/255.0 blue: 237.0/255.0 alpha: 1.0],
                         [UIColor colorWithRed:93.0/255.0 green: 155.0/255.0 blue: 236.0/255.0 alpha: 1.0],
                         [UIColor colorWithRed:79.0/255.0 green: 192.0/255.0 blue: 232.0/255.0 alpha: 1.0],
                         [UIColor colorWithRed:72.0/255.0 green: 207.0/255.0 blue: 174.0/255.0 alpha: 1.0],
                         [UIColor colorWithRed:160.0/255.0 green: 212.0/255.0 blue: 104.0/255.0 alpha: 1.0],
                         [UIColor colorWithRed:253.0/255.0 green: 207.0/255.0 blue: 85.0/255.0 alpha: 1.0],
                         [UIColor colorWithRed:251.0/255.0 green: 110.0/255.0 blue: 82.0/255.0 alpha: 1.0],
                         [UIColor colorWithRed:237.0/255.0 green: 85.0/255.0 blue: 100.0/255.0 alpha: 1.0],
                         [UIColor redColor],
                         [UIColor greenColor],
                         [UIColor blueColor],
                         [UIColor cyanColor],
                         [UIColor yellowColor],
                         [UIColor magentaColor],
                         [UIColor orangeColor],
                         [UIColor purpleColor],
                         [UIColor brownColor],
                         ];
    // selectColor = colorList[0];
    // Variables for setting the Color Buttons
    CGFloat xCoord  = split_width;
    CGFloat yCoord = split_width;
    CGFloat buttonWidth = butWidth-10;
    CGFloat buttonHeight = butWidth-10;
    CGFloat gapBetweenButtons = 15;
    xCoord += gapBetweenButtons;
    // Counter for Fonts
    int colorsCount = 0;
    UIButton * colorButt;
    int colorTag = 0;
    selectColor = colorList[10];
    for (colorsCount = 0; colorsCount <= colorList.count; ++colorsCount) {
        colorTag = colorsCount;
        
        // Create a Button for each Color ==========
        colorButt = [UIButton buttonWithType:UIButtonTypeCustom];
        colorButt.frame = CGRectMake(xCoord, yCoord, buttonWidth, buttonHeight);
        
        if (colorsCount == 0) {
            colorButt.backgroundColor = [UIColor clearColor];
            [colorButt setImage:[UIImage imageNamed:@"colorpicker"] forState:UIControlStateNormal];
            colorButt.tag = 9999;  // Assign a tag to each button
        }else
        {
            colorButt.backgroundColor = colorList[colorsCount-1];
            colorButt.tag = colorTag-1;  // Assign a tag to each button
            
        }
        
        
        // if (!selectColor) {
        //     selectColor = colorButt.backgroundColor;
        // }
        colorButt.layer.cornerRadius = colorButt.frame.size.width/2;
        colorButt.showsTouchWhenHighlighted = NO;
        [colorButt addTarget:self action:@selector(colorButtTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        xCoord +=  buttonWidth + gapBetweenButtons;
        [self.colorScrollView addSubview:colorButt ];
        if (colorsCount > 0) {
            if (selectColor == colorList[colorsCount-1]) {
                colorButt.transform = CGAffineTransformMakeScale(0.5, 0.5);
            }
        }else
        {
            xCoord +=  gapBetweenButtons;
        }
        
    } // END LOOP ================================
    
    // Place Buttons into the ScrollView =====
    self.colorScrollView.contentSize = CGSizeMake((buttonWidth+gapBetweenButtons) * (CGFloat)colorsCount+10+2*gapBetweenButtons, yCoord);
    self.colorScrollView.backgroundColor = [UIColor clearColor];
    buttonWidth *= mulitper;
    buttonHeight *= mulitper;
    colorsCount = 0;
    colorTag = 0;
    xCoord  = split_width;
    yCoord = split_width;
    gapBetweenButtons = 10;
    
    // Loop for creating buttons ========
    NSString*str = [[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY];
    
    if (str&&[str isEqualToString:@"YES"])
    {

    }
    int ccount = 0;
    for (colorsCount = 0; colorsCount < layoutcount; ++colorsCount) {
        colorTag = colorsCount;
        
        // Create a Button for each Color ==========
        colorButt = [UIButton buttonWithType:UIButtonTypeCustom];
        colorButt.frame = CGRectMake(xCoord, yCoord, buttonWidth, buttonHeight);
        colorButt.tag = colorTag;  // Assign a tag to each button
        // colorButt.backgroundColor = colorList[colorsCount];
        // if (!selectColor) {
        //     selectColor = colorButt.backgroundColor;
        // }
        int ** array11 = listarray[colorsCount];
        int rcount = rowcount[colorsCount];
        if (rcount == 3) {
            ccount++;
        }else
        {
            ccount = 0;
        }
        UIImage * bgimage ;

        bgimage = [ImageHelper gerGridHomeImage:array11 rowCount:rowcount[colorsCount] splitLen:5 gridLen:25 heartImg:nil];
        [colorButt addTarget:self action:@selector(layoutButtTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [colorButt setBackgroundImage:bgimage forState:UIControlStateNormal];
        // colorButt.layer.cornerRadius = colorButt.frame.size.width/2;
        colorButt.showsTouchWhenHighlighted = NO;
        
        
        xCoord +=  buttonWidth + gapBetweenButtons;
        [self.layoutScrollView addSubview:colorButt ];
    } // END LOOP ================================
    
    // Place Buttons into the ScrollView =====
    self.layoutScrollView.contentSize = CGSizeMake((buttonWidth+gapBetweenButtons) * (CGFloat)colorsCount+10, yCoord);
    self.layoutScrollView.backgroundColor = [UIColor clearColor];
}

-(void)filterLayoutButton:(int)rowCount butWidth:(CGFloat)butWidth
{
    int split_width = 5;
    CGFloat mulitper = 1.5;
    CGFloat xCoord  = split_width;
    CGFloat yCoord = split_width;
    CGFloat buttonWidth = butWidth-10;
    CGFloat buttonHeight = butWidth-10;
    CGFloat gapBetweenButtons = 15;
    buttonWidth *= mulitper;
    buttonHeight *= mulitper;
    int colorsCount = 0;
    int colorTag = 0;
    xCoord  = split_width;
    yCoord = split_width;
    gapBetweenButtons = 10;
    [self.layoutScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    // Loop for creating buttons ========

    int ccount = 0;
    UIButton * colorButt;
    for (colorsCount = 0; colorsCount < layoutcount; ++colorsCount) {
        int crowcount = rowcount[colorsCount];
        if (rowCount!= -1 && crowcount != rowCount) {
            continue;
        }
        colorTag = colorsCount;
        
        // Create a Button for each Color ==========
        colorButt = [UIButton buttonWithType:UIButtonTypeCustom];
        colorButt.frame = CGRectMake(xCoord, yCoord, buttonWidth, buttonHeight);
        colorButt.tag = colorTag;  // Assign a tag to each button
        // colorButt.backgroundColor = colorList[colorsCount];
        // if (!selectColor) {
        //     selectColor = colorButt.backgroundColor;
        // }
        int ** array11 = listarray[colorsCount];
        
        if (crowcount == 3) {
            ccount++;
        }else
        {
            ccount = 0;
        }
        UIImage * bgimage ;

            bgimage = [ImageHelper gerGridHomeImage:array11 rowCount:rowcount[colorsCount] splitLen:5 gridLen:25 heartImg:nil];
            [colorButt addTarget:self action:@selector(layoutButtTapped:) forControlEvents:UIControlEventTouchUpInside];

        [colorButt setBackgroundImage:bgimage forState:UIControlStateNormal];
        // colorButt.layer.cornerRadius = colorButt.frame.size.width/2;
        colorButt.showsTouchWhenHighlighted = NO;
        
        
        xCoord +=  buttonWidth + gapBetweenButtons;
        [self.layoutScrollView addSubview:colorButt ];
    } // END LOOP ================================
    xCoord +=  buttonWidth + gapBetweenButtons;
    // Place Buttons into the ScrollView =====
    self.layoutScrollView.contentSize = CGSizeMake(xCoord+10, yCoord);
    self.layoutScrollView.backgroundColor = [UIColor clearColor];
}

-(IBAction)askforrateme:(id)sender
{
    NSString*str = [[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY];
    
    if ((str&&[str isEqualToString:@"YES"])) {
        
    }
    else{
        if([SKStoreReviewController respondsToSelector:@selector(requestReview)]) {
            [[UIApplication sharedApplication].keyWindow endEditing:YES];
            [SKStoreReviewController requestReview];
        }
    }
}



- (void)setSelectedColor:(UIColor *)color
{
    selectColor = color;
    self.gardientImage = nil;
    for (UIView * subView in self.colorScrollView.subviews) {
        subView.transform = CGAffineTransformIdentity;
    }
    [self selectColor:nil];
}

-(IBAction)colorButtTapped:(id)sender
{
    UIButton * but = sender;
    if (but&&but.tag != 9999) {
        selectColor = but.backgroundColor;
        self.gardientImage = nil;
        for (UIView * subView in self.colorScrollView.subviews) {
            subView.transform = CGAffineTransformIdentity;
        }
        but.transform = CGAffineTransformMakeScale(0.5, 0.5);
        [self selectColor:nil];
    }else if (but&&but.tag == 9999)
    {
        /*HRSampleColorPickerViewController2 *controller;
         controller = [[HRSampleColorPickerViewController2 alloc] initWithColor:selectColor fullColor:YES];
         controller.delegate = self;
         [self.navigationController pushViewController:controller
         animated:YES];*/
        if (!ilColorPicker) {
            ilColorPicker = [[ILColorPickerDualExampleController alloc] init];
            ilColorPicker.delegate = self;
        }
        
        ilColorPicker.initSize = self.viewimg.frame.size;
        [self.navigationController pushViewController:ilColorPicker
                                             animated:YES];
    }
    
    
}
- (void)enterStartcolor:(UIImage *)hairImage
{
    self.gardientImage = hairImage;
    [self selectColor:hairImage];
}
-(void)selectColor:(UIImage*)gadientColor
{
    if (!selectColor&&!gadientColor) {
        return;
    }
    int ** array11 = listarray[selectIndex];
    
    int indexc = 1;
    for (int i = 0; i < 1; i++) {
        for (int j = 0; j < gridCount; j++) {
            UIImageView * subView = [self.viewimg viewWithTag:indexc];
            if (subView) {
                if (array11[i][j] != 1) {
                    if (gadientColor) {
                        subView.image = [ImageHelper getSplitImageFromMask:gadientColor rowCount:gridCount indexx:j indexy:i];
                    }else
                    {
                        subView.backgroundColor = selectColor;
                        subView.image = nil;
                    }
                    
                }else
                {
                    subView.backgroundColor = [UIColor clearColor];
                    subView.image = nil;
                }
            }
            indexc++;
        }
    }
}

-(IBAction)alphaChange:(id)sender
{
    UISlider * slider =sender;
    CGFloat alpha = slider.value;
    slideLable.frame = CGRectMake(5+slideWth*(float)slider.value+20/2-35*(float)slider.value,self.layoutScrollView.frame.origin.y-5-55,40,25);
    slideLable.text=[NSString stringWithFormat:@"%d%%",(int)((float)slider.value*100)];
    for (UIView * subview in self.viewimg.subviews) {
        if (subview.tag > 0) {
            subview.alpha = alpha;
        }
    }
}

-(IBAction)layoutButtTapped:(id)sender
{
    UIButton * but = sender;
    for (UIView * subView in self.layoutScrollView.subviews) {
        subView.transform = CGAffineTransformIdentity;
    }
    but.transform = CGAffineTransformMakeScale(0.5, 0.5);
    int index = but.tag;
    selectIndex = index;
    int ** array11 = listarray[index];
    int rc = rowcount[index];
    BOOL hascolor = NO;
    for (int i = 0; i < 1; i++) {
        for (int j = 0; j < rc ; j++) {
            if (array11[i][j] != 1) {
                hascolor = YES;
                break;
            }
        }
    }
    self.colorScrollView.hidden = !hascolor;
    slideLable.hidden = !hascolor;
    UISlider * slider = [self.view viewWithTag:108888];
    if (slider) {
        slider.hidden = !hascolor;
    }
    UIView * sliderLabel = [self.view viewWithTag:108887];
    if (sliderLabel) {
        sliderLabel.hidden = !hascolor;
    }
    if (gridCount != rc) {
        [self changeGrid:rc];
    }
    [self updateColorView];
    if (self.gardientImage) {
        [self selectColor:self.gardientImage];
    }else
    {
        [self selectColor:nil];
    }
    
}

- (void)constrain:(id)sender
{
    self.actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:
                        NSLocalizedString(@"AutoFit", nil),
                        @"1 x 2",
                        @"1 x 3",
                        @"1 x 4",
                        @"1 x 5",
                        @"1 x 6",
                        @"1 x 7",
                        @"1 x 8",
                        NSLocalizedString(@"All", nil),nil];
    [self.actionSheet showFromToolbar:self.navigationController.toolbar];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.colorScrollView.hidden = YES;
    slideLable.hidden = YES;
    UISlider * slider = [self.view viewWithTag:108888];
    if (slider) {
        slider.hidden = YES;
    }
    UIView * sliderLabel = [self.view viewWithTag:108887];
    if (sliderLabel) {
        sliderLabel.hidden = YES;
    }
    if (buttonIndex == 0) {
        [self autofix];
        [self filterLayoutButton:-1 butWidth:buttonWidthNor];
    }else if(buttonIndex == 8){
        [self filterLayoutButton:-1 butWidth:buttonWidthNor];
    }else if(buttonIndex > 0 && buttonIndex < 8) {
        [self changeGrid:buttonIndex+1];
        [self filterLayoutButton:buttonIndex+1 butWidth:buttonWidthNor];
    }
}
#pragma mark --- 自适应大小(定义3格)
-(void)autofix
{
    [self changeGrid:3];
//    CGSize orgSize = self.originalImage.size;
//    int count = orgSize.height/(orgSize.width/3);
//    if (count <= 0) {
//        count = 1;
//    }
//    if (gridCount != count) {
//        [self changeGrid:count];
//    }
    
}
#pragma mark ------self.colorScrollView
-(void)changeGrid:(int)curgridcount
{
    gridCount = curgridcount;
    CGFloat h = WINDOW.bounds.size.width;
    h = WINDOW.bounds.size.width/gridCount;
    //减去35的高度
    CGFloat maxh = self.colorScrollView.frame.origin.y-35-self.btnBack.frame.origin.y-self.btnBack.frame.size.height-6;
    CGFloat orgy = self.btnBack.frame.origin.y+self.btnBack.frame.size.height+3;
    if (h > maxh) {//不存在
        CGFloat w = WINDOW.bounds.size.width*maxh/h;
        self.viewimg.frame = CGRectMake((WINDOW.bounds.size.width-w)*0.5f, orgy, w, maxh);
    }else
    {
        self.viewimg.frame = CGRectMake(0, orgy+(maxh-h)*0.5f, WINDOW.bounds.size.width, h);
    }
    
    self.viewimg.image = [self getImageByGridCount:gridCount frame:self.viewimg.frame] ;
    if (self.viewimg.subviews&&self.viewimg.subviews.count > 0) {
        [self.viewimg.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    [self updateMaskRect];
    [self updateMaskPath];
}

-(void)updateColorView
{
    if (self.viewimg.subviews&&self.viewimg.subviews.count > 0) {
        [self.viewimg.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    CGFloat subWidth = self.viewimg.frame.size.width/3;
    CGFloat subHeight = self.viewimg.frame.size.height/gridCount;
    int indexc = 1;
    UISlider * slider = [self.view viewWithTag:108888];
    CGFloat alpha = 0.6;
    if (slider) {
        alpha = slider.value;
    }
    for (int i = 0; i < gridCount; i++) {
        for (int j = 0; j < 3; j++) {
            UIImageView * subView = [[UIImageView alloc] initWithFrame:CGRectMake(j*subWidth, i*subHeight, subWidth, subHeight)];
            subView.backgroundColor = [UIColor clearColor];
            subView.alpha = alpha;
            subView.tag = indexc;
            [self.viewimg addSubview:subView];
            indexc++;
        }
    }
}

-(UIImage*)getImageByGridCount:(int)count frame:(CGRect)frame
{
    CGSize size = CGSizeMake(frame.size.width*2, frame.size.height*2);
    CGFloat colwidth = size.width/count;
    CGFloat colheight = colwidth;
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 2);  //线宽
    CGContextSetAllowsAntialiasing(context, true);
    CGContextSetRGBStrokeColor(context, 0.0 , 0.0, 0.0, 1.0);  //线的颜色
    CGContextBeginPath(context);
    for (int i = 1; i < count ; i++) {
        CGContextMoveToPoint(context, colwidth*i, 0);  //起点坐标
        CGContextAddLineToPoint(context, colwidth*i, size.height);   //终点坐标
    }
    for (int i = 1; i < 1; i++) {
        CGContextMoveToPoint(context, 0, colheight*i);  //起点坐标
        CGContextAddLineToPoint(context, size.width, colheight*i);   //终点坐标
    }
    CGContextStrokePath(context);
    UIImage *retImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //    UIImage * retImg = nil;
    //    if (count <= 3) {
    //        retImg = [UIImage imageNamed:[NSString stringWithFormat:@"Grid_%d",count*3]] ;
    //    }else
    //    {
    //        retImg = [UIImage imageNamed:@"Grid_9"];
    //        UIImage * gridTwo = [UIImage imageNamed:@"Grid_6"];
    //        CGSize retSize = CGSizeMake(retImg.size.width, retImg.size.width*count/3);
    //        UIGraphicsBeginImageContextWithOptions(retSize, NO, 0.0);
    //        [retImg drawInRect:CGRectMake(0, 0, retSize.width, retSize.width)];
    //        for (int i = 4; i <= count; i++) {
    //            [gridTwo drawInRect:CGRectMake(0, (2+i-4)*retSize.width/3, retSize.width, retSize.width*2/3)];
    //        }
    //        retImg = UIGraphicsGetImageFromCurrentImageContext();
    //        UIGraphicsEndImageContext();
    //    }
    return retImg;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.originalStatusBarHidden = [UIApplication sharedApplication].statusBarHidden;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    self.originalNavigationControllerNavigationBarHidden = self.navigationController.navigationBarHidden;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.originalNavigationControllerViewBackgroundColor = self.navigationController.view.backgroundColor;
    self.navigationController.view.backgroundColor = [UIColor blackColor];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:self.originalStatusBarHidden];
    [self.navigationController setNavigationBarHidden:self.originalNavigationControllerNavigationBarHidden animated:animated];
    self.navigationController.view.backgroundColor = self.originalNavigationControllerViewBackgroundColor;
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    [self updateMaskRect];
    [self layoutImageScrollView];
    [self layoutOverlayView];
    [self updateMaskPath];
    // [self.view setNeedsUpdateConstraints];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if (!self.imageScrollView.zoomView) {
        [self displayImage];
    }
}

//- (void)updateViewConstraints
//{
//    [super updateViewConstraints];
//
//    if (!self.didSetupConstraints) {
//        // ---------------------------
//        // The label "Move and Scale".
//        // ---------------------------
//
//        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self.moveAndScaleLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual
//                                                                         toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0f
//                                                                       constant:0.0f];
//        [self.view addConstraint:constraint];
//
//        CGFloat constant = kPortraitMoveAndScaleLabelVerticalMargin;
//        self.moveAndScaleLabelTopConstraint = [NSLayoutConstraint constraintWithItem:self.moveAndScaleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual
//                                                                              toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f
//                                                                            constant:constant];
//        [self.view addConstraint:self.moveAndScaleLabelTopConstraint];
//
//        // --------------------
//        // The button "Cancel".
//        // --------------------
//
//        constant = kPortraitCancelAndChooseButtonsHorizontalMargin;
//        constraint = [NSLayoutConstraint constraintWithItem:self.cancelButton attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual
//                                                     toItem:self.view attribute:NSLayoutAttributeLeft multiplier:1.0f
//                                                   constant:constant];
//        [self.view addConstraint:constraint];
//
//        constant = -kPortraitCancelAndChooseButtonsVerticalMargin;
//        self.cancelButtonBottomConstraint = [NSLayoutConstraint constraintWithItem:self.cancelButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual
//                                                                            toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0f
//                                                                          constant:constant];
//        [self.view addConstraint:self.cancelButtonBottomConstraint];
//
//        // --------------------
//        // The button "Choose".
//        // --------------------
//
//        constant = -kPortraitCancelAndChooseButtonsHorizontalMargin;
//        constraint = [NSLayoutConstraint constraintWithItem:self.chooseButton attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual
//                                                     toItem:self.view attribute:NSLayoutAttributeRight multiplier:1.0f
//                                                   constant:constant];
//        [self.view addConstraint:constraint];
//
//        constant = -kPortraitCancelAndChooseButtonsVerticalMargin;
//        self.chooseButtonBottomConstraint = [NSLayoutConstraint constraintWithItem:self.chooseButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual
//                                                                            toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0f
//                                                                          constant:constant];
//        [self.view addConstraint:self.chooseButtonBottomConstraint];
//
//        self.didSetupConstraints = YES;
//    } else {
//        if ([self isPortraitInterfaceOrientation]) {
//            self.moveAndScaleLabelTopConstraint.constant = kPortraitMoveAndScaleLabelVerticalMargin;
//            self.cancelButtonBottomConstraint.constant = -kPortraitCancelAndChooseButtonsVerticalMargin;
//            self.chooseButtonBottomConstraint.constant = -kPortraitCancelAndChooseButtonsVerticalMargin;
//        } else {
//            self.moveAndScaleLabelTopConstraint.constant = kLandscapeMoveAndScaleLabelVerticalMargin;
//            self.cancelButtonBottomConstraint.constant = -kLandscapeCancelAndChooseButtonsVerticalMargin;
//            self.chooseButtonBottomConstraint.constant = -kLandscapeCancelAndChooseButtonsVerticalMargin;
//        }
//    }
//}

#pragma mark - Custom Accessors

- (RSKImageScrollView *)imageScrollView
{
    if (!_imageScrollView) {
        _imageScrollView = [[RSKImageScrollView alloc] init];
        _imageScrollView.clipsToBounds = NO;
        _imageScrollView.aspectFill = self.avoidEmptySpaceAroundImage;
    }
    return _imageScrollView;
}

- (RSKTouchView *)overlayView
{
    if (!_overlayView)
    {
        _overlayView = [[RSKTouchView alloc] init];
        _overlayView.receiver = self.imageScrollView;
        [_overlayView.layer addSublayer:self.maskLayer];
    }
    return _overlayView;
}

- (CAShapeLayer *)maskLayer
{
    if (!_maskLayer) {
        _maskLayer = [CAShapeLayer layer];
        _maskLayer.fillRule = kCAFillRuleEvenOdd;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            _maskLayer.fillColor = [UIColor whiteColor].CGColor;//self.maskLayerColor.CGColor;
        }else
        {
            //----修改
            _maskLayer.fillColor = [UIColor whiteColor].CGColor;
            //_maskLayer.fillColor = [UIColor colorWithRed:130/255.0 green:92/255.0 blue:119/255.0 alpha:1].CGColor;
            // _maskLayer.fillColor = self.maskLayerColor.CGColor;
        }
    }
    return _maskLayer;
}

- (UIColor *)maskLayerColor
{
    if (!_maskLayerColor) {
        //        UIImage * img = [UIImage imageNamed:@"overlayBG"];
        //_maskLayerColor = [UIColor colorWithRed:130/255.0 green:92/255.0 blue:119/255.0 alpha:1];
        //[UIColor colorWithPatternImage:[ImageHelper image:img fillSize:self.view.frame.size]];
    }
    return _maskLayerColor;
}

- (UILabel *)moveAndScaleLabel
{
    if (!_moveAndScaleLabel) {
        _moveAndScaleLabel = [[UILabel alloc] init];
        _moveAndScaleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _moveAndScaleLabel.backgroundColor = [UIColor clearColor];
        _moveAndScaleLabel.text = NSLocalizedString(@"Move and Scale", @"Move and Scale label");
        _moveAndScaleLabel.textColor = [UIColor whiteColor];
        _moveAndScaleLabel.opaque = NO;
    }
    return _moveAndScaleLabel;
}

- (UIButton *)cancelButton
{
    if (!_cancelButton) {
        _cancelButton = [[UIButton alloc] init];
        _cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_cancelButton setTitle:NSLocalizedString(@"Cancel", @"Cancel button") forState:UIControlStateNormal];
        [_cancelButton addTarget:self action:@selector(onCancelButtonTouch:) forControlEvents:UIControlEventTouchUpInside];
        _cancelButton.opaque = NO;
    }
    return _cancelButton;
}

- (UIButton *)chooseButton
{
    if (!_chooseButton) {
        _chooseButton = [[UIButton alloc] init];
        _chooseButton.translatesAutoresizingMaskIntoConstraints = NO;
        [_chooseButton setTitle:NSLocalizedString(@"Choose", @"Choose button") forState:UIControlStateNormal];
        [_chooseButton addTarget:self action:@selector(onChooseButtonTouch:) forControlEvents:UIControlEventTouchUpInside];
        _chooseButton.opaque = NO;
    }
    return _chooseButton;
}

- (UITapGestureRecognizer *)doubleTapGestureRecognizer
{
    if (!_doubleTapGestureRecognizer) {
        _doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        _doubleTapGestureRecognizer.delaysTouchesEnded = NO;
        _doubleTapGestureRecognizer.numberOfTapsRequired = 2;
        _doubleTapGestureRecognizer.delegate = self;
    }
    return _doubleTapGestureRecognizer;
}

- (UIRotationGestureRecognizer *)rotationGestureRecognizer
{
    if (!_rotationGestureRecognizer) {
        _rotationGestureRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotation:)];
        _rotationGestureRecognizer.delaysTouchesEnded = NO;
        _rotationGestureRecognizer.delegate = self;
        _rotationGestureRecognizer.enabled = self.isRotationEnabled;
    }
    return _rotationGestureRecognizer;
}

- (CGRect)cropRect
{
    CGRect cropRect = CGRectZero;
    float zoomScale = 1.0 / self.imageScrollView.zoomScale;
    cropRect.origin.x = round(self.imageScrollView.contentOffset.x * zoomScale);
    cropRect.origin.y = round(self.imageScrollView.contentOffset.y * zoomScale);
    cropRect.size.width = CGRectGetWidth(self.imageScrollView.bounds) * zoomScale;
    cropRect.size.height = CGRectGetHeight(self.imageScrollView.bounds) * zoomScale;
    cropRect = CGRectIntegral(cropRect);
    return cropRect;
}

- (CGFloat)rotationAngle
{
    CGAffineTransform transform = self.imageScrollView.transform;
    CGFloat rotationAngle = atan2(transform.b, transform.a);
    return rotationAngle;
}

- (CGFloat)zoomScale
{
    return self.imageScrollView.zoomScale;
}

- (void)setAvoidEmptySpaceAroundImage:(BOOL)avoidEmptySpaceAroundImage
{
    if (_avoidEmptySpaceAroundImage != avoidEmptySpaceAroundImage) {
        _avoidEmptySpaceAroundImage = avoidEmptySpaceAroundImage;
        
        self.imageScrollView.aspectFill = avoidEmptySpaceAroundImage;
    }
}

- (void)setRotationEnabled:(BOOL)rotationEnabled
{
    if (_rotationEnabled != rotationEnabled) {
        _rotationEnabled = rotationEnabled;
        
        self.rotationGestureRecognizer.enabled = rotationEnabled;
    }
}

- (void)setOriginalImage:(UIImage *)originalImage
{
    if (![_originalImage isEqual:originalImage]) {
        _originalImage = originalImage;
        if (self.isViewLoaded) {
            [self displayImage];
        }
    }
}

- (void)setMaskPath:(UIBezierPath *)maskPath
{
    if (![_maskPath isEqual:maskPath]) {
        _maskPath = maskPath;
        
        UIBezierPath *clipPath = [UIBezierPath bezierPathWithRect:self.overlayView.frame];
        [clipPath appendPath:maskPath];
        clipPath.usesEvenOddFillRule = YES;
        
        CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"path"];
        pathAnimation.duration = [CATransaction animationDuration];
        pathAnimation.timingFunction = [CATransaction animationTimingFunction];
        [self.maskLayer addAnimation:pathAnimation forKey:@"path"];
        
        self.maskLayer.path = [clipPath CGPath];
    }
}

- (void)setRotationAngle:(CGFloat)rotationAngle
{
    if (self.rotationAngle != rotationAngle) {
        CGFloat rotation = (rotationAngle - self.rotationAngle);
        CGAffineTransform transform = CGAffineTransformRotate(self.imageScrollView.transform, rotation);
        self.imageScrollView.transform = transform;
    }
}

#pragma mark - Action handling

- (void)onCancelButtonTouch:(UIBarButtonItem *)sender
{
    [self cancelCrop];
}

- (void)onChooseButtonTouch:(UIBarButtonItem *)sender
{
    [self cropImage];
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)gestureRecognizer
{
    [self reset:YES];
}

- (void)handleRotation:(UIRotationGestureRecognizer *)gestureRecognizer
{
    [self setRotationAngle:(self.rotationAngle + gestureRecognizer.rotation)];
    gestureRecognizer.rotation = 0;
    
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        [UIView animateWithDuration:kLayoutImageScrollViewAnimationDuration
                              delay:0.0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             [self layoutImageScrollView];
                         }
                         completion:nil];
    }
}

#pragma mark - Private

- (BOOL)isPortraitInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation);
}

- (void)reset:(BOOL)animated
{
    if (animated) {
        [UIView beginAnimations:@"rsk_reset" context:NULL];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:kResetAnimationDuration];
        [UIView setAnimationBeginsFromCurrentState:YES];
    }
    
    [self resetRotation];
    [self resetFrame];
    [self resetZoomScale];
    [self resetContentOffset];
    
    if (animated) {
        [UIView commitAnimations];
    }
}

- (void)resetContentOffset
{
    CGSize boundsSize = self.imageScrollView.bounds.size;
    CGRect frameToCenter = self.imageScrollView.zoomView.frame;
    
    CGPoint contentOffset;
    if (CGRectGetWidth(frameToCenter) > boundsSize.width) {
        contentOffset.x = (CGRectGetWidth(frameToCenter) - boundsSize.width) * 0.5f;
    } else {
        contentOffset.x = 0;
    }
    if (CGRectGetHeight(frameToCenter) > boundsSize.height) {
        contentOffset.y = (CGRectGetHeight(frameToCenter) - boundsSize.height) * 0.5f;
    } else {
        contentOffset.y = 0;
    }
    
    self.imageScrollView.contentOffset = contentOffset;
}

- (void)resetFrame
{
    [self layoutImageScrollView];
}

- (void)resetRotation
{
    [self setRotationAngle:0.0];
}

- (void)resetZoomScale
{
    CGFloat zoomScale;
    if (CGRectGetWidth(self.view.bounds) > CGRectGetHeight(self.view.bounds)) {
        zoomScale = CGRectGetHeight(self.view.bounds) / self.originalImage.size.height;
    } else {
        zoomScale = CGRectGetWidth(self.view.bounds) / self.originalImage.size.width;
    }
    self.imageScrollView.zoomScale = zoomScale;
}

- (NSArray *)intersectionPointsOfLineSegment:(RSKLineSegment)lineSegment withRect:(CGRect)rect
{
    RSKLineSegment top = RSKLineSegmentMake(CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect)),
                                            CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect)));
    
    RSKLineSegment right = RSKLineSegmentMake(CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect)),
                                              CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect)));
    
    RSKLineSegment bottom = RSKLineSegmentMake(CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect)),
                                               CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect)));
    
    RSKLineSegment left = RSKLineSegmentMake(CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect)),
                                             CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect)));
    
    CGPoint p0 = RSKLineSegmentIntersection(top, lineSegment);
    CGPoint p1 = RSKLineSegmentIntersection(right, lineSegment);
    CGPoint p2 = RSKLineSegmentIntersection(bottom, lineSegment);
    CGPoint p3 = RSKLineSegmentIntersection(left, lineSegment);
    
    NSMutableArray *intersectionPoints = [@[] mutableCopy];
    if (!RSKPointIsNull(p0)) {
        [intersectionPoints addObject:[NSValue valueWithCGPoint:p0]];
    }
    if (!RSKPointIsNull(p1)) {
        [intersectionPoints addObject:[NSValue valueWithCGPoint:p1]];
    }
    if (!RSKPointIsNull(p2)) {
        [intersectionPoints addObject:[NSValue valueWithCGPoint:p2]];
    }
    if (!RSKPointIsNull(p3)) {
        [intersectionPoints addObject:[NSValue valueWithCGPoint:p3]];
    }
    
    return [intersectionPoints copy];
}

- (void)displayImage
{
    if (self.originalImage) {
        [self.imageScrollView displayImage:self.originalImage];
        [self reset:NO];
    }
}

- (void)layoutImageScrollView
{
    CGRect frame = CGRectZero;
    
    // The bounds of the image scroll view should always fill the mask area.
    switch (self.cropMode) {
        case RSKImageCropModeSquare: {
            if (self.rotationAngle == 0.0) {
                frame = self.maskRect;
            }
            else {
                // Step 1: Rotate the left edge of the initial rect of the image scroll view clockwise around the center by `rotationAngle`.
                CGRect initialRect = self.maskRect;
                CGFloat rotationAngle = self.rotationAngle;
                
                CGPoint leftTopPoint = CGPointMake(initialRect.origin.x, initialRect.origin.y);
                CGPoint leftBottomPoint = CGPointMake(initialRect.origin.x, initialRect.origin.y + initialRect.size.height);
                RSKLineSegment leftLineSegment = RSKLineSegmentMake(leftTopPoint, leftBottomPoint);
                
                CGPoint pivot = RSKRectCenterPoint(initialRect);
                
                CGFloat alpha = fabs(rotationAngle);
                RSKLineSegment rotatedLeftLineSegment = RSKLineSegmentRotateAroundPoint(leftLineSegment, pivot, alpha);
                
                // Step 2: Find the points of intersection of the rotated edge with the initial rect.
                NSArray *points = [self intersectionPointsOfLineSegment:rotatedLeftLineSegment withRect:initialRect];
                
                // Step 3: If the number of intersection points more than one
                // then the bounds of the rotated image scroll view does not completely fill the mask area.
                // Therefore, we need to update the frame of the image scroll view.
                // Otherwise, we can use the initial rect.
                if (points.count > 1) {
                    // We have a right triangle.
                    
                    // Step 4: Calculate the altitude of the right triangle.
                    if ((alpha > M_PI_2) && (alpha < M_PI)) {
                        alpha = alpha - M_PI_2;
                    } else if ((alpha > (M_PI + M_PI_2)) && (alpha < (M_PI + M_PI))) {
                        alpha = alpha - (M_PI + M_PI_2);
                    }
                    CGFloat sinAlpha = sin(alpha);
                    CGFloat cosAlpha = cos(alpha);
                    CGFloat hypotenuse = RSKPointDistance([points[0] CGPointValue], [points[1] CGPointValue]);
                    CGFloat altitude = hypotenuse * sinAlpha * cosAlpha;
                    
                    // Step 5: Calculate the target width.
                    CGFloat initialWidth = CGRectGetWidth(initialRect);
                    CGFloat targetWidth = initialWidth + altitude * 2;
                    
                    // Step 6: Calculate the target frame.
                    CGFloat scale = targetWidth / initialWidth;
                    CGPoint center = RSKRectCenterPoint(initialRect);
                    frame = RSKRectScaleAroundPoint(initialRect, center, scale, scale);
                    
                    // Step 7: Avoid floats.
                    frame.origin.x = round(CGRectGetMinX(frame));
                    frame.origin.y = round(CGRectGetMinY(frame));
                    frame = CGRectIntegral(frame);
                } else {
                    // Step 4: Use the initial rect.
                    frame = initialRect;
                }
            }
            break;
        }
        case RSKImageCropModeCircle: {
            frame = self.maskRect;
            break;
        }
        case RSKImageCropModeCustom: {
            if ([self.dataSource respondsToSelector:@selector(imageCropViewControllerCustomMovementRect:)]) {
                frame = [self.dataSource imageCropViewControllerCustomMovementRect:self];
            } else {
                // Will be changed to `CGRectNull` in version `2.0.0`.
                frame = self.maskRect;
            }
            break;
        }
    }
    
    CGAffineTransform transform = self.imageScrollView.transform;
    self.imageScrollView.transform = CGAffineTransformIdentity;
    self.imageScrollView.frame = frame;
    self.imageScrollView.transform = transform;
}

- (void)layoutOverlayView
{
    CGRect frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds) * 2, CGRectGetHeight(self.view.bounds) * 2);
    self.overlayView.frame = frame;
}

- (void)updateMaskRect
{
    
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
    CGFloat viewHeight = CGRectGetHeight(self.view.frame);
    
    CGFloat length;
    if ([self isPortraitInterfaceOrientation]) {
        length = MIN(viewWidth, viewHeight) - kPortraitSquareMaskRectInnerEdgeInset * 2;
    } else {
        length = MIN(viewWidth, viewHeight) - kLandscapeSquareMaskRectInnerEdgeInset * 2;
    }
    
    //CGSize maskSize = CGSizeMake(length, length);
    
    CGRect r = self.viewimg.frame;//CGRectMake((viewWidth - self.viewimg.frame.size.width) * 0.5f, (viewHeight - self.viewimg.frame.size.height) * 0.5f, self.viewimg.frame.size.width, self.viewimg.frame.size.height);
    self.maskRect = r;
}

- (void)updateMaskPath
{
    switch (self.cropMode) {
        case RSKImageCropModeCircle: {
            self.maskPath = [UIBezierPath bezierPathWithOvalInRect:self.maskRect];
            break;
        }
        case RSKImageCropModeSquare: {
            self.maskPath = [UIBezierPath bezierPathWithRect:self.maskRect];
            break;
        }
        case RSKImageCropModeCustom: {
            if ([self.dataSource respondsToSelector:@selector(imageCropViewControllerCustomMaskPath:)]) {
                self.maskPath = [self.dataSource imageCropViewControllerCustomMaskPath:self];
            } else {
                self.maskPath = nil;
            }
            break;
        }
    }
}

- (UIImage *)croppedImage:(UIImage *)image cropMode:(RSKImageCropMode)cropMode cropRect:(CGRect)cropRect rotationAngle:(CGFloat)rotationAngle zoomScale:(CGFloat)zoomScale maskPath:(UIBezierPath *)maskPath applyMaskToCroppedImage:(BOOL)applyMaskToCroppedImage
{
    // Step 1: check and correct the crop rect.
    CGSize imageSize = image.size;
    CGFloat x = CGRectGetMinX(cropRect);
    CGFloat y = CGRectGetMinY(cropRect);
    CGFloat width = CGRectGetWidth(cropRect);
    CGFloat height = CGRectGetHeight(cropRect);
    
    UIImageOrientation imageOrientation = image.imageOrientation;
    if (imageOrientation == UIImageOrientationRight || imageOrientation == UIImageOrientationRightMirrored) {
        cropRect.origin.x = y;
        cropRect.origin.y = round(imageSize.width - CGRectGetWidth(cropRect) - x);
        cropRect.size.width = height;
        cropRect.size.height = width;
    } else if (imageOrientation == UIImageOrientationLeft || imageOrientation == UIImageOrientationLeftMirrored) {
        cropRect.origin.x = round(imageSize.height - CGRectGetHeight(cropRect) - y);
        cropRect.origin.y = x;
        cropRect.size.width = height;
        cropRect.size.height = width;
    } else if (imageOrientation == UIImageOrientationDown || imageOrientation == UIImageOrientationDownMirrored) {
        cropRect.origin.x = round(imageSize.width - CGRectGetWidth(cropRect) - x);
        cropRect.origin.y = round(imageSize.height - CGRectGetHeight(cropRect) - y);
    }
    
    // Step 2: create an image using the data contained within the specified rect.
    CGImageRef croppedCGImage = CGImageCreateWithImageInRect(image.CGImage, cropRect);
    UIImage *croppedImage = [UIImage imageWithCGImage:croppedCGImage scale:1.0f orientation:imageOrientation];
    CGImageRelease(croppedCGImage);
    
    // Step 3: fix orientation of the cropped image.
    croppedImage = [croppedImage fixOrientation];
    
    // Step 4: If current mode is `RSKImageCropModeSquare` and the image is not rotated
    // or mask should not be applied to the image after cropping and the image is not rotated,
    // we can return the cropped image immediately.
    // Otherwise, we must further process the image.
    // Step 5: return the cropped image immediately.
    //    float xDimension = 0;
    //    float yDimension = 0;
    //    if (gridCount==1)
    //    {
    //        xDimension = 3600;
    //        yDimension = 3600;
    //    }
    //    else if (gridCount==2)
    //    {
    //        xDimension = 3600;
    //        yDimension = 2400;
    //    }
    //    else if (gridCount==3)
    //    {
    //        xDimension = 3600;
    //        yDimension = 1200;
    //    }
    //    UIGraphicsBeginImageContext(CGSizeMake(xDimension,yDimension));
    //    [image drawInRect:CGRectMake(0, 0, xDimension,yDimension)];
    //    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    //    UIGraphicsEndImageContext();
    
    //UIImage *resizedImage = [croppedImage resizedImageToSize: CGSizeMake(xDimension,yDimension)];//RJChange
    //return resizedImage;
    return croppedImage;
}

-(void)gridChange
{
    self.colorScrollView.hidden = YES;
    slideLable.hidden = YES;
    UISlider * slider = [self.view viewWithTag:108888];
    if (slider) {
        slider.hidden = YES;
    }
    UIView * sliderLabel = [self.view viewWithTag:108887];
    if (sliderLabel) {
        sliderLabel.hidden = YES;
    }
    gridCount ++;
    if (gridCount>9)
    {
        gridCount=2;
    }
    [self changeGrid:gridCount];
}

- (void)cropImage
{
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]&&![[NSUserDefaults standardUserDefaults] boolForKey:@"NOASKUPGRADE"]){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"There will be a big watermark in your photo.", nil) message:NSLocalizedString(@"Do you want to upgrade to Pro to Remove watermark and ADS?", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        //rate action
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Upgrade Now", nil) style:UIAlertActionStyleDefault handler:^(__unused UIAlertAction *action) {
            objUpgradeViewController = [[UpgradeViewController alloc] initWithNibName:@"UpgradeViewController" bundle:nil];
            objUpgradeViewController.delegate = self;
            [RJAnimation bounceAddSubViewToParentView:self.view ChildView:objUpgradeViewController.view];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Later", nil) style:UIAlertActionStyleDefault handler:^(__unused UIAlertAction *action) {
            [self realCropAction];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Don't ask again", nil) style:UIAlertActionStyleDefault handler:^(__unused UIAlertAction *action) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"NOASKUPGRADE"];
            [self realCropAction];
        }]];
        //get current view controller and present alert
        [self presentViewController:alert animated:YES completion:NULL];
    }else
    {
        [self realCropAction];
    }
    
}
-(void)upgradeSuccess
{
    [self realCropAction];
}
-(void)realCropAction
{
    if(self.isDomestic == 1){
        [self SudokuAction];
    }else{
    //修改 （选择添加视频）
    CGRect cropRect = self.cropRect;
    CGFloat rotationAngle = self.rotationAngle;
    
    UIImage *croppedImage = [self croppedImage:self.originalImage cropMode:self.cropMode cropRect:cropRect rotationAngle:rotationAngle zoomScale:self.imageScrollView.zoomScale maskPath:self.maskPath applyMaskToCroppedImage:self.applyMaskToCroppedImage];
        
//     全景图片不编辑 《太小无法编辑》
//        AddImageEditViewController *imageEdit = [[AddImageEditViewController alloc]init];
//        imageEdit.selectHome = YES;
//        imageEdit.editImage = croppedImage;
//        imageEdit.gridCount = gridCount;
//        [self.navigationController pushViewController:imageEdit animated:YES];
    HomeVideoSelectViewController *video = [[HomeVideoSelectViewController alloc] init];
    video.imageSource = croppedImage;
    video.selectHome = YES;
    video.gridCount = gridCount;
    //[self presentViewController:video animated:NO completion: nil];
    [self.navigationController pushViewController:video animated:YES];
    }
}
#pragma mark  ----- 九宫格分享
- (void)SudokuAction{
    CGRect cropRect = self.cropRect;
    CGFloat rotationAngle = self.rotationAngle;
    
    UIImage *croppedImage = [self croppedImage:self.originalImage cropMode:self.cropMode cropRect:cropRect rotationAngle:rotationAngle zoomScale:self.imageScrollView.zoomScale maskPath:self.maskPath applyMaskToCroppedImage:self.applyMaskToCroppedImage];
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]){
        croppedImage = [self createWithWaterMarkonImage:croppedImage];
    }
    //UIImage *showIg = [self imageFromView:self.viewimg];
    SudokuViewController *sudoku = [[SudokuViewController alloc]init];
    //sudoku.showImage = showIg;
    sudoku.gridCount = gridCount;
    sudoku.intShareType = 2;
    sudoku.imageSource = croppedImage;
    [self.navigationController pushViewController:sudoku animated:YES];
}
#pragma mark ---- 添加水印
-(UIImage*)createWithWaterMarkonImage:(UIImage*)image{
    CGSize size = image.size;
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage* waterMarkImage = [UIImage imageNamed:@"watermark"];
    
    float scaleFactor = image.size.width / 320;
    float newHeight = waterMarkImage.size.height * scaleFactor;
    float newWidth = waterMarkImage.size.width * scaleFactor;
    if (newHeight > size.height/3) {
        newWidth = (size.height/3)*newWidth/newHeight;
        newHeight = size.height/3;
    }
    [waterMarkImage drawInRect:CGRectMake(size.width - newWidth,size.height-newHeight, newWidth,newHeight)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
//对图片尺寸进行压缩--
-(UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    //    新创建的位图上下文 newSize为其大小
    UIGraphicsBeginImageContext(newSize);
    //    对图片进行尺寸的改变
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    //    从当前上下文中获取一个UIImage对象  即获取新的图片对象
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    // Return the new image.
    return newImage;
}

-(UIImage*)getMaskColorCombineImage:(UIImage*)cropImg
{
    CGSize imgSize = cropImg.size;
    CGRect frame = self.viewimg.frame;
    CGAffineTransform t = self.viewimg.transform;
    CGFloat scalex = imgSize.width/frame.size.width;
    CGFloat scaley = imgSize.height/frame.size.height;
    UIImage * orgimg= self.viewimg.image;
    self.viewimg.image = nil;
    UIView * containView = [[UIView alloc] init];
    containView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width*scalex, frame.size.height*scaley);
    self.viewimg.transform = CGAffineTransformMakeScale(scalex, scaley);
    UIImageView * cropImgview = [[UIImageView alloc] initWithFrame:containView.bounds];
    cropImgview.image = cropImg;
    [containView addSubview:cropImgview];
    [containView addSubview:self.viewimg];
    self.viewimg.center = CGPointMake(containView.frame.size.width/2, containView.frame.size.height/2);
    UIImage * colorImg = [ImageHelper imageFromView:containView];
    [self.view addSubview:self.viewimg];
    self.viewimg.transform = t;
    self.viewimg.frame = frame;
    self.viewimg.image = orgimg;
    return colorImg;
    
}
-(BOOL)prefersStatusBarHidden { return YES; }
- (void)checkifshowrateus
{
    [self.delegate checkifshowrateus];
}
- (void)showgoogleinitial
{
    [self.delegate showgoogleinitial];
}
- (void)cancelCrop
{
    [self.navigationController popToRootViewControllerAnimated:YES];
   // [self dismissViewControllerAnimated:YES completion:nil];
    [self performSelector:@selector(showgoogleinitial) withObject:nil afterDelay:0.5];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

@end
