//
//  MainPostViewController.h
//  Instagrid Post
//
//  Created by macbookair on 30/7/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
NS_ASSUME_NONNULL_BEGIN

@interface MainPostViewController : UIViewController<GADInterstitialDelegate>
- (void)pushUpgradeView;
@end

NS_ASSUME_NONNULL_END
