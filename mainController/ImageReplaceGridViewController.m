//
//  ImageReplaceGridViewController.m
//  Instagrid Post
//
//  Created by macbookair on 5/8/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import "ImageReplaceGridViewController.h"
#import "PostImagehelper.h"
#import "UIImage+EKTilesMaker.h"

@interface ImageReplaceGridViewController ()
@property (nonatomic,strong)UIView *natView;
@property (nonatomic,strong)UIImageView *editImageView; //底层视图
@property (nonatomic,strong)UIImageView *maskImageView; //方格视图
@property (nonatomic,strong)UIImageView *replaceImageView;
@property (nonatomic,strong)UIView *tabView;
@property (nonatomic,strong)NSMutableArray *dataArray; //存储格子的位置

@end

@implementation ImageReplaceGridViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.dataArray = [[NSMutableArray alloc]init];
    [self loadUI];
}
-(BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)loadUI{
    [self createNatView];
    [self createTabView];
    [self createMainView];
}
- (void)createNatView{
    //滤镜的tabView
    self.natView = [[UIView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth,60+KDNavH)];
    [self.view addSubview:self.natView];
    //返回按钮
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setFrame:CGRectMake(10, 15+KDNavH, 35, 35)];
    [btnBack setImage:[UIImage imageNamed:@"backBlack_icn"] forState:UIControlStateNormal];
    //[self.btnBack setImage:[UIImage imageNamed:@"back_sel"] forState:UIControlStateHighlighted];
    btnBack.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [btnBack addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:btnBack];
    
    //UILabel *titleLabel = [UILabel ]
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2-100,15+KDNavH,200, 35)];
    titleLabel.text  = NSLocalizedString(@"Replace image",nil);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [self.natView addSubview:titleLabel];
    //确定按钮
    UIButton *doneBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneBack setFrame:CGRectMake(ScreenWidth - 45, 15+KDNavH, 35, 35)];
    [doneBack setImage:[UIImage imageNamed:@"doneSel_icn"] forState:UIControlStateNormal];
    //[self.btnBack setImage:[UIImage imageNamed:@"back_sel"] forState:UIControlStateHighlighted];
    doneBack.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [doneBack addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:doneBack];
    
    UIImageView *natLineIg = [[UIImageView alloc]initWithFrame:CGRectMake(0,VIEW_H(self.natView)-2, ScreenWidth,2)];
    natLineIg.image = [UIImage imageNamed:@"tabLine"];//[UIColor lightGrayColor];
    [self.natView addSubview:natLineIg];
}
-(void)createTabView{
    self.tabView = [[UIView alloc]initWithFrame:CGRectMake(0,ScreenHeight - 55 - KDNavH,ScreenWidth, 55+KDNavH)];
    UIImageView *LineIg = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, ScreenWidth,2)];
    LineIg.image = [UIImage imageNamed:@"tabLine"];//[UIColor lightGrayColor];
    [self.tabView addSubview:LineIg];
    
    UIButton *cancelBut = [[UIButton alloc]initWithFrame:CGRectMake(0, 2, ScreenWidth/2,52+KDNavH)];
    [cancelBut setTitle:NSLocalizedString(@"Cancel",nil) forState:UIControlStateNormal];
    cancelBut.backgroundColor = [UIColor whiteColor];
    [cancelBut setTitleColor:mainColor forState:UIControlStateNormal];
    cancelBut.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [cancelBut addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self.tabView addSubview:cancelBut];
    
    UIImageView *finishImageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2, 2, ScreenWidth/2,52+KDNavH)];
    finishImageView.userInteractionEnabled = YES;
    finishImageView.image = [UIImage imageNamed:@"finishIg"];
    [self.tabView addSubview:finishImageView];
    
    UIButton *finishBut = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth/2, 2, ScreenWidth/2,52+KDNavH)];
    finishBut.backgroundColor = [UIColor clearColor];
    //finishBut.imageView.image = [UIImage imageNamed:@"finishIg"];
    [finishBut setTitle:NSLocalizedString(@"Done",nil) forState:UIControlStateNormal];
    //[finishBut setImage:[UIImage imageNamed:@"finishIg"] forState:UIControlStateNormal]; ;
    [finishBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [finishBut addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    finishBut.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [self.tabView addSubview:finishBut];
    
    [self.view addSubview:self.tabView];
}
- (void)createMainView{
    CGRect mainRect;
    CGFloat h = ScreenWidth;
    if(self.selectHome){
        h = WINDOW.bounds.size.width/self.gridCount;
    }else{
        h = WINDOW.bounds.size.width*self.gridCount/3;
    }
    CGFloat maxh = ScreenHeight - VIEW_H(self.natView)-10 - VIEW_H(self.tabView);//self.colorScrollView.frame.origin.y-self.btnBack.frame.origin.y-self.btnBack.frame.size.height-6;
    CGFloat orgy = VIEW_YH(self.natView)+5;//self.btnBack.frame.origin.y+self.btnBack.frame.size.height+3;
    if (h > maxh) {
        CGFloat w = WINDOW.bounds.size.width*maxh/h;
        mainRect = CGRectMake((WINDOW.bounds.size.width-w)*0.5f, orgy, w, maxh);
    }else
    {
        mainRect = CGRectMake(0, orgy+(maxh-h)*0.5f, WINDOW.bounds.size.width, h);
    }
    
    self.replaceImageView = [[UIImageView alloc]initWithFrame:mainRect];
//    if(self.selectHome){
//        self.maskImageView.image = [PostImagehelper getHomeImageByGridCount:self.gridCount frame:mainRect] ;
//    }else{
//        self.maskImageView.image = [PostImagehelper getImageByGridCount:self.gridCount frame:mainRect] ;
//    }
    
    self.replaceImage = [PostImagehelper image:self.replaceImage centerInSize:self.baseImage.size];//self.replaceImage;
    self.replaceImageView.image = self.replaceImage;
    [self.view addSubview:self.replaceImageView];
    
//    self.editImageView = [[UIImageView alloc]initWithFrame:mainRect];
//    self.editImageView.image = self.baseImage;
//    [self.view addSubview:self.editImageView];
    CGFloat gridW = mainRect.size.width/3;
    CGFloat imageRate = self.baseImage.size.width/mainRect.size.width;
    int indexc = 0;
    int maxI,maxJ;
    if(self.selectHome){
        maxI = 1;
        maxJ = self.gridCount;
        gridW = mainRect.size.width/self.gridCount;
    }else{
        maxI = self.gridCount;
        maxJ = 3;
    }
    for(int i = 0;i<maxI;i++){
        for(int j = 0;j<maxJ;j++){
            CGRect baseRect ;
            CGRect cutRect;
            UIImageView *baseImageView = [[UIImageView alloc]init];
            baseRect =  CGRectMake(gridW*j+mainRect.origin.x,mainRect.origin.y+gridW*i,gridW, gridW);
            baseImageView.frame = baseRect;
            cutRect = CGRectMake(imageRate*gridW*j,imageRate*gridW*i, imageRate*gridW,imageRate*gridW);
            baseImageView.image = [self.baseImage imageInRect:cutRect];
            baseImageView.tag = 600+indexc;
            //将结构体类型的CGRect转换成对象类型
            NSValue *value = [NSValue valueWithCGRect:cutRect] ;
            [self.dataArray addObject:value ];
            
            UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(gridW*j+(gridW-30)/2+mainRect.origin.x,mainRect.origin.y+gridW*i+(gridW-30)/2,30, 30)];
            [button setImage:[UIImage imageNamed:@"addReplace_icn"] forState:UIControlStateNormal];
            [button setImage:[UIImage imageNamed:@"cancelReplace_icn"] forState:UIControlStateSelected];
            [button addTarget:self action:@selector(addAndReplaceImageAction:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = 500+indexc;
//            NSArray *array = @[@(i),@(j)];
//            [self.dataArray addObject:array];
            [self.view addSubview:baseImageView];
            [self.view addSubview:button];
            indexc++;
        }
    }
    self.maskImageView = [[UIImageView alloc]initWithFrame:mainRect];
    if(self.selectHome){
        self.maskImageView.image = [PostImagehelper getHomeImageByGridCount:self.gridCount frame:mainRect] ;
    }else{
       self.maskImageView.image = [PostImagehelper getImageByGridCount:self.gridCount frame:mainRect] ;
    }
    [self.view addSubview:self.maskImageView];
}

- (void)addAndReplaceImageAction:(UIButton *)sender{
    UIView *viewIg = [self.view viewWithTag:sender.tag+100];
    if(sender.selected){
        sender.selected = NO;
        viewIg.hidden = NO;
    }else{
        sender.selected = YES;
        viewIg.hidden = YES;
    }
}
- (void)cancelAction{
    [self.navigationController popViewControllerAnimated:nil];
}
- (void)doneAction{
    for(UIView *view in self.view.subviews){
        if([view isKindOfClass:[UIImageView class]] && view.tag >= 600){
            if(view.hidden){
                NSInteger index = view.tag - 600;
                CGRect imageRect = [self.dataArray[index] CGRectValue];
                UIImage *addImage = [self.replaceImage imageInRect:imageRect];
              //拼接
                self.baseImage = [PostImagehelper addImage:addImage toImage:self.baseImage AddToX:imageRect.origin.x AddToY:imageRect.origin.y];
                
            }
        }
    }
    [self.delegate finishImageReplaceDeal:self.baseImage];
    [self.navigationController popViewControllerAnimated:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
