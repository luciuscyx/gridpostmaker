//
//  InstaGridShareViewController.h
//  Instagrid Post
//
//  Created by Mangal on 02/07/15.
//  Copyright (c) 2015 RJLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoSelectViewController : UIViewController <UIDocumentInteractionControllerDelegate>
{

    NSMutableArray *arrayImageTiles;
    int currentCount;
    UIDocumentInteractionController *docController;
}
@property (nonatomic, strong)  UIButton *btnSelected;
@property (nonatomic, strong)  UIDocumentInteractionController *docController;

@property (nonatomic, assign) int gridCount;
@property (nonatomic, strong) UIImage *imageSource;
@property (nonatomic, strong) UIImage *orgimageSource;
@property (strong, nonatomic) UILabel *labelInstruction;
@property (assign, nonatomic) BOOL selectHome;




@end
