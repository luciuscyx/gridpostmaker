//
//  CutVideoViewController.h
//  Instagrid Post
//
//  Created by chenteng on 2018/4/24.
//  Copyright © 2018年 RJLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CutVideoViewController : UIViewController
@property (nonatomic,strong)NSString *videoPath;//视频的路径
@property (nonatomic,strong)NSURL *imageVurl;//图片视频的url
@property (nonatomic,copy)void (^lastPathBlock)(NSString *lastpath);

@property (nonatomic,assign)BOOL isType;//Yes 是从主页面跳转过来（对视频和图片进行处理）
@property (nonatomic,strong)UIImage *selectImage;
@property (nonatomic,strong)NSURL *selectVideoUrl;
@property (nonatomic,assign)NSInteger isDomestic;
@end
