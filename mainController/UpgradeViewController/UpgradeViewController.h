//
//  UpgradeViewController.h
//  Instagrid Post
//
//  Created by Mangal on 30/06/15.
//  Copyright (c) 2015 RJLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol UpgradeViewControllerDelegate
-(void)upgradeSuccess;
@end


@interface UpgradeViewController : UIViewController <inAppDelegate>
{
    
}
@property (weak, nonatomic) IBOutlet UIView *viewPopOver;
@property (nonatomic,retain)id <UpgradeViewControllerDelegate> delegate;
- (IBAction)btnUpgradeToFullPressed:(id)sender;
- (IBAction)btnClosePressed:(id)sender;
@end
