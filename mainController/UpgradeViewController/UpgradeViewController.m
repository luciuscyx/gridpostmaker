//
//  UpgradeViewController.m
//  Instagrid Post
//
//  Created by Mangal on 30/06/15.
//  Copyright (c) 2015 RJLabs. All rights reserved.
//

#import "UpgradeViewController.h"

@interface UpgradeViewController ()
@property (weak, nonatomic) IBOutlet UILabel *upgradeLbl;
@property (weak, nonatomic) IBOutlet UILabel *noadsLbl;
@property (weak, nonatomic) IBOutlet UILabel *nomarkLbl;
@property (weak, nonatomic) IBOutlet UIButton *upgradeBtn;

@end

@implementation UpgradeViewController

@synthesize viewPopOver;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.frame = CGRectMake(0, 0, WINDOW.frame.size.width, WINDOW.frame.size.height);
    self.upgradeLbl.text = NSLocalizedString(@"UPGRADE", nil);
    self.noadsLbl.text = NSLocalizedString(@"REMOVE ADS", nil);
    self.nomarkLbl.text = NSLocalizedString(@"NO WATERMARKS", nil);
    [self.upgradeBtn setTitle:NSLocalizedString(@"UPGRADE ONLY $1.99", nil) forState:UIControlStateNormal];
    self.viewPopOver.frame = CGRectMake((self.view.bounds.size.width-self.viewPopOver.frame.size.width)*0.5f, (self.view.bounds.size.height-self.viewPopOver.frame.size.height)*0.5f, self.viewPopOver.frame.size.width, self.viewPopOver.frame.size.height);
    // Do any additional setup after loading the view from its nib.
}
-(BOOL)prefersStatusBarHidden { return YES; }
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnUpgradeToFullPressed:(id)sender
{
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"])
    {
        InAppSharedClass *objInAppShare = [InAppSharedClass sharedInstance];
        [objInAppShare loadProduct:self withDelegate:self bundleId:UPGRADE_PRO strKey:UPGRADE_PRO_KEY];
    }
}

- (IBAction)btnClosePressed:(id)sender
{
    [self.view removeFromSuperview];
}


-(void)productPurchaseSuccess:(NSString *)bundleId strKey:(NSString *)strKey
{
    CIAlert(@"Thanks for purchasing", @"You are successfully upgraded to pro");
    
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:UPGRADE_PRO_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if (self.delegate) {
        [self.delegate upgradeSuccess];
    }
    [SVProgressHUD dismiss];
    [self.view removeFromSuperview];
}

-(void)productPurchaseFailure:(NSString *)bundleId strKey:(NSString *)strKey
{
    CIError(@"Error occur while purchasing");
    [SVProgressHUD dismiss];
}

@end
