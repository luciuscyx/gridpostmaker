//
//  AddImageEditViewController.m
//  Instagrid Post
//
//  Created by macbookair on 3/8/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import "AddImageEditViewController.h"
#import "ImagePostBaseFilter.h"
#import "FilePathDtata.h"
#import <Photos/Photos.h>
#import "HomeCollectionViewCells.h"
#import "PostImagehelper.h"
#import "ImageReplaceGridViewController.h"
#import "ImageAddMaskImageViewController.h"
#import "SudokuViewController.h"
#import "VideoSelectViewController.h"
#import "ILColorPickerDualExampleController.h"
#import "EraserCLTextView.h"
#import "GhostLensTextCustomSettingView.h"
#import "PostTabButton.h"
#import "UpgradeViewController.h"

#define FiltersNumber 5
@interface AddImageEditViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,ImageReplaceGridViewControllerDelegate,ImageAddMaskImageViewControllerDelegate,GhostLensTextCustomSettingViewDelegate,EraserCLTextViewDelegate>
{
    CGFloat cellSize;
    CGFloat separation;
    
    UIColor * selectColor;
    int selectIndex;
    ILColorPickerDualExampleController * ilColorPicker;
    NSArray* colorList;
}
@property (nonatomic,strong)UIView *natView;
//@property (nonatomic,str)
@property (nonatomic,strong)UIImageView *editImageView; //底层视图
@property (nonatomic,strong)UIImageView *maskImageView; //方格视图

//@property (nonatomic,strong)UIView *tabView;//整个tap
@property (nonatomic,strong)UIView *slideActionView;  //tap功能
@property (nonatomic,strong)UIView *tabActionView;   //tap选择按钮

@property (nonatomic,strong)UIScrollView *filterScrollView;//滤镜选择视图
@property (nonatomic,strong)NSMutableArray *filterImageArray;
@property (nonatomic,strong)UIImageView *filterSelectView;
@property (nonatomic,assign)NSInteger selectFilterTag;
@property (nonatomic,strong)UIImage *filterBaseImage;

@property (nonatomic,strong)  UIView *imageTabView;
@property (nonatomic,strong)  NSMutableArray *imageArray;
@property (nonatomic,strong)  NSArray *segArray;
@property (nonatomic, strong) PHFetchResult *result;
@property (nonatomic, strong) PHImageRequestOptions *requestOptions;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic,assign)  NSInteger count;

@property (nonatomic,assign) NSInteger selectTabTag;
@property (nonatomic,assign) NSInteger isDomestic;

@property (nonatomic,strong)UIView *textMainView;
@property (nonatomic,strong)UIView *textTabView;

@property (nonatomic,strong)EraserCLTextView *addNewTextView; //文本编辑
@property (nonatomic,strong)UIView *textFontSpaceView; //字体间距
@property (nonatomic,strong)NSMutableArray *textViewArray;//存储文本的数组

//添加字体边框
@property (nonatomic,strong)UIView *textBorderColorView;//字体变旷颜色
//@property (nonatomic,strong) GradientView*  MainBorderColorslider;
@property (strong, nonatomic) UIImageView *twoSlideView;
@property (strong, nonatomic) UIImageView *twoDeepView;
@property (nonatomic,strong)  UIColor   *borderColor;
@property (nonatomic,strong)  UIColor  *circleBorderColor;
@property (nonatomic,strong)UISlider *layerBorderSlider;//字体边框阴影的范围
@property (nonatomic,assign)NSInteger textTabSelectTag; //一级菜单的Tag

@property (nonatomic,strong)GhostLensTextCustomSettingView *settingView;
@property (nonatomic,strong)UIView *tabTextScroll;

@property (nonatomic,strong)UIScrollView *textStyleScrollView;
@property (nonatomic,strong)UIView *textFontSelView;
@property (nonatomic,strong)NSMutableArray *fontNameArray;
@property (nonatomic,assign)NSInteger textFontSelTag;

@property (nonatomic,strong)UIView *textAlignmentView;

@property (nonatomic,strong)UIView *textColorView;
@property (nonatomic,strong)UIScrollView *colorScrollView;

@property (nonatomic,strong)UILabel *titleNatLabel;
@property (nonatomic,strong)NSArray *titleArray;
@property (nonatomic,strong)UpgradeViewController *objUpgradeViewController;
//@property (nonatomic,strong)UIButton *
@end

@implementation AddImageEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.isDomestic = [[NSUserDefaults standardUserDefaults] integerForKey:@"isDomestic"];
    self.view.backgroundColor = [UIColor whiteColor];
    [self loadData];
    [self loadUI];
}
- (void)viewWillDisappear:(BOOL)animated{
    //[[NSUserDefaults standardUserDefaults]setObject:self.filterImageArray forKey:@"allFilterImageinfo"];
}
- (void)loadData{
    //NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:@"allFilterImageinfo"];
    //self.filterImageArray = [NSMutableArray arrayWithArray:array];
    self.imageArray = [NSMutableArray new];
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        switch (status) {
            case PHAuthorizationStatusAuthorized:
                [self getAllPhotosFromCamera];
                break;
            case PHAuthorizationStatusRestricted:
                break;
            case PHAuthorizationStatusDenied:
                break;
            default:
                break;
        }
    }];
    colorList=@[
                [UIColor colorWithRed:1.0 green: 1.0 blue:1.0 alpha: 1.0],
                [UIColor blackColor],
                [UIColor colorWithRed:204.0/255.0 green: 208.0/255.0 blue: 217.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:101.0/255.0 green: 109.0/255.0 blue: 120.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:236.0/255.0 green: 136/255.0 blue: 192.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:172.0/255.0 green: 146.0/255.0 blue: 237.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:93.0/255.0 green: 155.0/255.0 blue: 236.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:79.0/255.0 green: 192.0/255.0 blue: 232.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:72.0/255.0 green: 207.0/255.0 blue: 174.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:160.0/255.0 green: 212.0/255.0 blue: 104.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:253.0/255.0 green: 207.0/255.0 blue: 85.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:251.0/255.0 green: 110.0/255.0 blue: 82.0/255.0 alpha: 1.0],
                [UIColor colorWithRed:237.0/255.0 green: 85.0/255.0 blue: 100.0/255.0 alpha: 1.0],
                [UIColor redColor],
                [UIColor greenColor],
                [UIColor blueColor],
                [UIColor cyanColor],
                [UIColor yellowColor],
                [UIColor magentaColor],
                [UIColor orangeColor],
                [UIColor purpleColor],
                [UIColor brownColor],
                ];
    selectColor = colorList[18];
    self.textViewArray = [[NSMutableArray alloc]init];
}
-(BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)loadUI{
    [self createNatView];
    [self createTabView];
    [self createMainView];
    [self loadFilterAction];
    [self createCollectImageView];
    
    //加载文字字体样式时，存在耗时
    [self performSelector:@selector(loadTextAction) withObject:nil afterDelay:0.1];
}
- (void)createNatView{
    //滤镜的tabView
    self.natView = [[UIView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth,60+KDNavH)];
    [self.view addSubview:self.natView];
    //返回按钮
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setFrame:CGRectMake(10, 15+KDNavH, 35, 35)];
    [btnBack setImage:[UIImage imageNamed:@"backBlack_icn"] forState:UIControlStateNormal];
    //[self.btnBack setImage:[UIImage imageNamed:@"back_sel"] forState:UIControlStateHighlighted];
    btnBack.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [btnBack addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:btnBack];
    
    UILabel *titleNatLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2-100,15+KDNavH,200, 30)];
    titleNatLabel.text  = NSLocalizedString(@"Image editing",nil);//self.titleArray[2];
    titleNatLabel.textAlignment = NSTextAlignmentCenter;
    titleNatLabel.textColor = [UIColor blackColor];
    titleNatLabel.font = [UIFont boldSystemFontOfSize:15];
    [self.natView addSubview:titleNatLabel];
    
    //确定按钮
    UIButton *doneBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneBack setFrame:CGRectMake(ScreenWidth - 45, 15+KDNavH, 35, 35)];
    [doneBack setImage:[UIImage imageNamed:@"doneSel_icn"] forState:UIControlStateNormal];
    //[self.btnBack setImage:[UIImage imageNamed:@"back_sel"] forState:UIControlStateHighlighted];
    doneBack.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [doneBack addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:doneBack];
    
    self.imageTabView = [[UIView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth,60+KDNavH)];
    self.imageTabView .hidden = YES;
    [self.view addSubview:self.imageTabView];
    
    
    
    UIButton *albumBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [albumBack setFrame:CGRectMake(10, 15+KDNavH, 35, 35)];
    [albumBack setImage:[UIImage imageNamed:@"backBlack_icn"] forState:UIControlStateNormal];
    //[self.btnBack setImage:[UIImage imageNamed:@"back_sel"] forState:UIControlStateHighlighted];
    albumBack.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [albumBack addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self.imageTabView addSubview:albumBack];
    
    
    //self.titleArray = @[NSLocalizedString(@"Replace image",nil),NSLocalizedString(@"Add image",nil),NSLocalizedString(@"Filters",nil),NSLocalizedString(@"Watermark",nil)];
    self.titleNatLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2-100,15+KDNavH,200, 30)];
    self.titleNatLabel.text  = NSLocalizedString(@"Image editing",nil);//self.titleArray[2];
    self.titleNatLabel.textAlignment = NSTextAlignmentCenter;
    self.titleNatLabel.textColor = [UIColor blackColor];
    self.titleNatLabel.font = [UIFont boldSystemFontOfSize:15];
    [self.imageTabView addSubview:self.titleNatLabel];
    
//    UILabel *AlbumLabel = [[UILabel alloc]initWithFrame:CGRectMake(VIEW_XW(imageLabel),15+KDNavH,50, 30)];
//    AlbumLabel.text  = @"相薄";
//    AlbumLabel.textColor = [UIColor lightGrayColor];
//    AlbumLabel.font = [UIFont boldSystemFontOfSize:15];
//    [self.imageTabView addSubview:AlbumLabel];
    
    UIButton *cameraBut = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth - 50,15+KDNavH, 30, 30)];
    [cameraBut setImage:[UIImage imageNamed:@"AddCamera_icn"] forState:UIControlStateNormal];
    [cameraBut addTarget:self action:@selector(selectCameraAction) forControlEvents:UIControlEventTouchUpInside];
    [self.imageTabView addSubview:cameraBut];
    
    UIImageView *natLineIg = [[UIImageView alloc]initWithFrame:CGRectMake(0,VIEW_H(self.imageTabView)-2, ScreenWidth,2)];
    natLineIg.image = [UIImage imageNamed:@"tabLine"];//[UIColor lightGrayColor];
    [self.imageTabView addSubview:natLineIg];
}
- (void)createTabView{
    //self.tabView = [[UIView alloc]initWithFrame:CGRectMake(0,ScreenHeight - 150 - KDNavH-2,ScreenWidth,150+KDNavH+2)];
    //[self.view addSubview:self.tabView];
    NSArray *tabTextArray = @[NSLocalizedString(@"Replace",nil),NSLocalizedString(@"Add",nil),NSLocalizedString(@"Filter",nil),NSLocalizedString(@"Text",nil)];
    self.slideActionView = [[UIView alloc]initWithFrame:CGRectMake(0,ScreenHeight - 150 - KDNavH,ScreenWidth,100)];
    [self.view addSubview:self.slideActionView];
    
    self.tabActionView = [[UIView alloc]initWithFrame:CGRectMake(0,ScreenHeight - 50 - KDNavH,ScreenWidth,50+KDNavH)];
    CGFloat tabActW = 40;
    CGFloat tabActLineW = (ScreenWidth - tabActW*4)/5;
    for(int i = 0;i<4;i++){
        PostTabButton *tabBut = [[PostTabButton alloc]initWithFrame:CGRectMake(tabActLineW+(tabActW+tabActLineW)*i,5, tabActW, tabActW)];
         [tabBut setImage:[UIImage imageNamed:[NSString stringWithFormat:@"editImage_nor%d",i]] forState:UIControlStateNormal];
         [tabBut setImage:[UIImage imageNamed:[NSString stringWithFormat:@"editImage_sel%d",i]] forState:UIControlStateSelected];
        [tabBut setTitle:tabTextArray[i] forState:UIControlStateNormal];
        [tabBut setTitleColor:RGB(207,210,213) forState:UIControlStateNormal];
        [tabBut setTitleColor:mainColor forState:UIControlStateSelected];
        [tabBut addTarget:self action:@selector(selectTabAction:) forControlEvents:UIControlEventTouchUpInside];
        
        if(i == 2){
            tabBut.selected = YES;
        }
        tabBut.tag = 100+i;
        [self.tabActionView addSubview:tabBut];
    }
    UIImageView *tabLineIg = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, ScreenWidth,2)];
    tabLineIg.image = [UIImage imageNamed:@"tabLine"];//[UIColor lightGrayColor];
    [self.tabActionView addSubview:tabLineIg];
    [self.view addSubview:self.tabActionView];
}
- (void)selectTabAction:(UIButton *)sender{
    if(sender.selected && sender.tag != 103){
        return;
    }
    for(UIView *view in self.tabActionView.subviews){
        if([view isKindOfClass:[UIButton class]]){
         UIButton *but = (UIButton *)view;
         but.selected = NO;
        }
    }
    
    sender.selected = YES;
    self.selectTabTag = sender.tag - 100;
    //self.titleNatLabel.text  = self.titleArray[self.selectTabTag];
    switch (self.selectTabTag) {
        case 0:
            self.natView.hidden = YES;
            self.imageTabView.hidden = NO;
            self.collectionView.hidden = NO;
            self.editImageView.hidden = YES;
            self.maskImageView.hidden = YES;
            self.slideActionView.hidden = YES;
            break;
        case 1:
            self.natView.hidden = YES;
            self.imageTabView.hidden = NO;
            self.collectionView.hidden = NO;
            self.editImageView.hidden = YES;
            self.maskImageView.hidden = YES;
            self.slideActionView.hidden = YES;
            break;
        case 2:
            self.filterBaseImage = self.editImage;
            self.natView.hidden = NO;
            self.imageTabView.hidden = YES;
            self.collectionView.hidden = YES;
            self.editImageView.hidden = NO;
            self.maskImageView.hidden = NO;
            self.slideActionView.hidden = NO;
            break;
        case 3:
            self.textMainView.hidden = NO;
            self.natView.hidden = NO;
            self.imageTabView.hidden = YES;
            self.collectionView.hidden = YES;
            self.editImageView.hidden = NO;
            self.maskImageView.hidden = NO;
            self.slideActionView.hidden = YES;
            [self textFileView];
            break;
        default:
            break;
    }
}
- (void)createMainView{
//    h = WINDOW.bounds.size.width/gridCount;
//    //减去35的高度
//    CGFloat maxh = self.colorScrollView.frame.origin.y-35-self.btnBack.frame.origin.y-self.btnBack.frame.size.height-6;
//    CGFloat orgy = self.btnBack.frame.origin.y+self.btnBack.frame.size.height+3;
//    if (h > maxh) {//不存在
//        CGFloat w = WINDOW.bounds.size.width*maxh/h;
//        self.viewimg.frame = CGRectMake((WINDOW.bounds.size.width-w)*0.5f, orgy, w, maxh);
//    }else
//    {
//        self.viewimg.frame = CGRectMake(0, orgy+(maxh-h)*0.5f, WINDOW.bounds.size.width, h);
//    }
//
//    self.viewimg.image = [self getImageByGridCount:gridCount frame:self.viewimg.frame] ;
//    if (self.viewimg.subviews&&self.viewimg.subviews.count > 0) {
//        [self.viewimg.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//    }
    
    
    CGRect mainRect;
    CGFloat h = ScreenWidth;
    if(self.selectHome){
        h = WINDOW.bounds.size.width/self.gridCount;
    }else{
        h = WINDOW.bounds.size.width*self.gridCount/3;
    }
    CGFloat maxh = ScreenHeight - VIEW_YH(self.natView)-10 - VIEW_H(self.slideActionView) -VIEW_H(self.tabActionView) ;//self.colorScrollView.frame.origin.y-self.btnBack.frame.origin.y-self.btnBack.frame.size.height-6;
    CGFloat orgy = VIEW_YH(self.natView)+5;//self.btnBack.frame.origin.y+self.btnBack.frame.size.height+3;
    if (h > maxh) {
        CGFloat w = WINDOW.bounds.size.width*maxh/h;
        mainRect = CGRectMake((WINDOW.bounds.size.width-w)*0.5f, orgy, w, maxh);
    }else
    {
        mainRect = CGRectMake(0, orgy+(maxh-h)*0.5f, WINDOW.bounds.size.width, h);
    }
    
    self.editImageView = [[UIImageView alloc]initWithFrame:mainRect];
    self.editImageView.userInteractionEnabled = YES;
    self.editImageView.image = self.editImage;
    [self.view addSubview:self.editImageView];
    
    self.maskImageView = [[UIImageView alloc]initWithFrame:mainRect];
    if(self.selectHome){
      self.maskImageView.image = [PostImagehelper getHomeImageByGridCount:self.gridCount frame:mainRect] ;
    }else{
      self.maskImageView.image = [PostImagehelper getImageByGridCount:self.gridCount frame:mainRect] ;
    }
    //self.editImageView.image = self.editImage;
    [self.view addSubview:self.maskImageView];
    
}
-(void)loadFilterAction{
    CGFloat filterW = 70;
    self.filterScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,(VIEW_H(self.slideActionView)-filterW)/2,kDeviceWidth, filterW)];
    self.filterScrollView.showsHorizontalScrollIndicator = NO;
//    BOOL isExist = false ;
//    if(self.filterImageArray && self.filterImageArray.count == 20 ){
//        isExist = YES;
//    }
    int filterSum = 20;
    for(int i = 0;i<filterSum;i++){
        UIImage *filterImage = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:[NSString stringWithFormat:@"filterImage%d",i] ofType:@"png"]];//[UIImage imageNamed:[NSString stringWithFormat:@"filterImage%d",i]];
        UIButton * button = [[UIButton alloc]initWithFrame:CGRectMake(10+(filterW+10)*i,0, filterW, filterW)];
//        if(!isExist){
//           UIImage *butimg = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"filterBaseIg" ofType:@"png"]];
//           filterImage = [[ImagePostBaseFilter sharedFilter] filteredImage:butimg withFilterName:newNameArray[i] withFilterVersion2:i+1];
//          [self.filterImageArray addObject: [FilePathDtata saveImageFile:filterImage index:i]];
//        }else{
//             filterImage = [UIImage imageWithContentsOfFile:[BASEPATH stringByAppendingPathComponent:self.filterImageArray[i]]];
//        }
        button.tag = 300+i;
        [button setImage:filterImage forState:UIControlStateNormal];
        [button addTarget:self action:@selector(selectFilterAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.filterScrollView addSubview:button];
        
        UIImageView *buyMview = [[UIImageView alloc]initWithFrame:CGRectMake(filterW-15,5, 10, 10)];
        buyMview.image = [UIImage imageNamed:@"buyMImage"];
        buyMview.hidden = YES;
        [button addSubview:buyMview];
        if(i> FiltersNumber)
            buyMview.hidden = NO;
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]) {
            buyMview.hidden = YES;
        }
    }
    self.filterBaseImage = self.editImage;
    self.filterSelectView = [[UIImageView alloc]initWithFrame:CGRectMake(10+(filterW-30)/2,(VIEW_H(self.filterScrollView)-30)/2, 30, 30)];
    self.filterSelectView.image = [UIImage imageNamed:@"filter_sel"];
    [self.filterScrollView addSubview:self.filterSelectView];
    [self.filterScrollView setContentSize:CGSizeMake(10+(filterW+10)*filterSum, filterW)];
    [self.slideActionView addSubview:self.filterScrollView];
}
-(void)loadTextAction{
    self.textMainView = [[UIView alloc]initWithFrame:CGRectMake(0,ScreenHeight - 200-KDNavH,ScreenWidth,200+KDNavH)];
    self.textMainView.backgroundColor = [UIColor whiteColor];
    self.textMainView.hidden = YES;
    [self.view addSubview:self.textMainView];
    
    _settingView = [[GhostLensTextCustomSettingView alloc] initWithFrame:CGRectMake(0,kDeviceHeight - 200 -KDNavH, kDeviceWidth, 200+KDNavH)];
    //_settingView.backgroundColor = [CLImageEditorTheme toolbarColor];
    _settingView.textColor = [CLImageEditorTheme toolbarTextColor];
    _settingView.fontPickerForegroundColor = _settingView.backgroundColor;
    _settingView.delegate = self;
    [self.view addSubview:_settingView];
    _settingView.hidden = YES;
    UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage * img = [UIImage imageNamed:@"textDelete"];
    [okButton setImage:img forState:UIControlStateNormal];
    okButton.frame = CGRectMake(_settingView.width-32, 0, 32, 32);
    [okButton addTarget:self action:@selector(pushedButton:) forControlEvents:UIControlEventTouchUpInside];
    [_settingView addSubview:okButton];
    
    
    self.textTabView = [[UIView alloc]initWithFrame:CGRectMake(0,0,kDeviceWidth,50)];
    self.textTabView.backgroundColor = [UIColor whiteColor];
    [self.textMainView addSubview: self.textTabView ];
    
    CGFloat textTabW  = 40;
    CGFloat textTabline = (kDeviceWidth - 40 - textTabW*5)/7;
    
    self.tabTextScroll = [[UIView alloc]initWithFrame:CGRectMake(0, 0,VIEW_W(self.textTabView)-textTabline*1.5-40, VIEW_H(self.textTabView))];
    [self.textTabView addSubview:self.tabTextScroll];
    for(int i = 0;i < 5;i++){
        UIButton *textTabButton = [[UIButton alloc]initWithFrame:CGRectMake(textTabline/2+(textTabline+textTabW)*i,5, textTabW, textTabW)];
        [textTabButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"textTabNor%d",i]] forState:UIControlStateNormal];
        if(i>0 && i <4){
        [textTabButton setImage:[UIImage imageNamed:[NSString stringWithFormat:@"textTabSel%d",i]] forState:UIControlStateSelected];
        }
        if(i == 1){
            textTabButton.selected = YES;
        }
        textTabButton.tag = 900+i;
        [textTabButton addTarget:self action:@selector(textTabAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.tabTextScroll addSubview:textTabButton];
    }
    UIButton *pushButton = [[UIButton alloc]initWithFrame:CGRectMake(kDeviceWidth-50,5, 40, 40)];
    [pushButton setImage:[UIImage imageNamed:@"pushtext_icn.png"] forState:UIControlStateNormal];
    [pushButton addTarget:self action:@selector(pushViewAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.textTabView addSubview:pushButton];
    
    //字体样式
    self.textStyleScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,VIEW_YH(self.textTabView)+5,kDeviceWidth,150)];
    //字体样式
    NSMutableArray *fontNames = [NSMutableArray new];
    for (NSString *familyName in [UIFont familyNames]) {
        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
            [fontNames addObject:fontName];
        }
    }
    self.fontNameArray = [[NSMutableArray alloc]initWithArray:fontNames];
    for(int j = 0;j< fontNames.count;j++){
        UIButton *textFontbutton = [[UIButton alloc]initWithFrame:CGRectMake(30,40*j,200,40)];
        //UILabel *textFrame = [[UILabel alloc] initWithFrame:CGRectMake(20,0,kDeviceWidth-40, 30)];
        [textFontbutton setTitle:@"INSTAGRID POST" forState:UIControlStateNormal];
        [textFontbutton setTitleColor:RGB(208, 208, 208) forState:UIControlStateNormal];
        [textFontbutton setTitleColor:mainColor forState:UIControlStateSelected];
        textFontbutton.titleLabel.textAlignment = NSTextAlignmentLeft;
        textFontbutton.tag = 1200+j;
        if(j == 0){
            textFontbutton.selected = YES;
        }
        textFontbutton.titleLabel.font = [UIFont fontWithName:[fontNames objectAtIndex:j] size:18.0f];
        [textFontbutton addTarget:self action:@selector(selectTextfontAction:) forControlEvents:UIControlEventTouchUpInside];
        //textFrame.textColor = [UIColor whiteColor];
        //textFontbutton.text
        [self.textStyleScrollView addSubview:textFontbutton];
    }
    //self.fontNames = [fontNames copy];
        self.textFontSelView = [[UIView alloc]initWithFrame:CGRectMake(20,15,10,10)];
        self.textFontSelView.layer.masksToBounds = YES;
        self.textFontSelView.layer.cornerRadius = 5;
        self.textFontSelView.backgroundColor = mainColor;
        [self.textStyleScrollView addSubview:self.textFontSelView ];
    self.textStyleScrollView.contentSize = CGSizeMake(kDeviceWidth,30*fontNames.count);
    [self.textMainView addSubview:self.textStyleScrollView];
    
    
    //字体大小和间距
    self.textAlignmentView = [[UIView alloc]initWithFrame:CGRectMake(0,VIEW_YH(self.textTabView),kDeviceWidth,200)];
    self.textAlignmentView.hidden = YES;
    [self.textMainView addSubview: self.textAlignmentView];
    CGFloat textRectH = 40;
    CGFloat textRectW = 40;
    //60
    CGFloat textRectLine = (VIEW_W(self.textAlignmentView)-textRectW*3)/6;
    for(int i = 0 ;i<3;i++){
        UIButton *rectSelectBut = [[UIButton alloc]initWithFrame:CGRectMake(textRectLine*2+(textRectLine+textRectW)*i,10,textRectW ,textRectH)];
        rectSelectBut.tag = 200+i;
        [rectSelectBut setImage:[UIImage imageNamed:[NSString stringWithFormat:@"textRect%d_nol",i]] forState:UIControlStateNormal];
         [rectSelectBut setImage:[UIImage imageNamed:[NSString stringWithFormat:@"textRect%d_sel",i]] forState:UIControlStateSelected];
        if(i == 1){
            rectSelectBut.selected = YES;
        }
        [rectSelectBut addTarget:self action:@selector(rectSelectAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.textAlignmentView addSubview:rectSelectBut];
    }
    //140
    
    //横间距
    UIImageView *horImageView = [[UIImageView alloc]initWithFrame:CGRectMake(50,60, 30,30)];
    horImageView.image = [UIImage imageNamed:@"fontHor"];
    [self.textAlignmentView addSubview:horImageView];
    
    //UIImage *fontImage = [UIImage imageNamed:@"finishIg"];
    UISlider *HorizontalSlider = [[UISlider alloc] initWithFrame:CGRectMake(VIEW_XW(horImageView)+10,60,VIEW_W(self.textAlignmentView )-150, 30)];
    HorizontalSlider.maximumValue = 160;
    HorizontalSlider.minimumValue = 0;
    HorizontalSlider.value = 0;
    [HorizontalSlider setMinimumTrackTintColor:mainColor];
    //[HorizontalSlider setThumbImage:fontImage forState:UIControlStateNormal];
    [HorizontalSlider addTarget:self action:@selector(HorizontalSliderAction:) forControlEvents:UIControlEventValueChanged];
    [self.textAlignmentView addSubview:HorizontalSlider];
    
    //竖间距
    UIImageView *verImageView = [[UIImageView alloc]initWithFrame:CGRectMake(50,VIEW_YH(HorizontalSlider)+10, 30,30)];
    verImageView.image = [UIImage imageNamed:@"fontVer"];
    [self.textAlignmentView addSubview:verImageView];
    UISlider *verticalSlider = [[UISlider alloc] initWithFrame:CGRectMake(VIEW_XW(verImageView)+10,VIEW_YH(HorizontalSlider)+10,VIEW_W(self.textAlignmentView )-150 , 30)];
    verticalSlider.maximumValue = 100;
    verticalSlider.minimumValue = 0;
    verticalSlider.value = 0;
    [verticalSlider setMinimumTrackTintColor:mainColor];
    //[verticalSlider setThumbImage:fontImage forState:UIControlStateNormal];
    [verticalSlider addTarget:self action:@selector(verticalSliderAction:) forControlEvents:UIControlEventValueChanged];
    [self.textAlignmentView addSubview:verticalSlider];
    
    //字体颜色
    self.textColorView = [[UIView alloc]initWithFrame:CGRectMake(0,VIEW_YH(self.textTabView), ScreenWidth, 200)];
    self.textColorView.hidden = YES;
    [self.textMainView addSubview:self.textColorView ];
    
    UIView *textFontWView = [[UIView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth,50)];
    textFontWView.tag = 99;
    for(int k = 0;k<2;k++){
        UIButton *textWBut = [[UIButton alloc]initWithFrame:CGRectMake((ScreenWidth-80 - 40*2)/2+(40+80)*k,5,40,40)];
        [textWBut setImage:[UIImage imageNamed:[NSString stringWithFormat:@"textFontW%d_nol",k]] forState:UIControlStateNormal];
        [textWBut setImage:[UIImage imageNamed:[NSString stringWithFormat:@"textFontW%d_sel",k]] forState:UIControlStateSelected];
        if(k == 0){
            textWBut.selected = YES;
        }
        [textWBut addTarget:self action:@selector(textFontWAction:) forControlEvents:UIControlEventTouchUpInside];
        textWBut.tag = 50+k;
        [textFontWView addSubview:textWBut];
    }
    [self.textColorView addSubview:textFontWView];
    
    //
    //颜色选择
    CGFloat colorW = 10;
    self.colorScrollView = [[UIScrollView alloc] init];
    self.colorScrollView.frame = CGRectMake(colorW,VIEW_YH(textFontWView),ScreenWidth - colorW*2 , 40);
    self.colorScrollView.backgroundColor = [UIColor clearColor];
    self.colorScrollView.showsHorizontalScrollIndicator = NO;
    self.colorScrollView.userInteractionEnabled = YES;
    self.colorScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.textColorView addSubview:self.colorScrollView];
    CGFloat colorButtW = 25;
    for (int i = 0; i < colorList.count;i++) {
        UIButton * colorButt = [UIButton buttonWithType:UIButtonTypeCustom];
        colorButt.frame = CGRectMake(10+(colorButtW+15)*i,5,colorButtW, colorButtW);
        colorButt.layer.masksToBounds = YES;
        colorButt.layer.cornerRadius = colorButtW/2;
         colorButt.tag = 800+i;
//        if (i == 0) {
//            colorButt.backgroundColor = [UIColor clearColor];
//            [colorButt setImage:[UIImage imageNamed:@"colorpicker"] forState:UIControlStateNormal];
//        }else{
            colorButt.backgroundColor = colorList[i];
            colorButt.layer.borderWidth = 1;
             colorButt.layer.borderColor = [UIColor clearColor].CGColor;
            if(i == 0){
                colorButt.layer.borderColor = [UIColor lightGrayColor].CGColor;
            }
        if(selectColor == colorList[i]) {
            colorButt.layer.borderColor = mainColor.CGColor;
            colorButt.transform = CGAffineTransformMakeScale(1.2, 1.2);
        }
//        }
        [colorButt addTarget:self action:@selector(colorButtTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.colorScrollView addSubview:colorButt ];
    }
    self.colorScrollView.contentSize = CGSizeMake(10+(colorButtW+15)*(colorList.count),40);
    
    //字体透明度
    UISlider *textAlpSlider = [[UISlider alloc] initWithFrame:CGRectMake(50,VIEW_YH(self.colorScrollView)+10,VIEW_W(self.textColorView )-100, 30)];
    textAlpSlider.maximumValue = 1;
    textAlpSlider.minimumValue = 0;
    textAlpSlider.value = 1;
    [textAlpSlider setMinimumTrackTintColor:mainColor];
    //[HorizontalSlider setThumbImage:fontImage forState:UIControlStateNormal];
    [textAlpSlider addTarget:self action:@selector(textAlpSliderAction:) forControlEvents:UIControlEventValueChanged];
    [self.textColorView addSubview:textAlpSlider];
    
}
#pragma mark --- 添加新的编辑框

-(void)textFileView{
    [self textViewisHidden];
    EraserCLTextView *textView = [[EraserCLTextView alloc]initWithFrame:CGRectMake(0, 0, 132, 132)];
    textView.delegate = self;
    CGFloat ratio = MIN( (0.2 *self.editImageView.width) / textView.width, (0.05 * self.editImageView.height) /textView.height);
    [textView setScale:ratio];
    [self.editImageView addSubview:textView];
    [textView setActiveTextView];
    textView.font =  [UIFont fontWithName:[self.fontNameArray firstObject] size:20.0f];
    textView.text = NSLocalizedString(@"Tap to edit",nil);
    //[textView sizeToFitWithMaxWidth:self.MainImageView.frame.size.width-50 lineHeight:0.2*self.MainImageView.frame.size.height-20];
    [textView sizeToFitWithMaxWidth:self.editImageView.frame.size.width-80 lineHeight:0.2*self.editImageView.frame.size.height-50];
    [textView  clearLabelStyle];
    [textView  drawLabel];
    //textView.strokeColor = _settingView.selectedBorderColor;
    //textView.strokeWidth = _settingView.selectedBorderWidth;
    //textView.font = _settingView.selectedFont;
    textView.center = CGPointMake(VIEW_W(self.editImageView)-VIEW_W(textView)/2, VIEW_H(self.editImageView)-VIEW_H(textView)/2);
    [self.textViewArray addObject:textView];
    self.addNewTextView = textView;
}
#pragma mark ------ 隐藏编辑框上的编辑按钮
- (void)textViewisHidden{
    for(int i =0;i < self.textViewArray.count;i++){
        EraserCLTextView *textView = self.textViewArray[i];
        textView.deleteButton.hidden = YES;
        textView.mirrorButton.hidden = YES;
        textView.circleView.hidden = YES;
        textView.label.layer.borderColor = [UIColor clearColor].CGColor;
        textView.userInteractionEnabled = YES;
    }
}
#pragma mark ------ 弹出键盘
- (void)beginTextEditting
{
    [self showSettingViewWithMenuIndex:0];
    [_settingView becomeFirstResponder];
}
- (void)showSettingViewWithMenuIndex:(NSInteger)index
{
    if(_settingView.hidden){
        _settingView.hidden = NO;
        [_settingView showSettingMenuWithIndex:index animated:NO];
    }
    else{
        [_settingView showSettingMenuWithIndex:index animated:YES];
    }
}
#pragma mark ------ 隐藏键盘
- (void)pushedButton:(UIButton*)button
{
    [_settingView resignFirstResponder];
    _settingView.hidden = YES;
}
#pragma mark- EraserCLTextViewdelegate
- (void)tapSelectTextView:(UIView *)textView{
    [self textViewisHidden];
    self.addNewTextView = (EraserCLTextView *)textView;
    self.addNewTextView.deleteButton.hidden = NO;
    self.addNewTextView.mirrorButton.hidden = NO;
    self.addNewTextView.circleView.hidden = NO;
    self.addNewTextView.label.layer.borderColor = mainColor.CGColor;
    self.addNewTextView.userInteractionEnabled = YES;
    
//    if( [self.addNewTextView.text isEqualToString:NSLocalizedString(@"Tap to edit",nil)]){
//        self.addNewTextView.text = @"";
//    }
//    _settingView.selectedText = self.addNewTextView.text;
//    if(_settingView.hidden){
//        _settingView.hidden = NO;
//        [self beginTextEditting];
//    }
}
//删除某一个编辑框 并隐藏键盘款
- (void)deleteTextView:(UIView *)textView{
    [self.textViewArray removeObject:textView];
    [self.addNewTextView removeFromSuperview];
    self.addNewTextView = nil;
    [_settingView resignFirstResponder];
    _settingView.hidden = YES;
}
#pragma mark- Setting view delegate

- (void)textSettingView:(GhostLensTextCustomSettingView *)settingView didChangeText:(NSString *)text
{
    //    self.isUsing = YES;
    self.addNewTextView.text = text;
    [self.addNewTextView sizeToFitWithMaxWidth:self.editImageView.width lineHeight:0.2*self.editImageView.height];
    [self.addNewTextView drawLabel];
}
#pragma mark ---- 字体样式
- (void)selectTextfontAction:(UIButton *)sender{
    if(self.textFontSelTag == sender.tag-1200){
        return;
    }
    self.textFontSelView.frame = CGRectMake(20,sender.frame.origin.y+15,10,10);
    UIButton *but = (UIButton *)[self.view viewWithTag:self.textFontSelTag+1200];
    but.selected = NO;
    sender.selected = YES;
    self.textFontSelTag = sender.tag - 1200;
    UIFont *font =  [UIFont fontWithName:[self.fontNameArray objectAtIndex:self.textFontSelTag] size:14];
    self.addNewTextView.font = font;
    [self.addNewTextView sizeToFitWithMaxWidth:self.addNewTextView.width lineHeight:self.addNewTextView.height];
    self.addNewTextView.label.adjustsFontSizeToFitWidth = YES;
    [self.addNewTextView clearLabelStyle];
    [self.addNewTextView drawLabel];
}
#pragma mark ---- 字体位置
-(void)rectSelectAction:(UIButton *)sender{
    for(UIView *view in self.textAlignmentView.subviews){
        if([view isKindOfClass:[UIButton class]]){
            UIButton *but = (UIButton *)view;
            but.selected = NO;
        }
    }
    NSInteger agIndex = sender.tag - 200;
    sender.selected = YES;
    switch (agIndex) {
        case 0:
            //左
            [self.addNewTextView setTextAlignment:NSTextAlignmentLeft];
            break;
        case 1:
            //中 默认
            [self.addNewTextView setTextAlignment:NSTextAlignmentCenter];
            break;
        case 2:
            //左
            [self.addNewTextView setTextAlignment:NSTextAlignmentRight];
            break;
        default:
            break;
    }
    [self.addNewTextView clearLabelStyle];
    [self.addNewTextView drawLabel];
}
#pragma mark ---- 字体竖间距调节
- (void)verticalSliderAction:(UISlider *)sender{
    [self.addNewTextView setLetterSpace:sender.value];
    //[self.addNewTextView sizeToFitWithMaxWidth:self.videoView.width lineHeight:0.2*self.videoView.height];
    [self.addNewTextView drawLabel];
}
#pragma mark ---- 字体横间距调节
- (void)HorizontalSliderAction:(UISlider *)sender{
    [self.addNewTextView setLineSpace:sender.value];
    //[self.addNewTextView sizeToFitWithMaxWidth:self.videoView.width lineHeight:0.2*self.videoView.height];
    [self.addNewTextView drawLabel];
}
#pragma mark ---- 字体加宽
-(void)textFontWAction:(UIButton *)sender{
    UIView *WView = [self.view viewWithTag:99];
    for(UIView *view in WView.subviews){
        if([view isKindOfClass:[UIButton class]]){
            UIButton *but = (UIButton *)view;
            but.selected = NO;
        }
    }
    sender.selected = YES;
    NSInteger textWtag = sender.tag -50;
    if(textWtag == 0){
      //普通
        //[self.addNewTextView setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20]];
        [self.addNewTextView setFont:[UIFont systemFontOfSize:20]];
        [self.addNewTextView sizeToFitWithMaxWidth:self.addNewTextView.label.width lineHeight:self.addNewTextView.label.height];
    }else{
      //加粗
        //self.addNewTextView.strokeWidth = 0.1;
        //self.addNewTextView.strokeColor = selectColor;
        //[self.addNewTextView showWhichLabel:0];
        [self.addNewTextView setFont:[UIFont fontWithName:@"Helvetica-Bold" size:20]];
        [self.addNewTextView sizeToFitWithMaxWidth:self.addNewTextView.width lineHeight:self.addNewTextView.height];
    }
    self.addNewTextView.label.adjustsFontSizeToFitWidth = YES;
    [self.addNewTextView clearLabelStyle];
    [self.addNewTextView drawLabel];
}

#pragma mark ---- 字体颜色
-(void)colorButtTapped:(UIButton *)sender{
    for (UIView * subView in self.colorScrollView.subviews) {
        subView.transform = CGAffineTransformIdentity;
        subView.layer.borderColor = [UIColor clearColor].CGColor;
        if(subView.tag == 800){
            subView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
    }
//    if(sender.tag == 800){
       //渐变颜色
//    }else{
        sender.transform = CGAffineTransformMakeScale(1.2, 1.2);
        sender.layer.borderColor = mainColor.CGColor;
        selectColor = sender.backgroundColor;
//    }
    self.addNewTextView.fillColor = selectColor;
    [self.addNewTextView clearLabelStyle];
    [self.addNewTextView drawLabel];
}
#pragma mark ---- 字体透明度
-(void)textAlpSliderAction:(UISlider *)sender{
    [self.addNewTextView setTextAlpha:sender.value];
}

- (void)textTabAction:(UIButton *)sender{
    for(UIView *view in self.tabTextScroll.subviews ){
        if([view isKindOfClass:[UIButton class]]){
           UIButton *but = (UIButton *)view;
            but.selected = NO;
        }
    }
    NSInteger seleTag = sender.tag - 900;
    //self.textTabSelectTag = sender.tag - 900;
    switch (seleTag) {
        case 0:
            if(self.addNewTextView){
            if( [self.addNewTextView.text isEqualToString:NSLocalizedString(@"Tap to edit",nil)]){
                self.addNewTextView.text = @"";
            }
            _settingView.selectedText = self.addNewTextView.text;
            [self beginTextEditting];
            }
            break;
            
        case 1:
            sender.selected = YES;
            if(seleTag != self.textTabSelectTag){
                self.textStyleScrollView.hidden = NO;
                self.textAlignmentView.hidden = YES;
                self.textColorView.hidden = YES;
            }
            break;
            
        case 2:
             sender.selected = YES;
            if(seleTag != self.textTabSelectTag){
                self.textStyleScrollView.hidden = YES;
                self.textAlignmentView.hidden = NO;
                self.textColorView.hidden = YES;
            }
            break;
            
        case 3:
             sender.selected = YES;
            if(seleTag != self.textTabSelectTag){
                self.textStyleScrollView.hidden = YES;
                self.textAlignmentView.hidden = YES;
                self.textColorView.hidden = NO;
            }
            break;
            
        case 4:
            [self textFileView];
            break;
        default:
            break;
    }
    self.textTabSelectTag = seleTag;
}
- (void)pushViewAction:(UIButton *)sender{
    self.textMainView.hidden = YES;
    _settingView.hidden = YES;
    if(self.textViewArray.count>0 && self.selectTabTag == 3){
        NSMutableArray *temparray = [[NSMutableArray alloc]initWithArray:self.textViewArray];
        for(int i =0;i < temparray.count;i++){
            EraserCLTextView *textView = temparray[i];
            textView.deleteButton.hidden = YES;
            textView.mirrorButton.hidden = YES;
            textView.circleView.hidden = YES;
            textView.label.layer.borderWidth = 0;
            textView.userInteractionEnabled = NO;
            if([textView.text isEqualToString:@""] || textView.text==nil||[textView.text isEqualToString:NSLocalizedString(@"Tap to edit",nil)]){
                [self.textViewArray removeObject:textView];
                [textView removeFromSuperview];
            }
        }
    }
}
- (void)selectFilterAction:(UIButton *)sender{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]) {
        [self pushUpgradeView];
        return;
    }
    
    if (self.selectFilterTag == sender.tag-300) {
        return;
    }
    self.filterSelectView.center = sender.center;
    self.selectFilterTag = sender.tag-300;
    UIImage *imageFinished = [[ImagePostBaseFilter sharedFilter]filteredImage:self.filterBaseImage withFilterName:newNameArray[self.selectFilterTag] withFilterVersion2:self.selectFilterTag+1];
    self.editImage = imageFinished;
    self.editImageView.image = imageFinished;
    //[self showFilterImageView];
}
//- (void)showFilterImageView{
//        UIImage *imageFinished = [[ImagePostBaseFilter sharedFilter]filteredImage:imageFinished withFilterName:newNameArray[selectFilterTag] withFilterVersion2:selectFilterTag+1];
//}
- (void)createCollectImageView{
    //加载UI
    CGRect collectRect = CGRectMake(0,VIEW_YH(self.natView)+5, ScreenWidth,ScreenHeight-VIEW_YH(self.natView)-VIEW_H(self.tabActionView)-10);

    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc]init];
    //flow.headerReferenceSize =CGSizeMake(ScreenWidth,45);
    cellSize = floor(([UIScreen mainScreen].bounds.size.width-9)/4);
    separation = ([UIScreen mainScreen].bounds.size.width - cellSize*4)/5;
    self.collectionView = [[UICollectionView alloc]initWithFrame:collectRect collectionViewLayout:flow];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    [self.view addSubview:self.collectionView];
    self.collectionView.hidden = YES;
    [self.collectionView registerClass:[HomeCollectionViewCells class] forCellWithReuseIdentifier:@"HomeCollectionViewCells"];
}
- (void)getAllPhotosFromCamera{
    self.requestOptions = [[PHImageRequestOptions alloc] init];
    self.requestOptions.resizeMode   = PHImageRequestOptionsResizeModeFast;
    self.requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeFastFormat;
    self.requestOptions.synchronous = true;
    PHFetchOptions *fetchOptions = [PHFetchOptions new];
    fetchOptions.sortDescriptors =  @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    self.result = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
    self.count = self.result.count;
    self.imageArray = [NSMutableArray arrayWithCapacity:self.count];
    
    for (int i=0; i<self.count; i++) {
        [self.imageArray addObject:[NSNull null]];
    }
    dispatch_async(dispatch_get_main_queue(),^{
        //[self.collectionView reloadData];
        if (self.count <= 0) {
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"There are no photos in your album.", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
        }
    });
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(cellSize, cellSize);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.count;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return separation;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return separation;
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(separation, separation, separation, separation); // top, left, bottom, right
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeCollectionViewCells *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeCollectionViewCells" forIndexPath:indexPath];
    cell.videoView.hidden = YES;
    id object = self.imageArray[indexPath.row];
    CGSize size;
    if([object isKindOfClass:[UIImage class]]) {
            cell.imgView.image = self.imageArray[indexPath.row];
    }
    else {
            PHAsset *set = self.result[indexPath.row];
            size = CGSizeMake(cellSize, cellSize);
            [[PHImageManager defaultManager] requestImageForAsset:set
                                                       targetSize:size
                                                      contentMode:PHImageContentModeDefault
                                                          options:self.requestOptions
                                                    resultHandler:^void(UIImage *image, NSDictionary *info) {
                                                        if(set.mediaType == PHAssetMediaTypeVideo){
                                                            cell.imgView.image = image;
                                                        }else{
                                                            image = [PostImagehelper squareImageFromImage:image scaledToSize:cellSize];
                                                            if (indexPath.row < self.imageArray.count) {
                                                                [self.imageArray replaceObjectAtIndex:indexPath.row withObject:image];
                                                                cell.imgView.image = self.imageArray[indexPath.row];
                                                            }else
                                                            {
                                                                [self.imageArray addObject:image];
                                                                cell.imgView.image = image;
                                                            }
                                                            
                                                        }
                                                    }];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
    requestOptions.resizeMode   = PHImageRequestOptionsResizeModeNone;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.synchronous = true;
    PHAsset *phAsset = self.result[indexPath.row];
    [[PHImageManager defaultManager] requestImageForAsset:phAsset
                                               targetSize:PHImageManagerMaximumSize
                                              contentMode:PHImageContentModeDefault
                                                  options:requestOptions
                                            resultHandler:^void(UIImage *image, NSDictionary *info) {
                                                if(self.selectTabTag == 0){
                                                    ImageReplaceGridViewController *replace = [[ImageReplaceGridViewController alloc]init];
                                                    replace.delegate = self;
                                                    replace.baseImage = self.editImage;
                                                    replace.gridCount = self.gridCount;
                                                    replace.replaceImage = image;
                                                    replace.selectHome = self.selectHome;
                                                [self.navigationController pushViewController:replace animated:NO];
                                                }else{
                                                    ImageAddMaskImageViewController *addImageView = [[ImageAddMaskImageViewController alloc]init];
                                                    addImageView.delegate = self;
                                                    addImageView.baseImage = self.editImage;
                                                    addImageView.gridCount = self.gridCount;
                                                    addImageView.addMaskmage = image;
                                                    addImageView.selectHome = self.selectHome;
                                                    [self.navigationController pushViewController:addImageView animated:NO];
                                                }
                                            }];
}
#pragma mark --- 选择相机
-(void)selectCameraAction{
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *cameraPicker = [UIImagePickerController new];
            cameraPicker.delegate = self;
            cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            cameraPicker.allowsEditing = NO;
            [self presentViewController:cameraPicker animated:YES completion:nil];
        }
}

#pragma mark ---- ImageReplaceGridViewControllerDelegate
- (void)finishImageReplaceDeal:(UIImage *)resultImage{
    if(resultImage){
         self.editImage = resultImage;
         self.editImageView.image = resultImage;
    }
    UIButton *button = (UIButton *)[self.view viewWithTag:102];//返回时默认选择滤镜
    [self selectTabAction:button];
}

#pragma mark ---- ImageAddMaskImageViewControllerDelegate
- (void)finishImageAddMaskImageDeal:(UIImage *)resultImage{
    if(resultImage){
        self.editImage = resultImage;
        self.editImageView.image = resultImage;
    }
    UIButton *button = (UIButton *)[self.view viewWithTag:102];//返回时默认选择滤镜
    [self selectTabAction:button];
}

- (void)cancelAction{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)doneAction{
    UIImage *croppedImage = self.editImage;
    NSMutableArray *temparray = [[NSMutableArray alloc]initWithArray:self.textViewArray];
    for(int k =0;k < temparray.count;k++){
        EraserCLTextView *textView = temparray[k];
        textView.hidden  = NO;
        if([textView.text isEqualToString:NSLocalizedString(@"Tap to edit",nil)] ||[textView.text isEqualToString:@""]){
            [self.textViewArray removeObject:textView];
            [textView removeFromSuperview];
        }
    }
    if(self.textViewArray.count >0){
        //[[EraserSigtonUserInfo shareUserInfoArray] showProgressView:self.view labelText:nil];
        [self textViewisHidden];
        CGSize imgOutSize  = croppedImage.size;
        self.editImageView.transform = CGAffineTransformIdentity;
        CGFloat scalex = imgOutSize.width/self.editImageView.frame.size.width;
        CGFloat scaley = imgOutSize.height/self.editImageView.frame.size.height;
        if (scalex > scaley) {
            scalex = scaley;
        }else
        {
            scaley = scalex;
        }
        if (scalex < 1) {
            scalex = 1;
        }
        if (scaley < 1) {
            scaley = 1;
        }
        UIView * newMainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.editImageView.frame.size.width*scalex, self.editImageView.frame.size.height*scaley)];
        newMainView.backgroundColor = [UIColor clearColor];
        float tmpalpha;
        for (int i = 0; i < self.textViewArray.count; i++) {
            UIView * subView = [self.textViewArray objectAtIndex:i];
            tmpalpha = subView.alpha;
            subView.alpha = 1;
            [subView removeFromSuperview];
            [newMainView addSubview:subView];
            CGPoint pt = subView.center;
            subView.transform = CGAffineTransformConcat(subView.transform,CGAffineTransformMakeScale(scalex, scaley)) ;
            subView.center = CGPointMake(pt.x*scalex, pt.y*scaley);
        }
        UIImage *patternIg = [PostImagehelper imageFromView:newMainView];
        //image = [ImageHelper addImageLogo:image image:patternIg rect:CGRectMake(0, 0,image.size.width,image.size.height)];
        if (patternIg) {
            croppedImage = [PostImagehelper addImageLogo:croppedImage image:patternIg rect:CGRectMake(0, 0,patternIg.size.width ,patternIg.size.height)];
        }
    }
    [self.textViewArray removeAllObjects];
    if(self.isDomestic == 1){
        if (![[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]){
            croppedImage = [PostImagehelper createWithWaterMarkonImage:croppedImage];
        }
        //UIImage *showIg = [self imageFromView:self.viewimg];
        SudokuViewController *sudoku = [[SudokuViewController alloc]init];
        //sudoku.showImage = showIg;
        sudoku.gridCount = self.gridCount;
        sudoku.intShareType = 1;
        sudoku.imageSource = croppedImage;
        [self.navigationController pushViewController:sudoku animated:YES];
    }else{
        //视频的水印在后面添加
        VideoSelectViewController *video = [[VideoSelectViewController alloc] init];
        video.imageSource = croppedImage;
        video.gridCount = self.gridCount;
        [self.navigationController pushViewController:video animated:YES];
    }
}

#pragma mark image picker delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
        UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
        [picker dismissViewControllerAnimated:YES completion:^{
            if(self.selectTabTag == 0){
                ImageReplaceGridViewController *replace = [[ImageReplaceGridViewController alloc]init];
                replace.delegate = self;
                replace.baseImage = self.editImage;
                replace.gridCount = self.gridCount;
                replace.replaceImage = image;
                replace.selectHome = self.selectHome;
                [self.navigationController pushViewController:replace animated:NO];
            }else{
                ImageAddMaskImageViewController *addImageView = [[ImageAddMaskImageViewController alloc]init];
                addImageView.delegate = self;
                addImageView.baseImage = self.editImage;
                addImageView.gridCount = self.gridCount;
                addImageView.addMaskmage = image;
                addImageView.selectHome = self.selectHome;
                [self.navigationController pushViewController:addImageView animated:NO];
            }
        }];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
}
#pragma mark --- 更新视图
- (void)pushUpgradeView{
    //    if([[NSUserDefaults standardUserDefaults] boolForKey:@"enterFromPush"] && ![[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY]){
    self.objUpgradeViewController = [[UpgradeViewController alloc] initWithNibName:@"UpgradeViewController" bundle:nil];
    //self.objUpgradeViewController.delegate = self;
    [RJAnimation bounceAddSubViewToParentView:self.view ChildView:self.objUpgradeViewController.view];
    //        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"enterFromPush"];
    //    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
