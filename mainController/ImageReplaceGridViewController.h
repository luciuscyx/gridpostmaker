//
//  ImageReplaceGridViewController.h
//  Instagrid Post
//
//  Created by macbookair on 5/8/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ImageReplaceGridViewControllerDelegate <NSObject>

- (void)finishImageReplaceDeal:(UIImage *)resultImage;

@end

@interface ImageReplaceGridViewController : UIViewController
@property (nonatomic,strong)UIImage *baseImage;
@property (nonatomic,strong)UIImage *replaceImage;
@property (nonatomic,weak)id <ImageReplaceGridViewControllerDelegate>delegate;
@property (nonatomic,assign)int gridCount;
@property (nonatomic,assign)BOOL selectHome;
@end

NS_ASSUME_NONNULL_END
