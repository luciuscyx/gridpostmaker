//
//  ImageAddMaskImageViewController.h
//  Instagrid Post
//
//  Created by macbookair on 6/8/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ImageAddMaskImageViewControllerDelegate <NSObject>

- (void)finishImageAddMaskImageDeal:(UIImage *)resultImage;

@end
@interface ImageAddMaskImageViewController : UIViewController
@property (nonatomic,strong)UIImage *baseImage;
@property (nonatomic,strong)UIImage *addMaskmage;
@property (nonatomic,weak)id <ImageAddMaskImageViewControllerDelegate>delegate;
@property (nonatomic,assign)int gridCount;
@property (nonatomic,assign)BOOL selectHome;

@end

NS_ASSUME_NONNULL_END
