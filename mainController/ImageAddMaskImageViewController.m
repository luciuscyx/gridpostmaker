//
//  ImageAddMaskImageViewController.m
//  Instagrid Post
//
//  Created by macbookair on 6/8/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import "ImageAddMaskImageViewController.h"
#import "PostImagehelper.h"
#import "UIImage+EKTilesMaker.h"
#import "EditBackMImageView.h"
#import "ImageHelper.h"
@interface ImageAddMaskImageViewController ()<EditBackMImageViewDelegate>
@property (nonatomic,strong)UIImageView *baseImageView; //底层视图
@property (nonatomic,strong)UIImageView *maskImageView; //方格视图
@property (nonatomic,strong)UIImageView *gridLineView; //方格视图
@property (nonatomic,strong)UIView *tabView;
@property (nonatomic,strong)UIView *natView;
@property (nonatomic,strong)EditBackMImageView *backMImageView;
@end

@implementation ImageAddMaskImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createUI];
}

- (void)createUI{
    [self createTabView];
    [self createNatView];
    [self createMainView];
}
- (void)createTabView{
    self.tabView = [[UIView alloc]initWithFrame:CGRectMake(0,ScreenHeight - 55 - KDNavH,ScreenWidth, 55+KDNavH)];
    UIImageView *LineIg = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, ScreenWidth,2)];
    LineIg.image = [UIImage imageNamed:@"tabLine"];//[UIColor lightGrayColor];
    [self.tabView addSubview:LineIg];
    
    UIButton *cancelBut = [[UIButton alloc]initWithFrame:CGRectMake(0, 2, ScreenWidth/2,52+KDNavH)];
    [cancelBut setTitle:NSLocalizedString(@"Cancel",nil) forState:UIControlStateNormal];
    cancelBut.backgroundColor = [UIColor whiteColor];
    [cancelBut setTitleColor:mainColor forState:UIControlStateNormal];
    cancelBut.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [cancelBut addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self.tabView addSubview:cancelBut];
    
    UIImageView *finishImageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2, 2, ScreenWidth/2,52+KDNavH)];
    finishImageView.userInteractionEnabled = YES;
    finishImageView.image = [UIImage imageNamed:@"finishIg"];
    [self.tabView addSubview:finishImageView];
    
    UIButton *finishBut = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth/2, 2, ScreenWidth/2,52+KDNavH)];
    finishBut.backgroundColor = [UIColor clearColor];
    //finishBut.imageView.image = [UIImage imageNamed:@"finishIg"];
    [finishBut setTitle:NSLocalizedString(@"Done",nil) forState:UIControlStateNormal];
    //[finishBut setImage:[UIImage imageNamed:@"finishIg"] forState:UIControlStateNormal]; ;
    [finishBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [finishBut addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    finishBut.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [self.tabView addSubview:finishBut];
    
    [self.view addSubview:self.tabView];
}
- (void)createNatView{
    //滤镜的tabView
    self.natView = [[UIView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth,60+KDNavH)];
    [self.view addSubview:self.natView];
    //返回按钮
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setFrame:CGRectMake(10, 15+KDNavH, 35, 35)];
    [btnBack setImage:[UIImage imageNamed:@"backBlack_icn"] forState:UIControlStateNormal];
    //[self.btnBack setImage:[UIImage imageNamed:@"back_sel"] forState:UIControlStateHighlighted];
    btnBack.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [btnBack addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:btnBack];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(ScreenWidth/2-100,15+KDNavH,200, 35)];
    titleLabel.text  = NSLocalizedString(@"Add image",nil);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [self.natView addSubview:titleLabel];
    
    //确定按钮
    UIButton *doneBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [doneBack setFrame:CGRectMake(ScreenWidth - 45, 15+KDNavH, 35, 35)];
    [doneBack setImage:[UIImage imageNamed:@"doneSel_icn"] forState:UIControlStateNormal];
    //[self.btnBack setImage:[UIImage imageNamed:@"back_sel"] forState:UIControlStateHighlighted];
    doneBack.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [doneBack addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:doneBack];
    
    UIImageView *natLineIg = [[UIImageView alloc]initWithFrame:CGRectMake(0,VIEW_H(self.natView)-2, ScreenWidth,2)];
    natLineIg.image = [UIImage imageNamed:@"tabLine"];//[UIColor lightGrayColor];
    [self.natView addSubview:natLineIg];
}
- (void)createMainView{
    CGRect mainRect;
    CGFloat h = ScreenWidth;
    if(self.selectHome){
        h = WINDOW.bounds.size.width/self.gridCount;
    }else{
        h = WINDOW.bounds.size.width*self.gridCount/3;
    }
    CGFloat maxh = ScreenHeight - VIEW_H(self.natView)-10 - VIEW_H(self.tabView);//self.colorScrollView.frame.origin.y-self.btnBack.frame.origin.y-self.btnBack.frame.size.height-6;
    CGFloat orgy = VIEW_YH(self.natView)+5;//self.btnBack.frame.origin.y+self.btnBack.frame.size.height+3;
    if (h > maxh) {
        CGFloat w = WINDOW.bounds.size.width*maxh/h;
        mainRect = CGRectMake((WINDOW.bounds.size.width-w)*0.5f, orgy, w, maxh);
    }else
    {
        mainRect = CGRectMake(0, orgy+(maxh-h)*0.5f, WINDOW.bounds.size.width, h);
    }
    self.baseImageView = [[UIImageView alloc]initWithFrame:mainRect];
    self.baseImageView.userInteractionEnabled = YES;
    self.baseImageView.image = self.baseImage;
    [self.view addSubview:self.baseImageView];
    
    CGSize s,cmpsize ;
    CGRect rect = mainRect;
    UIImageView *imageView2 = [UIImageView new];
    imageView2.image = self.addMaskmage;
    cmpsize = self.addMaskmage.size;
    if (rect.size.width > rect.size.height) {
        s= CGSizeMake(rect.size.height*cmpsize.width/(2*cmpsize.height),rect.size.height/2);
    }else
    {
        s= CGSizeMake(rect.size.width/2,rect.size.width*cmpsize.height/(2*cmpsize.width));
    }
    self.backMImageView = [[EditBackMImageView alloc] initWithFrame:CGRectMake((rect.size.width-s.width)/2, (rect.size.height-s.height)/2, s.width, s.height)];
    self.backMImageView.userInteractionEnabled = YES;
    self.backMImageView.delegate = self;
    self.backMImageView.contentView = imageView2;
    //[self hiddenStickerEdit];
    [self.backMImageView showEditingHandles];
    [self.baseImageView addSubview: self.backMImageView];
    
    self.gridLineView = [[UIImageView alloc]initWithFrame:mainRect];
    if(self.selectHome){
        self.gridLineView.image = [PostImagehelper getHomeImageByGridCount:self.gridCount frame:mainRect] ;
    }else{
        self.gridLineView.image = [PostImagehelper getImageByGridCount:self.gridCount frame:mainRect] ;
    }
    //self.gridLineView.image = [PostImagehelper getImageByGridCount:self.gridCount frame:mainRect] ;
    [self.view addSubview:self.gridLineView];
}

#pragma mark ---- EditBackMImageView代理方法

- (void)stickerViewDidClose:(EditBackMImageView *)sticker
{
 // [self.matchClothesArray removeObject:sticker];
}

-(void)stickerViewDidBeginEditing:(EditBackMImageView *)sticker
{
//    [self hiddenStickerEdit];
//    [sticker showEditingHandles];
}
- (void)cancelAction{
    [self.navigationController popViewControllerAnimated:nil];
}
- (void)doneAction{
    [self.backMImageView hideEditingHandles];
    CGSize imgOutSize  = self.baseImage.size;
    //self.MainImageView.transform = CGAffineTransformIdentity;
    CGFloat scalex = imgOutSize.width/self.baseImageView.frame.size.width;
    CGFloat scaley = imgOutSize.height/self.baseImageView.frame.size.height;
    if (scalex > scaley) {
        scalex = scaley;
    }else
    {
        scaley = scalex;
    }
    if (scalex < 1) {
        scalex = 1;
    }
    if (scaley < 1) {
        scaley = 1;
    }
    UIView * newMainView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.baseImageView.frame.size.width*scalex, self.baseImageView.frame.size.height*scaley)];
    newMainView.backgroundColor = [UIColor clearColor];
    UIView * subView = self.backMImageView;
    float tmpalpha;
    tmpalpha = subView.alpha;
    subView.alpha = 1;
    [subView removeFromSuperview];
    [newMainView addSubview:subView];
    CGPoint pt = subView.center;
    subView.transform = CGAffineTransformConcat(subView.transform,CGAffineTransformMakeScale(scalex, scaley)) ;
    subView.center = CGPointMake(pt.x*scalex, pt.y*scaley);
    UIImage *maskIg = [ImageHelper imageFromView:newMainView];
    self.baseImage = [ImageHelper addImageLogo:self.baseImage image:maskIg rect:CGRectMake(0, 0,maskIg.size.width ,maskIg.size.height)];
    [self.delegate finishImageAddMaskImageDeal:self.baseImage];
    [self.navigationController popViewControllerAnimated:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
