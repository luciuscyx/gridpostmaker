//
//  CutShareViewController.m
//  Instagrid Post
//
//  Created by chenteng on 2018/4/28.
//  Copyright © 2018年 RJLabs. All rights reserved.
//

#import "CutShareViewController.h"
#import "SHKActivityIndicator.h"
@interface CutShareViewController ()<UIDocumentInteractionControllerDelegate>
@property (nonatomic,strong)UIImageView *mainImageView;
@property (nonatomic,strong)UIView *natView;
@property (nonatomic,strong)UIView *tabView;

@end

@implementation CutShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImageView *mainImage = [[UIImageView alloc]initWithFrame:self.view.bounds];
    mainImage.backgroundColor = [UIColor whiteColor];
    //mainImage.image = [UIImage imageNamed:@"bg"];
    [self.view addSubview:mainImage];
    [self loadMainView];
    [self createTabView];
}
-(BOOL)prefersStatusBarHidden {
    return YES;
}
-(void)loadMainView{
    self.natView = [[UIView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth, 60+KDNavH)];
    [self.view addSubview:self.natView];
    //添加左按钮
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnBack setFrame:CGRectMake(8,15+KDNavH, 35, 35)];
    [btnBack setImage:[UIImage imageNamed:@"backBlack_icn"] forState:UIControlStateNormal];
    // [btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
    btnBack.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [btnBack addTarget:self action:@selector(btnBackPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:btnBack];
    
    //添加右按钮
    UIButton *btnNext = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnNext setFrame:CGRectMake(ScreenWidth - 45,15+KDNavH, 35, 35)];
    [btnNext setImage:[UIImage imageNamed:@"backHome_new.png"] forState:UIControlStateNormal];
    // [btnNext setImage:[UIImage imageNamed:@"next_sel"] forState:UIControlStateHighlighted];
    btnNext.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [btnNext addTarget:self action:@selector(homeAction) forControlEvents:UIControlEventTouchUpInside];
    [self.natView addSubview:btnNext];
    
    //UILabel * instructionLabel = [[UILabel alloc] init];
    //CGFloat orgx = btnBack.frame.origin.x+btnBack.frame.size.width+10;
    /*instructionLabel.frame = CGRectMake(orgx, btnBack.frame.origin.y, btnNext.frame.origin.x-orgx-10, btnBack.frame.size.height+10);
    instructionLabel.textColor = [UIColor whiteColor];
    instructionLabel.text = @"TAP EACH PIC IN THE FOLLWOING ORDER TO SHARE IT ON INSTAGRAM";
    instructionLabel.numberOfLines = 0;
    instructionLabel.textAlignment = NSTextAlignmentCenter;
    // [instructionLabel sizeToFit];
    instructionLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14];
    [self.view addSubview:instructionLabel];*/
    
    self.mainImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0,(ScreenHeight-ScreenWidth)/2 ,ScreenWidth, ScreenWidth)];
    //[self.mainImageView addTarget:self action:@selector(playAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.mainImageView];
    if(self.shareImage){
        self.mainImageView.image =  self.shareImage;
    }else{
        self.mainImageView.image =  [self getVideoPreViewImage:self.shareVurl];
        UIImageView * imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"plays.png"]];
        imgView.frame = CGRectMake(0, 0, 40, 40);
        imgView.center = CGPointMake(self.mainImageView.frame.size.width/2, self.mainImageView.frame.size.height/2);
        [self.mainImageView addSubview:imgView];
    }
    
//    UIButton *shareBut = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth/2-100,ScreenHeight-40-10-KDNavH, 200,40)];
//    [shareBut setTitle:NSLocalizedString(@"SHARE TO INSTAGRAM",nil) forState:UIControlStateNormal];
//    [shareBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    shareBut.backgroundColor = [UIColor blackColor];
//    shareBut.titleLabel.font = [UIFont systemFontOfSize:15];
//    shareBut.layer.cornerRadius = 10;
//    shareBut.layer.masksToBounds = YES;
//    [shareBut addTarget:self action:@selector(playAction:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:shareBut];
//    //保存相册说明
//    UILabel *infoLabel = [[UILabel alloc]initWithFrame:CGRectMake(20,ScreenHeight-10-10-KDNavH-VIEW_H(shareBut)-50,ScreenWidth-40,50)];
//    infoLabel.textColor = [UIColor whiteColor];
//    infoLabel.text = NSLocalizedString(@"THIS RESULT HAS BEEN SAVED TO YOUR ALBUM!",nil);
//    infoLabel.numberOfLines = 0;
//    infoLabel.textAlignment = NSTextAlignmentCenter;
//
//    // [instructionLabel sizeToFit];
//    infoLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
//    [self.view addSubview:infoLabel];
}
- (void)createTabView{
    self.tabView = [[UIView alloc]initWithFrame:CGRectMake(0,ScreenHeight - 55 - KDNavH,ScreenWidth, 55+KDNavH)];
    UIImageView *LineIg = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, ScreenWidth,2)];
    LineIg.image = [UIImage imageNamed:@"tabLine"];//[UIColor lightGrayColor];
    [self.tabView addSubview:LineIg];
    
    UIButton *instaBut = [[UIButton alloc]initWithFrame:CGRectMake(0, 2, ScreenWidth/2,52+KDNavH)];
    [instaBut setTitle:@"Instagram分享" forState:UIControlStateNormal];
    instaBut.backgroundColor = [UIColor whiteColor];
    [instaBut setTitleColor:mainColor forState:UIControlStateNormal];
    instaBut.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [instaBut addTarget:self action:@selector(shareInstarAction) forControlEvents:UIControlEventTouchUpInside];
    [self.tabView addSubview:instaBut];
    
    UIImageView *finishImageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2, 2, ScreenWidth/2,52+KDNavH)];
    finishImageView.userInteractionEnabled = YES;
    finishImageView.image = [UIImage imageNamed:@"finishIg"];
    [self.tabView addSubview:finishImageView];
    
    UIButton *saveToAlbumBut = [[UIButton alloc]initWithFrame:CGRectMake(ScreenWidth/2, 2, ScreenWidth/2,52+KDNavH)];
    saveToAlbumBut.backgroundColor = [UIColor clearColor];
    //finishBut.imageView.image = [UIImage imageNamed:@"finishIg"];
    [saveToAlbumBut setTitle:@"存到相册" forState:UIControlStateNormal];
    //[finishBut setImage:[UIImage imageNamed:@"finishIg"] forState:UIControlStateNormal]; ;
    [saveToAlbumBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [saveToAlbumBut addTarget:self action:@selector(saveToAlbumAction) forControlEvents:UIControlEventTouchUpInside];
    saveToAlbumBut.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [self.tabView addSubview:saveToAlbumBut];
    [self.view addSubview:self.tabView];
}

- (void)btnBackPressed:(UIButton *)sender
{
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
//返回首页
- (void)homeAction{
    [self.navigationController popToRootViewControllerAnimated:YES];
    if([SKStoreReviewController respondsToSelector:@selector(requestReview)]) {
        [[UIApplication sharedApplication].keyWindow endEditing:YES];
        [SKStoreReviewController requestReview];
    }
}
#pragma mark ----分享按钮
- (void)shareInstarAction{
    
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if (![[UIApplication sharedApplication] canOpenURL:instagramURL])
    {
        CIError(@"Download Instagram app from app store firstly please!");
    }
    
    //上传图片
    if(self.shareImage){
        UIImage *image = self.shareImage;
        NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"Image.igo"];
        NSData *imageData = UIImageJPEGRepresentation(image, 1);
        [imageData writeToFile:savedImagePath atomically:YES];
        
        [self addSkipBackupAttributeToItemAtPath:savedImagePath];
        
        NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", savedImagePath]];
        
        //NSURL *imageUrl = [NSURL fileURLWithPath:savedImagePath];
        NSString *caption = @"Uploaded from #instagridpost";
        if([[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]){
            caption = @"";
        }
        UIDocumentInteractionController *docController = [UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
        docController.UTI = @"com.instagram.exclusivegram";
        docController.delegate = self;
        docController.annotation = [NSDictionary dictionaryWithObject:caption forKey:@"InstagramCaption"];
        [docController presentOpenInMenuFromRect:CGRectZero inView:self.view animated:YES];
        
    }//上传视频
    else{
        //NSString *pathRow = [NSString stringWithFormat:@"%d",self.imageArray.count-1-row];
        NSURL *pathUrl = self.shareVurl;
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library writeVideoAtPathToSavedPhotosAlbum:pathUrl
                                    completionBlock:^(NSURL *assetURL, NSError *error) {
                                        if (error) {
                                            NSLog(@"Save video fail:%@",error);
                                        } else {
                                            NSURL *instagramURL = [NSURL URLWithString:@"instagram://camera"];
                                            [[UIApplication sharedApplication]openURL:instagramURL];
                                        }
                                          //self.mainImageView.userInteractionEnabled = NO;
                                    }];
    }
}

- (void)saveToAlbumAction{
    [[SHKActivityIndicator currentIndicator] show];
    if(self.shareImage){
        [self saveImageToPhotos:self.shareImage];
    }else{
        [self exportDidFinish:self.shareVurl];
    }
}
// 获取视频第一帧图片
- (UIImage*) getVideoPreViewImage:(NSURL *)path
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:path options:nil];
    AVAssetImageGenerator *assetGen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    assetGen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [assetGen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *videoImage = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return videoImage;
}
- (void)documentInteractionController:(UIDocumentInteractionController *)controller
        willBeginSendingToApplication:(NSString *)application
{
    //self.mainImageView.userInteractionEnabled = NO;
}
- (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)filePathString
{
    NSURL *fileURL = [NSURL fileURLWithPath:filePathString];
    
    assert([[NSFileManager defaultManager] fileExistsAtPath: [fileURL path]]);
    
    NSError *error = nil;
    
    BOOL success = [fileURL setResourceValue:[NSNumber numberWithBool: YES]
                                      forKey: NSURLIsExcludedFromBackupKey
                                       error: &error];
    return success;
}

//把UIImage保存到系统相册
- (void)saveImageToPhotos:(UIImage*)savedImage
{
    // UIImageWriteToSavedPhotosAlbum(image,self,  @selector(imageSavedToPhotosAlbum: didFinishSavingWithError: contextInfo:), nil);
    
    UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
    
}
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    
    if (error == nil) {
        NSLog(@"存入手机相册成功");
        [[SHKActivityIndicator currentIndicator] hide];
        [[SHKActivityIndicator currentIndicator] displayCompleted:NSLocalizedString(@"Saved!", nil)];
    }else{
        NSLog(@"存入手机相册失败");
        [[SHKActivityIndicator currentIndicator] hide];
        [[SHKActivityIndicator currentIndicator] displayCompleted:NSLocalizedString(@"Image Saving Failed!", nil)];
    }
    
}
//视频保存到相册
- (void)exportDidFinish:(NSURL*)outputURL
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
        [library writeVideoAtPathToSavedPhotosAlbum:outputURL
                                    completionBlock:^(NSURL *assetURL, NSError *error){
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            if (error) {
                                                NSLog(@"Video Saving Failed");
                                                [[SHKActivityIndicator currentIndicator] hide];
                                                [[SHKActivityIndicator currentIndicator] displayCompleted:NSLocalizedString(@"Video Saving Failed!", nil)];
                                                //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil,nil];
                                                //  [alert show];
                                            }else{
                                                [[SHKActivityIndicator currentIndicator] hide];
                                                [[SHKActivityIndicator currentIndicator] displayCompleted:NSLocalizedString(@"Saved!", nil)];
                                                //NSLog(@"Video Saved");
                                                // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil,nil];
                                                //  [alert show];
                                            }
                                            
                                        });
                                        
                                    }];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
