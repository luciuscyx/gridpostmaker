//
//  HomeCollectionViewCells.m
//  MyInstagrids
//
//  Created by chenteng on 2017/10/17.
//  Copyright © 2017年 fh. All rights reserved.
//

#import "HomeCollectionViewCells.h"

@implementation HomeCollectionViewCells

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self !=nil){
        [self loadUI];
    }
    return self;
}
- (void)loadUI{
    self.imgView = [[UIImageView alloc]initWithFrame:CGRectZero];
    [self.contentView addSubview:self.imgView];
    
    self.videoView = [[UIView alloc]initWithFrame:CGRectZero];
    [self.imgView addSubview:self.videoView];
    
    self.videoTypeView = [[UIImageView alloc]initWithFrame:CGRectZero];
    self.videoTypeView.image =[UIImage imageNamed:@"videotype"];
    [self.videoView addSubview:self.videoTypeView];
    self.videoView.hidden = YES;
    
    self.videoTime = [[UILabel alloc]initWithFrame:CGRectZero];
    self.videoTime.textAlignment = NSTextAlignmentRight;
    self.videoTime.font = [UIFont systemFontOfSize:12];
    self.videoTime.textColor = [UIColor whiteColor] ;
    [self.videoView addSubview:self.videoTime];
    
    UIColor *colorOne = [UIColor clearColor];//[UIColor colorWithRed:1.0f  green:1.0f blue:1.0f  alpha:0.8];
    UIColor *colorTwo = [UIColor colorWithRed:0.0f  green:0.0f   blue:0.0f   alpha:0.7];
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    //设置开始和结束位置(设置渐变的方向)
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(0, 1);
    gradient.colors = colors;
    gradient.frame = self.videoView.bounds;
    [self.videoView.layer insertSublayer:gradient atIndex:0];
}
- (void)layoutSubviews{
    self.imgView .frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
    self.videoView .frame = CGRectMake(0,self.frame.size.height-30,self.frame.size.width,30);
    self.videoTypeView.frame = CGRectMake(10,5,20,20);
    self.videoTime.frame = CGRectMake(self.frame.size.width-60,5,50, 20);
}
- (void)clear{
    self.videoTypeView.image = [UIImage imageNamed:@""];
    self.videoTime.text = @"";
}
@end
