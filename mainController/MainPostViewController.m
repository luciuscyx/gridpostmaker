//
//  MainPostViewController.m
//  Instagrid Post
//
//  Created by macbookair on 30/7/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import "MainPostViewController.h"
#import "CCAutoScrollView.h"
#import "SelectImageViewController.h"
#import "PushOtherInfoView.h"
#import "UpgradeViewController.h"

#define appStoreId @"944665061"  //推广的appsID

#define MY_BANNER_UNIT_ID @"ca-app-pub-7766793965493521/2172275184"
#define MY_BANNER_UNIT_ID_IPAD @"ca-app-pub-7766793965493521/2172275184"

@interface MainPostViewController ()<PushOtherInfoViewDeletage,UpgradeViewControllerDelegate>
{
    GADBannerView*adMobAd;
    int trycount;
    GADInterstitial *interstitial_;
    BOOL cansendGADInter;
    BOOL hasshowbreakpoint;
}
@property (nonatomic,assign)NSInteger isDomestic; // 1:微信模式  非1为instal
@property (nonatomic,strong)CCAutoScrollView *visaScrollingView;
@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic,strong)NSArray *segArray;
@property (nonatomic,assign)NSInteger selectIndex; //0:九宫格  1:全景。2:方形
@property (nonatomic,strong)UIButton *weChatButton;
@property (nonatomic,strong)UIButton *instagramButton;
@property (nonatomic,strong)PushOtherInfoView *PushOtherView;
//@property (nonatomic,assign)BOOL isWeChatModel;  //YES：微信  NO：inst

@property (nonatomic,strong)UpgradeViewController *objUpgradeViewController;


//@property (weak, nonatomic) UIView *TopView;
@property (nonatomic,strong)UIView *adsView;
//@property (nonatomic,strong)UIView *ShinView;
@property (nonatomic,strong)UIButton *removeAdsBut;
@property (nonatomic,strong)UIControl *squareButtont;



@end

@implementation MainPostViewController

#pragma mark 定义私有变量
//是否已经购买“去除广告”
//iAd广告
static BOOL bannerIsVisible;
//AdMob广告
static BOOL adMobNotFailed;

static BOOL featureDPurchased=NO;
#pragma mark -
#pragma mark iAd广告代理
- (void)interstitial:(GADInterstitial *)interstitial
didFailToReceiveAdWithError:(GADRequestError *)error {
    // Alert the error.
    cansendGADInter = YES;
}

- (void)interstitialDidReceiveAd:(GADInterstitial *)interstitial {
    [interstitial presentFromRootViewController:self];
    cansendGADInter = YES;
}
- (void)showgoogleinitial
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]) {
        return;
    }
}

- (void)cancelBannerViewAction {
    NSLog(@"[iAd]: 广告动作已经取消!");
}


- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    NSLog(@"[AdMob]: 广告已经加载.");
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"]) {
        return;
    }
    // get the view frame
    CGRect frame = self.view.frame;
    CGRect Adsframe;
    // if (self.viewController!= nil && self.viewController.view.superview != nil) {
    // put the ad at the bottom of the screen
    if([[[UIDevice currentDevice] model] rangeOfString:@"iPad"].location == 0){
        Adsframe = CGRectMake((frame.size.width - GAD_SIZE_728x90.width)/2,0, GAD_SIZE_728x90.width, GAD_SIZE_728x90.height);
    }else{
        Adsframe = CGRectMake((frame.size.width - GAD_SIZE_320x50.width)/2,0,  GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
    }
    
    
    if([[UIScreen mainScreen] bounds].size.width == 320){
        self.adsView.frame = CGRectMake(Adsframe.origin.x,0,Adsframe.size.width, Adsframe.size.height);
        bannerView.frame = CGRectMake(0,0,Adsframe.size.width, Adsframe.size.height);
        self.removeAdsBut.frame = CGRectMake(VIEW_W(self.adsView)-30, -5, 30, 30);
    }else{
        if(IPHONEX_SERIES){
            self.adsView.frame = CGRectMake(Adsframe.origin.x,kDeviceHeight - Adsframe.size.height - 50 ,Adsframe.size.width+15, Adsframe.size.height+20);
            bannerView.frame = CGRectMake(0, 20,Adsframe.size.width, Adsframe.size.height);
            self.removeAdsBut.frame = CGRectMake(VIEW_W(self.adsView)-30, 0, 30, 30);
        }else{
            self.adsView.frame = CGRectMake(Adsframe.origin.x,kDeviceHeight - Adsframe.size.height,Adsframe.size.width, Adsframe.size.height);
            bannerView.frame = CGRectMake(0, 0,Adsframe.size.width, Adsframe.size.height);
            self.removeAdsBut.frame = CGRectMake(VIEW_W(self.adsView)-30,-5, 30, 30);
        }
    }
    [self.adsView addSubview:bannerView];
    [self.adsView addSubview:self.removeAdsBut];
    trycount = 0;
//    [self.view addSubview:self.adsView];
//    if (self.magnifyingView&&self.magnifyingView.magnifyingGlass.superview) {
//        [self.view bringSubviewToFront:self.magnifyingView.magnifyingGlass];
//    }else
//    {
        [self.view bringSubviewToFront:self.adsView];
//    }
    
    
}
#pragma mark ------ 去除水印广告
- (void)removeAdsAction:(UIButton *)sender{
    //[self buyFilter];
    [self pushUpgradeView];
}
-(void)adView:(GADBannerView *)bannerView
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"[AdMob]: 加载广告失败.");
    if (bannerView != nil) {
        [self.adsView removeFromSuperview];
        self.adsView = nil;
        [self.removeAdsBut removeFromSuperview];
        self.removeAdsBut = nil;
        [bannerView removeFromSuperview];
        bannerView.delegate = nil;
        bannerView = nil;
    }
    
    
    adMobNotFailed = NO;
    //试试iAd
    if (trycount < 10) {
        trycount++;
        // [self tryiAdWhenAdMobFailed];
    }
    
}


#pragma mark -
#pragma mark Ads广告相关处理
// Request a new ad. If a new ad is successfully loaded, it will be animated into location.
/*- (void)refreshAdMob:(NSTimer *)timer {
 [adMobAd requestFreshAd];
 }*/

- (void)disposeAds {
    BOOL is_iAdON = NO;
    BOOL is_adMobON = NO;
    
    //分析设备可显示哪一家广告
    
    is_adMobON = YES;
    
    if(!is_adMobON)
        is_iAdON = YES;
    
    //检测购买(支持程序内购买“去除广告”成功后消除广告)
    if(featureDPurchased) {
        is_adMobON = NO;
        is_iAdON = NO;
    }
    
    //测试设置
    //is_adMobON = YES;
    //is_iAdON = NO;
    
    //打开广告
    if(is_adMobON || is_iAdON) {
        [self beginAdmob];
    }
    else {
        //关闭广告
        if(adMobAd && adMobNotFailed) {
            //关闭AdMob
            [self.adsView removeFromSuperview];
            self.adsView  = nil;
            [adMobAd removeFromSuperview];
            adMobAd = nil;
        }
        
    }
    
    
}

-(void)beginAdmob
{
    if (featureDPurchased) {
        return;
    }
    if(self.adsView){
        [self.adsView removeFromSuperview];
        self.adsView = nil;
        [self.removeAdsBut removeFromSuperview];
        self.removeAdsBut = nil;
    }
    //启用AdMob
    if(!adMobAd) {
        //CGSize sizeToRequest;
        if([[[UIDevice currentDevice] model] rangeOfString:@"iPad"].location == 0)
        {
            adMobAd = [[GADBannerView alloc]
                       initWithFrame:CGRectMake(0.0,
                                                -GAD_SIZE_728x90.height,
                                                GAD_SIZE_728x90.width,
                                                GAD_SIZE_728x90.height)];
            
            adMobAd.adUnitID = MY_BANNER_UNIT_ID_IPAD;
            //sizeToRequest = ADMOB_SIZE_748x110; //CGSizeMake(748, 110);
        }
        else
        {
            if(IPHONEX_SERIES){
                adMobAd = [[GADBannerView alloc]
                           initWithFrame:CGRectMake(0.0,
                                                    -GAD_SIZE_320x50.height+40,
                                                    GAD_SIZE_320x50.width,
                                                    GAD_SIZE_320x50.height)];
                adMobAd.adUnitID = MY_BANNER_UNIT_ID;
            }else{
                adMobAd = [[GADBannerView alloc]
                           initWithFrame:CGRectMake(0.0,
                                                    -GAD_SIZE_320x50.height,
                                                    GAD_SIZE_320x50.width,
                                                    GAD_SIZE_320x50.height)];
                adMobAd.adUnitID = MY_BANNER_UNIT_ID;
            }
        }
        trycount = 0;
        self.adsView = [[UIView alloc]initWithFrame:CGRectZero];
        self.adsView.backgroundColor = [UIColor clearColor];
        self.removeAdsBut = [[UIButton alloc]initWithFrame:CGRectZero];
        [self.removeAdsBut setImage:[UIImage imageNamed:@"removeads"] forState:UIControlStateNormal];
        [self.removeAdsBut addTarget:self action:@selector(removeAdsAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    if (adMobAd.superview == nil) {
        adMobAd.rootViewController = self;
        [adMobAd setDelegate:self];
        // Initiate a generic request to load it with an ad.
        [adMobAd loadRequest:[GADRequest request]];
        [self.view addSubview:self.adsView];
        //adMobAd = [AdMobView requestAdOfSize:sizeToRequest withDelegate:self]; // start a new ad request
    }
}
-(void)removeADS
{
    
    if (adMobAd != nil) {
        [adMobAd removeFromSuperview];
        adMobAd.delegate = nil;
        adMobAd = nil;
    }
    if(self.adsView){
        [self.adsView removeFromSuperview];
        self.adsView = nil;
        [self.removeAdsBut removeFromSuperview];
        self.removeAdsBut = nil;
    }
}


-(BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)checkifshowrateus
{
    NSNumber * checkCount = [[NSUserDefaults standardUserDefaults] objectForKey:@"checkCount"];
    if (checkCount&&[checkCount intValue] > 2) {
        if([SKStoreReviewController respondsToSelector:@selector(requestReview)]) {
            [[UIApplication sharedApplication].keyWindow endEditing:YES];
            [SKStoreReviewController requestReview];
        }
    }
    if (!checkCount) {
        [[NSUserDefaults standardUserDefaults]  setObject:[NSNumber numberWithInt:0] forKey:@"checkCount"];
    }else
    {
        [[NSUserDefaults standardUserDefaults]  setObject:[NSNumber numberWithInt:[checkCount intValue]+1] forKey:@"checkCount"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    NSNumber * ccc = [[NSUserDefaults standardUserDefaults] objectForKey:@"openCount"];
    if (!ccc) {
        ccc = [NSNumber numberWithInt:0];
    }
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:[ccc intValue]+1] forKey:@"openCount"];
    
    self.isDomestic = [[NSUserDefaults standardUserDefaults] integerForKey:@"isDomestic"];
    [self loadUI];
}
- (void)loadAds{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY] isEqualToString:@"YES"] || (kDeviceHeight - VIEW_YH(self.squareButtont)) < 50)
    {
        featureDPurchased = YES;
        
    }else {
        featureDPurchased = NO;
        
    }
    if (!featureDPurchased) {
        cansendGADInter = YES;
        [self showgoogleinitial];
        [self disposeAds];
        hasshowbreakpoint = NO;
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0f) {
        }
    }else
    {
        cansendGADInter = NO;
        hasshowbreakpoint = YES;
    }
    
}
- (void)viewWillAppear:(BOOL)animated{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"enterFromPush"] && ![[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY]){
         [self pushUpgradeView];
         [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"enterFromPush"];
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [[NSUserDefaults standardUserDefaults] setInteger:self.isDomestic forKey:@"isDomestic"];
}
- (void)loadUI{
    if(self.isDomestic == 1){
        self.segArray = @[@"imageScro1",@"imageScro2",@"imageScro3"];
    }else{
        self.segArray = @[@"imageScro1_En",@"imageScro2_En",@"imageScro3_En"];
    }
    self.visaScrollingView = [[CCAutoScrollView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth, ScreenWidth) WithImagesArray:self.segArray timerInterval:3];
    [self.view addSubview:self.visaScrollingView];
    self.selectIndex = 0;
    
    NSArray *arrayW = [NSArray arrayWithObjects:NSLocalizedString(@"WeChatMode", nil),NSLocalizedString(@"InstagramMode", nil), nil];
    UIView *segView = [[UIView alloc]initWithFrame:CGRectMake(ScreenWidth/2-120,VIEW_YH(self.visaScrollingView)-50,240,50)];
    segView.backgroundColor = RGB(240, 240, 240);
    segView.layer.masksToBounds = YES;
    segView.layer.cornerRadius = 25;
//    segView.layer.shadowColor = mainColor.CGColor;//[UIColor grayColor].CGColor;
//    segView.layer.shadowOffset = CGSizeZero;//阴影偏移量
//    segView.layer.shadowOpacity = 0.8;//阴影的透明度
//    segView.layer.shadowRadius = 5;//阴影的圆角
    [self.view addSubview:segView];
    CALayer *subLayer=[CALayer layer];
    CGRect fixframe = segView.frame;
    subLayer.frame= fixframe;
    subLayer.cornerRadius = 25;
    subLayer.backgroundColor=[[UIColor grayColor] colorWithAlphaComponent:0.6].CGColor;
    subLayer.masksToBounds=NO;
    subLayer.shadowColor = [UIColor grayColor].CGColor;//shadowColor阴影颜色
    subLayer.shadowOffset = CGSizeMake(3,2);//shadowOffset阴影偏移,x向右偏移3，y向下偏移2，默认(0, -3),这个跟shadowRadius配合使用
    subLayer.shadowOpacity = 0.6;//阴影透明度，默认0
    subLayer.shadowRadius = 5;//阴影半径，默认3
    [self.view.layer insertSublayer:subLayer below:segView.layer];
    
    self.weChatButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 120, 50)];
    self.weChatButton.selected = NO;
    //self.weChatButton.backgroundColor = [UIColor whiteColor];
    self.weChatButton.backgroundColor = RGB(240, 240, 240);
    [self.weChatButton setTitle:arrayW[0] forState:UIControlStateNormal];
    //if([APPDELEGATE ])
    self.weChatButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.weChatButton setTitleColor:mainColor forState:UIControlStateSelected];
    [self.weChatButton setTitleColor:RGB(140, 140, 140) forState:UIControlStateNormal];
    [self.weChatButton addTarget:self action:@selector(selectWeChatAction) forControlEvents:UIControlEventTouchUpInside];
    self.weChatButton.layer.masksToBounds = YES;
    self.weChatButton.layer.cornerRadius = 25;
//    self.weChatButton.layer.shadowColor = [UIColor grayColor].CGColor;
//    self.weChatButton.layer.shadowOffset = CGSizeZero;//阴影偏移量
//    self.weChatButton.layer.shadowOpacity = 0.8;//阴影的透明度
//    self.weChatButton.layer.shadowRadius = 5;//阴影的圆角
    [segView addSubview:self.weChatButton];
    
    self.instagramButton = [[UIButton alloc]initWithFrame:CGRectMake(VIEW_XW(self.weChatButton), 0, 120, 50)];
    self.instagramButton.selected = NO;
    self.instagramButton.backgroundColor = [UIColor whiteColor];
    //self.instagramButton.backgroundColor = RGB(240, 240, 240);
    [self.instagramButton setTitle:arrayW[1] forState:UIControlStateNormal];
    self.instagramButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.instagramButton setTitleColor:mainColor forState:UIControlStateSelected];
    [self.instagramButton setTitleColor:RGB(140, 140, 140) forState:UIControlStateNormal];
    [self.instagramButton addTarget:self action:@selector(selectInstagramAction) forControlEvents:UIControlEventTouchUpInside];
    self.instagramButton.layer.masksToBounds = YES;
    self.instagramButton.layer.cornerRadius = 25;
    [segView addSubview:self.instagramButton];
    if(self.isDomestic == 1){
        self.weChatButton.selected = YES;
        self.weChatButton.backgroundColor = [UIColor whiteColor];
        self.instagramButton.backgroundColor = RGB(240, 240, 240);
        self.segArray = @[@"imageScro1",@"imageScro2",@"imageScro3"];
         //self.visaScrollingView.imagesArray = @[@"imageScro1",@"imageScro2",@"imageScro3"];
    }else{
        self.instagramButton.selected = YES;
        self.instagramButton.backgroundColor = [UIColor whiteColor];
        self.weChatButton.backgroundColor = RGB(240, 240, 240);
        self.segArray = @[@"imageScro1_En",@"imageScro2_En",@"imageScro3_En"];
    }
    //self.visaScrollingView.imagesArray = [[NSMutableArray alloc]initWithArray:self.segArray];
    //九宫格
    CGFloat mainH = 100;
    UIImageView *gridMainImageView  = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/8,VIEW_YH(segView)+30,ScreenWidth/4*3,mainH)];
    gridMainImageView.image = [UIImage imageNamed:@"gridIg_icn"];
    [self.view addSubview:gridMainImageView];
    UIImageView *gridIgview = [[UIImageView alloc]initWithFrame:CGRectMake(VIEW_W(gridMainImageView)/2-40,(VIEW_H(gridMainImageView)-40)/2,40,40)];
    gridIgview.image = [UIImage imageNamed:@"grid_white.png"];
    [gridMainImageView addSubview:gridIgview];
    
    UILabel *gridLabel = [[UILabel alloc]initWithFrame:CGRectMake(VIEW_W(gridMainImageView)/2+5,(VIEW_H(gridMainImageView)-40)/2,VIEW_W(gridMainImageView)/2-20, 40)];
    gridLabel.text = NSLocalizedString(@"Grid", nil);//@"九宫格";
    gridLabel.textColor = [UIColor whiteColor];
    gridLabel.textAlignment = NSTextAlignmentLeft;
    gridLabel.font  = [UIFont boldSystemFontOfSize:14];
    [gridMainImageView addSubview:gridLabel];
    
    UIControl *gridButtont = [[UIControl alloc]initWithFrame:gridMainImageView.frame];
    [gridButtont addTarget:self action:@selector(selectGridAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:gridButtont];
    
    //全景
    UIImageView *panoramicMainImageView  = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/8,VIEW_YH(gridMainImageView)+20,ScreenWidth/8*3-10,mainH)];
//    panoramicMainImageView.layer.masksToBounds = YES;
//    panoramicMainImageView.layer.cornerRadius = 10;
    panoramicMainImageView.backgroundColor = [UIColor whiteColor];
    //panoramicMainImageView.image = [UIImage imageNamed:@"gridIg_icn"];
    [self.view addSubview:panoramicMainImageView];
    CALayer *panoramicSubLayer=[CALayer layer];
    panoramicSubLayer.frame= panoramicMainImageView.frame;
    panoramicSubLayer.cornerRadius = 8;
    panoramicSubLayer.backgroundColor=[[UIColor grayColor] colorWithAlphaComponent:0.6].CGColor;
    panoramicSubLayer.masksToBounds=NO;
    panoramicSubLayer.shadowColor = [UIColor grayColor].CGColor;//shadowColor阴影颜色
    panoramicSubLayer.shadowOffset = CGSizeMake(3,2);//shadowOffset阴影偏移,x向右偏移3，y向下偏移2，默认(0, -3),这个跟shadowRadius配合使用
    panoramicSubLayer.shadowOpacity = 0.6;//阴影透明度，默认0
    panoramicSubLayer.shadowRadius = 5;//阴影半径，默认3
    [self.view.layer insertSublayer:panoramicSubLayer below:panoramicMainImageView.layer];
    UIImageView *panoramicIgview = [[UIImageView alloc]initWithFrame:CGRectMake(VIEW_W(panoramicMainImageView)/2-40,(VIEW_H(panoramicMainImageView)-40)/2,40,40)];
    panoramicIgview.image = [UIImage imageNamed:@"quanping"];
    [panoramicMainImageView addSubview:panoramicIgview];
    
    UILabel *panoramicLabel = [[UILabel alloc]initWithFrame:CGRectMake(VIEW_W(panoramicMainImageView)/2,(VIEW_H(panoramicMainImageView)-40)/2,VIEW_W(panoramicMainImageView)/2, 40)];
    panoramicLabel.text = NSLocalizedString(@"Panorama",nil);
    panoramicLabel.textColor = mainColor;
    panoramicLabel.textAlignment = NSTextAlignmentLeft;
    panoramicLabel.font  = [UIFont boldSystemFontOfSize:12];
    [panoramicMainImageView addSubview:panoramicLabel];
    
    UIControl *panoramicButtont = [[UIControl alloc]initWithFrame:panoramicMainImageView.frame];
    [panoramicButtont addTarget:self action:@selector(selectpanoramicAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:panoramicButtont];
    
    
    //方形 square
    UIImageView *squareMainImageView  = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenWidth/2+10,VIEW_YH(gridMainImageView)+20,ScreenWidth/8*3-10,mainH)];
    squareMainImageView.backgroundColor = [UIColor whiteColor];
    //panoramicMainImageView.image = [UIImage imageNamed:@"gridIg_icn"];
    [self.view addSubview:squareMainImageView];
    CALayer *squareSubLayer=[CALayer layer];
    squareSubLayer.frame= squareMainImageView.frame;
    squareSubLayer.cornerRadius = 8;
    squareSubLayer.backgroundColor=[[UIColor grayColor] colorWithAlphaComponent:0.6].CGColor;
    squareSubLayer.masksToBounds=NO;
    squareSubLayer.shadowColor = [UIColor grayColor].CGColor;//shadowColor阴影颜色
    squareSubLayer.shadowOffset = CGSizeMake(3,2);//shadowOffset阴影偏移,x向右偏移3，y向下偏移2，默认(0, -3),这个跟shadowRadius配合使用
    squareSubLayer.shadowOpacity = 0.6;//阴影透明度，默认0
    squareSubLayer.shadowRadius = 5;//阴影半径，默认3
    [self.view.layer insertSublayer:squareSubLayer below:squareMainImageView.layer];
    
    UIImageView *squareIgview = [[UIImageView alloc]initWithFrame:CGRectMake(VIEW_W(squareMainImageView)/2-50,(VIEW_H(squareMainImageView)-50)/2,50,50)];
    squareIgview.image = [UIImage imageNamed:@"fangxing"];
    [squareMainImageView addSubview:squareIgview];
    
    UILabel *squareLabel = [[UILabel alloc]initWithFrame:CGRectMake(VIEW_W(squareMainImageView)/2,(VIEW_H(squareMainImageView)-50)/2,VIEW_W(squareMainImageView)/2, 50)];
    squareLabel.text = NSLocalizedString(@"CropSize",nil);//@"方形";
    squareLabel.textColor = mainColor;
    squareLabel.textAlignment = NSTextAlignmentLeft;
    squareLabel.font  = [UIFont boldSystemFontOfSize:12];
    [squareMainImageView addSubview:squareLabel];
    
    self.squareButtont = [[UIControl alloc]initWithFrame:squareMainImageView.frame];
    [self.squareButtont addTarget:self action:@selector(selectSquareAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.squareButtont];
    
    //App推荐
    NSUserDefaults *useDef = [NSUserDefaults standardUserDefaults];
    NSInteger enterSum = [useDef integerForKey:@"enterSum"];
    if(enterSum>1){
        [self performSelector:@selector(pushLoadAppView) withObject:nil afterDelay:0.5];
    }
    
}
- (void)selectWeChatAction{
    self.isDomestic = 1;
    //self.isWeChatModel = YES;
    self.instagramButton.selected = NO;
    self.weChatButton.selected = YES;
    self.weChatButton.backgroundColor = [UIColor whiteColor];
    self.instagramButton.backgroundColor = RGB(240, 240, 240);
    self.segArray = @[@"imageScro1",@"imageScro2",@"imageScro3"];
    [self.visaScrollingView setRefreshImageArray: self.segArray];
    
    //[self.visaScrollingView removeFromSuperview];
    //self.visaScrollingView = [[CCAutoScrollView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth, ScreenWidth) WithImagesArray:self.segArray timerInterval:3];
    //[self.view addSubview:self.visaScrollingView];
    //self.selectIndex = 0;
}

- (void)selectInstagramAction{
    self.isDomestic = 0;
    //self.isWeChatModel = NO;
    self.weChatButton.selected = NO;
    self.instagramButton.selected = YES;
    self.instagramButton.backgroundColor = [UIColor whiteColor];
    self.weChatButton.backgroundColor = RGB(240, 240, 240);
    self.segArray = @[@"imageScro1_En",@"imageScro2_En",@"imageScro3_En"];
    [self.visaScrollingView setRefreshImageArray: self.segArray];
    
    //[self.visaScrollingView removeFromSuperview];
    //self.visaScrollingView = [[CCAutoScrollView alloc]initWithFrame:CGRectMake(0,0,ScreenWidth, ScreenWidth) WithImagesArray:self.segArray timerInterval:3];
}
- (void)selectGridAction{
    self.selectIndex = 0;
    SelectImageViewController *selectIGView = [[SelectImageViewController alloc]init];
    selectIGView.isDomestic = self.isDomestic;
    selectIGView.modelIndex = self.selectIndex;
    [self.navigationController pushViewController:selectIGView animated:YES];
   // [self presentViewController:selectIGView animated:YES completion:nil];
}
- (void)selectpanoramicAction{
    self.selectIndex = 1;
    SelectImageViewController *selectIGView = [[SelectImageViewController alloc]init];
    selectIGView.isDomestic = self.isDomestic;
    selectIGView.modelIndex = self.selectIndex;
    [self.navigationController pushViewController:selectIGView animated:YES];
  //  [self presentViewController:selectIGView animated:YES completion:nil];
}
- (void)selectSquareAction{
    self.selectIndex = 2;
    SelectImageViewController *selectIGView = [[SelectImageViewController alloc]init];
    selectIGView.isDomestic = self.isDomestic;
    selectIGView.modelIndex = self.selectIndex;
    [self.navigationController pushViewController:selectIGView animated:YES];
    //[self presentViewController:selectIGView animated:YES completion:nil];
}
//
#pragma mark ---- 弹出app推荐View
- (void)pushLoadAppView{
    
    if(!self.PushOtherView){
        self.PushOtherView = [[PushOtherInfoView alloc]initWithFrame:CGRectMake(0, 0,ScreenWidth,ScreenHeight)];
        self.PushOtherView.deletage = self;
        [self.view addSubview:self.PushOtherView];
    }
    self.PushOtherView.hidden = YES;
    BOOL isPushAppInfo = [[NSUserDefaults standardUserDefaults] boolForKey:@"isPushAppInfo"];
    if(!isPushAppInfo){
            [self.PushOtherView setupViewWithAppIdentifier:appStoreId completionBlock:^(NSError *error){
                if(!error){
                    self.PushOtherView.hidden = NO;
                    //[self refreshTabView:isDidShare isHiddenOther:YES];
                }
            }];
     }
}
#pragma mark --- PushOtherInfoViewDelegate;
- (void)PushAppStorePage{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/cn/app/id%@",appStoreId]]];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isPushAppInfo"];
   self.PushOtherView.hidden = YES;
}

#pragma mark --- 更新视图
- (void)pushUpgradeView{
//    if([[NSUserDefaults standardUserDefaults] boolForKey:@"enterFromPush"] && ![[NSUserDefaults standardUserDefaults] objectForKey:UPGRADE_PRO_KEY]){
        self.objUpgradeViewController = [[UpgradeViewController alloc] initWithNibName:@"UpgradeViewController" bundle:nil];
        self.objUpgradeViewController.delegate = self;
        [RJAnimation bounceAddSubViewToParentView:self.view ChildView:self.objUpgradeViewController.view];
//        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"enterFromPush"];
//    }
}

#pragma mark --- UpgradeViewControllerDelegate
-(void)upgradeSuccess{
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
