//
//  SelectImageViewController.h
//  Instagrid Post
//
//  Created by macbookair on 30/7/2019.
//  Copyright © 2019 RJLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GoogleMobileAds.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelectImageViewController : UIViewController<GADInterstitialDelegate>
{
    GADInterstitial *interstitial_;
}
@property(nonatomic, retain) GADInterstitial *interstitial;

@property (nonatomic,assign)NSInteger isDomestic;  //1：微信  非1:instal
@property (nonatomic,assign)NSInteger modelIndex; //0:九宫格  1:全景。2:方形
@end

NS_ASSUME_NONNULL_END
